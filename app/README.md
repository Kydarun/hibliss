# Hi-Bliss Project

## Build Instructions for Android (Customer App)
In Terminal, navigate to ```app``` directory, then run:
````
flutter build apk --release --flavor customer
````
To build a release AAB to upload to Google Play Store, run this instead:
````
flutter build appbundle --release --flavor customer
````

## Build Instructions for Android (Therapist App)
In Terminal, navigate to ```app``` directory, then run:
````
flutter build apk --release --flavor therapist --dart-define=isTherapist=true
````
To build a release AAB to upload to Google Play Store, run this instead:
````
flutter build appbundle --release --flavor therapist --dart-define=isTherapist=true
````

## Build Instructions for iOS (Customer App)
In Terminal, navigate to ```app``` directory, then run:
````
flutter build ios --release --flavor customer
````
Then open ```ios/Runner.xcworkspace```. DO NOT open ```ios/Runner.xcodeproj``` because it will not compile correctly.
Use XCode IDE to finish the remaining compilation process.

## Build Instructions for iOS (Therapist App)
In Terminal, navigate to ```app``` directory, then run:
````
flutter build ios --release --flavor therapist --dart-define=isTherapist=true
````
Then open ```ios/Runner.xcworkspace```. DO NOT open ```ios/Runner.xcodeproj``` because it will not compile correctly.
Use XCode IDE to finish the remaining compilation process.