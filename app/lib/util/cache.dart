import 'dart:convert';

import 'package:app/main.dart';
import 'package:app/model/auth.dart';
import 'package:app/model/notifications.dart';
import 'package:app/rest/auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';

class Cache {
  final BuildContext context;

  Cache({required this.context});

  Future<void> login({required Auth auth}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('me', json.encode(auth.toJson()));
    await context.read<Session>().refresh();
  }

  Future<void> logout() async {
    await AuthRest(context: context).logout();
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.clear();
    context.read<Session>().setLoggedIn(value: false);
    await context.read<Cart>().logout();
    await context.read<Cart>().load();
  }

  Future<Auth?> me() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    if (_prefs.containsKey('me')) {
      final _me = _prefs.getString('me');
      if (_me?.isNotEmpty ?? false) {
        return Auth.fromJson(json.decode(_me!));
      }
    }
    return null;
  }

  Future<bool> isLoggedIn() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.containsKey('me');
  }

  Future<Notifications> getNotifications() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    if (_prefs.containsKey('settings')) {
      return Notifications.fromJson(json.decode(_prefs.getString('settings')!));
    }
    return Notifications(messageNotification: true);
  }

  Future<void> setNotifications({required Notifications settings}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('settings', json.encode(settings.toJson()));
  }
}
