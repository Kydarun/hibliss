class EnvironmentConfig {
  static const isTherapist = bool.fromEnvironment('isTherapist', defaultValue: false);
}
