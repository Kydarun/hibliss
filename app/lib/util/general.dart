import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class General {
  final BuildContext context;

  General({required this.context});

  String formatDate({required String dateTime}) {
    DateTime _startDate = DateFormat('dd/MM/yyyy HH:mm:ss').parse(dateTime);
    return DateFormat('dd MMM yyyy h:mmaa').format(_startDate);
  }
}
