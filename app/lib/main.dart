import 'dart:convert';

import 'package:app/model/address.dart';
import 'package:app/model/cart.dart';
import 'package:app/model/checkout.dart';
import 'package:app/rest/address.dart';
import 'package:app/rest/cart.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/cache.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/chat/appointment.dart';
import 'package:app/views/chat/booking.dart';
import 'package:app/views/chat/care_plan.dart';
import 'package:app/views/notifications/appointment.dart';
import 'package:app/views/notifications/care_plan.dart';
import 'package:app/views/notifications/new_job.dart';
import 'package:app/views/notifications/payout.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/splash.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> _navigator =
      GlobalKey(debugLabel: 'FCM Navigator');

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null && initialMessage.data['type'] != null) {
      int? _type = int.tryParse(initialMessage.data['type']);
      int? _id = int.tryParse(initialMessage.data['notifId']);
      _navigate(_id, _type);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      int? _type = int.tryParse(message.data['type']);
      int? _id = int.tryParse(message.data['notifId']);
      _navigate(_id, _type);
    });

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
            alert: true, sound: true, badge: true);
    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings('ic_launcher_foreground'));
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
      if (payload != null) {
        final message = json.decode(payload);
        int? _type = int.tryParse(message['type']);
        int? _id = int.tryParse(message['notifId']);
        _navigate(_id, _type);
      }
    });

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: android.smallIcon,
            )),
            payload: json.encode(message.data));
      }
    });
  }

  void _navigate(int? id, int? type) {
    if (type != null && id != null) {
      if (EnvironmentConfig.isTherapist) {
        switch (type) {
          case 1:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    AppointmentReminderWidget(notificationId: id)));
            break;
          case 2:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    NewJobReminderWidget(notificationId: id)));
            break;
          case 3:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    PayoutReminderWidget(notificationId: id)));
            break;
          case 4:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    CarePlanReminderWidget(notificationId: id)));
            break;
          default:
            break;
        }
      } else {
        switch (type) {
          case 1:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    CustomerAppointmentReminderWidget(notificationId: id)));
            break;
          case 2:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    CustomerCarePlanReminderWidget(notificationId: id)));
            break;
          case 3:
            _navigator.currentState?.push(MaterialPageRoute(
                builder: (context) =>
                    CustomerBookingConfirmedReminder(notificationId: id)));
            break;
          default:
            break;
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp().then((value) {
      setupInteractedMessage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoadingState>(
          create: (context) => LoadingState(),
        ),
        ChangeNotifierProvider<Session>(
          create: (ctx) => Session(context: context)..refresh(),
        ),
        ChangeNotifierProvider<Cart>(
            create: (context) => Cart(context: context)..load())
      ],
      child: MaterialApp(
        title: 'Hi-Bliss',
        debugShowCheckedModeBanner: false,
        navigatorKey: _navigator,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          fontFamily: 'Niveau Grotesk',
          appBarTheme: AppBarTheme(
              shadowColor: Colors.transparent,
              centerTitle: true,
              titleTextStyle: TextStyle(
                  fontFamily: EnvironmentConfig.isTherapist
                      ? 'Montserrat'
                      : 'Niveau Grotesk',
                  fontWeight: EnvironmentConfig.isTherapist
                      ? FontWeight.bold
                      : FontWeight.w500,
                  fontSize: 20,
                  color: Colors.white)),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
              backgroundColor: EnvironmentConfig.isTherapist
                  ? Color(0xff161640)
                  : Colors.white,
              selectedLabelStyle: TextStyle(
                  fontFamily: 'Soleil',
                  fontSize: 10,
                  color: Theme.of(context).primaryColor),
              unselectedItemColor: EnvironmentConfig.isTherapist
                  ? Colors.white
                  : Color(0xffb3b3b4),
              unselectedLabelStyle: TextStyle(
                  fontFamily: 'Soleil',
                  fontSize: 10,
                  color: EnvironmentConfig.isTherapist
                      ? Colors.white
                      : Color(0xffb3b3b4))),
          textTheme: Theme.of(context).textTheme.copyWith(
              headline1: TextStyle(
                  fontFamily: 'Niveau Grotesk',
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Color(0xff161640)),
              headline4: TextStyle(
                  fontFamily: 'Niveau Grotesk',
                  fontSize: 17,
                  color: Color(0xff161640)),
              bodyText2: TextStyle(fontFamily: 'Niveau Grotesk', fontSize: 15),
              bodyText1: TextStyle(
                  fontFamily: 'Niveau Grotesk',
                  fontSize: 15,
                  color: Color(0xff161640)),
              subtitle1: TextStyle(
                  fontFamily: 'Niveau Grotest',
                  fontSize: 15,
                  color: Color(0xff575877)),
              subtitle2: TextStyle(
                  fontFamily: 'Niveau Grotest',
                  fontSize: 15,
                  color: Color(0xff161640)),
              caption: TextStyle(fontFamily: 'Niveau Grotesk', fontSize: 13),
              headline5: TextStyle(
                  fontFamily: 'Niveau Grotesk',
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: Color(0xff161640)),
              headline6: TextStyle(
                  fontFamily: 'Niveau Grotesk',
                  fontSize: 13,
                  color: Color(0xff0095cf))),
          /*cupertinoOverrideTheme: CupertinoThemeData(
              textTheme: CupertinoTextThemeData(
                  dateTimePickerTextStyle: TextStyle(
                      fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 40))),*/
          hintColor: Color(0xff9193a6),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                  shadowColor:
                      MaterialStateProperty.all<Color>(Color(0x660095cf)),
                  padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                      EdgeInsets.symmetric(vertical: 13.5)),
                  textStyle: MaterialStateProperty.all<TextStyle?>(TextStyle(
                      fontFamily: 'Soleil',
                      fontWeight: FontWeight.bold,
                      fontSize: 18)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))))),
          textButtonTheme: TextButtonThemeData(
              style: ButtonStyle(
            padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                EdgeInsets.symmetric(vertical: 15)),
            textStyle: MaterialStateProperty.all<TextStyle?>(
                TextStyle(fontFamily: 'Soleil', fontSize: 15)),
          )),
          primaryColor: EnvironmentConfig.isTherapist
              ? Color(0xff161640)
              : Color(0xff3b84c5),
        ),
        home: Splash(),
      ),
    );
  }
}

class Session with ChangeNotifier {
  BuildContext context;

  Session({required this.context});

  bool _isLoggedIn = false;

  refresh() async {
    _isLoggedIn = await Cache(context: context).isLoggedIn();
    notifyListeners();
  }

  void setLoggedIn({required bool value}) {
    this._isLoggedIn = value;
    notifyListeners();
  }

  bool get isLoggedIn => _isLoggedIn;
}

class Cart with ChangeNotifier {
  BuildContext context;
  List<ShoppingCart> _carts = [];
  Address? address;
  Checkout? checkout;

  Cart({required this.context});

  List<ShoppingCart> get carts => _carts;

  setAddress({required Address address, String? orderIds}) async {
    this.address = address;
    await load(orderIds: orderIds);
  }

  logout() async {
    this.address = null;
    await load();
    notifyListeners();
  }

  bool isAllSelected() => _carts.length == (checkout?.orderItem?.length ?? -1);

  String selectedOrderIds() =>
      checkout?.orderItem
          ?.map((item) => item.orderId.toString())
          .reduce((value, element) => '$value$element,') ??
      '';

  load({String? orderIds, bool? shouldFetchPrimaryEmail = false}) async {
    try {
      // Run a getProfiile API again to ensure that user has been logged out.
      await UserRest(context: context).me();
      final _isLoggedIn = await Cache(context: context).isLoggedIn();
      if (_isLoggedIn) {
        if (address == null || (shouldFetchPrimaryEmail ?? false)) {
          address = (await AddressRest(context: context).list())
              .dataResults
              ?.where((element) => element.isPrimary == "Yes")
              .firstOrNull;
        } else {
          address = (await AddressRest(context: context).list())
              .dataResults
              ?.where((element) => element.addressId == address?.addressId)
              .first;
        }

        final _cartRes = await CartRest(context: context)
            .getShoppingCart(addressId: address?.addressId);
        _carts = _cartRes.dataResults ?? [];
        final _checkoutRes = await CartRest(context: context)
            .getCheckoutSummary(
                addressId: address?.addressId, orderIds: orderIds);
        checkout = _checkoutRes.dataResults;
        notifyListeners();
      } else {
        _carts.clear();
        checkout = null;
        notifyListeners();
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      CommonUI(context: context).snackbar(
          message: 'Failed to fetch shopping cart. Please try again.');
    }
  }
}
