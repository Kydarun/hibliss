import 'package:json_annotation/json_annotation.dart';

part 'settings.g.dart';

@JsonSerializable()
class SettingsViewModel {
  bool message;
  bool appointment;
  bool promotion;

  SettingsViewModel({this.message = false, this.appointment = false, this.promotion = false});

  factory SettingsViewModel.fromJson(Map<String, dynamic> json) =>
      _$SettingsViewModelFromJson(json);
  Map<String, dynamic> toJson() => _$SettingsViewModelToJson(this);
}
