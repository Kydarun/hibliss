// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartViewModel _$CartViewModelFromJson(Map<String, dynamic> json) =>
    CartViewModel(
      alacarte: json['alacarte'] == null
          ? null
          : StoreAlacarte.fromJson(json['alacarte'] as Map<String, dynamic>),
      package: json['package'] == null
          ? null
          : StorePackage.fromJson(json['package'] as Map<String, dynamic>),
      product: json['product'] == null
          ? null
          : StoreProduct.fromJson(json['product'] as Map<String, dynamic>),
      quantity: json['quantity'] as int?,
      therapistId: json['therapistId'] as int?,
      therapistName: json['therapistName'] as String?,
    );

Map<String, dynamic> _$CartViewModelToJson(CartViewModel instance) =>
    <String, dynamic>{
      'package': instance.package,
      'alacarte': instance.alacarte,
      'product': instance.product,
      'therapistId': instance.therapistId,
      'therapistName': instance.therapistName,
      'quantity': instance.quantity,
    };
