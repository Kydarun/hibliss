// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingsViewModel _$SettingsViewModelFromJson(Map<String, dynamic> json) =>
    SettingsViewModel(
      message: json['message'] as bool? ?? false,
      appointment: json['appointment'] as bool? ?? false,
      promotion: json['promotion'] as bool? ?? false,
    );

Map<String, dynamic> _$SettingsViewModelToJson(SettingsViewModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'appointment': instance.appointment,
      'promotion': instance.promotion,
    };
