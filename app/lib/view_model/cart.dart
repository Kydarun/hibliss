import 'package:app/model/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart.g.dart';

@JsonSerializable()
class CartViewModel {
  final StorePackage? package;
  final StoreAlacarte? alacarte;
  final StoreProduct? product;
  final int? therapistId;
  final String? therapistName;
  final int? quantity;

  CartViewModel(
      {this.alacarte,
      this.package,
      this.product,
      this.quantity,
      this.therapistId,
      this.therapistName});

  factory CartViewModel.fromJson(Map<String, dynamic> json) => _$CartViewModelFromJson(json);
  Map<String, dynamic> toJson() => _$CartViewModelToJson(this);
}
