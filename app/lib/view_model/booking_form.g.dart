// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking_form.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingFormViewModel _$BookingFormViewModelFromJson(
        Map<String, dynamic> json) =>
    BookingFormViewModel(
      orderId: json['orderId'] as int?,
      relationshipId: json['relationshipId'] as int?,
      therapistId: json['therapistId'] as int?,
      fullName: json['fullName'] as String?,
      dateOfBirth: json['dateOfBirth'] as String?,
      gender: json['gender'] as int?,
      weight: (json['weight'] as num?)?.toDouble(),
      height: (json['height'] as num?)?.toDouble(),
      email: json['email'] as String?,
      contact: json['contact'] as String?,
      timeslot: json['timeslot'] as String?,
    );

Map<String, dynamic> _$BookingFormViewModelToJson(
        BookingFormViewModel instance) =>
    <String, dynamic>{
      'orderId': instance.orderId,
      'relationshipId': instance.relationshipId,
      'therapistId': instance.therapistId,
      'fullName': instance.fullName,
      'dateOfBirth': instance.dateOfBirth,
      'gender': instance.gender,
      'weight': instance.weight,
      'height': instance.height,
      'email': instance.email,
      'contact': instance.contact,
      'timeslot': instance.timeslot,
    };
