import 'dart:typed_data';

import 'package:app/model/address.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'careplan.g.dart';

class CarePlanViewModel {
  int? relationshipId;
  String? fullName;
  DateTime? dob;
  String? email;
  double? weight;
  double? height;
  Address? address;
  String? type;
  Uint8List? affectedArea;
  String? currentSymptoms;
  String? currentSymptomsDesc;
  String? anyMedication;
  String? anyMedicationDesc;
  String? medicalHistory;
  String? existingCustomer;
  DateTime? callback;
  CarePlanPatient? patient;
  CarePlanPain? pain;
  CarePlanOsteoarthritis? osteoarthritis;
  CarePlanDetox? detox;
  CarePlanSkin? skin;
  CarePlanStress? stress;
  CarePlanStroke? stroke;
  CarePlanEye? eye;

  CarePlanViewModel({
    this.relationshipId,
    this.fullName,
    this.dob,
    this.email,
    this.weight,
    this.height,
    this.address,
    this.type,
    this.currentSymptoms,
    this.currentSymptomsDesc,
    this.anyMedication,
    this.anyMedicationDesc,
    this.medicalHistory,
    this.existingCustomer,
    this.callback,
    this.patient,
    this.pain,
    this.osteoarthritis,
    this.detox,
    this.skin,
    this.stress,
    this.stroke,
    this.eye,
  });
}

@JsonSerializable()
class CarePlanPatient {
  String? name;
  String? ic;
  String? email;
  String? gender;
  String? phoneNumber;
  DateTime? birthday;
  double? weight;
  double? height;
  String? address;
  String? state;
  String? postalCode;
  int? relationshipId;

  CarePlanPatient(
      {this.name,
      this.ic,
      this.email,
      this.phoneNumber,
      this.birthday,
      this.weight,
      this.height,
      this.address,
      this.state,
      this.postalCode,
      this.relationshipId});

  factory CarePlanPatient.fromJson(Map<String, dynamic> json) =>
      _$CarePlanPatientFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanPatientToJson(this);
}

@JsonSerializable()
class CarePlanPain {
  int? levelOfPain;
  int? painStart;
  String? xray;
  String? numbness;
  String? muscleAches;
  String? stabbingPain;
  String? limitedFlexibility;
  String? backMovement;
  String? painRadiates;
  String? painBending;
  String? comments;

  CarePlanPain(
      {this.levelOfPain,
      this.painStart,
      this.xray,
      this.numbness,
      this.muscleAches,
      this.stabbingPain,
      this.limitedFlexibility,
      this.backMovement,
      this.painRadiates,
      this.painBending,
      this.comments});

  factory CarePlanPain.fromJson(Map<String, dynamic> json) =>
      _$CarePlanPainFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanPainToJson(this);
}

@JsonSerializable()
class CarePlanOsteoarthritis {
  int? levelOfPain;
  int? painStart;
  String? xray;
  String? numbness;
  String? jointSwelling;
  String? jointInstability;
  String? muscleWeakness;
  String? lossMuscle;
  String? diabetes;
  String? chronicPain;
  String? comments;

  CarePlanOsteoarthritis({
    this.levelOfPain,
    this.painStart,
    this.xray,
    this.numbness,
    this.jointSwelling,
    this.jointInstability,
    this.muscleWeakness,
    this.lossMuscle,
    this.diabetes,
    this.chronicPain,
    this.comments,
  });

  factory CarePlanOsteoarthritis.fromJson(Map<String, dynamic> json) =>
      _$CarePlanOsteoarthritisFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanOsteoarthritisToJson(this);
}

@JsonSerializable()
class CarePlanDetox {
  String? sleepQuality;
  String? appetite;
  String? toiletCondition;
  String? highBloodPressure;
  String? highCholesterol;
  String? diabetes;
  String? insomnia;
  String? lethargy;
  String? constipation;
  String? hormonalImbalance;
  String? comments;
  String? improveHighBlood;
  String? improveCholesterol;
  String? improveDiabetes;
  String? improveKidney;
  String? improveLiver;
  String? improveReproductive;
  String? improveStamina;
  String? improveComments;

  CarePlanDetox({
    this.sleepQuality,
    this.appetite,
    this.toiletCondition,
    this.highBloodPressure,
    this.highCholesterol,
    this.diabetes,
    this.insomnia,
    this.lethargy,
    this.constipation,
    this.hormonalImbalance,
    this.comments,
    this.improveHighBlood,
    this.improveCholesterol,
    this.improveDiabetes,
    this.improveKidney,
    this.improveLiver,
    this.improveReproductive,
    this.improveStamina,
    this.improveComments,
  });

  factory CarePlanDetox.fromJson(Map<String, dynamic> json) =>
      _$CarePlanDetoxFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanDetoxToJson(this);
}

@JsonSerializable()
class CarePlanSkin {
  String? factorFood;
  String? factorWeather;
  String? factorOthers;
  String? dryness;
  String? itchiness;
  String? patches;
  String? bumps;
  String? reddish;
  String? diagnosisDoctor;
  String? comments;

  CarePlanSkin({
    this.factorFood,
    this.factorWeather,
    this.factorOthers,
    this.dryness,
    this.itchiness,
    this.patches,
    this.bumps,
    this.reddish,
    this.diagnosisDoctor,
    this.comments,
  });

  factory CarePlanSkin.fromJson(Map<String, dynamic> json) =>
      _$CarePlanSkinFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanSkinToJson(this);
}

@JsonSerializable()
class CarePlanStress {
  String? insomnia;
  String? headache;
  String? chestPain;
  String? lackAppetite;
  String? hairLoss;
  String? comments;

  CarePlanStress({
    this.insomnia,
    this.headache,
    this.chestPain,
    this.lackAppetite,
    this.hairLoss,
    this.comments,
  });

  factory CarePlanStress.fromJson(Map<String, dynamic> json) =>
      _$CarePlanStressFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanStressToJson(this);
}

@JsonSerializable()
class CarePlanEye {
  String? dryEyes;
  String? wateryEye;
  String? itchyEye;
  String? strainEyes;
  String? darkCircle;
  String? comments;

  CarePlanEye({
    this.dryEyes,
    this.wateryEye,
    this.itchyEye,
    this.strainEyes,
    this.darkCircle,
    this.comments,
  });

  factory CarePlanEye.fromJson(Map<String, dynamic> json) =>
      _$CarePlanEyeFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanEyeToJson(this);
}

@JsonSerializable()
class CarePlanStroke {
  int? sufferYears;
  int? sufferMonths;
  String? dailyActivities;
  String? levelOfStamina;
  String? walkingStick;
  String? wheelchair;
  String? walkingFrame;
  String? splint;
  String? brace;
  String? weakness;
  String? paralysis;
  String? poorBalance;
  String? painNumbness;
  String? burning;
  String? comments;

  CarePlanStroke({
    this.sufferYears,
    this.sufferMonths,
    this.dailyActivities,
    this.levelOfStamina,
    this.walkingStick,
    this.wheelchair,
    this.walkingFrame,
    this.splint,
    this.brace,
    this.weakness,
    this.paralysis,
    this.poorBalance,
    this.painNumbness,
    this.burning,
    this.comments,
  });

  factory CarePlanStroke.fromJson(Map<String, dynamic> json) =>
      _$CarePlanStrokeFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanStrokeToJson(this);
}

@JsonSerializable()
class CarePlanDef {
  String? type;
  String? title;
  String? subtitle;
  List<String>? symptoms;

  CarePlanDef({this.type, this.title, this.subtitle, this.symptoms});

  factory CarePlanDef.fromJson(Map<String, dynamic> json) =>
      _$CarePlanDefFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanDefToJson(this);
}

enum CarePlanType { pain, osteoarthritis, detox, skin, stress, eye, stroke }
