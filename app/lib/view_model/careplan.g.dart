// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'careplan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarePlanPatient _$CarePlanPatientFromJson(Map<String, dynamic> json) =>
    CarePlanPatient(
      name: json['name'] as String?,
      ic: json['ic'] as String?,
      email: json['email'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      birthday: json['birthday'] == null
          ? null
          : DateTime.parse(json['birthday'] as String),
      weight: (json['weight'] as num?)?.toDouble(),
      height: (json['height'] as num?)?.toDouble(),
      address: json['address'] as String?,
      state: json['state'] as String?,
      postalCode: json['postalCode'] as String?,
      relationshipId: json['relationshipId'] as int?,
    )..gender = json['gender'] as String?;

Map<String, dynamic> _$CarePlanPatientToJson(CarePlanPatient instance) =>
    <String, dynamic>{
      'name': instance.name,
      'ic': instance.ic,
      'email': instance.email,
      'gender': instance.gender,
      'phoneNumber': instance.phoneNumber,
      'birthday': instance.birthday?.toIso8601String(),
      'weight': instance.weight,
      'height': instance.height,
      'address': instance.address,
      'state': instance.state,
      'postalCode': instance.postalCode,
      'relationshipId': instance.relationshipId,
    };

CarePlanPain _$CarePlanPainFromJson(Map<String, dynamic> json) => CarePlanPain(
      levelOfPain: json['levelOfPain'] as int?,
      painStart: json['painStart'] as int?,
      xray: json['xray'] as String?,
      numbness: json['numbness'] as String?,
      muscleAches: json['muscleAches'] as String?,
      stabbingPain: json['stabbingPain'] as String?,
      limitedFlexibility: json['limitedFlexibility'] as String?,
      backMovement: json['backMovement'] as String?,
      painRadiates: json['painRadiates'] as String?,
      painBending: json['painBending'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanPainToJson(CarePlanPain instance) =>
    <String, dynamic>{
      'levelOfPain': instance.levelOfPain,
      'painStart': instance.painStart,
      'xray': instance.xray,
      'numbness': instance.numbness,
      'muscleAches': instance.muscleAches,
      'stabbingPain': instance.stabbingPain,
      'limitedFlexibility': instance.limitedFlexibility,
      'backMovement': instance.backMovement,
      'painRadiates': instance.painRadiates,
      'painBending': instance.painBending,
      'comments': instance.comments,
    };

CarePlanOsteoarthritis _$CarePlanOsteoarthritisFromJson(
        Map<String, dynamic> json) =>
    CarePlanOsteoarthritis(
      levelOfPain: json['levelOfPain'] as int?,
      painStart: json['painStart'] as int?,
      xray: json['xray'] as String?,
      numbness: json['numbness'] as String?,
      jointSwelling: json['jointSwelling'] as String?,
      jointInstability: json['jointInstability'] as String?,
      muscleWeakness: json['muscleWeakness'] as String?,
      lossMuscle: json['lossMuscle'] as String?,
      diabetes: json['diabetes'] as String?,
      chronicPain: json['chronicPain'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanOsteoarthritisToJson(
        CarePlanOsteoarthritis instance) =>
    <String, dynamic>{
      'levelOfPain': instance.levelOfPain,
      'painStart': instance.painStart,
      'xray': instance.xray,
      'numbness': instance.numbness,
      'jointSwelling': instance.jointSwelling,
      'jointInstability': instance.jointInstability,
      'muscleWeakness': instance.muscleWeakness,
      'lossMuscle': instance.lossMuscle,
      'diabetes': instance.diabetes,
      'chronicPain': instance.chronicPain,
      'comments': instance.comments,
    };

CarePlanDetox _$CarePlanDetoxFromJson(Map<String, dynamic> json) =>
    CarePlanDetox(
      sleepQuality: json['sleepQuality'] as String?,
      appetite: json['appetite'] as String?,
      toiletCondition: json['toiletCondition'] as String?,
      highBloodPressure: json['highBloodPressure'] as String?,
      highCholesterol: json['highCholesterol'] as String?,
      diabetes: json['diabetes'] as String?,
      insomnia: json['insomnia'] as String?,
      lethargy: json['lethargy'] as String?,
      constipation: json['constipation'] as String?,
      hormonalImbalance: json['hormonalImbalance'] as String?,
      comments: json['comments'] as String?,
      improveHighBlood: json['improveHighBlood'] as String?,
      improveCholesterol: json['improveCholesterol'] as String?,
      improveDiabetes: json['improveDiabetes'] as String?,
      improveKidney: json['improveKidney'] as String?,
      improveLiver: json['improveLiver'] as String?,
      improveReproductive: json['improveReproductive'] as String?,
      improveStamina: json['improveStamina'] as String?,
      improveComments: json['improveComments'] as String?,
    );

Map<String, dynamic> _$CarePlanDetoxToJson(CarePlanDetox instance) =>
    <String, dynamic>{
      'sleepQuality': instance.sleepQuality,
      'appetite': instance.appetite,
      'toiletCondition': instance.toiletCondition,
      'highBloodPressure': instance.highBloodPressure,
      'highCholesterol': instance.highCholesterol,
      'diabetes': instance.diabetes,
      'insomnia': instance.insomnia,
      'lethargy': instance.lethargy,
      'constipation': instance.constipation,
      'hormonalImbalance': instance.hormonalImbalance,
      'comments': instance.comments,
      'improveHighBlood': instance.improveHighBlood,
      'improveCholesterol': instance.improveCholesterol,
      'improveDiabetes': instance.improveDiabetes,
      'improveKidney': instance.improveKidney,
      'improveLiver': instance.improveLiver,
      'improveReproductive': instance.improveReproductive,
      'improveStamina': instance.improveStamina,
      'improveComments': instance.improveComments,
    };

CarePlanSkin _$CarePlanSkinFromJson(Map<String, dynamic> json) => CarePlanSkin(
      factorFood: json['factorFood'] as String?,
      factorWeather: json['factorWeather'] as String?,
      factorOthers: json['factorOthers'] as String?,
      dryness: json['dryness'] as String?,
      itchiness: json['itchiness'] as String?,
      patches: json['patches'] as String?,
      bumps: json['bumps'] as String?,
      reddish: json['reddish'] as String?,
      diagnosisDoctor: json['diagnosisDoctor'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanSkinToJson(CarePlanSkin instance) =>
    <String, dynamic>{
      'factorFood': instance.factorFood,
      'factorWeather': instance.factorWeather,
      'factorOthers': instance.factorOthers,
      'dryness': instance.dryness,
      'itchiness': instance.itchiness,
      'patches': instance.patches,
      'bumps': instance.bumps,
      'reddish': instance.reddish,
      'diagnosisDoctor': instance.diagnosisDoctor,
      'comments': instance.comments,
    };

CarePlanStress _$CarePlanStressFromJson(Map<String, dynamic> json) =>
    CarePlanStress(
      insomnia: json['insomnia'] as String?,
      headache: json['headache'] as String?,
      chestPain: json['chestPain'] as String?,
      lackAppetite: json['lackAppetite'] as String?,
      hairLoss: json['hairLoss'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanStressToJson(CarePlanStress instance) =>
    <String, dynamic>{
      'insomnia': instance.insomnia,
      'headache': instance.headache,
      'chestPain': instance.chestPain,
      'lackAppetite': instance.lackAppetite,
      'hairLoss': instance.hairLoss,
      'comments': instance.comments,
    };

CarePlanEye _$CarePlanEyeFromJson(Map<String, dynamic> json) => CarePlanEye(
      dryEyes: json['dryEyes'] as String?,
      wateryEye: json['wateryEye'] as String?,
      itchyEye: json['itchyEye'] as String?,
      strainEyes: json['strainEyes'] as String?,
      darkCircle: json['darkCircle'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanEyeToJson(CarePlanEye instance) =>
    <String, dynamic>{
      'dryEyes': instance.dryEyes,
      'wateryEye': instance.wateryEye,
      'itchyEye': instance.itchyEye,
      'strainEyes': instance.strainEyes,
      'darkCircle': instance.darkCircle,
      'comments': instance.comments,
    };

CarePlanStroke _$CarePlanStrokeFromJson(Map<String, dynamic> json) =>
    CarePlanStroke(
      sufferYears: json['sufferYears'] as int?,
      sufferMonths: json['sufferMonths'] as int?,
      dailyActivities: json['dailyActivities'] as String?,
      levelOfStamina: json['levelOfStamina'] as String?,
      walkingStick: json['walkingStick'] as String?,
      wheelchair: json['wheelchair'] as String?,
      walkingFrame: json['walkingFrame'] as String?,
      splint: json['splint'] as String?,
      brace: json['brace'] as String?,
      weakness: json['weakness'] as String?,
      paralysis: json['paralysis'] as String?,
      poorBalance: json['poorBalance'] as String?,
      painNumbness: json['painNumbness'] as String?,
      burning: json['burning'] as String?,
      comments: json['comments'] as String?,
    );

Map<String, dynamic> _$CarePlanStrokeToJson(CarePlanStroke instance) =>
    <String, dynamic>{
      'sufferYears': instance.sufferYears,
      'sufferMonths': instance.sufferMonths,
      'dailyActivities': instance.dailyActivities,
      'levelOfStamina': instance.levelOfStamina,
      'walkingStick': instance.walkingStick,
      'wheelchair': instance.wheelchair,
      'walkingFrame': instance.walkingFrame,
      'splint': instance.splint,
      'brace': instance.brace,
      'weakness': instance.weakness,
      'paralysis': instance.paralysis,
      'poorBalance': instance.poorBalance,
      'painNumbness': instance.painNumbness,
      'burning': instance.burning,
      'comments': instance.comments,
    };

CarePlanDef _$CarePlanDefFromJson(Map<String, dynamic> json) => CarePlanDef(
      type: json['type'] as String?,
      title: json['title'] as String?,
      subtitle: json['subtitle'] as String?,
      symptoms: (json['symptoms'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$CarePlanDefToJson(CarePlanDef instance) =>
    <String, dynamic>{
      'type': instance.type,
      'title': instance.title,
      'subtitle': instance.subtitle,
      'symptoms': instance.symptoms,
    };
