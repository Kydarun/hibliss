import 'package:json_annotation/json_annotation.dart';

part 'booking_form.g.dart';

@JsonSerializable()
class BookingFormViewModel {
  int? orderId;
  int? relationshipId;
  int? therapistId;
  String? fullName;
  String? dateOfBirth;
  int? gender;
  double? weight;
  double? height;
  String? email;
  String? contact;
  String? timeslot;

  BookingFormViewModel(
      {this.orderId,
      this.relationshipId,
      this.therapistId,
      this.fullName,
      this.dateOfBirth,
      this.gender,
      this.weight,
      this.height,
      this.email,
      this.contact,
      this.timeslot});

  factory BookingFormViewModel.fromJson(Map<String, dynamic> json) =>
      _$BookingFormViewModelFromJson(json);
  Map<String, dynamic> toJson() => _$BookingFormViewModelToJson(this);
}
