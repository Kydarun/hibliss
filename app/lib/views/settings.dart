import 'package:app/model/notifications.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/cache.dart';
import 'package:app/views/shared/common.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  Notifications _settings = Notifications();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(centerTitle: true, title: Text('Notification')),
        body: Container(
          margin: EdgeInsets.only(top: 20, left: 15, right: 15),
          child: ListView(
            children: [
              _buildSwitch(
                  context: context,
                  title: 'Message Notification',
                  initialValue: _settings.messageNotification ?? false,
                  onChanged: (value) {
                    update(_settings..messageNotification = value);
                  }),
              _buildSwitch(
                  context: context,
                  title: 'Appointment Notification',
                  initialValue: _settings.appointmentNotification ?? false,
                  onChanged: (value) {
                    update(_settings..appointmentNotification = value);
                  }),
              _buildSwitch(
                  context: context,
                  title: 'Promotion Notification',
                  initialValue: _settings.promotionNotification ?? false,
                  onChanged: (value) {
                    update(_settings..promotionNotification = value);
                  })
            ],
          ),
        ));
  }

  Widget _buildSwitch(
          {required BuildContext context,
          required String title,
          bool initialValue = false,
          Function(bool)? onChanged}) =>
      Container(
        margin: EdgeInsets.only(bottom: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style:
                    Theme.of(context).textTheme.subtitle2?.copyWith(fontWeight: FontWeight.w500)),
            CupertinoSwitch(
              value: initialValue,
              onChanged: onChanged,
            )
          ],
        ),
      );

  Future<void> fetch() async {
    try {
      final _apiRes = await UserRest(context: context).getNotification();
      if (_apiRes.dataResults != null) {
        setState(() {
          _settings = _apiRes.dataResults!;
        });
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrintStack(stackTrace: stacktrace);
      final _res = await Cache(context: context).getNotifications();
      setState(() {
        _settings = _res;
      });
    }
  }

  Future<void> update(Notifications notifications) async {
    try {
      final _apiRes =
          await UserRest(context: context).editNotification(notification: notifications);
      if (_apiRes.success) {
        await Cache(context: context).setNotifications(settings: notifications);
        setState(() {
          _settings = notifications;
        });
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrintStack(stackTrace: stacktrace);
      CommonUI(context: context).snackbar(message: 'An error occurred. Please try again.');
    }
  }
}
