import 'package:app/main.dart';
import 'package:app/model/response.dart';
import 'package:app/model/video.dart';
import 'package:app/rest/general.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/careplan/create_1.dart';
import 'package:app/views/careplan/view/list.dart';
import 'package:app/views/home.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart' as yt;
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_metadata/youtube_metadata.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  final _toWhom = [
    "Myself",
    "Spouse",
    "Parents",
    "Grandparents",
    "Siblings",
    "Others"
  ];
  String _selectedToWhom = "Myself";

  @override
  Widget build(BuildContext context) {
    return FutureProvider<Response<Video>>(
      initialData: Response(httpCode: -1),
      create: (context) => GeneralRest(context: context).videos(),
      child: Container(
        color: Color(0xfff4f7fa),
        child: ListView(
          children: [
            Row(
              children: [
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(right: 1),
                  child: Consumer<Session>(
                    builder: (context, session, child) => ElevatedButton.icon(
                        onPressed: () {
                          if (session.isLoggedIn) {
                            showModalBottomSheet(
                                context: context,
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                builder: (ctx) => Container(
                                      height: 558,
                                      child: ListView(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(top: 35),
                                            child: Center(
                                              child: Text('Create Care Plan',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 5, bottom: 20),
                                            child: Center(
                                              child: Text(
                                                  'You are creating this plan for...',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2
                                                      ?.copyWith(
                                                          color: Color(
                                                              0xff5b5b79))),
                                            ),
                                          ),
                                          ..._toWhom.map((to) => InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    _selectedToWhom = to;
                                                  });
                                                  Navigator.of(context).pop();
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              CreateCarePlanStepOne(
                                                                  relationshipId:
                                                                      _toWhom.indexOf(
                                                                              to) +
                                                                          1)));
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      left: 20,
                                                      right: 20,
                                                      top: 10),
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 17,
                                                      horizontal: 20),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      color: Color(0xffe4ebf2)),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(to,
                                                            style: Theme.of(ctx)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontFamily:
                                                                        'Soleil',
                                                                    fontSize:
                                                                        17,
                                                                    color: Color(
                                                                        0xff161640))),
                                                      ),
                                                      Image.asset(
                                                          'res/icons/ic_check_round_${to == _selectedToWhom ? "blue" : "blueborder"}.png',
                                                          width: 21)
                                                    ],
                                                  ),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ));
                          } else {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Login()));
                          }
                        },
                        icon: Image.asset(
                          'res/icons/ic_addplan.png',
                          height: 20,
                        ),
                        label: Container(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: Text('Create Care Plan')),
                        style: Theme.of(context)
                            .elevatedButtonTheme
                            .style
                            ?.copyWith(
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Color(0xff6d6f72)),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              textStyle: MaterialStateProperty.all<TextStyle?>(
                                  TextStyle(
                                      color: Color(0xff6d6f72),
                                      fontFamily: 'Niveau Grotesk',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15)),
                            )),
                  ),
                )),
                Expanded(
                    child: Consumer<Session>(
                  builder: (context, session, child) => Container(
                    margin: EdgeInsets.only(left: 0.5),
                    child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              settings: RouteSettings(name: '/home'),
                              builder: (context) => session.isLoggedIn
                                  ? CarePlanList()
                                  : Login()));
                        },
                        icon: Image.asset(
                          'res/icons/ic_viewplan.png',
                          height: 20,
                        ),
                        label: Container(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: Text('View Care Plan')),
                        style: Theme.of(context)
                            .elevatedButtonTheme
                            .style
                            ?.copyWith(
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Color(0xff6d6f72)),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              textStyle: MaterialStateProperty.all<TextStyle?>(
                                  TextStyle(
                                      color: Color(0xff6d6f72),
                                      fontFamily: 'Niveau Grotesk',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15)),
                            )),
                  ),
                ))
              ],
            ),
            Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 5),
              padding:
                  EdgeInsets.only(left: 25, right: 15, top: 10, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Our Services',
                      style: Theme.of(context).textTheme.headline5),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_body.png',
                                title: 'Pain',
                                onClick: () {
                                  _navigate(0);
                                }),
                          ),
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_detox.png',
                                title: 'Detox',
                                onClick: () {
                                  _navigate(2);
                                }),
                          ),
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_stress.png',
                                title: 'Stress',
                                onClick: () {
                                  _navigate(4);
                                }),
                          ),
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_eye.png',
                                title: 'Eye',
                                onClick: () {
                                  _navigate(5);
                                }),
                          )
                        ]),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_skin.png',
                                title: 'Skin',
                                onClick: () {
                                  _navigate(3);
                                }),
                          ),
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_osteo.png',
                                title: 'Osteoarthritis',
                                onClick: () {
                                  _navigate(1);
                                }),
                          ),
                          Expanded(
                            child: _buildService(
                                context: context,
                                imagePath: 'res/icons/img_stroke.png',
                                title: 'Post-stroke',
                                onClick: () {
                                  _navigate(6);
                                }),
                          ),
                          Expanded(child: Container()),
                        ]),
                  )
                ],
              ),
            ),
            /*Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 2),
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: Text('Promotion',
                        style: Theme.of(context).textTheme.headline5),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 7),
                    child: Row(
                      children: [
                        Expanded(
                          child: _buildPromotion(
                              context: context,
                              imagePath: 'res/images/home.png',
                              title: '1st Trial Promotions',
                              expiry: DateTime.now(),
                              onClick: () {}),
                        ),
                        Expanded(
                          child: _buildPromotion(
                              context: context,
                              imagePath: 'res/images/home.png',
                              title: 'Lucky Draw!',
                              expiry: DateTime.now(),
                              onClick: () {}),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),*/
            Consumer<Response<Video>>(
              builder: (context, response, child) => Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 2),
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Testimonies',
                        style: Theme.of(context).textTheme.headline5),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 181,
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          if (response.httpCode == -1)
                            StatelessLoadingIndicator()
                          else
                            ...response.dataResults!
                                .where((element) =>
                                    element.id >= 1 && element.id <= 5)
                                .map((item) =>
                                    _YouTubeThumbnail(link: item.link))
                                .toList()
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Consumer<Response<Video>>(
              builder: (context, response, child) => Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 2),
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Hi-Bliss TV',
                        style: Theme.of(context).textTheme.headline5),
                    Container(
                        height: 181,
                        margin: EdgeInsets.only(top: 10),
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: [
                            if (response.httpCode == -1)
                              StatelessLoadingIndicator()
                            else
                              ...response.dataResults!
                                  .where((element) =>
                                      element.id >= 6 && element.id <= 10)
                                  .map((item) =>
                                      _YouTubeThumbnail(link: item.link))
                                  .toList()
                          ],
                        ))
                  ],
                ),
              ),
            ),
            Consumer<Response<Video>>(
              builder: (context, response, child) => Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 2),
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Announcement',
                        style: Theme.of(context).textTheme.headline5),
                    Container(
                      height: 181,
                      margin: EdgeInsets.only(top: 10),
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          if (response.httpCode == -1)
                            StatelessLoadingIndicator()
                          else
                            ...response.dataResults!
                                .where((element) =>
                                    element.id >= 11 && element.id <= 15)
                                .map((item) =>
                                    _YouTubeThumbnail(link: item.link))
                                .toList()
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildService(
          {required BuildContext context,
          required String imagePath,
          required String title,
          required VoidCallback onClick}) =>
      InkWell(
        onTap: onClick,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(imagePath, width: 62),
              Text(title,
                  style: Theme.of(context).textTheme.bodyText1,
                  overflow: TextOverflow.ellipsis)
            ],
          ),
        ),
      );

  Widget _buildPromotion(
          {required BuildContext context,
          required String imagePath,
          required String title,
          required DateTime expiry,
          required VoidCallback onClick}) =>
      InkWell(
        onTap: onClick,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: AspectRatio(
                    aspectRatio: 1,
                    child: Image.asset(imagePath, fit: BoxFit.contain)),
              ),
              Container(
                  margin: EdgeInsets.only(top: 5),
                  child: Text(title,
                      style: Theme.of(context).textTheme.headline5)),
              Row(
                children: [
                  Image.asset('res/icons/ic_calendar_gray.png', width: 11),
                  Container(
                      margin: EdgeInsets.only(left: 5),
                      child: Text(
                          'Until ${DateFormat("dd MMM YYYY").format(expiry)}'))
                ],
              )
            ],
          ),
        ),
      );

  _navigate(int index) {
    context.read<TabChangeNotifier>().update(index: 1);
    context.read<ShopTabChangeNotifier>().update(index: index);
  }
}

class _YouTubeThumbnail extends StatefulWidget {
  final String link;

  _YouTubeThumbnail({required this.link});

  @override
  __YouTubeThumbnailState createState() => __YouTubeThumbnailState();
}

class __YouTubeThumbnailState extends State<_YouTubeThumbnail>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureProvider<MetaDataModel?>(
      create: (context) => YoutubeMetaData.getData(widget.link),
      catchError: (context, error) => null,
      initialData: null,
      child: Consumer<MetaDataModel?>(
        builder: (context, video, child) => Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 4)
          ]),
          margin: EdgeInsets.only(right: 10),
          width: 240,
          child: video == null
              ? StatelessLoadingIndicator()
              : GestureDetector(
                  onTap: () {
                    launch(widget.link);
                  },
                  child: Column(
                    children: [
                      Container(
                        height: 135,
                        child: Stack(
                          children: [
                            Image.network(
                              video.thumbnailUrl!,
                              width: 240,
                              fit: BoxFit.cover,
                            ),
                            /*Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                  margin: EdgeInsets.all(4),
                                  padding: EdgeInsets.symmetric(vertical: 3, horizontal: 4),
                                  color: Color(0xcc000000),
                                  child: Text(
                                    '0:00',
                                    style: Theme.of(context).textTheme.bodyText1?.copyWith(
                                        fontFamily: 'Roboto', color: Colors.white, fontSize: 12),
                                  )),
                            )*/
                          ],
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: 8, bottom: 4, left: 5, right: 5),
                          color: Color(0xfff1f1f1),
                          child: Text(video.title!,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis))
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
