import 'package:app/main.dart';
import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/model/store.dart';
import 'package:app/rest/booking.dart';
import 'package:app/rest/cart.dart';
import 'package:app/rest/store.dart';
import 'package:app/views/booking/booking.dart';
import 'package:app/views/booking/detail.dart';
import 'package:app/views/home.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:app/views/shop/info.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

class BookingTab extends StatefulWidget {
  const BookingTab({Key? key}) : super(key: key);

  @override
  _BookingTabState createState() => _BookingTabState();
}

class _BookingTabState extends State<BookingTab> {
  int _currentSelectedIndex = 0;

  final _subTabs = [_UnbookedSubTab(), _UpcomingSubTab(), _HistorySubTab()];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20, left: 10, bottom: 20),
          child: Row(children: [
            _buildSubTab(context, 'To book', 0),
            _buildSubTab(context, 'Upcoming', 1),
            _buildSubTab(context, 'History', 2)
          ]),
        ),
        Expanded(child: _subTabs[_currentSelectedIndex])
      ],
    );
  }

  Widget _buildSubTab(BuildContext context, String title, int index) =>
      GestureDetector(
        onTap: () {
          setState(() {
            _currentSelectedIndex = index;
          });
        },
        child: Container(
            decoration: BoxDecoration(
                color: _currentSelectedIndex == index
                    ? Color(0xff17b491)
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(12)),
            padding: EdgeInsets.symmetric(horizontal: 13, vertical: 1),
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline5?.copyWith(
                  color: _currentSelectedIndex == index
                      ? Colors.white
                      : Color(0xff161640)),
            )),
      );
}

class _UnbookedSubTab extends StatefulWidget {
  const _UnbookedSubTab({Key? key}) : super(key: key);

  @override
  __UnbookedSubTabState createState() => __UnbookedSubTabState();
}

class __UnbookedSubTabState extends State<_UnbookedSubTab>
    with AutomaticKeepAliveClientMixin {
  final _toWhom = [
    "Myself",
    "Spouse",
    "Parents",
    "Grandparents",
    "Siblings",
    "Others"
  ];
  String _selectedToWhom = "Myself";

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: FutureProvider<Response<BookingOrder>>(
        create: (context) => BookingRest(context: context).getUnbookedOrder(),
        initialData: Response(httpCode: -1),
        child: Consumer<Response<BookingOrder>>(
          builder: (context, booking, child) => booking.httpCode == -1
              ? StatelessLoadingIndicator()
              : ChangeNotifierProvider<BookingOrderListChangeNotifier>(
                  create: (context) => BookingOrderListChangeNotifier(
                      orders: booking.dataResults ?? []),
                  child: Consumer<BookingOrderListChangeNotifier>(
                    builder: (context, notifier, child) => LoadingIndicator(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: ListView(
                          children: notifier.orders!
                              .map((book) => InkWell(
                                    onTap: () {
                                      showModalBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          builder: (ctx) => Container(
                                                height: 558,
                                                child: ListView(
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 35),
                                                      child: Center(
                                                        child: Text(
                                                            'Book an appointment',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .headline5),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5, bottom: 20),
                                                      child: Center(
                                                        child: Text(
                                                            'You are booking this appointment for...',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText2
                                                                ?.copyWith(
                                                                    color: Color(
                                                                        0xff5b5b79))),
                                                      ),
                                                    ),
                                                    ..._toWhom
                                                        .map((to) => InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  _selectedToWhom =
                                                                      to;
                                                                });
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                                Navigator.of(
                                                                        context)
                                                                    .push(MaterialPageRoute(
                                                                        builder: (context) => BookingPage(
                                                                            order:
                                                                                book,
                                                                            relationshipId: _toWhom.indexOf(to) +
                                                                                1)))
                                                                    .then(
                                                                        (value) {
                                                                  reload(
                                                                          context:
                                                                              context)
                                                                      .then(
                                                                          (_) {
                                                                    if (value
                                                                        is int) {
                                                                      Navigator.of(
                                                                              context)
                                                                          .push(
                                                                              MaterialPageRoute(builder: (context) => BookingDetail(bookingId: value)));
                                                                    }
                                                                  });
                                                                });
                                                              },
                                                              child: Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            20,
                                                                        right:
                                                                            20,
                                                                        top:
                                                                            10),
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        vertical:
                                                                            17,
                                                                        horizontal:
                                                                            20),
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10),
                                                                    color: Color(
                                                                        0xffe4ebf2)),
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: [
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              10),
                                                                      child: Text(
                                                                          to,
                                                                          style: Theme.of(ctx).textTheme.bodyText1?.copyWith(
                                                                              fontFamily: 'Soleil',
                                                                              fontSize: 17,
                                                                              color: Color(0xff161640))),
                                                                    ),
                                                                    Image.asset(
                                                                        'res/icons/ic_check_round_${to == _selectedToWhom ? "blue" : "blueborder"}.png',
                                                                        width:
                                                                            21)
                                                                  ],
                                                                ),
                                                              ),
                                                            ))
                                                  ],
                                                ),
                                              ));
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 5),
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Row(
                                        children: [
                                          if ((book.imageLink?.length ?? 0) > 0)
                                            Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Image.network(
                                                    book.imageLink![0].link!,
                                                    width: 84,
                                                    fit: BoxFit.fitWidth))
                                          else
                                            Image.asset(
                                              'res/images/home.png',
                                              width: 84,
                                            ),
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Expanded(
                                                        child: Text(
                                                            book.itemName ??
                                                                'No title',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .headline5),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Text(
                                                          'Order ID: ${book.displayOrderId}',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1)),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          top: 10),
                                                      child: Text(
                                                          'Book an appointment',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .bodyText1
                                                              ?.copyWith(
                                                                  color: Color(
                                                                      0xff0095cf),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold)))
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ))
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Future reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await BookingRest(context: context).getUnbookedOrder();
    context
        .read<BookingOrderListChangeNotifier>()
        .update(orders: _res.dataResults ?? []);
    context.read<LoadingState>().toggle(value: false);
  }

  @override
  bool get wantKeepAlive => true;
}

class _UpcomingSubTab extends StatefulWidget {
  const _UpcomingSubTab({Key? key}) : super(key: key);

  @override
  __UpcomingSubTabState createState() => __UpcomingSubTabState();
}

class __UpcomingSubTabState extends State<_UpcomingSubTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: FutureProvider<Response<BookingOrder>>(
        create: (context) =>
            BookingRest(context: context).getUpcomingBookingOrder(),
        initialData: Response(httpCode: -1),
        child: Consumer<Response<BookingOrder>>(
          builder: (context, booking, child) => booking.httpCode == -1
              ? StatelessLoadingIndicator()
              : ChangeNotifierProvider<BookingOrderListChangeNotifier>(
                  create: (context) => BookingOrderListChangeNotifier(
                      orders: booking.dataResults ?? []),
                  child: Consumer<BookingOrderListChangeNotifier>(
                    builder: (context, notifier, child) => LoadingIndicator(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: ListView(
                          children: notifier.orders!
                              .map((book) => InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (ctx) => BookingDetail(
                                                  bookingId: book.bookingId)))
                                          .then((value) {
                                        reload(context: context);
                                      });
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 5),
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Row(
                                        children: [
                                          if ((book.imageLink?.length ?? 0) > 0)
                                            Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Image.network(
                                                    book.imageLink![0].link!,
                                                    width: 84,
                                                    fit: BoxFit.fitWidth))
                                          else
                                            Image.asset(
                                              'res/images/home.png',
                                              width: 84,
                                            ),
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Expanded(
                                                        child: Text(
                                                            book.itemName ??
                                                                'No title',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .headline5),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Text(
                                                          'Order ID: ${book.displayOrderId}',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1)),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Row(
                                                        children: [
                                                          Image.asset(
                                                              'res/icons/ic_clock.png',
                                                              width: 13),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 5),
                                                            child: Text(
                                                                book.timeSlot!,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1),
                                                          ),
                                                        ],
                                                      )),
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ))
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Future reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await BookingRest(context: context).getUpcomingBookingOrder();
    context
        .read<BookingOrderListChangeNotifier>()
        .update(orders: _res.dataResults ?? []);
    context.read<LoadingState>().toggle(value: false);
  }

  @override
  bool get wantKeepAlive => true;
}

class _HistorySubTab extends StatefulWidget {
  const _HistorySubTab({Key? key}) : super(key: key);

  @override
  __HistorySubTabState createState() => __HistorySubTabState();
}

class __HistorySubTabState extends State<_HistorySubTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: LoadingIndicator(
        child: FutureProvider<Response<BookingOrder>>(
          create: (context) =>
              BookingRest(context: context).getBookingHistory(),
          initialData: Response(httpCode: -1),
          child: Consumer<Response<BookingOrder>>(
            builder: (context, booking, child) => booking.httpCode == -1
                ? StatelessLoadingIndicator()
                : Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ListView(
                      children: booking.dataResults!
                          .map((book) => Container(
                                margin: EdgeInsets.only(bottom: 5),
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  children: [
                                    if ((book.imageLink?.length ?? 0) > 0)
                                      Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Image.network(
                                              book.imageLink![0].link!,
                                              width: 84,
                                              fit: BoxFit.fitWidth))
                                    else
                                      Image.asset(
                                        'res/images/home.png',
                                        width: 84,
                                      ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                        builder: (ctx) =>
                                                            BookingDetail(
                                                                bookingId: book
                                                                    .bookingId,
                                                                isHistory: true,
                                                                booking: book)))
                                                    .then((value) {
                                                  if (value is int &&
                                                      value == 4) {
                                                    context
                                                        .read<
                                                            TabChangeNotifier>()
                                                        .update(index: 4);
                                                  }
                                                });
                                              },
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                        book.itemName ??
                                                            'No title',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline5),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: Text(
                                                    'Order ID: ${book.displayOrderId}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1)),
                                            if (book.reorderFlag ?? false)
                                              GestureDetector(
                                                onTap: () {
                                                  reorder(book);
                                                },
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Text('Reorder',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                color: Color(
                                                                    0xff0095cf),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold))),
                                              )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))
                          .toList(),
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  Future<void> reorder(BookingOrder book) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await BookingRest(context: context)
        .getBooking(bookingId: book.bookingId!);
    try {
      if (_res.dataResults!.productType == 'Service') {
        await CartRest(context: context).addServiceToCart(
            productId: book.productId ?? 0,
            customerAddressId: book.customerAddressId ?? 0,
            therapistId: book.therapistId ?? 0,
            therapistAddressId: book.therapistAddressId ?? 0);
      } else {
        await CartRest(context: context).addGoodsToCart(
            productId: book.productId ?? 0,
            customerAddressId: book.customerAddressId ?? 0);
      }
      await context.read<Cart>().load();
      context.read<TabChangeNotifier>().update(index: 4);
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }

  /*Future<void> reorder(BookingOrder book) async {
    context.read<LoadingState>().toggle(value: true);
    try {
      final _res = await BookingRest(context: context)
          .getBooking(bookingId: book.bookingId!);
      if (_res.dataResults!.productType == "Service") {
        final _category = await StoreRest(context: context)
            .service(id: _res.dataResults!.productId!);
        final _list = await StoreRest(context: context)
            .list(categoryId: _category.dataResults!.categoryId!);
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (context) => ItemInfo(
                    alacarte: _list.dataResults!.alacarte?.firstWhereOrNull(
                        (element) =>
                            element.id! == _res.dataResults!.productId!),
                    package: _list.dataResults!.package?.firstWhereOrNull(
                        (element) =>
                            element.id! == _res.dataResults!.productId!))))
            .then((value) {
          if (value is int) {
            context.read<TabChangeNotifier>().update(index: value);
          }
        });
      } else if (_res.dataResults!.productType == "Package") {
        final _category = await StoreRest(context: context)
            .package(id: _res.dataResults!.productId!);
        final _list = await StoreRest(context: context)
            .list(categoryId: _category.dataResults!.categoryId!);
        context.read<TabChangeNotifier>().update(index: 1);
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (context) => ItemInfo(
                    package: _list.dataResults!.package!.firstWhere(
                        (element) =>
                            element.id! == _res.dataResults!.productId!,
                        orElse: () => StorePackage()))))
            .then((value) {
          if (value is int) {
            context.read<TabChangeNotifier>().update(index: value);
          }
        });
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }*/

  @override
  bool get wantKeepAlive => true;
}

class BookingOrderListChangeNotifier with ChangeNotifier {
  List<BookingOrder>? orders;

  BookingOrderListChangeNotifier({this.orders = const []});

  update({required List<BookingOrder> orders}) {
    this.orders = orders;
    notifyListeners();
  }
}
