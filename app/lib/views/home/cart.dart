import 'package:app/main.dart';
import 'package:app/model/address.dart';
import 'package:app/model/cart.dart';
import 'package:app/rest/cart.dart';
import 'package:app/views/cart/checkout.dart';
import 'package:app/views/home.dart';
import 'package:app/views/profile/address/list.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:app/views/shared/listview.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartTab extends StatefulWidget {
  final VoidCallback? onDelete;

  CartTab({Key? key, this.onDelete}) : super(key: key);

  @override
  CartTabState createState() => CartTabState();
}

class CartTabState extends State<CartTab> {
  final GlobalKey<MultiSelectableListViewState<ShoppingCart>> _listViewKey =
      GlobalKey();

  bool _isSelectedAll = false;
  bool _isDeletingCart = false;
  bool _canDelete = false;

  toggleDeleteCart({required bool isDeleting}) {
    setState(() {
      _isDeletingCart = isDeleting;
      _isSelectedAll = false;
    });
    _listViewKey.currentState?.deSelectAll();
  }

  @override
  void initState() {
    super.initState();
    _isSelectedAll = false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    context.read<Cart>().load();
  }

  @override
  Widget build(BuildContext context) {
    return UnauthenticatedWithoutOverlay(
      child: LoadingIndicator(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (ctx) => AddressList(callback: (item) {
                          _changeAddress(address: item);
                        })));
              },
              child: Container(
                color: Theme.of(context).primaryColor,
                padding:
                    EdgeInsets.only(left: 25, right: 20, top: 7, bottom: 12),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset('res/icons/ic_location.png', width: 14),
                      Consumer<Cart>(
                          builder: (context, cart, child) => Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    '${cart.address?.address ?? 'No address selected.'}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        ?.copyWith(color: Colors.white),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              )),
                      Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Image.asset(
                              'res/icons/ic_right_arrow_white.png',
                              width: 8))
                    ]),
              ),
            ),
            Expanded(
                child: Consumer<Cart>(
              builder: (context, cart, child) => cart.carts.length == 0
                  ? Container(
                      child: Center(
                        child: Text('No items.',
                            style: Theme.of(context).textTheme.headline4),
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(top: 20),
                      child: MultiSelectableListView<ShoppingCart>(
                        key: _listViewKey,
                        builder: (context, item) => Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white),
                          margin: EdgeInsets.only(bottom: 5, right: 10),
                          padding: EdgeInsets.only(
                              left: 5, top: 5, bottom: 5, right: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              if ((item?.imageLink?.length ?? 0) > 0)
                                Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Image.network(
                                        item!.imageLink![0].link!,
                                        width: 84,
                                        fit: BoxFit.fitWidth))
                              else
                                Image.asset(
                                  'res/images/home.png',
                                  width: 84,
                                ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(item!.itemName!,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis),
                                    if (item.productType == "Good")
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                                'res/icons/ic_check_dark.png',
                                                width: 13),
                                            Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Text(
                                                      item.productRemark!,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                      maxLines: 1,
                                                      overflow: TextOverflow
                                                          .ellipsis)),
                                            )
                                          ],
                                        ),
                                      ),
                                    if (item.productType == "Service" ||
                                        item.productType == "Package")
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                                'res/icons/ic_clock.png',
                                                width: 13),
                                            Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Text(
                                                      '${item.duration} minutes',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1)),
                                            )
                                          ],
                                        ),
                                      ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5),
                                      child: Row(
                                        children: [
                                          Image.asset('res/icons/ic_money.png',
                                              width: 13),
                                          Expanded(
                                            child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text('${item.subTotal}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            color: Color(
                                                                0xff0095cf),
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 2),
                                padding: EdgeInsets.only(
                                    right: 9, left: 16, top: 3, bottom: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Color(0xffededf0)),
                                child: DropdownButton<int>(
                                    icon: Image.asset(
                                        'res/icons/ic_down_black.png'),
                                    isDense: true,
                                    iconSize: 8,
                                    underline: Container(),
                                    value: item.totalItem,
                                    onChanged:
                                        (_listViewKey.currentState?.selected().where((element) => element.orderId == item.orderId).length ?? 0) >
                                                0
                                            ? (value) {
                                                _updateItem(
                                                    item: item,
                                                    newQuantity: value!);
                                              }
                                            : null,
                                    selectedItemBuilder: (context) => List.generate(99, (index) => index + 1)
                                        .map((n) => Center(
                                            child: Container(
                                                margin:
                                                    EdgeInsets.only(right: 15),
                                                child: Text('$n',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText2))))
                                        .toList(),
                                    items: List.generate(99, (index) => index + 1)
                                        .map((n) => DropdownMenuItem<int>(child: Text('$n', style: Theme.of(context).textTheme.bodyText2), value: n))
                                        .toList()),
                              )
                            ],
                          ),
                        ),
                        items: cart.carts,
                        onSelected: (items) async {
                          if (_isDeletingCart) {
                            setState(() {
                              _canDelete = (_listViewKey.currentState
                                          ?.selected()
                                          .length ??
                                      0) >
                                  0;
                            });
                          } else {
                            context.read<LoadingState>().toggle(value: true);
                            try {
                              if (items.length == 0) {
                                await context.read<Cart>().load();
                              } else {
                                await context.read<Cart>().load(
                                    orderIds: items
                                        .map((item) => '${item.orderId}')
                                        .reduce((value, element) =>
                                            '$value,$element'));
                              }
                              setState(() {
                                _isSelectedAll =
                                    context.read<Cart>().isAllSelected();
                              });
                            } on Exception catch (e, stacktrace) {
                              debugPrint('$e');
                              debugPrint('$stacktrace');
                            } finally {
                              context.read<LoadingState>().toggle(value: false);
                            }
                          }
                        },
                      ),
                    ),
            )),
            if (!_isDeletingCart)
              Consumer<Cart>(
                builder: (context, cart, child) => cart.carts.length == 0
                    ? Container()
                    : Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                        color: Colors.white,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _isSelectedAll = !_isSelectedAll;
                                });
                                if (_isSelectedAll) {
                                  _listViewKey.currentState?.selectAll();
                                } else {
                                  _listViewKey.currentState?.deSelectAll();
                                }
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 5, left: 10),
                                child: Row(
                                  children: [
                                    Image.asset(
                                        'res/icons/${context.read<Cart>().isAllSelected() ? "ic_check_round_blue.png" : "ic_check_round_blueborder.png"}',
                                        width: 25),
                                    Container(
                                        margin: EdgeInsets.only(left: 5),
                                        child: Text('All',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1))
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 9),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text('Subtotal:',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1),
                                          Text(
                                              'RM${cart.checkout?.subtotal?.toStringAsFixed(2) ?? '0.00'}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5
                                                  ?.copyWith(
                                                      color: Theme.of(context)
                                                          .primaryColor))
                                        ]),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text('Travel:',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1),
                                          Text(
                                              'RM${cart.checkout?.travelFee?.toStringAsFixed(2) ?? '0.00'}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  ?.copyWith(
                                                      color: Theme.of(context)
                                                          .primaryColor))
                                        ])
                                  ],
                                ),
                              ),
                            ),
                            ElevatedButton(
                                onPressed: (_listViewKey.currentState
                                                ?.selected()
                                                .length ??
                                            0) ==
                                        0
                                    ? null
                                    : () {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    CheckoutPage()))
                                            .then((value) {
                                          if (value is int) {
                                            context
                                                .read<TabChangeNotifier>()
                                                .update(index: value);
                                          }
                                        });
                                      },
                                style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                                    shadowColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.transparent),
                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)))),
                                child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 24),
                                    child: Text('Check Out')))
                          ],
                        ),
                      ),
              )
            else
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _isSelectedAll = !_isSelectedAll;
                        });
                        if (_isSelectedAll) {
                          _listViewKey.currentState?.selectAll();
                        } else {
                          _listViewKey.currentState?.deSelectAll();
                        }
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 5, left: 10),
                        child: Row(
                          children: [
                            Image.asset(
                                'res/icons/${(_listViewKey.currentState?.isAllSelected() ?? false) ? "ic_check_round_blue.png" : "ic_check_round_blueborder.png"}',
                                width: 25),
                            Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text('All',
                                    style:
                                        Theme.of(context).textTheme.bodyText1))
                          ],
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: _canDelete
                            ? () {
                                showDialog(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (ctx) => HiBlissDialog(
                                          context: context,
                                          title: 'Confirm Delete',
                                          message:
                                              'Are you sure you want to \ndelete item(s)?',
                                          onConfirm: () {
                                            _deleteItems(
                                                items: _listViewKey.currentState
                                                        ?.selected() ??
                                                    []);
                                          },
                                        ));
                              }
                            : null,
                        style: Theme.of(context)
                            .elevatedButtonTheme
                            .style
                            ?.copyWith(
                                shadowColor: MaterialStateProperty.all<Color>(
                                    Colors.transparent),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)))),
                        child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            child: Text('Delete')))
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  _updateItem({required ShoppingCart item, required int newQuantity}) async {
    context.read<LoadingState>().toggle(value: true);
    await CartRest(context: context)
        .updateOrder(orderId: item.orderId!, itemCount: newQuantity);
    final _items = _listViewKey.currentState?.selected() ?? [];
    if (_items.length == 0) {
      await context.read<Cart>().load();
    } else {
      await context.read<Cart>().load(
          orderIds: _items
              .map((item) => '${item.orderId}')
              .reduce((value, element) => '$value,$element'));
    }
    context.read<LoadingState>().toggle(value: false);
  }

  _deleteItems({required List<ShoppingCart> items}) async {
    try {
      context.read<LoadingState>().toggle(value: true);
      for (ShoppingCart item in items) {
        await CartRest(context: context).deleteOrder(orderId: item.orderId!);
      }
      setState(() {
        _isDeletingCart = false;
        _canDelete = false;
      });
      if (widget.onDelete != null) {
        widget.onDelete!();
      }
      _listViewKey.currentState?.deSelectAll();
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      CommonUI(context: context)
          .snackbar(message: 'Failed to delete, please try again.');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }

  _changeAddress({required Address address}) async {
    context.read<LoadingState>().toggle(value: true);
    final _items = _listViewKey.currentState?.selected() ?? [];
    if (_items.length == 0) {
      await context.read<Cart>().setAddress(address: address);
    } else {
      await context.read<Cart>().setAddress(
          address: address,
          orderIds: _items
              .map((item) => '${item.orderId}')
              .reduce((value, element) => '$value,$element'));
    }
    context.read<LoadingState>().toggle(value: false);
  }
}
