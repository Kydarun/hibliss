import 'package:app/main.dart';
import 'package:app/model/category.dart';
import 'package:app/model/response.dart';
import 'package:app/model/store.dart';
import 'package:app/model/therapist.dart';
import 'package:app/rest/store.dart';
import 'package:app/views/home.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shop/info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShopTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureProvider<Response<Category>>(
      create: (context) => StoreRest(context: context).categories(),
      initialData: Response(httpCode: -1),
      child: Consumer<Response<Category>>(
          builder: (context, response, child) => response.httpCode == -1
              ? StatelessLoadingIndicator()
              : DefaultTabController(
                  length: response.dataResults!.length,
                  initialIndex: context
                      .read<ShopTabChangeNotifier>()
                      .selectedShopTabIndex,
                  child: Column(
                    children: [
                      Container(
                        color: Theme.of(context).primaryColor,
                        child: Container(
                          margin: EdgeInsets.only(left: 14),
                          child: TabBar(
                              isScrollable: true,
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  ?.copyWith(color: Colors.white),
                              indicator: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.white, width: 3))),
                              indicatorColor: Colors.white,
                              indicatorPadding: EdgeInsets.only(bottom: 1),
                              indicatorWeight: 3,
                              unselectedLabelStyle: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  ?.copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal),
                              tabs: context
                                  .read<Response<Category>>()
                                  .dataResults!
                                  .map((category) => Tab(
                                        text: category.category,
                                      ))
                                  .toList()),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                            children: response.dataResults!
                                .map((category) =>
                                    _ShopCategoryTab(categoryId: category.id))
                                .toList()),
                      )
                    ],
                  ))),
    );
  }
}

class _ShopCategoryTab extends StatefulWidget {
  final int categoryId;

  _ShopCategoryTab({required this.categoryId});

  @override
  _ShopCategoryTabState createState() => _ShopCategoryTabState();
}

class _ShopCategoryTabState extends State<_ShopCategoryTab>
    with AutomaticKeepAliveClientMixin {
  int _currentSelectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureProvider<Response2<Store>>(
      create: (context) =>
          StoreRest(context: context).list(categoryId: widget.categoryId),
      initialData: Response2(httpCode: -1),
      child: Consumer<Response2<Store>>(
        builder: (context, response, child) => response.httpCode == -1
            ? StatelessLoadingIndicator()
            : Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        child: Row(children: [
                          _buildSubTab(context, 'A la carte', 0),
                          _buildSubTab(context, 'Package', 1),
                          _buildSubTab(context, 'Store', 2)
                        ]),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Column(
                        children: _buildItems(response.dataResults!,
                            _currentSelectedIndex, () {}),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  List<Widget> _buildItems(Store store, int index, VoidCallback onClick) {
    switch (index) {
      case 0:
        if ((store.alacarte?.length ?? 0) == 0) {
          return [Text('No items.')];
        }
        return store.alacarte!
            .map((alacarte) => InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => ItemInfo(alacarte: alacarte)))
                        .then((value) {
                      if (value is int) {
                        context.read<TabChangeNotifier>().update(index: value);
                      }
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 5),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: _buildItemUI(
                        title: alacarte.title!,
                        price: alacarte.price!,
                        duration: alacarte.duration!,
                        image: alacarte.imageLink),
                  ),
                ))
            .toList();
      case 1:
        if ((store.package?.length ?? 0) == 0) {
          return [Text('No items.')];
        }
        return store.package!
            .map((package) => InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => ItemInfo(package: package)))
                        .then((value) {
                      if (value is int) {
                        context.read<TabChangeNotifier>().update(index: value);
                      }
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 5),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: _buildItemUI(
                        title: package.title!,
                        price: package.price!,
                        duration: package.duration!,
                        image: package.imageLink),
                  ),
                ))
            .toList();
      case 2:
        if ((store.store?.length ?? 0) == 0) {
          return [Text('No items.')];
        }
        return store.store!
            .map((product) => InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => ItemInfo(product: product)))
                        .then((value) {
                      if (value is int) {
                        context.read<TabChangeNotifier>().update(index: value);
                      }
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 5),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: _buildItemUI(
                        title: product.title!,
                        price: product.price!,
                        remarks: product.remarks!,
                        image: product.imageLink),
                  ),
                ))
            .toList();
      default:
        return [];
    }
  }

  Widget _buildItemUI(
          {List<ImageLink>? image,
          required String title,
          int? duration,
          String? remarks,
          required String price}) =>
      Row(
        children: [
          if ((image?.length ?? 0) > 0)
            Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Image.network(image![0].link!,
                    width: 84, fit: BoxFit.fitWidth))
          else
            Image.asset(
              'res/images/home.png',
              width: 84,
            ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: Text(title,
                              style: Theme.of(context).textTheme.headline5)),
                    ],
                  ),
                  if (duration != null)
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        children: [
                          Image.asset('res/icons/ic_clock.png', width: 13),
                          Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text('$duration minutes',
                                  style: Theme.of(context).textTheme.bodyText1))
                        ],
                      ),
                    ),
                  if (remarks != null)
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        children: [
                          Image.asset('res/icons/ic_check_dark.png', width: 13),
                          Expanded(
                            child: Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(remarks,
                                    style:
                                        Theme.of(context).textTheme.bodyText1)),
                          )
                        ],
                      ),
                    ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Row(
                      children: [
                        Image.asset('res/icons/ic_money.png', width: 13),
                        Container(
                            margin: EdgeInsets.only(left: 5),
                            child: Text('RM$price',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(
                                        color: Color(0xff0095cf),
                                        fontWeight: FontWeight.bold)))
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      );

  Widget _buildSubTab(BuildContext context, String title, int index) =>
      GestureDetector(
        onTap: () {
          setState(() {
            _currentSelectedIndex = index;
          });
        },
        child: Container(
            decoration: BoxDecoration(
                color: _currentSelectedIndex == index
                    ? Color(0xff17b491)
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(12)),
            padding: EdgeInsets.symmetric(horizontal: 13, vertical: 1),
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline5?.copyWith(
                  color: _currentSelectedIndex == index
                      ? Colors.white
                      : Color(0xff161640)),
            )),
      );

  @override
  bool get wantKeepAlive => true;
}
