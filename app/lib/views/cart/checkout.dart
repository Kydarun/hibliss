import 'dart:convert';

import 'package:app/main.dart';
import 'package:app/model/cart.dart';
import 'package:app/rest/payment.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/cache.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:timelines/timelines.dart';
import 'package:provider/provider.dart';

class CheckoutPage extends StatelessWidget {
  const CheckoutPage({Key? key}) : super(key: key);

  static const platform = const MethodChannel('com.hibliss/payment');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Checkout'),
      ),
      body: LoadingIndicator(
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              margin: EdgeInsets.only(
                                  left: 10, right: 10, top: 15, bottom: 10),
                              child: Text('Order Summary',
                                  style:
                                      Theme.of(context).textTheme.headline5)),
                          Divider(
                            height: 1,
                            color: Color(0xffcecece),
                          ),
                          ...context
                                  .read<Cart>()
                                  .checkout
                                  ?.orderItem
                                  ?.map((item) => Container(
                                        margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                            right: 10,
                                            bottom: 15),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Text('${item.itemCount}x',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline5),
                                                Expanded(
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 10),
                                                    child: Text(
                                                        '${item.itemName}',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline4),
                                                  ),
                                                ),
                                                Text(
                                                    '${context.read<Cart>().carts.where((element) => element.orderId == item.orderId).first.subTotal}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline4),
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 5),
                                              padding: EdgeInsets.all(15),
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              child: FixedTimeline.tileBuilder(
                                                theme: TimelineThemeData(
                                                    nodePosition: 0,
                                                    indicatorPosition: 0),
                                                builder: TimelineTileBuilder
                                                    .connected(
                                                        itemCount:
                                                            item.itemType ==
                                                                    'Product'
                                                                ? 1
                                                                : 2,
                                                        indicatorBuilder:
                                                            (context, index) {
                                                          if (item.itemType ==
                                                              'Product') {
                                                            switch (index) {
                                                              case 0:
                                                                return Image
                                                                    .asset(
                                                                  'res/icons/ic_location.png',
                                                                  width: 17,
                                                                );
                                                              default:
                                                                return Container();
                                                            }
                                                          } else {
                                                            switch (index) {
                                                              case 0:
                                                                return Image.asset(
                                                                    'res/icons/ic_user.png',
                                                                    width: 17);
                                                              case 1:
                                                                return Image
                                                                    .asset(
                                                                  'res/icons/ic_location.png',
                                                                  width: 17,
                                                                );
                                                              default:
                                                                return Container();
                                                            }
                                                          }
                                                        },
                                                        connectorBuilder: (context,
                                                                index,
                                                                connectorType) =>
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          8),
                                                              child:
                                                                  DashedLineConnector(
                                                                color: Color(
                                                                    0xffdfdfe3),
                                                              ),
                                                            ),
                                                        contentsBuilder:
                                                            (context, index) {
                                                          if (item.itemType ==
                                                              'Product') {
                                                            switch (index) {
                                                              case 0:
                                                                return Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10),
                                                                  child: Text(
                                                                      '${item.customerAddress ?? 'None'}',
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .headline5
                                                                          ?.copyWith(
                                                                              fontWeight: FontWeight.normal)),
                                                                );
                                                              default:
                                                                return Container();
                                                            }
                                                          } else {
                                                            switch (index) {
                                                              case 0:
                                                                return Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10,
                                                                          bottom:
                                                                              20),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                          'Therapist: ${item.therapistName ?? 'None'}',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .headline5),
                                                                      Text(
                                                                          '${item.therapistAddress ?? 'None'}',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .headline5
                                                                              ?.copyWith(fontWeight: FontWeight.normal))
                                                                    ],
                                                                  ),
                                                                );
                                                              case 1:
                                                                return Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10),
                                                                  child: Text(
                                                                      '${item.customerAddress ?? 'None'}',
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .headline5
                                                                          ?.copyWith(
                                                                              fontWeight: FontWeight.normal)),
                                                                );
                                                              default:
                                                                return Container();
                                                            }
                                                          }
                                                        }),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                  .toList() ??
                              [],
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Subtotal:',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(fontSize: 17)),
                                Text(
                                    'RM${context.read<Cart>().checkout?.subtotal?.toStringAsFixed(2) ?? '0.00'}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(fontSize: 17))
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Tax:',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(fontSize: 17)),
                                  Text(
                                      'RM${context.read<Cart>().checkout?.tax?.toStringAsFixed(2) ?? '0.00'}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(fontSize: 17))
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Travel Fee:',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(fontSize: 17)),
                                  Text(
                                      'RM${context.read<Cart>().checkout?.travelFee?.toStringAsFixed(2) ?? '0.00'}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(fontSize: 17))
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Divider(
                                height: 1,
                                color: Color(0xffededf0),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Total:',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5),
                                  Text(
                                      'RM${context.read<Cart>().checkout?.total?.toStringAsFixed(2) ?? '0.00'}',
                                      style:
                                          Theme.of(context).textTheme.headline5)
                                ],
                              ),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Container(
                margin:
                    EdgeInsets.only(bottom: 40, top: 10, left: 20, right: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                        child: ElevatedButton(
                            onPressed: () {
                              _pay(context);
                            },
                            child: Text('Pay Now'))),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _pay(BuildContext context) async {
    try {
      context.read<LoadingState>().toggle(value: true);
      // TODO: Revert amount to original instead of 1.00.
      final _refNoRes = await PaymentRest(context: context).getRefNo(
          orderIds: (context.read<Cart>().checkout?.orderItem ?? [])
              .map((item) => '${item.orderId}')
              .reduce((value, element) => '$value,$element'),
          amount: context.read<Cart>().checkout?.total ?? 1.00);
      final _settings =
          json.decode(await rootBundle.loadString("res/strings/settings.json"));
      final _me = await UserRest(context: context).me();
      final _result = await platform.invokeMethod('pay', {
        'ipay_merchant_key': _settings['ipay_merchant_key'],
        'ipay_merchant_code': _settings['ipay_merchant_code'],
        'ref_no': _refNoRes.dataResults,
        'amount': NumberFormat('#,###.00')
            .format(context.read<Cart>().checkout?.total ?? 1.00),
        "product_description": "Hi-Bliss Order",
        "user_name": _me.dataResults![0].name,
        "user_email": _me.dataResults![0].email,
        "user_contact": _me.dataResults![0].phoneNumber,
        "signature": sha256
            .convert(utf8.encode(
                '${_settings['ipay_merchant_key']}${_settings['ipay_merchant_code']}${_refNoRes.dataResults!}100MYR'))
            .toString()
      });
      if (_result is String) {
        final _requery = await PaymentRest(context: context).verifyPayment(
            refNo: _refNoRes.dataResults!, transactionId: _result);
        if (_requery.dataResults ?? false) {
          CommonUI(context: context).snackbar(message: 'Payment successful.');
          await context.read<Cart>().load();
          Navigator.of(context).pop(2);
        } else {
          CommonUI(context: context).snackbar(
              message:
                  'There was a problem with payment. Please contact Hi-Bliss Customer Support.');
        }
      } else {
        CommonUI(context: context).snackbar(
            message:
                'There was a problem with payment. Please contact Hi-Bliss Customer Support.');
      }
    } on PlatformException catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      CommonUI(context: context).snackbar(
          message: e.message ?? 'An error occurred. Please try again.');
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      CommonUI(context: context).snackbar(
          message:
              'There was a problem with payment. Please contact Hi-Bliss Customer Support.');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}
