import 'package:app/main.dart';
import 'package:app/model/cart.dart';
import 'package:app/model/response.dart';
import 'package:app/model/store.dart';
import 'package:app/model/therapist.dart';
import 'package:app/rest/address.dart';
import 'package:app/rest/cart.dart';
import 'package:app/rest/store.dart';
import 'package:app/view_model/cart.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/home.dart';
import 'package:app/views/profile/address/list.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:app/views/shared/listview.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shop/reviews.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

class ItemInfo extends StatefulWidget {
  final StoreAlacarte? alacarte;
  final StorePackage? package;
  final StoreProduct? product;

  ItemInfo({this.alacarte, this.package, this.product});

  @override
  State<ItemInfo> createState() => _ItemInfoState();
}

class _ItemInfoState extends State<ItemInfo> {
  bool _isAddingToCart = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          var _viewModel = ItemInfoViewModel();
          if (widget.alacarte != null) {
            _viewModel = ItemInfoViewModel(
                image: (widget.alacarte?.imageLink?.length ?? 0) > 0
                    ? widget.alacarte?.imageLink?.first.link
                    : null,
                title: widget.alacarte?.title,
                description: widget.alacarte?.description,
                duration: widget.alacarte?.duration,
                price: widget.alacarte?.price,
                activeFlag: widget.alacarte?.activeFlag,
                rating: widget.alacarte?.rating,
                totalReviews: widget.alacarte?.totalReviews,
                reviews: widget.alacarte?.reviews);
          } else if (widget.package != null) {
            _viewModel = ItemInfoViewModel(
                image: (widget.package?.imageLink?.length ?? 0) > 0
                    ? widget.package?.imageLink?.first.link
                    : null,
                title: widget.package?.title,
                description: widget.package?.description,
                duration: widget.package?.duration,
                price: widget.package?.price,
                activeFlag: widget.package?.activeFlag,
                rating: widget.package?.rating,
                totalReviews: widget.package?.totalReviews,
                reviews: widget.package?.reviews);
          } else if (widget.product != null) {
            _viewModel = ItemInfoViewModel(
                image: (widget.product?.imageLink?.length ?? 0) > 0
                    ? widget.product?.imageLink?.first.link
                    : null,
                title: widget.product?.title,
                description: widget.product?.description,
                remarks: widget.product?.remarks,
                price: widget.product?.price,
                activeFlag: widget.product?.activeFlag,
                rating: widget.product?.rating,
                totalReviews: widget.product?.totalReviews,
                reviews: widget.product?.reviews);
          }
          return LoadingIndicator(
            child: Column(
              children: [
                Expanded(
                  child: CustomScrollView(
                    slivers: [
                      SliverAppBar(
                        pinned: true,
                        floating: false,
                        expandedHeight: 331,
                        leading: IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Image.asset('res/icons/ic_back_round.png',
                                width: 36)),
                        actions: [
                          Consumer<Cart>(
                            builder: (context, cart, child) => Stack(
                              children: [
                                IconButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(4);
                                    },
                                    icon: Image.asset(
                                        'res/icons/ic_cart_round.png',
                                        width: 36)),
                                if (cart.carts.length > 0)
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      width: 18,
                                      height: 18,
                                      decoration: BoxDecoration(
                                          color: Color(0xffff3434),
                                          borderRadius:
                                              BorderRadius.circular(9),
                                          border: Border.all(
                                              color: Colors.white, width: 1)),
                                      child: Center(
                                        child: Text('${cart.carts.length}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                ?.copyWith(
                                                    fontSize: 10,
                                                    color: Colors.white)),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                          )
                        ],
                        flexibleSpace: FlexibleSpaceBar(
                          //title: Text(_viewModel.title ?? 'Item'),
                          centerTitle: true,
                          background: _viewModel.image == null
                              ? Image.asset('res/images/home.png',
                                  fit: BoxFit.cover)
                              : Image.network(_viewModel.image!,
                                  fit: BoxFit.cover),
                        ),
                      ),
                      SliverToBoxAdapter(
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 23),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text(_viewModel.title ?? 'N/A',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SeeMoreReviews(
                                                        item: _viewModel)));
                                      },
                                      child: Image.asset(
                                          'res/icons/icn_review.png',
                                          width: 24),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text('Price: RM${_viewModel.price}',
                                    style:
                                        Theme.of(context).textTheme.headline5),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Text(
                                    _viewModel.description ??
                                        _viewModel.remarks ??
                                        '',
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    if (_viewModel.rating == null)
                                      Text('No ratings.',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1)
                                    else
                                      Text(
                                          '${_viewModel.rating?.toStringAsFixed(1)}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1),
                                    Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: RatingBar(
                                          minRating: 0,
                                          maxRating: 5,
                                          ignoreGestures: true,
                                          initialRating: _viewModel.rating ?? 0,
                                          allowHalfRating: true,
                                          itemSize: 16,
                                          ratingWidget: RatingWidget(
                                              full: Icon(Icons.star,
                                                  color: Color(0xffffc758)),
                                              half: Icon(
                                                  Icons.star_half_outlined,
                                                  color: Color(0xffffc758)),
                                              empty: Icon(Icons.star_border,
                                                  color: Color(0xffffc758))),
                                          onRatingUpdate: (rating) =>
                                              debugPrint('$rating')),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 30, bottom: 6),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Ratings & Reviews',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1),
                                    if ((_viewModel.reviews?.length ?? 0) > 0)
                                      InkWell(
                                        child: Text('See more',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                ?.copyWith(
                                                    color: Color(0xff0095cf))),
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SeeMoreReviews(
                                                          item: _viewModel)));
                                        },
                                      )
                                  ],
                                ),
                              ),
                              if ((_viewModel.reviews?.length ?? 0) > 0)
                                ..._viewModel.reviews
                                        ?.map((review) => Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5),
                                              margin: EdgeInsets.only(top: 10),
                                              child: IntrinsicHeight(
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    ClipOval(
                                                      child: CircleAvatar(
                                                        radius: 27,
                                                        child: Container(
                                                          width:
                                                              double.infinity,
                                                          height:
                                                              double.infinity,
                                                          color:
                                                              Color(0xffd1d1d1),
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 4),
                                                        decoration: BoxDecoration(
                                                            border: Border(
                                                                bottom: BorderSide(
                                                                    color: Color(
                                                                        0xffcecece),
                                                                    width: 1))),
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                    review.customerName ??
                                                                        'Annonymous',
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .headline5),
                                                                RatingBar(
                                                                    minRating:
                                                                        0,
                                                                    maxRating:
                                                                        5,
                                                                    ignoreGestures:
                                                                        true,
                                                                    initialRating:
                                                                        review.rating ??
                                                                            0,
                                                                    allowHalfRating:
                                                                        true,
                                                                    itemSize:
                                                                        10,
                                                                    ratingWidget: RatingWidget(
                                                                        full: Icon(Icons.star,
                                                                            color: Color(
                                                                                0xffffc758)),
                                                                        half: Icon(
                                                                            Icons
                                                                                .star_half_outlined,
                                                                            color: Color(
                                                                                0xffffc758)),
                                                                        empty: Icon(
                                                                            Icons
                                                                                .star_border,
                                                                            color: Color(
                                                                                0xffffc758))),
                                                                    onRatingUpdate:
                                                                        (rating) =>
                                                                            debugPrint('$rating'))
                                                              ],
                                                            ),
                                                            Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Text(
                                                                      review.review ??
                                                                          'N/A',
                                                                      maxLines:
                                                                          2,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .bodyText1),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ))
                                        .toList() ??
                                    []
                              else
                                Text('No reviews.',
                                    style:
                                        Theme.of(context).textTheme.bodyText1)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin:
                      EdgeInsets.only(top: 10, bottom: 40, left: 15, right: 15),
                  child: ElevatedButton(
                      child: Text('Add to cart'),
                      onPressed: () {
                        if (!context.read<Session>().isLoggedIn) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Login(onLogin: () {
                                    Navigator.of(context).pop();
                                  })));
                        } else if (_viewModel.activeFlag ?? false) {
                          _hasAddress(context).then((value) {
                            if (value) {
                              showModalBottomSheet(
                                  context: context,
                                  backgroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  isScrollControlled: true,
                                  builder: (ctx) => Container(
                                        height: 500,
                                        child: Column(
                                          children: [
                                            Container(
                                                margin: EdgeInsets.only(
                                                    top: 35, bottom: 10),
                                                child: Center(
                                                  child: Text(
                                                      'Select a therapist',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline5),
                                                )),
                                            Expanded(
                                              child: LoadMoreListView<
                                                      Therapist>(
                                                  create: (context, page) =>
                                                      StoreRest(
                                                              context: context)
                                                          .therapists(
                                                              page: page)
                                                          .catchError(
                                                              (e, stacktrace) {
                                                        debugPrint('$e');
                                                        debugPrint(
                                                            '$stacktrace');
                                                        showDialog(
                                                            context: context,
                                                            barrierDismissible:
                                                                false,
                                                            builder: (context) =>
                                                                HiBlissDialog(
                                                                  context:
                                                                      context,
                                                                  title:
                                                                      'No address',
                                                                  message:
                                                                      'Please add a primary address at My Profile -> My Address page.',
                                                                ));
                                                      }),
                                                  builder: (context, item) =>
                                                      _buildTherapistItem(
                                                          context: context,
                                                          therapist: item)),
                                            ),
                                          ],
                                        ),
                                      ));
                            } else {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (context) => HiBlissDialog(
                                        context: context,
                                        title: 'No address',
                                        message:
                                            'Please add a primary address at My Profile -> My Address page.',
                                      ));
                            }
                          });

                          /* showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              isScrollControlled: true,
                              builder: (cts) => Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20)),
                                    height: 558,
                                    child: FutureProvider<Response<Therapist>>(
                                      create: (context) =>
                                          StoreRest(context: context)
                                              .therapists()
                                              .catchError((e, stacktrace) {
                                        debugPrint('$e');
                                        debugPrint('$stacktrace');
                                        showDialog(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (context) => HiBlissDialog(
                                                  context: context,
                                                  title: 'No address',
                                                  message:
                                                      'Please add a primary address at My Profile -> My Address page.',
                                                ));
                                      }),
                                      initialData: Response(httpCode: -1),
                                      child: Consumer<Response<Therapist>>(
                                        builder: (context, response, child) =>
                                            ListView(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 35, bottom: 10),
                                              child: Center(
                                                child: Text('Select a therapist',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline5),
                                              ),
                                            ),
                                            if (response.httpCode == -1)
                                              StatelessLoadingIndicator()
                                            else ...[
                                              if ((response.dataResults?.length ??
                                                      0) >
                                                  0)
                                                ...addAnyTherapist(
                                                        therapists:
                                                            response.dataResults)!
                                                    .map((therapist) =>
                                                        _buildTherapistItem(
                                                            context: context,
                                                            therapist: therapist))
                                                    .toList()
                                              else ...[
                                                Container(
                                                    margin: EdgeInsets.symmetric(
                                                        horizontal: 20,
                                                        vertical: 20),
                                                    child: Text(
                                                        'You must create an address first.',
                                                        textAlign:
                                                            TextAlign.center)),
                                                Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 10),
                                                  child: ElevatedButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder: (context) =>
                                                                    AddressList()));
                                                      },
                                                      child:
                                                          Text('Create Address')),
                                                )
                                              ]
                                            ]
                                          ],
                                        ),
                                      ),
                                    ),
                                  )) */
                          ;
                        }
                      }),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Future<bool> _hasAddress(BuildContext context) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await AddressRest(context: context).list();
    context.read<LoadingState>().toggle(value: false);
    return (_res.dataResults?.length ?? 0) > 0;
  }

  List<Therapist>? addAnyTherapist(
      {List<Therapist>? therapists, int page = 1}) {
    List<Therapist>? retTherapists = therapists;
    if (therapists != null) {
      Therapist anyTherapist = therapists.first;
      therapists.forEach((therapist) {
        double? price = double.tryParse(therapist.travellingFee ?? "");
        double? anyTherapistPrice =
            double.tryParse(anyTherapist.travellingFee ?? "");
        if (price != null && anyTherapistPrice != null) {
          if (price < anyTherapistPrice) {
            anyTherapist = therapist;
          }
        }
      });
      retTherapists?.insert(
          0,
          Therapist(
              fullName: "Any Therapist",
              id: anyTherapist.id,
              addressId: anyTherapist.addressId));
    }
    return retTherapists;
  }

  Widget _buildTherapistItem(
          {required BuildContext context, Therapist? therapist}) =>
      InkWell(
        onTap: () async {
          if (context.read<Session>().isLoggedIn) {
            if (!_isAddingToCart) {
              setState(() {
                _isAddingToCart = true;
              });
              final _addressRes = await AddressRest(context: context).list();
              final _addressId = _addressRes.dataResults!
                  .firstWhere((element) => element.isPrimary == "Yes",
                      orElse: () => _addressRes.dataResults!.first)
                  .addressId!;

              if (widget.product != null) {
                await CartRest(context: context).addGoodsToCart(
                    productId: widget.product!.id!,
                    customerAddressId: _addressId);
              } else if (widget.alacarte != null) {
                await CartRest(context: context).addServiceToCart(
                    productId: widget.alacarte!.id!,
                    customerAddressId: _addressId,
                    therapistId: therapist!.id!,
                    therapistAddressId: therapist.addressId!);
              } else if (widget.package != null) {
                await CartRest(context: context).addServiceToCart(
                    productId: widget.package!.id!,
                    customerAddressId: _addressId,
                    therapistId: therapist!.id!,
                    therapistAddressId: therapist.addressId!);
              }
              await context.read<Cart>().load();
              setState(() {
                _isAddingToCart = false;
              });
              Navigator.of(context).pop();
            }
          } else {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => Login()));
          }
        },
        child: Container(
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 5),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xffe4ebf2),
              border: Border.all(color: Color(0xffd2e2f7))),
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xffe4e4e4),
                ),
                child: Center(
                  child: ClipOval(
                      child: Image.asset('res/images/home.png', width: 75)),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(therapist!.fullName!,
                          style: Theme.of(context).textTheme.headline5),
                      if (therapist.travellingDistance != null)
                        Container(
                          margin: EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              Image.asset('res/icons/ic_pin_drop.png',
                                  width: 8.5),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(therapist.travellingDistance!,
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Image.asset(
                                    'res/icons/ic_directions_car.png',
                                    width: 8.5),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(
                                    'RM${therapist.travellingFee ?? "0.00"}',
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                              ),
                            ],
                          ),
                        ),
                      if (therapist.contactNo != null)
                        Container(
                          margin: EdgeInsets.only(top: 3),
                          child: Row(
                            children: [
                              Image.asset('res/icons/ic_call_black.png',
                                  width: 8.5),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(therapist.contactNo!,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(color: Color(0xff0095cf))),
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
}

class ItemInfoViewModel {
  String? image;
  String? title;
  String? description;
  String? price;
  String? remarks;
  int? duration;
  bool? activeFlag;
  double? rating;
  int? totalReviews;
  List<Review>? reviews;

  ItemInfoViewModel(
      {this.image,
      this.title,
      this.description,
      this.price,
      this.remarks,
      this.duration,
      this.rating,
      this.totalReviews,
      this.reviews,
      this.activeFlag});
}
