import 'package:app/model/review.dart';
import 'package:app/views/shop/info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class SeeMoreReviews extends StatelessWidget {
  final ItemInfoViewModel item;

  const SeeMoreReviews({required this.item, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reviews'),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: EdgeInsets.only(top: 20, left: 15, right: 15),
                child: Text('Ratings & Reviews',
                    style: Theme.of(context).textTheme.headline5)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: [
                  Row(
                    children: [
                      if (item.rating == null)
                        Text('No ratings.',
                            style: Theme.of(context).textTheme.bodyText1)
                      else
                        Text('${item.rating?.toStringAsFixed(1)}',
                            style: Theme.of(context).textTheme.bodyText1),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: RatingBar(
                            minRating: 0,
                            maxRating: 5,
                            ignoreGestures: true,
                            initialRating: item.rating ?? 0,
                            allowHalfRating: true,
                            itemSize: 16,
                            ratingWidget: RatingWidget(
                                full:
                                    Icon(Icons.star, color: Color(0xffffc758)),
                                half: Icon(Icons.star_half_outlined,
                                    color: Color(0xffffc758)),
                                empty: Icon(Icons.star_border,
                                    color: Color(0xffffc758))),
                            onRatingUpdate: (rating) => debugPrint('$rating')),
                      )
                    ],
                  ),
                ],
              ),
            ),
            if (item.rating != null)
              Container(
                margin: EdgeInsets.only(top: 7, left: 15, right: 15),
                child: Text('Based on ${item.totalReviews} reviews',
                    style: TextStyle(
                        fontFamily: 'Soleil',
                        fontSize: 12,
                        color: Color(0xff818181))),
              ),
            Container(
              margin: EdgeInsets.only(top: 9, bottom: 10),
              child: Divider(
                height: 1,
                color: Color(0xffcecece),
              ),
            ),
            if ((item.reviews?.length ?? 0) > 0)
              Expanded(
                  child: ListView(
                children: item.reviews
                        ?.map((review) => Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white),
                              margin: EdgeInsets.only(bottom: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ClipOval(
                                    child: CircleAvatar(
                                      radius: 27,
                                      child: Container(
                                        width: double.infinity,
                                        height: double.infinity,
                                        color: Color(0xffd1d1d1),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                  review.customerName ??
                                                      'Annonymous',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5),
                                              RatingBar(
                                                  minRating: 0,
                                                  maxRating: 5,
                                                  ignoreGestures: true,
                                                  initialRating:
                                                      review.rating ?? 0,
                                                  allowHalfRating: true,
                                                  itemSize: 10,
                                                  ratingWidget: RatingWidget(
                                                      full: Icon(Icons.star,
                                                          color: Color(
                                                              0xffffc758)),
                                                      half: Icon(
                                                          Icons
                                                              .star_half_outlined,
                                                          color: Color(
                                                              0xffffc758)),
                                                      empty: Icon(
                                                          Icons.star_border,
                                                          color: Color(
                                                              0xffffc758))),
                                                  onRatingUpdate: (rating) =>
                                                      debugPrint('$rating'))
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: Text(
                                                    review.review ?? 'N/A',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ))
                        .toList() ??
                    [],
              ))
            else
              Text('No reviews.', style: Theme.of(context).textTheme.bodyText1)
          ],
        ),
      ),
    );
  }
}
