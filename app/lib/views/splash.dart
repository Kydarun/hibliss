import 'dart:async';

import 'package:app/main.dart';
import 'package:app/util/cache.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/home.dart';
import 'package:app/views/home_therapist.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 1), () async {
      _permission();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
            child: Image.asset(
                'res/logos/${EnvironmentConfig.isTherapist ? "therapist_foreground.png" : "home_foreground.png"}')),
      ),
    );
  }

  _permission() async {
    await Firebase.initializeApp();
    var _notificationStatus = await Permission.notification.status;
    if (_notificationStatus.isGranted) {
      _navigateToHome();
    } else {
      Permission.notification.request().then((status) {
        if (status.isDenied) {
          showDialog(
              context: context,
              builder: (ctx) => HiBlissDialog(
                    context: context,
                    title: 'Notification Permission required',
                    message: 'Hi-Bliss needs to send you notifications for upcoming jobs.',
                  )).then((_) {
            Permission.notification.request().then((_) {
              _navigateToHome();
            });
          });
        } else {
          _navigateToHome();
        }
      });
    }
  }

  _navigateToHome() async {
    await FirebaseMessaging.instance.requestPermission();
    if (EnvironmentConfig.isTherapist) {
      bool _isLoggedIn = await Cache(context: context).isLoggedIn();
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              settings: RouteSettings(name: '/home'),
              builder: (context) => _isLoggedIn ? TherapistHome() : Login()),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(settings: RouteSettings(name: '/home'), builder: (context) => Home()),
          (route) => false);
    }
  }
}
