import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/views/jobs/details.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timelines/timelines.dart';

class NewJobReminderWidget extends StatelessWidget {
  final int notificationId;

  const NewJobReminderWidget({required this.notificationId, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Job Order'),
      ),
      body: FutureProvider<Response<AppointmentReminder>>(
        initialData: Response(httpCode: -1),
        create: (context) => NotificationRest(context: context)
            .getAppointmentReminder(notificationId: notificationId),
        child: Consumer<Response<AppointmentReminder>>(
          builder: (context, response, child) => response.httpCode == -1
              ? StatelessLoadingIndicator()
              : ChangeNotifierProvider<AppointmentReminderChangeNotifier>(
                  create: (context) =>
                      AppointmentReminderChangeNotifier(response: response),
                  child: Consumer<AppointmentReminderChangeNotifier>(
                    builder: (context, notifier, child) => Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Expanded(
                            child: CustomScrollView(
                              slivers: [
                                SliverToBoxAdapter(
                                  child: Container(
                                    margin:
                                        EdgeInsets.only(top: 20, bottom: 20),
                                    child: Text('You have a new Job Order!',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5),
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Color(0xfff1f1f3)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(5),
                                                    topRight:
                                                        Radius.circular(5)),
                                                color: Theme.of(context)
                                                    .primaryColor),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('Order ID',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            fontFamily:
                                                                'Soleil',
                                                            color:
                                                                Colors.white)),
                                                Text(
                                                    '${notifier.response?.dataResults?[0].displayId ?? 'N/A'}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            fontFamily:
                                                                'Soleil',
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color:
                                                                Colors.white))
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 10, top: 20, bottom: 20),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                    notifier
                                                            .response
                                                            ?.dataResults?[0]
                                                            .itemName ??
                                                        'No title',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline5),
                                                Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                            'res/icons/ic_clock.png',
                                                            width: 13),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                              '${notifier.response?.dataResults?[0].duration} minutes',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1),
                                                        ),
                                                      ],
                                                    )),
                                                Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                            'res/icons/ic_calendar_gray.png',
                                                            width: 13),
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 5),
                                                            child: Text(
                                                                '${notifier.response?.dataResults?[0].appointmentTime}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1))
                                                      ],
                                                    )),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 10),
                                                  child:
                                                      FixedTimeline.tileBuilder(
                                                    theme: TimelineThemeData(
                                                        nodePosition: 0,
                                                        indicatorPosition: 0),
                                                    builder: TimelineTileBuilder
                                                        .connected(
                                                            itemCount: 1,
                                                            indicatorBuilder:
                                                                (context,
                                                                    index) {
                                                              switch (index) {
                                                                case 0:
                                                                  return Image
                                                                      .asset(
                                                                    'res/icons/ic_location.png',
                                                                    width: 17,
                                                                  );
                                                                default:
                                                                  return Container();
                                                              }
                                                            },
                                                            connectorBuilder:
                                                                (context, index,
                                                                        connectorType) =>
                                                                    Padding(
                                                                      padding: EdgeInsets.symmetric(
                                                                          vertical:
                                                                              8),
                                                                      child:
                                                                          DashedLineConnector(
                                                                        color: Color(
                                                                            0xffdfdfe3),
                                                                      ),
                                                                    ),
                                                            contentsBuilder:
                                                                (context,
                                                                    index) {
                                                              switch (index) {
                                                                case 0:
                                                                  return Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                10),
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                            'Customer: ${notifier.response?.dataResults?[0].customerName}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5),
                                                                        Text(
                                                                            '${notifier.response?.dataResults?[0].customerAddress}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.normal))
                                                                      ],
                                                                    ),
                                                                  );
                                                                default:
                                                                  return Container();
                                                              }
                                                            }),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      )),
                                ),
                                SliverToBoxAdapter(
                                  child: Container(
                                      margin: EdgeInsets.only(top: 5),
                                      padding:
                                          EdgeInsets.only(top: 15, bottom: 20),
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'For more details on this appointment, please view it at Jobs -> Upcoming.\n\nThank you,\nHi-Bliss Therapy.'),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(top: 20),
                                                child: Text(
                                                    '${notifier.response?.dataResults?[0].sentDate}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            color:
                                                                Colors.grey)))
                                          ])),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 40, top: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                      onPressed: (notifier
                                                      .response!
                                                      .dataResults![0]
                                                      .jobEnable ??
                                                  0) ==
                                              1
                                          ? () {
                                              Navigator.of(context)
                                                  .push(MaterialPageRoute(
                                                      builder: (context) =>
                                                          JobDetail(
                                                              bookingId: notifier
                                                                  .response!
                                                                  .dataResults![
                                                                      0]
                                                                  .jobId!,
                                                              jobCategory:
                                                                  "upcoming")))
                                                  .then((_) {
                                                _reload(context: context);
                                              });
                                            }
                                          : null,
                                      child: Text('Go To Job')),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Future _reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await NotificationRest(context: context)
        .getAppointmentReminder(notificationId: notificationId);
    context.read<AppointmentReminderChangeNotifier>().update(response: _res);
    context.read<LoadingState>().toggle(value: false);
  }
}

class AppointmentReminderChangeNotifier extends ChangeNotifier {
  Response<AppointmentReminder>? response;

  AppointmentReminderChangeNotifier({this.response});

  update({required Response<AppointmentReminder> response}) {
    this.response = response;
    notifyListeners();
  }
}
