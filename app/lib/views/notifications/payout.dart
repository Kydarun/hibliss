import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/views/notifications/pdf.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PayoutReminderWidget extends StatelessWidget {
  final int notificationId;

  const PayoutReminderWidget({required this.notificationId, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Payment Receipt'),
        ),
        body: FutureProvider<Response<PayoutNotification>>(
            initialData: Response(httpCode: -1),
            create: (context) => NotificationRest(context: context)
                .getPayoutNotification(notificationId: notificationId),
            child: Consumer<Response<PayoutNotification>>(
              builder: (context, response, child) => response.httpCode == -1
                  ? StatelessLoadingIndicator()
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: CustomScrollView(
                              slivers: [
                                SliverToBoxAdapter(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Text(
                                            '${response.dataResults?[0].displayId}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: RichText(
                                            text: TextSpan(
                                                text: 'Uploaded by ',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    ?.copyWith(
                                                        color: Colors.grey),
                                                children: [
                                              TextSpan(
                                                  text:
                                                      '${response.dataResults?[0].createBy}',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      ?.copyWith(
                                                          color: Color(
                                                              0xff0095cf)))
                                            ])),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 30),
                                        child: Text('''
Dear ${response.dataResults?[0].therapistName},

Please find the attached transaction receipt for your commission payout.

Thank you, 
Hi-Bliss Therapy.
                                    '''),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 20),
                                          child: Text(
                                              '${response.dataResults?[0].sentDate}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  ?.copyWith(
                                                      color: Colors.grey)))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 40, top: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                      onPressed: () {
                                        if (response.dataResults?[0].receiptLink
                                                ?.isNotEmpty ??
                                            false) {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ViewPayoutPdf(
                                                          payout: response
                                                                  .dataResults![
                                                              0])));
                                        } else {
                                          CommonUI(context: context).snackbar(
                                              message:
                                                  'PDF receipt not available at this time. Please contact Hi-Bliss Customer Support.');
                                        }
                                      },
                                      child: Text('View PDF')),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
            )));
  }
}
