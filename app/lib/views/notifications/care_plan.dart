import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CarePlanReminderWidget extends StatelessWidget {
  final int notificationId;

  const CarePlanReminderWidget({required this.notificationId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Care Plan'),
        ),
        body: FutureProvider<Response<ReviewCarePlanNotification>>(
            initialData: Response(httpCode: -1),
            create: (context) => NotificationRest(context: context)
                .getReviewCarePlanNotificationr(notificationId: notificationId),
            child: Consumer<Response<ReviewCarePlanNotification>>(
              builder: (context, response, child) => response.httpCode == -1
                  ? StatelessLoadingIndicator()
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: CustomScrollView(
                        slivers: [
                          SliverToBoxAdapter(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text('${response.dataResults?[0].displayId}',
                                      style: Theme.of(context).textTheme.headline5),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: RichText(
                                      text: TextSpan(
                                          text: 'Uploaded by ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(color: Colors.grey),
                                          children: [
                                        TextSpan(
                                            text: '${response.dataResults?[0].createBy}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                ?.copyWith(color: Color(0xff0095cf)))
                                      ])),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 30),
                                  child: Text('''
Dear ${response.dataResults?[0].therapistName},

Please find the attached care plan report for you to review and give recommendations. 

You can only create the report on the web portal. 

Please follow the steps below:
1. Go to Web Portal 
2. Click Care Plan Tab on the Menu Bar
3. Click 'New' on the table
4. Click the icon 'View Report' 
5. Click the button 'Create Report' 

Thank you, 
Hi-Bliss Therapy.
                              '''),
                                ),
                                Container(
                                    margin: EdgeInsets.only(top: 20),
                                    child: Text('${response.dataResults?[0].sentDate}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            ?.copyWith(color: Colors.grey)))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
            )));
  }
}
