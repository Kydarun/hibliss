import 'package:app/model/response.dart';
import 'package:app/rest/auth.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/auth/forgot.dart';
import 'package:app/views/pdpa_customer.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/terms.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

import '../home.dart';
import '../home_therapist.dart';

class Signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LoadingIndicator(
          child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: BlocProvider(
            create: (context) => SignUpFormBloc(context: context),
            child: Builder(
              builder: (context) {
                final _signupFormBloc = context.read<SignUpFormBloc>();
                return FormBlocListener<SignUpFormBloc, Response2<String>,
                    Response2<String>>(
                  onSuccess: (context, success) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            'Thank you for signing up. Please login using the credentials you set earlier.'),
                        duration: Duration(seconds: 1, milliseconds: 500)));
                    Navigator.of(context).pop();
                  },
                  onFailure: (context, failure) {
                    CommonUI(context: context).fail2(failure: failure);
                  },
                  child: Center(
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(top: 30, right: 10),
                                  padding: EdgeInsets.all(16),
                                  child: GestureDetector(
                                    child: Image.asset('res/icons/ic_close.png',
                                        width: 20),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Center(
                                  child: Image.asset(
                                      EnvironmentConfig.isTherapist
                                          ? 'res/icons/therapist.png'
                                          : 'res/images/home_banner.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.6),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Text('Create your Account',
                                    style:
                                        Theme.of(context).textTheme.headline1),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 16),
                                child: TextFieldBlocBuilder(
                                  textFieldBloc: _signupFormBloc.name,
                                  maxLength: 200,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  decoration: CommonUI(context: context)
                                      .decoration(hint: 'Full Name'),
                                  textInputAction: TextInputAction.next,
                                ),
                              ),
                              TextFieldBlocBuilder(
                                textFieldBloc: _signupFormBloc.email,
                                maxLength: 200,
                                style: Theme.of(context).textTheme.bodyText2,
                                decoration: CommonUI(context: context)
                                    .decoration(hint: 'Email'),
                                errorBuilder: (context, error) =>
                                    "The e-mail address is incorrect.",
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                              ),
                              TextFieldBlocBuilder(
                                  textFieldBloc: _signupFormBloc.password,
                                  maxLength: 200,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  suffixButton: SuffixButton.obscureText,
                                  autofillHints: [AutofillHints.password],
                                  decoration: CommonUI(context: context)
                                      .decoration(hint: 'Password')),
                              TextFieldBlocBuilder(
                                textFieldBloc: _signupFormBloc.confirm,
                                maxLength: 200,
                                style: Theme.of(context).textTheme.bodyText2,
                                suffixButton: SuffixButton.obscureText,
                                autofillHints: [AutofillHints.password],
                                decoration: CommonUI(context: context)
                                    .decoration(hint: 'Confirm Password'),
                              ),
                              Text(
                                  'Must contain at least 8 characters incl. numbers & symbols.',
                                  style: Theme.of(context).textTheme.caption),
                              Container(
                                margin: EdgeInsets.only(top: 30),
                                width: double.infinity,
                                child: ElevatedButton(
                                    child: Text('Sign Up'),
                                    onPressed: _signupFormBloc.submit),
                              ),
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(top: 10),
                                child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              settings:
                                                  RouteSettings(name: '/home'),
                                              builder: (context) =>
                                                  EnvironmentConfig.isTherapist
                                                      ? TherapistHome()
                                                      : Home()),
                                          (route) => false);
                                    },
                                    child: Text('Continue as Guest')),
                              ),
                            ],
                          ),
                        ),
                        SliverFillRemaining(
                          hasScrollBody: false,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 84),
                                  child: RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                        style:
                                            Theme.of(context).textTheme.caption,
                                        children: [
                                          TextSpan(
                                              text:
                                                  "By creating an account, you agree to the\n"),
                                          TextSpan(
                                              text: 'Terms & Conditions',
                                              style: DefaultTextStyle.of(
                                                      context)
                                                  .style
                                                  .copyWith(
                                                      color: Color(0xff3b84c5)),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              TermsOfUse()));
                                                }),
                                          TextSpan(text: '\n and \n'),
                                          TextSpan(
                                              text:
                                                  'Personal Data Protection Act (PDPA)',
                                              style: DefaultTextStyle.of(
                                                      context)
                                                  .style
                                                  .copyWith(
                                                      color: Color(0xff3b84c5)),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              PdpaCustomer()));
                                                }),
                                          TextSpan(text: '.'),
                                        ]),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            )),
      )),
    );
  }
}

class SignUpFormBloc extends FormBloc<Response2<String>, Response2<String>> {
  BuildContext context;

  final email = TextFieldBloc(
      name: 'Email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final password =
      TextFieldBloc(name: 'Password', validators: <String? Function(String?)>[
    FieldBlocValidators.required,
    (input) {
      if (input != null) {
        String pattern = r'^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~_?/\"' +
            '\'' r'])(?!.*?[ ]).{8,}$';
        RegExp regExp = new RegExp(pattern);
        if (!regExp.hasMatch(input)) {
          return 'Must contain at least 8 characters incl. numbers & symbols and no whitespace.';
        }
      }
      return null;
    }
  ]);

  final confirm = TextFieldBloc(name: 'Confirm Password');

  final name =
      TextFieldBloc(name: 'Name', validators: [FieldBlocValidators.required]);

  SignUpFormBloc({required this.context}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [name, email, password, confirm]);
    confirm
      ..addValidators([_confirmPassword(password)])
      ..subscribeToFieldBlocs([password]);
  }

  Validator<String> _confirmPassword(TextFieldBloc password) {
    return (String? confirmPassword) {
      if (confirmPassword == password.value) {
        return null;
      }
      return 'Password does not match.';
    };
  }

  @override
  void onSubmitting() async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await AuthRest(context: context).signup(
        email: email.value!, password: password.value!, name: name.value!);
    if (_res.success) {
      emitSuccess(successResponse: _res, canSubmitAgain: true);
    } else {
      emitFailure(failureResponse: _res);
    }
    context.read<LoadingState>().toggle(value: false);
  }
}
