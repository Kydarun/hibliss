import 'package:app/main.dart';
import 'package:app/model/auth.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/auth.dart';
import 'package:app/rest/cart.dart';
import 'package:app/util/cache.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/auth/first_time.dart';
import 'package:app/views/auth/forgot.dart';
import 'package:app/views/auth/signup.dart';
import 'package:app/views/home.dart';
import 'package:app/views/home_therapist.dart';
import 'package:app/views/shared/common.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:app/views/shared/loading.dart';

class Login extends StatelessWidget {
  final VoidCallback? onLogin;

  Login({this.onLogin});

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: BlocProvider(
            create: (context) => LoginFormBloc(context: context),
            child: Builder(builder: (context) {
              final _loginFormBloc = context.read<LoginFormBloc>();
              return FormBlocListener<LoginFormBloc, Response2<Auth>,
                  Response2<Auth>>(
                onSuccess: (context, success) {
                  if (success.successResponse?.dataResults?.resetPassword ??
                      false) {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => FirstTimePassword()));
                  } else if (onLogin != null) {
                    onLogin!();
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            settings: RouteSettings(name: '/home'),
                            builder: (context) => EnvironmentConfig.isTherapist
                                ? TherapistHome()
                                : Home()),
                        (route) => false);
                  }
                },
                onFailure: (context, failure) {
                  CommonUI(context: context).fail2(failure: failure);
                },
                child: Center(
                  child: CustomScrollView(
                    slivers: [
                      SliverToBoxAdapter(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (!EnvironmentConfig.isTherapist)
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(top: 30, right: 10),
                                  padding: EdgeInsets.all(16),
                                  child: GestureDetector(
                                    child: Image.asset('res/icons/ic_close.png',
                                        width: 20),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ),
                            Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Image.asset(
                                    EnvironmentConfig.isTherapist
                                        ? 'res/icons/therapist.png'
                                        : 'res/images/home_banner.png',
                                    width: MediaQuery.of(context).size.width *
                                        0.6),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 39),
                              alignment: Alignment.centerLeft,
                              child: Text('Login to your Account',
                                  style: Theme.of(context).textTheme.headline1),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 16),
                              child: TextFieldBlocBuilder(
                                style: Theme.of(context).textTheme.bodyText2,
                                textFieldBloc: _loginFormBloc.email,
                                decoration: CommonUI(context: context)
                                    .decoration(hint: 'Email'),
                                errorBuilder: (context, error) =>
                                    "The e-mail address is incorrect/not in our system.",
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                              ),
                            ),
                            TextFieldBlocBuilder(
                                padding: EdgeInsets.only(top: 10),
                                textFieldBloc: _loginFormBloc.password,
                                style: Theme.of(context).textTheme.bodyText2,
                                suffixButton: SuffixButton.obscureText,
                                autofillHints: [AutofillHints.password],
                                decoration: CommonUI(context: context)
                                    .decoration(hint: 'Password'),
                                textInputAction: TextInputAction.next),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ForgotPassword()));
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text("Forgot password?",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6)),
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(top: 31),
                              child: ElevatedButton(
                                  child: Text('Login'),
                                  onPressed: _loginFormBloc.submit),
                            ),
                            if (!EnvironmentConfig.isTherapist)
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(top: 10),
                                child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Continue as Guest')),
                              ),
                          ],
                        ),
                      ),
                      if (!EnvironmentConfig.isTherapist)
                        SliverFillRemaining(
                          hasScrollBody: false,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 84),
                                    child: RichText(
                                      text: TextSpan(
                                          style: DefaultTextStyle.of(context)
                                              .style,
                                          children: [
                                            TextSpan(
                                                text:
                                                    "Don't have an account? "),
                                            TextSpan(
                                                text: 'Sign up',
                                                style: DefaultTextStyle.of(
                                                        context)
                                                    .style
                                                    .copyWith(
                                                        color:
                                                            Color(0xff3b84c5)),
                                                recognizer:
                                                    TapGestureRecognizer()
                                                      ..onTap = () {
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        Signup()));
                                                      })
                                          ]),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                    ],
                  ),
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}

class LoginFormBloc extends FormBloc<Response2<Auth>, Response2<Auth>> {
  BuildContext context;

  final email = TextFieldBloc(
      name: 'Email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final password = TextFieldBloc(
      name: 'Password', validators: [FieldBlocValidators.required]);

  LoginFormBloc({required this.context}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [email, password]);
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      final _res = await AuthRest(context: context)
          .login(username: email.value!, password: password.value!);
      if (_res.success) {
        await Cache(context: context).login(auth: _res.dataResults!);
        if (!EnvironmentConfig.isTherapist) {
          await context.read<Cart>().load();
        }
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrintStack(stackTrace: stacktrace);
      emitFailure(
          failureResponse:
              Response2(httpCode: 1, message: 'Error occurred: $e'));
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}
