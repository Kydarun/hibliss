import 'package:app/model/response.dart';
import 'package:app/rest/auth.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/auth/signup.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  Response? _res;
  int _resetCount = 0;

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: BlocProvider(
                create: (context) => ForgotPasswordFormBloc(context: context),
                child: Builder(builder: (context) {
                  final _passwordBloc = context.read<ForgotPasswordFormBloc>();
                  return FormBlocListener<ForgotPasswordFormBloc, Response,
                      Response>(
                    onSuccess: (context, success) {
                      setState(() {
                        _res = success.successResponse;
                        _resetCount++;
                      });
                    },
                    onFailure: (context, failure) {
                      setState(() {
                        _res = failure.failureResponse;
                      });
                    },
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(top: 30, right: 10),
                                  padding: EdgeInsets.all(16),
                                  child: GestureDetector(
                                    child: Image.asset('res/icons/ic_close.png',
                                        width: 20),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 70),
                                child: Center(
                                  child: Image.asset(
                                      EnvironmentConfig.isTherapist
                                          ? 'res/icons/therapist.png'
                                          : 'res/images/home_banner.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.6),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Text('Reset Password',
                                    style:
                                        Theme.of(context).textTheme.headline1),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                    'Enter your email and we will send you a link to reset your password.',
                                    style: Theme.of(context).textTheme.caption),
                              ),
                              TextFieldBlocBuilder(
                                textFieldBloc: _passwordBloc.email,
                                style: Theme.of(context).textTheme.bodyText2,
                                errorBuilder: (context, error) =>
                                    "he e-mail address is incorrect/not in our system.",
                                decoration: CommonUI(context: context)
                                    .decoration(hint: 'Email'),
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                width: double.infinity,
                                child: ElevatedButton(
                                    child: Text(_resetCount == 0
                                        ? 'Send Link'
                                        : 'Resend Link'),
                                    onPressed: _passwordBloc.submit),
                              ),
                              if (_res != null)
                                Container(
                                    margin: EdgeInsets.only(top: 10),
                                    child: CommonUI(context: context).infoBox(
                                        isSuccess: _res?.success ?? false,
                                        message: _res?.message ?? ''))
                            ],
                          ),
                        ),
                        if (!EnvironmentConfig.isTherapist)
                          SliverFillRemaining(
                            hasScrollBody: false,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Expanded(
                                    child: Center(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 84),
                                    child: RichText(
                                        text: TextSpan(
                                            style: DefaultTextStyle.of(context)
                                                .style,
                                            children: [
                                          TextSpan(
                                              text: "Don't have an account? "),
                                          TextSpan(
                                              text: 'Sign up',
                                              style: DefaultTextStyle.of(
                                                      context)
                                                  .style
                                                  .copyWith(
                                                      color: Color(0xff3b84c5)),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Signup()));
                                                })
                                        ])),
                                  ),
                                ))
                              ],
                            ),
                          )
                      ],
                    ),
                  );
                }))),
      ),
    );
  }
}

class ForgotPasswordFormBloc extends FormBloc<Response, Response> {
  BuildContext context;

  final email = TextFieldBloc(
      name: 'Email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  ForgotPasswordFormBloc({required this.context}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [email]);
  }

  @override
  void onSubmitting() async {
    context.read<LoadingState>().toggle(value: true);
    final _res =
        await AuthRest(context: context).forgotPassword(email: email.value!);
    if (_res.success) {
      emitSuccess(successResponse: _res, canSubmitAgain: true);
    } else {
      emitFailure(failureResponse: _res);
    }
    context.read<LoadingState>().toggle(value: false);
  }
}
