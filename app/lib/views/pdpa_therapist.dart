import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdpaTherapist extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('PDPA'),
        ),
        body: Container(
            color: Colors.black54,
            padding: EdgeInsets.all(5),
            child: SfPdfViewer.network(
                "https://hi-bliss.com/wp-content/uploads/2020/12/Personal-Data-Protection-Notice-v1.pdf")));
  }
}
