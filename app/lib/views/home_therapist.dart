import 'dart:convert';

import 'package:app/main.dart';
import 'package:app/util/cache.dart';
import 'package:app/views/account/bank_account.dart';
import 'package:app/views/account/payout_settings.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/contact.dart';
import 'package:app/views/home.dart';
import 'package:app/views/jobs/history.dart';
import 'package:app/views/notifications/appointment.dart';
import 'package:app/views/notifications/care_plan.dart';
import 'package:app/views/notifications/new_job.dart';
import 'package:app/views/notifications/payout.dart';
import 'package:app/views/pdpa_therapist.dart';
import 'package:app/views/profile/address/list.dart';
import 'package:app/views/profile/password.dart';
import 'package:app/views/profile/profile.dart';
import 'package:app/views/settings.dart';
import 'package:app/views/terms.dart';
import 'package:app/views/therapist_chat/therapist_chat.dart';
import 'package:app/views/therapist_home/account.dart';
import 'package:app/views/therapist_home/home.dart';
import 'package:app/views/therapist_home/jobs.dart';
import 'package:app/views/therapist_home/message.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';

class TherapistHome extends StatefulWidget {
  @override
  _TherapistHomeState createState() => _TherapistHomeState();
}

class _TherapistHomeState extends State<TherapistHome> {
  final _drawerMenuExpanded = [false, false, false];
  final GlobalKey<AccountsTabState> _accountsKey = GlobalKey();

  late var _pages;
  late var _title;

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null && initialMessage.data['type'] != null) {
      int? _type = int.tryParse(initialMessage.data['type']);
      int? _id = int.tryParse(initialMessage.data['notifId']);
      _navigate(_id, _type);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      int? _type = int.tryParse(message.data['type']);
      int? _id = int.tryParse(message.data['notifId']);
      _navigate(_id, _type);
    });

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
            alert: true, sound: true, badge: true);
    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings('ic_launcher_foreground'));
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
      if (payload != null) {
        final message = json.decode(payload);
        int? _type = int.tryParse(message['type']);
        int? _id = int.tryParse(message['notifId']);
        _navigate(_id, _type);
      }
    });

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: android.smallIcon,
            )),
            payload: json.encode(message.data));
      }
    });
  }

  void _navigate(int? id, int? type) {
    if (type != null && id != null) {
      switch (type) {
        case 1:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  AppointmentReminderWidget(notificationId: id)));
          break;
        case 2:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => NewJobReminderWidget(notificationId: id)));
          break;
        case 3:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PayoutReminderWidget(notificationId: id)));
          break;
        case 4:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  CarePlanReminderWidget(notificationId: id)));
          break;
        default:
          break;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _pages = [
      TherapistHomeTab(),
      JobsTab(),
      TherapistMessageTab(),
      AccountsTab(
        key: _accountsKey,
      )
    ];
    _title = ["Home", "Jobs", "Messages", "Account"];
    //setupInteractedMessage();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TabChangeNotifier>(
      create: (context) => TabChangeNotifier(),
      child: Consumer<TabChangeNotifier>(
        builder: (context, tab, child) => Scaffold(
            drawer: Drawer(
              child: Column(
                children: [
                  DrawerHeader(
                      child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Text('MENU',
                        style: TextStyle(
                            fontFamily: 'Soleil',
                            fontSize: 30,
                            fontWeight: FontWeight.bold)),
                  )),
                  ExpansionTile(
                    onExpansionChanged: (isExpanded) {
                      setState(() {
                        _drawerMenuExpanded[0] = isExpanded;
                      });
                    },
                    initiallyExpanded: _drawerMenuExpanded[0],
                    trailing: Container(
                      margin: EdgeInsets.only(right: 24),
                      child: Image.asset(
                          'res/icons/ic_menu_arrow${_drawerMenuExpanded[0] ? "up" : "down"}.png',
                          width: 20),
                    ),
                    title: Row(
                      children: [
                        Image.asset(
                          'res/icons/ic_menu_account${_drawerMenuExpanded[0] ? "2" : ""}.png',
                          width: 20,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text('Account',
                                style: Theme.of(context).textTheme.headline1))
                      ],
                    ),
                    children: [
                      ListTile(
                        title: Text('My Profile',
                            style: Theme.of(context).textTheme.headline5),
                        onTap: () {
                          Navigator.of(context).pop();
                          Cache(context: context).isLoggedIn().then((value) {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    value ? Profile() : Login()));
                          });
                        },
                      ),
                      ListTile(
                        title: Text('My Addresses',
                            style: Theme.of(context).textTheme.headline5),
                        onTap: () {
                          Navigator.of(context).pop();
                          Cache(context: context).isLoggedIn().then((value) {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    value ? AddressList() : Login()));
                          });
                        },
                      ),
                      ListTile(
                          title: Text('My Password',
                              style: Theme.of(context).textTheme.headline5),
                          onTap: () {
                            Navigator.of(context).pop();
                            Cache(context: context).isLoggedIn().then((value) {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      value ? Password() : Login()));
                            });
                          })
                    ],
                  ),
                  ExpansionTile(
                    onExpansionChanged: (isExpanded) {
                      setState(() {
                        _drawerMenuExpanded[1] = isExpanded;
                      });
                    },
                    initiallyExpanded: _drawerMenuExpanded[1],
                    trailing: Container(
                      margin: EdgeInsets.only(right: 24),
                      child: Image.asset(
                          'res/icons/ic_menu_arrow${_drawerMenuExpanded[1] ? "up" : "down"}.png',
                          width: 20),
                    ),
                    title: Row(
                      children: [
                        Image.asset(
                            'res/icons/ic_menu_settings${_drawerMenuExpanded[1] ? "2" : ""}.png',
                            width: 20),
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text('Settings',
                                style: Theme.of(context).textTheme.headline1)),
                      ],
                    ),
                    children: [
                      Consumer<Session>(
                        builder: (context, session, child) => ListTile(
                          title: Text('Notifications',
                              style: Theme.of(context).textTheme.headline5),
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    session.isLoggedIn ? Settings() : Login()));
                          },
                        ),
                      ),
                    ],
                  ),
                  ListTile(
                    title: Row(
                      children: [
                        Image.asset('res/icons/ic_menu_terms.png', width: 20),
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text('PDPA',
                                style: Theme.of(context).textTheme.headline1)),
                      ],
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PdpaTherapist()));
                    },
                  ),
                  ListTile(
                    title: Row(
                      children: [
                        Image.asset('res/icons/ic_menu_contact.png', width: 20),
                        Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text('Contact Us',
                                style: Theme.of(context).textTheme.headline1)),
                      ],
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => ContactUs()));
                    },
                  ),
                  Consumer<Session>(
                    builder: (context, loggedIn, child) => Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 16, bottom: 25),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            margin: EdgeInsets.only(right: 37),
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(color: Color(0xffcecece)))),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (loggedIn.isLoggedIn)
                                  InkWell(
                                    child: Container(
                                      margin: EdgeInsets.only(top: 24),
                                      child: Row(children: [
                                        Image.asset(
                                            'res/icons/ic_menu_signout.png',
                                            width: 20),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text('Sign Out',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline1),
                                        )
                                      ]),
                                    ),
                                    onTap: () {
                                      Cache(context: context)
                                          .logout()
                                          .then((value) {
                                        Navigator.of(context)
                                            .pushAndRemoveUntil(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Login()),
                                                (route) => false);
                                      });
                                    },
                                  ),
                                Container(
                                    margin: EdgeInsets.only(top: 10),
                                    child: Text('Hi-Bliss App'))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            appBar: AppBar(
              title: Text(_title.elementAt(tab.selectedTabIndex)),
              centerTitle: true,
              actions: [
                IconButton(
                    onPressed: () {
                      if (tab.selectedTabIndex == 1) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => JobHistory()));
                      } else if (tab.selectedTabIndex == 3) {
                        showCupertinoModalPopup(
                            context: context,
                            builder: (context) => CupertinoActionSheet(
                                  actions: [
                                    CupertinoActionSheetAction(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      PayoutSettings()))
                                              .then((value) {
                                            if (value is bool && value) {
                                              _accountsKey.currentState
                                                  ?.refresh();
                                            }
                                          });
                                        },
                                        child: Text('Payout Settings')),
                                    CupertinoActionSheetAction(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      BankAccount()))
                                              .then((value) {
                                            if (value is bool && value) {
                                              _accountsKey.currentState
                                                  ?.refresh();
                                            }
                                          });
                                        },
                                        child: Text('Bank Account Settings'))
                                  ],
                                ));
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (conntext) => TherapistChat()));
                      }
                    },
                    icon: tab.selectedTabIndex == 1
                        ? Icon(Icons.history, color: Colors.white)
                        : (tab.selectedTabIndex == 3
                            ? Icon(Icons.settings, color: Colors.white)
                            : Image.asset('res/icons/ic_comment.png')))
              ],
            ),
            body: _pages.elementAt(tab.selectedTabIndex),
            bottomNavigationBar: BottomNavigationBar(
              currentIndex: tab.selectedTabIndex,
              type: BottomNavigationBarType.fixed,
              showSelectedLabels: true,
              showUnselectedLabels: true,
              selectedLabelStyle: TextStyle(fontFamily: 'Soleil', fontSize: 16),
              unselectedLabelStyle:
                  TextStyle(fontFamily: 'Soleil', fontSize: 14),
              onTap: (index) {
                tab.update(index: index);
              },
              items: [
                BottomNavigationBarItem(
                  icon: Image.asset('res/icons/ic_nav_therapist_home.png',
                      width: 47),
                  activeIcon: Image.asset(
                      'res/icons/ic_nav_therapist_home2.png',
                      width: 47),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                    icon: Image.asset('res/icons/ic_nav_job.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_job2.png', width: 47),
                    label: 'Jobs'),
                BottomNavigationBarItem(
                    icon:
                        Image.asset('res/icons/ic_nav_message.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_message2.png', width: 47),
                    label: 'Messages'),
                BottomNavigationBarItem(
                    icon:
                        Image.asset('res/icons/ic_nav_account.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_account2.png', width: 47),
                    label: 'Account')
              ],
            )),
      ),
    );
  }
}
