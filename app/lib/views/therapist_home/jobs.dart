import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/job.dart';
import 'package:app/views/home/booking.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:app/views/jobs/details.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobsTab extends StatefulWidget {
  const JobsTab({Key? key}) : super(key: key);

  @override
  _JobsTabState createState() => _JobsTabState();
}

class _JobsTabState extends State<JobsTab> {
  final GlobalKey<ReportingJobSubTabState> _reportingState = GlobalKey();
  var _subTabs = <Widget>[];

  @override
  void initState() {
    super.initState();
    _subTabs = [
      _UpcomingJobSubTab(onJobComplete: () {
        _reportingState.currentState?.reload();
      }),
      ChangeNotifierProvider<BookingOrderListChangeNotifier>(
          create: (context) => BookingOrderListChangeNotifier(orders: []),
          child: LoadingIndicator(
              child: _ReportingJobSubTab(key: _reportingState)))
    ];
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              child: Container(
                margin: EdgeInsets.only(left: 14),
                child: TabBar(
                    labelStyle: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(color: Colors.white),
                    indicator: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Colors.white, width: 3))),
                    indicatorColor: Colors.white,
                    indicatorPadding: EdgeInsets.only(bottom: 1),
                    indicatorWeight: 3,
                    unselectedLabelStyle: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(
                            color: Colors.white, fontWeight: FontWeight.normal),
                    tabs: ["Upcoming", "Reporting"]
                        .map((text) => Tab(
                              text: text,
                            ))
                        .toList()),
              ),
            ),
            Expanded(
                child: TabBarView(
              children: _subTabs,
            ))
          ],
        ));
  }
}

class _UpcomingJobSubTab extends StatefulWidget {
  final VoidCallback onJobComplete;

  const _UpcomingJobSubTab({required this.onJobComplete, Key? key})
      : super(key: key);

  @override
  _UpcomingJobSubTabState createState() => _UpcomingJobSubTabState();
}

class _UpcomingJobSubTabState extends State<_UpcomingJobSubTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: FutureProvider<Response<BookingOrder>>(
        create: (context) =>
            TherapistJobRest(context: context).getUpcomingJobs(),
        initialData: Response(httpCode: -1),
        child: Consumer<Response<BookingOrder>>(
          builder: (context, booking, child) => booking.httpCode == -1
              ? StatelessLoadingIndicator()
              : ChangeNotifierProvider<BookingOrderListChangeNotifier>(
                  create: (context) => BookingOrderListChangeNotifier(
                      orders: booking.dataResults ?? []),
                  child: Consumer<BookingOrderListChangeNotifier>(
                    builder: (context, notifier, child) => LoadingIndicator(
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                        child: ListView(
                            children: notifier.orders!
                                .map((book) => InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) => JobDetail(
                                                    bookingId: book.bookingId!,
                                                    jobCategory: "upcoming")))
                                            .then((value) {
                                          if (value is bool && value) {
                                            DefaultTabController.of(context)
                                                ?.index = 1;
                                            reload(context: context);
                                            widget.onJobComplete();
                                          }
                                        });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 5),
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Row(
                                          children: [
                                            if ((book.imageLink?.length ?? 0) >
                                                0)
                                              Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10)),
                                                  child: Image.network(
                                                      book.imageLink![0].link!,
                                                      width: 84,
                                                      fit: BoxFit.fitWidth))
                                            else
                                              Image.asset(
                                                'res/images/home.png',
                                                width: 84,
                                              ),
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                        book.itemName ??
                                                            'No title',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline5),
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                            top: 5),
                                                        child: Text(
                                                            'Order ID: ${book.displayOrderId}',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1)),
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                            top: 5),
                                                        child: Row(
                                                          children: [
                                                            Image.asset(
                                                                'res/icons/ic_clock.png',
                                                                width: 13),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 5),
                                                              child: Text(
                                                                  book
                                                                      .timeSlot!,
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .bodyText1),
                                                            ),
                                                          ],
                                                        ))
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList()),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Future reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await TherapistJobRest(context: context).getUpcomingJobs();
    context
        .read<BookingOrderListChangeNotifier>()
        .update(orders: _res.dataResults ?? []);
    context.read<LoadingState>().toggle(value: false);
  }

  @override
  bool get wantKeepAlive => true;
}

class _ReportingJobSubTab extends StatefulWidget {
  const _ReportingJobSubTab({Key? key}) : super(key: key);

  @override
  ReportingJobSubTabState createState() => ReportingJobSubTabState();
}

class ReportingJobSubTabState extends State<_ReportingJobSubTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: Consumer<BookingOrderListChangeNotifier>(
        builder: (context, notifier, child) =>
            (notifier.orders?.length ?? 0) == 0
                ? FutureProvider<Response<BookingOrder>>(
                    create: (context) => init(),
                    initialData: Response(httpCode: -1),
                    child: Consumer<Response<BookingOrder>>(
                      builder: (context, response, child) =>
                          response.httpCode == -1
                              ? StatelessLoadingIndicator()
                              : _buildView(response.dataResults!),
                    ),
                  )
                : _buildView(notifier.orders!),
      ),
    );
  }

  Widget _buildView(List<BookingOrder> orders) => Container(
        margin: EdgeInsets.only(left: 10, right: 10, top: 20),
        child: ListView(
            children: orders
                .map((book) => InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => JobDetail(
                                bookingId: book.bookingId!,
                                jobCategory: "reporting")));
                      },
                      child: Container(
                        margin: EdgeInsets.only(bottom: 5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          children: [
                            if ((book.imageLink?.length ?? 0) > 0)
                              Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Image.network(book.imageLink![0].link!,
                                      width: 84, fit: BoxFit.fitWidth))
                            else
                              Image.asset(
                                'res/images/home.png',
                                width: 84,
                              ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(book.itemName ?? 'No title',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5),
                                    Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: Text(
                                            'Order ID: ${book.displayOrderId}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1)),
                                    Container(
                                        margin: EdgeInsets.only(top: 10),
                                        child: Text('View Report',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                ?.copyWith(
                                                    color: Color(0xff0095cf),
                                                    fontWeight:
                                                        FontWeight.bold)))
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ))
                .toList()),
      );

  Future<Response<BookingOrder>> reload() async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await TherapistJobRest(context: context).getReportingJobs();
    context
        .read<BookingOrderListChangeNotifier>()
        .update(orders: _res.dataResults ?? []);
    context.read<LoadingState>().toggle(value: false);
    return _res;
  }

  Future<Response<BookingOrder>> init() async {
    final _res = await TherapistJobRest(context: context).getReportingJobs();
    context
        .read<BookingOrderListChangeNotifier>()
        .update(orders: _res.dataResults ?? []);
    return _res;
  }

  @override
  bool get wantKeepAlive => true;
}
