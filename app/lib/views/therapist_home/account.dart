import 'package:app/model/payout.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/payout.dart';
import 'package:app/views/account/received.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountsTab extends StatefulWidget {
  const AccountsTab({Key? key}) : super(key: key);

  @override
  AccountsTabState createState() => AccountsTabState();
}

class AccountsTabState extends State<AccountsTab> {
  var _subTabs = <Widget>[];
  final GlobalKey<_PendingSubTabState> _pendingKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _subTabs = [_PendingSubTab(key: _pendingKey), _ReceivedSubTab()];
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              child: Container(
                margin: EdgeInsets.only(left: 14),
                child: TabBar(
                    labelStyle:
                        Theme.of(context).textTheme.headline5?.copyWith(color: Colors.white),
                    indicator: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Colors.white, width: 3))),
                    indicatorColor: Colors.white,
                    indicatorPadding: EdgeInsets.only(bottom: 1),
                    indicatorWeight: 3,
                    unselectedLabelStyle: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(color: Colors.white, fontWeight: FontWeight.normal),
                    tabs: ["Pending", "Received"]
                        .map((text) => Tab(
                              text: text,
                            ))
                        .toList()),
              ),
            ),
            Expanded(
                child: TabBarView(
              children: _subTabs,
            ))
          ],
        ));
  }

  refresh() {
    _pendingKey.currentState?.reload();
  }
}

class _PendingSubTab extends StatefulWidget {
  const _PendingSubTab({Key? key}) : super(key: key);

  @override
  _PendingSubTabState createState() => _PendingSubTabState();
}

class _PendingSubTabState extends State<_PendingSubTab> with AutomaticKeepAliveClientMixin {
  Response2<PendingPayout>? _payouts;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    reload();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
      child: Container(
        margin: EdgeInsets.all(10),
        child: _isLoading
            ? StatelessLoadingIndicator()
            : ListView(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(7), color: Colors.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('My Revenue',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(fontWeight: FontWeight.w500)),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text('${_payouts?.dataResults?.pendingAmount ?? 'RM 0.00'}',
                              style: Theme.of(context).textTheme.headline5?.copyWith(fontSize: 30)),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: LinearProgressIndicator(
                              minHeight: 11,
                              value: 0.8,
                              backgroundColor: Color(0xffe3e3e5),
                              color: Color(0xff037fce),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                                'Next payout: ${_payouts?.dataResults?.nextPayoutDate ?? 'N/A'}',
                                style:
                                    Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 12)),
                          ),
                        )
                      ],
                    ),
                  ),
                  ..._payouts?.dataResults?.pendingPayout
                          ?.map((payout) => Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 5),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Text(payout.appointmentDate!,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    ?.copyWith(
                                                        fontSize: 10, color: Color(0xff707070))),
                                          ),
                                          Expanded(
                                            child: Divider(
                                              height: 1,
                                              color: Color(0xffa0a0a0),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    ...payout.orderDetails
                                            ?.map((order) => Container(
                                                  margin:
                                                      EdgeInsets.only(top: 15, left: 10, right: 10),
                                                  child: Row(
                                                    children: [
                                                      Image.asset('res/icons/ic_pending.png',
                                                          width: 38),
                                                      Expanded(
                                                        child: Container(
                                                          margin: EdgeInsets.only(left: 15),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment.start,
                                                            children: [
                                                              Text(order.orderId!,
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .bodyText1
                                                                      ?.copyWith(
                                                                          fontWeight:
                                                                              FontWeight.bold)),
                                                              Text(order.timeSlot!,
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .bodyText1
                                                                      ?.copyWith(fontSize: 12))
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Text(order.amount!,
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .headline5
                                                              ?.copyWith(color: Color(0xff0095cf)))
                                                    ],
                                                  ),
                                                ))
                                            .toList() ??
                                        []
                                  ],
                                ),
                              ))
                          .toList() ??
                      []
                ],
              ),
      ),
    );
  }

  Future reload() async {
    setState(() {
      _isLoading = true;
    });
    final _res = await PayoutRest(context: context).listPending();
    setState(() {
      _payouts = _res;
      _isLoading = false;
    });
  }

  @override
  bool get wantKeepAlive => true;
}

class _ReceivedSubTab extends StatefulWidget {
  const _ReceivedSubTab({Key? key}) : super(key: key);

  @override
  _ReceivedSubTabState createState() => _ReceivedSubTabState();
}

class _ReceivedSubTabState extends State<_ReceivedSubTab> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return UnauthenticatedWithoutOverlay(
        child: FutureProvider<Response<ReceivedPayout>>(
      initialData: Response(httpCode: -1),
      create: (context) => PayoutRest(context: context).listReceived(),
      child: Consumer<Response<ReceivedPayout>>(
        builder: (context, payouts, child) => payouts.httpCode == -1
            ? StatelessLoadingIndicator()
            : Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: ListView(
                  children: payouts.dataResults
                          ?.map((payout) => InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          ReceivedPayoutBreakdown(payout: payout)));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 5),
                                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7), color: Colors.white),
                                  child: Row(
                                    children: [
                                      Image.asset('res/icons/ic_received.png', width: 38),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(left: 15),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(payout.transactionDate!,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      ?.copyWith(fontWeight: FontWeight.bold)),
                                              Text(payout.displayPayoutId!,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      ?.copyWith(fontSize: 12))
                                            ],
                                          ),
                                        ),
                                      ),
                                      Text(payout.totalCommission!,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5
                                              ?.copyWith(color: Color(0xff0095cf)))
                                    ],
                                  ),
                                ),
                              ))
                          .toList() ??
                      [],
                ),
              ),
      ),
    ));
  }

  @override
  bool get wantKeepAlive => true;
}

class PendingPayoutChangeNotifier extends ChangeNotifier {
  PendingPayout payout;

  PendingPayoutChangeNotifier({required this.payout});

  update({required PendingPayout payout}) {
    this.payout = payout;
    notifyListeners();
  }
}
