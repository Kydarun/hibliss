import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/notifications/appointment.dart';
import 'package:app/views/notifications/care_plan.dart';
import 'package:app/views/notifications/new_job.dart';
import 'package:app/views/notifications/payout.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/therapist_chat/therapist_chat.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class TherapistMessageTab extends StatelessWidget {
  const TherapistMessageTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              child: Container(
                margin: EdgeInsets.only(left: 14),
                child: TabBar(
                    labelStyle: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(color: Colors.white),
                    indicator: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Colors.white, width: 3))),
                    indicatorColor: Colors.white,
                    indicatorPadding: EdgeInsets.only(bottom: 1),
                    indicatorWeight: 3,
                    unselectedLabelStyle: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(
                            color: Colors.white, fontWeight: FontWeight.normal),
                    tabs: ["Chat", "Notifications"]
                        .map((text) => Tab(
                              text: text,
                            ))
                        .toList()),
              ),
            ),
            Expanded(
                child: TabBarView(
              children: [
                Stack(
                  children: [
                    Center(
                      child: Text('Chat with Hi-Bliss\' Customer Service.'),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(
                                  bottom: 40, left: 20, right: 20),
                              child: ElevatedButton(
                                  onPressed: () {
                                    launch(Uri.encodeFull(
                                        "https://wa.me/60122355729"));
                                  },
                                  child: Text('Whatsapp')),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                NotificationListWidget()
              ],
            ))
          ],
        ));
  }
}

class NotificationListWidget extends StatelessWidget {
  const NotificationListWidget({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    return LoadingIndicator(
      child: FutureProvider<Response<NotificationMessage>>(
          create: (context) => NotificationRest(context: context).list(),
          initialData: Response(httpCode: -1),
          child: Consumer<Response<NotificationMessage>>(
            builder: (context, notifications, child) => notifications
                        .httpCode ==
                    -1
                ? StatelessLoadingIndicator()
                : ChangeNotifierProvider<NotificationMessageChangeNotifier>(
                    create: (context) => NotificationMessageChangeNotifier(
                        response: notifications),
                    child: Consumer<NotificationMessageChangeNotifier>(
                      builder: (context, notifier, child) => Container(
                        margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                        child: ListView(
                          children: notifier.response.dataResults
                                  ?.map((notification) => InkWell(
                                        onTap: () {
                                          switch (notification.type) {
                                            case 1:
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          AppointmentReminderWidget(
                                                              notificationId:
                                                                  notification
                                                                      .notifId!)));
                                              break;
                                            case 2:
                                              Navigator.of(context)
                                                  .push(MaterialPageRoute(
                                                      builder: (context) =>
                                                          NewJobReminderWidget(
                                                              notificationId:
                                                                  notification
                                                                      .notifId!)))
                                                  .then((_) {
                                                _reload(context: context);
                                              });
                                              break;
                                            case 3:
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          PayoutReminderWidget(
                                                              notificationId:
                                                                  notification
                                                                      .notifId!)));
                                              break;
                                            case 4:
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          CarePlanReminderWidget(
                                                              notificationId:
                                                                  notification
                                                                      .notifId!)));
                                              break;
                                            default:
                                              break;
                                          }
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 20),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                'res/icons/ic_notification_mail.png',
                                                width: 38,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                          notification.title ??
                                                              'No title',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline5),
                                                      Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 5),
                                                          child: Row(
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                    notification
                                                                            .message ??
                                                                        'No message',
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .bodyText1),
                                                              ),
                                                            ],
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(right: 5),
                                                  child: Image.asset(
                                                      'res/icons/ic_arrow_right.png',
                                                      width: 10))
                                            ],
                                          ),
                                        ),
                                      ))
                                  .toList() ??
                              [],
                        ),
                      ),
                    ),
                  ),
          )),
    );
  }

  Future _reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await NotificationRest(context: context).list();
    context.read<NotificationMessageChangeNotifier>().update(response: _res);
    context.read<LoadingState>().toggle(value: false);
  }
}
