import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

// ignore: must_be_immutable
class TherapistHomeTab extends StatelessWidget {
  WebViewController? _webViewController;

  TherapistHomeTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (await _webViewController?.canGoBack() ?? false) {
          await _webViewController?.goBack();
          return false;
        } else {
          Navigator.of(context).pop();
          return true;
        }
      },
      child: Container(
        child: WebView(
          onWebViewCreated: (controller) {
            _webViewController = controller;
          },
          javascriptMode: JavascriptMode.unrestricted,
          allowsInlineMediaPlayback: true,
          initialUrl: 'https://hi-bliss.com/hi-bliss-home-service/',
        ),
      ),
    );
  }
}
