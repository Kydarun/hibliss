import 'dart:convert';

import 'package:app/main.dart';
import 'package:app/util/cache.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/careplan/view/list.dart';
import 'package:app/views/chat/appointment.dart';
import 'package:app/views/chat/booking.dart';
import 'package:app/views/chat/care_plan.dart';
import 'package:app/views/chat/chat.dart';
import 'package:app/views/contact.dart';
import 'package:app/views/home/booking.dart';
import 'package:app/views/home/cart.dart';
import 'package:app/views/home/home.dart';
import 'package:app/views/home/shop.dart';
import 'package:app/views/pdpa_customer.dart';
import 'package:app/views/profile/address/list.dart';
import 'package:app/views/profile/password.dart';
import 'package:app/views/profile/profile.dart';
import 'package:app/views/settings.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/terms.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _drawerMenuExpanded = [false, false, false];

  final GlobalKey<CartTabState> _cartState = GlobalKey();
  bool _isDeletingCart = false;

  late var _pages;
  late var _title;

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null && initialMessage.data['type'] != null) {
      int? _type = int.tryParse(initialMessage.data['type']);
      int? _id = int.tryParse(initialMessage.data['notifId']);
      _navigate(_id, _type);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      int? _type = int.tryParse(message.data['type']);
      int? _id = int.tryParse(message.data['notifId']);
      _navigate(_id, _type);
    });

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
            alert: true, sound: true, badge: true);
    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings('ic_launcher_foreground'));
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
      if (payload != null) {
        final message = json.decode(payload);
        int? _type = int.tryParse(message['type']);
        int? _id = int.tryParse(message['notifId']);
        _navigate(_id, _type);
      }
    });

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: android.smallIcon,
            )),
            payload: json.encode(message.data));
      }
    });
  }

  void _navigate(int? id, int? type) {
    if (type != null && id != null) {
      switch (type) {
        case 1:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  CustomerAppointmentReminderWidget(notificationId: id)));
          break;
        case 2:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  CustomerCarePlanReminderWidget(notificationId: id)));
          break;
        case 3:
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  CustomerBookingConfirmedReminder(notificationId: id)));
          break;
        default:
          break;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _pages = [
      HomeTab(),
      ShopTab(),
      BookingTab(),
      Center(
        child: Text('Coming soon...'),
      ),
      CartTab(
          key: _cartState,
          onDelete: () {
            setState(() {
              _isDeletingCart = false;
            });
          })
    ];
    _title = ['Home', 'Shop', 'Booking', 'Report', 'Cart'];
    //setupInteractedMessage();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TabChangeNotifier>(
      create: (context) => TabChangeNotifier(),
      child: Consumer<TabChangeNotifier>(
        builder: (context, tab, child) => Scaffold(
            drawer: Drawer(
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                      child: Column(
                    children: [
                      DrawerHeader(
                          child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Text('MENU',
                            style: TextStyle(
                                fontFamily: 'Soleil',
                                fontSize: 30,
                                fontWeight: FontWeight.bold)),
                      )),
                      ExpansionTile(
                        onExpansionChanged: (isExpanded) {
                          setState(() {
                            _drawerMenuExpanded[0] = isExpanded;
                          });
                        },
                        initiallyExpanded: _drawerMenuExpanded[0],
                        trailing: Container(
                          margin: EdgeInsets.only(right: 24),
                          child: Image.asset(
                              'res/icons/ic_menu_arrow${_drawerMenuExpanded[0] ? "up" : "down"}.png',
                              width: 20),
                        ),
                        title: Row(
                          children: [
                            Image.asset(
                              'res/icons/ic_menu_account${_drawerMenuExpanded[0] ? "2" : ""}.png',
                              width: 20,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('Account',
                                    style:
                                        Theme.of(context).textTheme.headline1))
                          ],
                        ),
                        children: [
                          ListTile(
                            title: Text('My Profile',
                                style: Theme.of(context).textTheme.headline5),
                            onTap: () {
                              Navigator.of(context).pop();
                              Cache(context: context)
                                  .isLoggedIn()
                                  .then((value) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        value ? Profile() : Login()));
                              });
                            },
                          ),
                          ListTile(
                            title: Text('My Addresses',
                                style: Theme.of(context).textTheme.headline5),
                            onTap: () {
                              Navigator.of(context).pop();
                              Cache(context: context)
                                  .isLoggedIn()
                                  .then((value) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        value ? AddressList() : Login()));
                              });
                            },
                          ),
                          ListTile(
                            title: Text('My Care Plan',
                                style: Theme.of(context).textTheme.headline5),
                            onTap: () {
                              Navigator.of(context).pop();
                              Cache(context: context)
                                  .isLoggedIn()
                                  .then((value) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        value ? CarePlanList() : Login()));
                              });
                            },
                          ),
                          ListTile(
                              title: Text('My Password',
                                  style: Theme.of(context).textTheme.headline5),
                              onTap: () {
                                Navigator.of(context).pop();
                                Cache(context: context)
                                    .isLoggedIn()
                                    .then((value) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          value ? Password() : Login()));
                                });
                              })
                        ],
                      ),
                      ExpansionTile(
                        onExpansionChanged: (isExpanded) {
                          setState(() {
                            _drawerMenuExpanded[1] = isExpanded;
                          });
                        },
                        initiallyExpanded: _drawerMenuExpanded[1],
                        trailing: Container(
                          margin: EdgeInsets.only(right: 24),
                          child: Image.asset(
                              'res/icons/ic_menu_arrow${_drawerMenuExpanded[1] ? "up" : "down"}.png',
                              width: 20),
                        ),
                        title: Row(
                          children: [
                            Image.asset(
                                'res/icons/ic_menu_settings${_drawerMenuExpanded[1] ? "2" : ""}.png',
                                width: 20),
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('Settings',
                                    style:
                                        Theme.of(context).textTheme.headline1)),
                          ],
                        ),
                        children: [
                          Consumer<Session>(
                            builder: (context, session, child) => ListTile(
                              title: Text('Notifications',
                                  style: Theme.of(context).textTheme.headline5),
                              onTap: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => session.isLoggedIn
                                        ? Settings()
                                        : Login()));
                              },
                            ),
                          ),
                        ],
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Image.asset('res/icons/ic_menu_terms.png',
                                width: 20),
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('PDPA',
                                    style:
                                        Theme.of(context).textTheme.headline1)),
                          ],
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PdpaCustomer()));
                        },
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Image.asset('res/icons/ic_menu_contact.png',
                                width: 20),
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text('Contact Us',
                                    style:
                                        Theme.of(context).textTheme.headline1)),
                          ],
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ContactUs()));
                        },
                      ),
                    ],
                  )),
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Row(
                      children: [
                        Consumer<Session>(
                          builder: (context, loggedIn, child) => Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 16, bottom: 25),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  margin: EdgeInsets.only(right: 37),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          top: BorderSide(
                                              color: Color(0xffcecece)))),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      if (loggedIn.isLoggedIn)
                                        InkWell(
                                          child: Container(
                                            margin: EdgeInsets.only(top: 24),
                                            child: Row(children: [
                                              Image.asset(
                                                  'res/icons/ic_menu_signout.png',
                                                  width: 20),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Text('Sign Out',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline1),
                                              )
                                            ]),
                                          ),
                                          onTap: () {
                                            Cache(context: context)
                                                .logout()
                                                .then((value) {
                                              Navigator.of(context)
                                                  .pushAndRemoveUntil(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Home()),
                                                      (route) => false);
                                            });
                                          },
                                        ),
                                      Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: Text('Hi-Bliss App'))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            appBar: AppBar(
              title: Text(_title.elementAt(tab.selectedTabIndex)),
              centerTitle: true,
              actions: [
                IconButton(
                    onPressed: () {
                      if (tab.selectedTabIndex == 4) {
                        setState(() {
                          _isDeletingCart = !_isDeletingCart;
                        });
                        _cartState.currentState
                            ?.toggleDeleteCart(isDeleting: _isDeletingCart);
                      } else {
                        Navigator.of(context)
                            .push(
                                MaterialPageRoute(builder: (context) => Chat()))
                            .then((value) {
                          if (value is bool && value) {
                            tab.update(index: 4);
                          }
                        });
                      }
                    },
                    iconSize: 48,
                    icon: Image.asset(
                        'res/icons/${tab.selectedTabIndex == 4 ? (_isDeletingCart ? "ic_top_cancel.png" : "ic_top_trash.png") : "ic_top_comment.png"}'))
              ],
            ),
            body: ChangeNotifierProvider<ShopTabChangeNotifier>(
                create: (context) => ShopTabChangeNotifier(),
                child: _pages.elementAt(tab.selectedTabIndex)),
            bottomNavigationBar: BottomNavigationBar(
              currentIndex: tab.selectedTabIndex,
              type: BottomNavigationBarType.fixed,
              showSelectedLabels: true,
              showUnselectedLabels: true,
              selectedLabelStyle: TextStyle(fontFamily: 'Soleil', fontSize: 14),
              unselectedLabelStyle:
                  TextStyle(fontFamily: 'Soleil', fontSize: 12),
              onTap: (index) {
                tab.update(index: index);
              },
              items: [
                BottomNavigationBarItem(
                  icon: Image.asset('res/icons/ic_nav_home.png', width: 47),
                  activeIcon:
                      Image.asset('res/icons/ic_nav_home2.png', width: 47),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                    icon: Image.asset('res/icons/ic_nav_market.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_market2.png', width: 47),
                    label: 'Shop'),
                BottomNavigationBarItem(
                    icon:
                        Image.asset('res/icons/ic_nav_calendar.png', width: 47),
                    activeIcon: Image.asset('res/icons/ic_nav_calendar2.png',
                        width: 47),
                    label: 'Booking'),
                BottomNavigationBarItem(
                    icon: Image.asset('res/icons/ic_nav_report.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_report2.png', width: 47),
                    label: 'Report'),
                BottomNavigationBarItem(
                    icon: Image.asset('res/icons/ic_nav_cart.png', width: 47),
                    activeIcon:
                        Image.asset('res/icons/ic_nav_cart2.png', width: 47),
                    label: 'Cart')
              ],
            )),
      ),
    );
  }
}

class TabChangeNotifier with ChangeNotifier {
  int selectedTabIndex;

  TabChangeNotifier({this.selectedTabIndex = 0});

  update({int? index}) {
    if (index != null) {
      selectedTabIndex = index;
      notifyListeners();
    }
  }
}

class ShopTabChangeNotifier with ChangeNotifier {
  int selectedShopTabIndex;

  ShopTabChangeNotifier({this.selectedShopTabIndex = 0});

  update({required int index}) {
    selectedShopTabIndex = index;
    notifyListeners();
  }
}
