import 'package:app/model/notification.dart';
import 'package:app/views/shared/pdf_viewer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path/path.dart' as p;
import 'package:webview_flutter/webview_flutter.dart';

class ViewCarePlanPdf extends StatelessWidget {
  final CarePlanNotification carePlan;

  const ViewCarePlanPdf({required this.carePlan, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _ext = p.extension(carePlan.reportLink!).toLowerCase();
    return Scaffold(
      appBar: AppBar(
        title: Text('${carePlan.displayId}'),
        actions: [
          IconButton(
              onPressed: () {
                showCupertinoModalPopup(
                    context: context,
                    builder: (context) => Container(
                          margin:
                              EdgeInsets.only(bottom: 35, left: 7, right: 7),
                          child: CupertinoActionSheet(
                            cancelButton: CupertinoActionSheetAction(
                                isDestructiveAction: false,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text('Cancel')),
                            actions: [
                              CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    launch(carePlan.reportLink!);
                                  },
                                  child: Text('Save')),
                              CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Share.share(carePlan.reportLink!);
                                  },
                                  child: Text('Share')),
                            ],
                          ),
                        ));
              },
              icon: Icon(Icons.share, color: Colors.white))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        color: Color(0xff494949),
        child: CustomPdfViewer(url: carePlan.reportLink!),
      ),
    );
  }
}
