import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomerBookingConfirmedReminder extends StatelessWidget {
  final int notificationId;

  const CustomerBookingConfirmedReminder({required this.notificationId, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Reminder'),
        ),
        body: FutureProvider<Response<BookingReminder>>(
            initialData: Response(httpCode: -1),
            create: (context) => NotificationRest(context: context)
                .getBookingReminder(notificationId: notificationId),
            child: Consumer<Response<BookingReminder>>(
              builder: (context, response, child) => response.httpCode == -1
                  ? StatelessLoadingIndicator()
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: CustomScrollView(
                        slivers: [
                          SliverToBoxAdapter(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text('Your appointment has been succesfully booked!',
                                      style: Theme.of(context).textTheme.headline5),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(5),
                                          bottomRight: Radius.circular(5)),
                                      color: Colors.white),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(left: 15, right: 15, top: 16, bottom: 15),
                                      padding: EdgeInsets.all(8),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          color: Color(0xffededf0)),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Image.asset('res/icons/ic_calendar_gray.png',
                                              width: 13.5),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text(
                                                  '${response.dataResults?[0].appointmentTime}',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      ?.copyWith(
                                                          color: Color(0xff0095cf),
                                                          fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Divider(
                                    color: Color(0xffededf0),
                                    height: 1,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Order ID',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(fontSize: 17)),
                                      Text('${response.dataResults?[0].displayId}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(fontSize: 17))
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 40),
                                  child: Text('Gentle Reminder:',
                                      style: Theme.of(context).textTheme.headline5),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 30),
                                  child: Text('''
A gentle reminder that you may cancel your booking appointment for services within 2 days before your designated date.

If you decide to cancel your booking within 2 days before your designated date, you are liable to pay the full price and it is not refundable.
                                  '''),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
            )));
  }
}
