import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/notification.dart';
import 'package:app/rest/store.dart';
import 'package:app/views/chat/care_plan_pdf.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shop/info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomerCarePlanReminderWidget extends StatelessWidget {
  final int notificationId;

  const CustomerCarePlanReminderWidget({required this.notificationId, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Care Plan'),
        ),
        body: LoadingIndicator(
          child: Builder(
            builder: (context) => FutureProvider<
                    Response<CarePlanNotification>>(
                initialData: Response(httpCode: -1),
                create: (context) => NotificationRest(context: context)
                    .getCarePlanNotification(notificationId: notificationId),
                child: Consumer<Response<CarePlanNotification>>(
                  builder: (context, response, child) => response.httpCode == -1
                      ? StatelessLoadingIndicator()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 10),
                                      margin: EdgeInsets.only(bottom: 5),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(7),
                                          color: Colors.white),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                              width: 84,
                                              height: 84,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  gradient: LinearGradient(
                                                      colors: [
                                                        Color(0x36018aca),
                                                        Color(0x36afe5ff)
                                                      ],
                                                      begin:
                                                          Alignment.centerLeft,
                                                      end: Alignment
                                                          .centerRight)),
                                              child: Center(
                                                  child: Image.asset(
                                                      'res/icons/ic_description.png',
                                                      width: 36))),
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                      '${response.dataResults?[0].patientFullName ?? 'N/A'}',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline5),
                                                  Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Row(children: [
                                                      Image.asset(
                                                          'res/icons/ic_shield.png',
                                                          width: 15),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                            'Care Plan: ${response.dataResults?[0].category}',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1),
                                                      )
                                                    ]),
                                                  ),
                                                  Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                            'res/icons/ic_clock.png',
                                                            width: 13),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                              '${response.dataResults?[0].carePlanCreateDate}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SliverToBoxAdapter(
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(top: 20),
                                            child: Text(
                                                '${response.dataResults?[0].displayId}',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline5),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            child: RichText(
                                                text: TextSpan(
                                                    text: 'Created by ',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            color: Colors.grey),
                                                    children: [
                                                  TextSpan(
                                                      text:
                                                          '${response.dataResults?[0].therapistName ?? 'Administrator'}',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1
                                                          ?.copyWith(
                                                              color: Color(
                                                                  0xff0095cf)))
                                                ])),
                                          ),
                                          if (response.dataResults?[0]
                                                  .reportLink?.isNotEmpty ??
                                              false)
                                            Container(
                                              margin: EdgeInsets.only(top: 30),
                                              child: Text('''
Dear valued customer,

Please find the attached care plan report for your reference. 

Thank you, 
Hi-Bliss Therapy.
                                      '''),
                                            )
                                          else
                                            Container(
                                              margin: EdgeInsets.only(top: 30),
                                              child: Text('''
Dear valued customer,

We will get back to you within 2 business days from the date of the care plan submission.. 

Thank you, 
Hi-Bliss Therapy.
                                      '''),
                                            ),
                                          Container(
                                              margin: EdgeInsets.only(top: 20),
                                              child: Text(
                                                  '${response.dataResults?[0].sentDate}',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      ?.copyWith(
                                                          color: Colors.grey)))
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            if (response
                                    .dataResults?[0].reportLink?.isNotEmpty ??
                                false)
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: 40, top: 10, left: 20, right: 20),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: ElevatedButton(
                                              onPressed: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ViewCarePlanPdf(
                                                                carePlan: response
                                                                        .dataResults![
                                                                    0])));
                                              },
                                              child: Text('View PDF')),
                                        )
                                      ],
                                    ),
                                    if (response.dataResults?[0]
                                            .recommendedServices !=
                                        null)
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: ElevatedButton(
                                                  style: Theme.of(context)
                                                      .elevatedButtonTheme
                                                      .style
                                                      ?.copyWith(
                                                        backgroundColor:
                                                            MaterialStateProperty
                                                                .all<Color>(
                                                                    Colors
                                                                        .white),
                                                        shadowColor:
                                                            MaterialStateProperty
                                                                .all<Color>(Colors
                                                                    .black54),
                                                        textStyle: MaterialStateProperty.all<
                                                                TextStyle?>(
                                                            TextStyle(
                                                                fontFamily:
                                                                    'Soleil',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 18,
                                                                color: Colors
                                                                    .black)),
                                                      ),
                                                  onPressed: () {
                                                    _goToShop(
                                                        context,
                                                        response.dataResults![0]
                                                            .recommendedServices!);
                                                  },
                                                  child: Text('Go to Shop',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .button
                                                          ?.copyWith(
                                                              fontFamily:
                                                                  'Soleil',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 17,
                                                              color: Colors
                                                                  .black))),
                                            )
                                          ],
                                        ),
                                      ),
                                  ],
                                ),
                              )
                          ],
                        ),
                )),
          ),
        ));
  }

  _goToShop(BuildContext context, int id) async {
    context.read<LoadingState>().toggle(value: true);
    final _package = await StoreRest(context: context).service(id: id);
    context.read<LoadingState>().toggle(value: false);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => ItemInfo(alacarte: _package.dataResults)),
        ModalRoute.withName('/home'));
  }
}
