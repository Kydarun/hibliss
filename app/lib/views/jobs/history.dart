import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/job.dart';
import 'package:app/views/jobs/details.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobHistory extends StatelessWidget {
  const JobHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Unauthenticated(
      child: Scaffold(
        appBar: AppBar(
          title: Text('History'),
        ),
        body: FutureProvider<Response<BookingOrder>>(
          create: (context) => TherapistJobRest(context: context).getHistoryJobs(),
          initialData: Response(httpCode: -1),
          child: Consumer<Response<BookingOrder>>(
            builder: (context, booking, child) => booking.httpCode == -1
                ? StatelessLoadingIndicator()
                : Container(
                    margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                    child: ListView(
                        children: booking.dataResults
                                ?.map((book) => InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => JobDetail(
                                                bookingId: book.bookingId!,
                                                jobCategory: "history")));
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 5),
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10)),
                                        child: Row(
                                          children: [
                                            if ((book.imageLink?.length ?? 0) > 0)
                                              Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(10)),
                                                  child: Image.network(book.imageLink![0].link!,
                                                      width: 84, fit: BoxFit.fitWidth))
                                            else
                                              Image.asset(
                                                'res/images/home.png',
                                                width: 84,
                                              ),
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(book.itemName ?? 'No title',
                                                      style: Theme.of(context).textTheme.headline5),
                                                  Container(
                                                      margin: EdgeInsets.only(top: 5),
                                                      child: Text(
                                                          'Order ID: ${book.displayOrderId}',
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .bodyText1)),
                                                  Container(
                                                      margin: EdgeInsets.only(top: 5),
                                                      child: Row(
                                                        children: [
                                                          Image.asset('res/icons/ic_clock.png',
                                                              width: 13),
                                                          Container(
                                                            margin: EdgeInsets.only(left: 5),
                                                            child: Text(book.timeSlot!,
                                                                style: Theme.of(context)
                                                                    .textTheme
                                                                    .bodyText1),
                                                          ),
                                                        ],
                                                      ))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList() ??
                            []),
                  ),
          ),
        ),
      ),
    );
  }
}
