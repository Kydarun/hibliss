import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/booking.dart';
import 'package:app/rest/therapist/job.dart';
import 'package:app/util/general.dart';
import 'package:app/views/booking/detail.dart';
import 'package:app/views/jobs/pdf.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/jobs/qr.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:timelines/timelines.dart';

class JobDetail extends StatefulWidget {
  final int bookingId;
  final String jobCategory;

  const JobDetail(
      {required this.bookingId, required this.jobCategory, Key? key})
      : super(key: key);

  @override
  _JobDetailState createState() => _JobDetailState();
}

class _JobDetailState extends State<JobDetail> {
  @override
  Widget build(BuildContext context) {
    return FutureProvider<Response2<BookingOrder>>(
      create: (context) => TherapistJobRest(context: context)
          .getJob(bookingId: widget.bookingId, jobCategory: widget.jobCategory),
      initialData: Response2(httpCode: -1),
      child: Scaffold(
        appBar: AppBar(centerTitle: true, title: Text('Job Details')),
        body: Consumer<Response2<BookingOrder>>(
          builder: (context, booking, child) => booking.httpCode == -1
              ? StatelessLoadingIndicator()
              : ChangeNotifierProvider<BookingOrderChangeNotifier>(
                  create: (context) =>
                      BookingOrderChangeNotifier(order: booking.dataResults!),
                  child: Consumer<BookingOrderChangeNotifier>(
                    builder: (context, orderNotifier, child) =>
                        LoadingIndicator(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: [
                            Expanded(
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 10),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: Colors.white),
                                            child: Column(
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 10),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(5),
                                                              topRight: Radius
                                                                  .circular(5)),
                                                      color: Color(0xff17b491)),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text('Order ID',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .bodyText1
                                                              ?.copyWith(
                                                                  fontFamily:
                                                                      'Soleil',
                                                                  color: Colors
                                                                      .white)),
                                                      Text(
                                                          '${orderNotifier.order.displayOrderId}',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .bodyText1
                                                              ?.copyWith(
                                                                  fontFamily:
                                                                      'Soleil',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white))
                                                    ],
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    if ((orderNotifier
                                                                .order
                                                                .imageLink
                                                                ?.length ??
                                                            0) >
                                                        0)
                                                      Container(
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10)),
                                                          child: Image.network(
                                                              orderNotifier
                                                                  .order
                                                                  .imageLink![0]
                                                                  .link!,
                                                              width: 84,
                                                              fit: BoxFit
                                                                  .fitWidth))
                                                    else
                                                      Image.asset(
                                                        'res/images/home.png',
                                                        width: 84,
                                                      ),
                                                    Expanded(
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                                orderNotifier
                                                                        .order
                                                                        .itemName ??
                                                                    'No title',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .headline5),
                                                            Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 5),
                                                                child: Row(
                                                                  children: [
                                                                    Image.asset(
                                                                        'res/icons/ic_clock.png',
                                                                        width:
                                                                            13),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              5),
                                                                      child: Text(
                                                                          '${orderNotifier.order.duration} minutes',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .bodyText1),
                                                                    ),
                                                                  ],
                                                                )),
                                                            Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 5),
                                                                child: Row(
                                                                  children: [
                                                                    Image.asset(
                                                                        'res/icons/ic_money.png',
                                                                        width:
                                                                            13),
                                                                    Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                5),
                                                                        child: Text(
                                                                            'RM${orderNotifier.order.total}',
                                                                            style:
                                                                                Theme.of(context).textTheme.bodyText1?.copyWith(color: Color(0xff0095cf), fontWeight: FontWeight.bold)))
                                                                  ],
                                                                )),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 10,
                                                      bottom: 15,
                                                      left: 20,
                                                      right: 20),
                                                  child: Divider(
                                                    color: Color(0xffededf0),
                                                    height: 1,
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 15),
                                                  child:
                                                      FixedTimeline.tileBuilder(
                                                    theme: TimelineThemeData(
                                                        nodePosition: 0,
                                                        indicatorPosition: 0),
                                                    builder: TimelineTileBuilder
                                                        .connected(
                                                            itemCount: 2,
                                                            indicatorBuilder:
                                                                (context,
                                                                    index) {
                                                              switch (index) {
                                                                case 0:
                                                                  return Image.asset(
                                                                      'res/icons/ic_user.png',
                                                                      width:
                                                                          17);
                                                                case 1:
                                                                  return Image
                                                                      .asset(
                                                                    'res/icons/ic_location.png',
                                                                    width: 17,
                                                                  );
                                                                default:
                                                                  return Container();
                                                              }
                                                            },
                                                            connectorBuilder:
                                                                (context, index,
                                                                        connectorType) =>
                                                                    Padding(
                                                                      padding: EdgeInsets.symmetric(
                                                                          vertical:
                                                                              8),
                                                                      child:
                                                                          DashedLineConnector(
                                                                        color: Color(
                                                                            0xffdfdfe3),
                                                                      ),
                                                                    ),
                                                            contentsBuilder:
                                                                (context,
                                                                    index) {
                                                              switch (index) {
                                                                case 0:
                                                                  return Container(
                                                                    margin: EdgeInsets.only(
                                                                        left:
                                                                            10,
                                                                        bottom:
                                                                            20),
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                            'Therapist: ${orderNotifier.order.therapistName}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5),
                                                                        Text(
                                                                            '${orderNotifier.order.therapistAddress}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.normal))
                                                                      ],
                                                                    ),
                                                                  );
                                                                case 1:
                                                                  return Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                10),
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                            'Customer: ${orderNotifier.order.customerName}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5),
                                                                        Text(
                                                                            '${orderNotifier.order.customerAddress}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.normal))
                                                                      ],
                                                                    ),
                                                                  );
                                                                default:
                                                                  return Container();
                                                              }
                                                            }),
                                                  ),
                                                ),
                                                Container(
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              bottomLeft: Radius
                                                                  .circular(5),
                                                              bottomRight:
                                                                  Radius
                                                                      .circular(
                                                                          5)),
                                                      color: Colors.white),
                                                  child: Align(
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          left: 15,
                                                          right: 15,
                                                          top: 16,
                                                          bottom: 15),
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Color(
                                                              0xffededf0)),
                                                      child: Row(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          Image.asset(
                                                              'res/icons/ic_calendar_gray.png',
                                                              width: 13.5),
                                                          Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 10),
                                                              child: Text(
                                                                  '${orderNotifier.order.displayTimeSlot}',
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .bodyText1
                                                                      ?.copyWith(
                                                                          color: Color(
                                                                              0xff0095cf),
                                                                          fontWeight:
                                                                              FontWeight.bold)))
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          padding: EdgeInsets.only(
                                              top: 15,
                                              left: 20,
                                              right: 20,
                                              bottom: 20),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Colors.white),
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text('Subtotal:',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1
                                                          ?.copyWith(
                                                              fontSize: 17)),
                                                  Text(
                                                      'RM${orderNotifier.order.subtotal!.toStringAsFixed(2)}',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1
                                                          ?.copyWith(
                                                              fontSize: 17))
                                                ],
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 4),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text('Tax:',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                fontSize: 17)),
                                                    Text(
                                                        'RM${orderNotifier.order.tax?.toStringAsFixed(2)}',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                fontSize: 17))
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 4),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text('Travel Fee:',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                fontSize: 17)),
                                                    Text(
                                                        'RM${orderNotifier.order.travelFee?.toStringAsFixed(2)}',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                fontSize: 17))
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                child: Divider(
                                                  height: 1,
                                                  color: Color(0xffededf0),
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text('Total:',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline5),
                                                    Text(
                                                        'RM${orderNotifier.order.total?.toStringAsFixed(2)}',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline5)
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        if (orderNotifier
                                                .order.authCode?.isNotEmpty ??
                                            false) ...[
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 15, bottom: 15),
                                            child: Divider(
                                              height: 1,
                                              color: Color(0xffcecece),
                                            ),
                                          ),
                                          Container(
                                            child: Center(
                                              child: Text('Authentication Code',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline4),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            child: Center(
                                              child: Text(
                                                  '${orderNotifier.order.authCodeRequestTime ?? 'Time unavailable'}',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline4
                                                      ?.copyWith(fontSize: 12)),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 8),
                                            child: Center(
                                              child: Text(
                                                  '${orderNotifier.order.authCode}',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5
                                                      ?.copyWith(fontSize: 40)),
                                            ),
                                          )
                                        ],
                                        if (orderNotifier.order.displayReportId
                                                ?.isNotEmpty ??
                                            false) ...[
                                          Container(
                                            width: double.infinity,
                                            margin: EdgeInsets.only(
                                                top: 15, left: 5, right: 5),
                                            child: Text(
                                                '${orderNotifier.order.displayReportId}',
                                                textAlign: TextAlign.left,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.bold)),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 5, left: 5, right: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                    'From ${orderNotifier.order.reportGeneratedBy ?? 'N/A'}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            fontSize: 12)),
                                                Text(
                                                    'From ${orderNotifier.order.reportGeneratedDate ?? 'N/A'}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1
                                                        ?.copyWith(
                                                            fontSize: 12))
                                              ],
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 10, left: 5, right: 5),
                                            child: Text(
                                                'Please find the attached report generated based on the session completed on ${orderNotifier.order.appointmentDate}.',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText1),
                                          )
                                        ]
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 40, top: 10, left: 10, right: 10),
                              child: Row(
                                children: [
                                  if (widget.jobCategory == "upcoming")
                                    Expanded(
                                        child: ElevatedButton(
                                            onPressed: ((orderNotifier
                                                            .order
                                                            .authCode
                                                            ?.isEmpty ??
                                                        false) ||
                                                    (orderNotifier.order
                                                            .isCompleteEnabled ??
                                                        false))
                                                ? () {
                                                    if (orderNotifier
                                                            .order
                                                            .authCode
                                                            ?.isEmpty ??
                                                        false) {
                                                      showDialog(
                                                          context: context,
                                                          builder: (ctx) =>
                                                              HiBlissDialog(
                                                                context:
                                                                    context,
                                                                title:
                                                                    'Start Session',
                                                                message:
                                                                    'Are you sure you want to start your\nsession now?',
                                                                onConfirm: () {
                                                                  _startJob(
                                                                      context);
                                                                },
                                                              ));
                                                    } else if (orderNotifier
                                                            .order
                                                            .isCompleteEnabled ??
                                                        false) {
                                                      showDialog(
                                                          context: context,
                                                          builder: (ctx) =>
                                                              HiBlissDialog(
                                                                context:
                                                                    context,
                                                                title:
                                                                    'Complete Session',
                                                                message:
                                                                    'Please confirm that you are closing\nthis session.',
                                                                onConfirm: () {
                                                                  _endJob(
                                                                      context);
                                                                },
                                                              ));
                                                    }
                                                  }
                                                : null,
                                            child: Text((orderNotifier.order
                                                        .authCode?.isEmpty ??
                                                    false)
                                                ? 'Start'
                                                : 'Complete')))
                                  else if (orderNotifier
                                          .order.cancelDate?.isEmpty ??
                                      true)
                                    Expanded(
                                      child: ElevatedButton(
                                          onPressed: () {
                                            if (orderNotifier
                                                    .order
                                                    .displayReportId
                                                    ?.isNotEmpty ??
                                                false) {
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ViewReportPdf(
                                                              order:
                                                                  orderNotifier
                                                                      .order)));
                                            } else {
                                              showDialog(
                                                  context: context,
                                                  builder: (ctx) => HiBlissDialog(
                                                      context: context,
                                                      icon:
                                                          'res/icons/ic_alert_yellow.png',
                                                      message:
                                                          'Please create a report on\nthe Web Portal.'));
                                            }
                                          },
                                          child: Text('View Report')),
                                    )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  _startJob(BuildContext context) async {
    final _res = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => QrScanner()));
    if (_res != null && _res is String) {
      final _qrData = _res.split(",");
      if (_qrData.length != 3) {
        CommonUI(context: context).snackbar(message: 'Invalid QR code.');
      } else {
        context.read<LoadingState>().toggle(value: true);
        final _startJobRes = await TherapistJobRest(context: context)
            .startJob(bookingId: widget.bookingId, qrCode: _res);
        if (_startJobRes.httpCode == 0 && (_startJobRes.errorCode == null)) {
          final _jobRes = await TherapistJobRest(context: context).getJob(
              bookingId: widget.bookingId, jobCategory: widget.jobCategory);
          context
              .read<BookingOrderChangeNotifier>()
              .update(order: _jobRes.dataResults!);
          context.read<LoadingState>().toggle(value: false);
          showModalBottomSheet(
              context: context,
              isDismissible: true,
              isScrollControlled: true,
              enableDrag: true,
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20))),
              builder: (ctx) => Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(ctx).size.height > 727
                            ? 727
                            : MediaQuery.of(ctx).size.height),
                    height: 727,
                    child: Column(
                      children: [
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Image.asset('res/icons/ic_msg_check.png',
                                  width: 78),
                              Container(
                                margin: EdgeInsets.only(top: 30),
                                child: Text('Authentication Code',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline5
                                        ?.copyWith(fontFamily: 'Montserrat')),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 3, bottom: 30),
                                child: Text(
                                    General(context: context).formatDate(
                                        dateTime:
                                            '${_qrData[1]} ${_qrData[2]}'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(fontFamily: 'Montserrat')),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, bottom: 10),
                          child: Center(
                            child: Text('${_startJobRes.message}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    ?.copyWith(
                                        fontFamily: 'Montserrat',
                                        fontSize: 50)),
                          ),
                        ),
                        Divider(height: 1, color: Color(0xffcecece)),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: 40, top: 10, left: 10, right: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: ElevatedButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text('Okay')))
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ));
        } else {
          CommonUI(context: context)
              .snackbar(message: 'Failed to start job. Please try again.');
          context.read<LoadingState>().toggle(value: false);
        }
      }
    }
  }

  _endJob(BuildContext context) async {
    final _res = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => QrScanner()));
    if (_res != null && _res is String) {
      final _qrData = _res.split(",");
      if (_qrData.length != 6) {
        CommonUI(context: context).snackbar(message: 'Invalid QR code.');
      } else {
        context.read<LoadingState>().toggle(value: true);
        final _completeJobRes = await TherapistJobRest(context: context)
            .completeJob(bookingId: widget.bookingId, qrCode: _res);
        if (_completeJobRes.success) {
          final _jobRes = await TherapistJobRest(context: context).getJob(
              bookingId: widget.bookingId, jobCategory: widget.jobCategory);
          context
              .read<BookingOrderChangeNotifier>()
              .update(order: _jobRes.dataResults!);
          context.read<LoadingState>().toggle(value: false);
          Navigator.of(context).pop(true);
        } else {
          CommonUI(context: context).snackbar(
              message: "Failed to complete job. The QR code is invalid.");
          context.read<LoadingState>().toggle(value: false);
        }
      }
    }
  }
}
