import 'package:app/model/payout.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/payout.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';

class BankAccount extends StatelessWidget {
  const BankAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bank Account'),
      ),
      body: FutureProvider<Response2<PendingPayout>>(
        initialData: Response2(httpCode: -1),
        create: (context) => PayoutRest(context: context).listPending(),
        child: LoadingIndicator(
          child: Consumer<Response2<PendingPayout>>(
            builder: (context, payout, child) => payout.httpCode == -1
                ? StatelessLoadingIndicator()
                : Container(
                    margin: EdgeInsets.all(10),
                    child: BlocProvider(
                      create: (context) =>
                          BankAccountFormBloc(context: context, payout: payout.dataResults!),
                      child: Builder(builder: (context) {
                        final _bankAccountFormBloc = context.read<BankAccountFormBloc>();
                        return FormBlocListener<BankAccountFormBloc, Response, Response>(
                          onSuccess: (context, successResponse) {
                            CommonUI(context: context).snackbar(message: 'Saved successfully.');
                            Navigator.of(context).pop(true);
                          },
                          onFailure: (context, failureResponse) {
                            CommonUI(context: context).snackbar(
                                message:
                                    'Failed to save due to an error. If the issue persists, please contact Hi-Bliss customer service.');
                          },
                          child: CustomScrollView(
                            slivers: [
                              SliverToBoxAdapter(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Text(
                                          'Please enter your preferred bank account to receive your payment.',
                                          style: Theme.of(context).textTheme.bodyText1),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 20, bottom: 4),
                                      child: Text('Bank Name',
                                          style: Theme.of(context).textTheme.headline5),
                                    ),
                                    TextFieldBlocBuilder(
                                      textFieldBloc: _bankAccountFormBloc.payoutBankName,
                                      style: Theme.of(context).textTheme.bodyText1,
                                      decoration: CommonUI(context: context).decoration2(),
                                      textInputAction: TextInputAction.next,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 20, bottom: 4),
                                      child: Text('Bank Account Number',
                                          style: Theme.of(context).textTheme.headline5),
                                    ),
                                    TextFieldBlocBuilder(
                                      textFieldBloc: _bankAccountFormBloc.payoutBankAccount,
                                      style: Theme.of(context).textTheme.bodyText1,
                                      keyboardType: TextInputType.numberWithOptions(
                                          signed: false, decimal: false),
                                      decoration: CommonUI(context: context).decoration2(),
                                      textInputAction: TextInputAction.next,
                                    ),
                                  ],
                                ),
                              ),
                              SliverFillRemaining(
                                hasScrollBody: false,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 40),
                                        child: ElevatedButton(
                                            child: Text('Save'),
                                            onPressed: _bankAccountFormBloc.submit),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

class BankAccountFormBloc extends FormBloc<Response, Response> {
  BuildContext context;
  PendingPayout payout;

  final payoutBankName =
      TextFieldBloc(name: 'payoutBankName', validators: [FieldBlocValidators.required]);
  final payoutBankAccount =
      TextFieldBloc(name: 'payoutBankAccount', validators: [FieldBlocValidators.required]);

  BankAccountFormBloc({required this.context, required this.payout}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [payoutBankName, payoutBankAccount]);
    payoutBankName.updateInitialValue(payout.payoutBankName);
    payoutBankAccount.updateInitialValue(payout.payoutBankAccount);
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      final _res = await PayoutRest(context: context)
          .editBank(bankName: payoutBankName.value!, bankAccount: payoutBankAccount.value!);
      if (_res.success) {
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}
