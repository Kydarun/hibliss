import 'package:app/model/payout.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/payout.dart';
import 'package:app/views/account/receipt.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReceivedPayoutBreakdown extends StatelessWidget {
  final ReceivedPayout payout;

  const ReceivedPayoutBreakdown({required this.payout, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${payout.transactionDate}'),
      ),
      body: FutureProvider<Response2<ReceivedPayoutDetail>>(
        initialData: Response2(httpCode: -1),
        create: (context) => PayoutRest(context: context).getReceived(payoutId: payout.payoutId!),
        child: Consumer<Response2<ReceivedPayoutDetail>>(
            builder: (context, payouts, child) => payouts.httpCode == -1
                ? StatelessLoadingIndicator()
                : Column(
                    children: [
                      Expanded(
                        child: ListView(
                          children: payouts.dataResults?.receivedPayout
                                  ?.map((payout) => Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Column(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(horizontal: 5),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(right: 10),
                                                    child: Text(payout.appointmentDate!,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1
                                                            ?.copyWith(
                                                                fontSize: 10,
                                                                color: Color(0xff707070))),
                                                  ),
                                                  Expanded(
                                                    child: Divider(
                                                      height: 1,
                                                      color: Color(0xffa0a0a0),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            ...payout.orderDetails
                                                    ?.map((order) => Container(
                                                          margin: EdgeInsets.only(
                                                              top: 15, left: 10, right: 10),
                                                          child: Row(
                                                            children: [
                                                              Image.asset(
                                                                  'res/icons/ic_payout_history.png',
                                                                  width: 38),
                                                              Expanded(
                                                                child: Container(
                                                                  margin: EdgeInsets.only(left: 15),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment.start,
                                                                    children: [
                                                                      Text(order.orderId!,
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .bodyText1
                                                                              ?.copyWith(
                                                                                  fontWeight:
                                                                                      FontWeight
                                                                                          .bold)),
                                                                      Text(order.timeSlot!,
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .bodyText1
                                                                              ?.copyWith(
                                                                                  fontSize: 12))
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(order.amount!,
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .headline5
                                                                      ?.copyWith(
                                                                          color: Color(0xff0095cf)))
                                                            ],
                                                          ),
                                                        ))
                                                    .toList() ??
                                                []
                                          ],
                                        ),
                                      ))
                                  .toList() ??
                              [],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                              child: ElevatedButton(
                                  child: Text('View PDF'),
                                  onPressed: () {
                                    if (payouts.dataResults?.pdfReceiptLink?.isNotEmpty ?? false) {
                                      Navigator.of(context).push(MaterialPageRoute(
                                          builder: (context) =>
                                              PayoutReceipt(payout: payouts.dataResults!)));
                                    } else {
                                      CommonUI(context: context)
                                          .snackbar(message: 'No PDF documents available.');
                                    }
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
      ),
    );
  }
}
