import 'package:app/model/payout.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/therapist/payout.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';

class PayoutSettings extends StatelessWidget {
  final _settings = [
    _PayoutSetting(
        value: 'Weekly',
        title: 'Weekly Payout',
        description: 'Payout on every Saturday.'),
    _PayoutSetting(
        value: 'Bi-weekly',
        title: 'Bi-Weekly Payout',
        description: 'Payout on 15th & 28th of every month.'),
    _PayoutSetting(
        value: 'Monthly',
        title: 'Monthly Payout',
        description: 'Payout on 28th of every month.')
  ];

  PayoutSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payout Settings'),
      ),
      body: FutureProvider<Response2<PendingPayout>>(
        initialData: Response2(httpCode: -1),
        create: (context) => PayoutRest(context: context).listPending(),
        child: LoadingIndicator(
          child: Consumer<Response2<PendingPayout>>(
            builder: (context, payout, child) => payout.httpCode == -1
                ? StatelessLoadingIndicator()
                : Container(
                    margin: EdgeInsets.all(10),
                    child: BlocProvider(
                      create: (context) => PayoutsettingsFormBloc(
                          context: context, payout: payout.dataResults!),
                      child: Builder(builder: (context) {
                        final _payoutSettingsFormBloc =
                            context.read<PayoutsettingsFormBloc>();
                        return FormBlocListener<PayoutsettingsFormBloc,
                            Response, Response>(
                          onSuccess: (context, successResponse) {
                            CommonUI(context: context)
                                .snackbar(message: 'Saved successfully.');
                            Navigator.of(context).pop(true);
                          },
                          onFailure: (context, failureResponse) {
                            CommonUI(context: context).snackbar(
                                message:
                                    'Failed to save due to an error. If the issue persists, please contact Hi-Bliss customer service.');
                          },
                          child: CustomScrollView(
                            slivers: [
                              SliverToBoxAdapter(
                                child: Column(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 10, bottom: 20),
                                      child: Text(
                                          'Please select the frequency of your revenue payout.',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1),
                                    ),
                                    Container(
                                        child:
                                            BlocBuilder<
                                                    InputFieldBloc<String,
                                                        Object>,
                                                    InputFieldBlocState>(
                                                bloc: _payoutSettingsFormBloc
                                                    .payoutFrequency,
                                                builder:
                                                    (context, state) => Column(
                                                          children: _settings
                                                              .map(
                                                                  (setting) =>
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          _payoutSettingsFormBloc
                                                                              .payoutFrequency
                                                                              .updateValue(setting.value);
                                                                        },
                                                                        child:
                                                                            Container(
                                                                          margin:
                                                                              EdgeInsets.only(bottom: 5),
                                                                          padding: EdgeInsets.symmetric(
                                                                              vertical: 15,
                                                                              horizontal: 20),
                                                                          decoration: BoxDecoration(
                                                                              borderRadius: BorderRadius.circular(7),
                                                                              color: Colors.white),
                                                                          child:
                                                                              Row(
                                                                            children: [
                                                                              Image.asset('res/icons/ic_viewplan.png', width: 20),
                                                                              Expanded(
                                                                                child: Container(
                                                                                  margin: EdgeInsets.symmetric(horizontal: 15),
                                                                                  child: Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    children: [
                                                                                      Text(setting.title, style: Theme.of(context).textTheme.headline5),
                                                                                      Text(setting.description, style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 12))
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Image.asset('res/icons/${_payoutSettingsFormBloc.payoutFrequency.value == setting.value ? 'ic_msg_check.png' : 'ic_check_border.png'}', width: 20)
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ))
                                                              .toList(),
                                                        )))
                                  ],
                                ),
                              ),
                              SliverFillRemaining(
                                hasScrollBody: false,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 40),
                                        child: ElevatedButton(
                                            child: Text('Save'),
                                            onPressed:
                                                _payoutSettingsFormBloc.submit),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

class PayoutSettingsFormBloc extends FormBloc<Response, Response> {
  BuildContext context;
  PendingPayout payout;

  final payoutSettings = InputFieldBloc<String, Object>(
      name: 'payoutSettings', validators: [FieldBlocValidators.required]);

  PayoutSettingsFormBloc({required this.context, required this.payout})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [payoutSettings]);
    payoutSettings.updateInitialValue(payout.payoutFrequency);
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      final _res = await PayoutRest(context: context)
          .editPayout(payout: payoutSettings.value!);
      if (_res.success) {
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}

class PayoutsettingsFormBloc extends FormBloc<Response, Response> {
  BuildContext context;
  PendingPayout payout;

  final payoutFrequency = InputFieldBloc<String, Object>(
      name: 'payoutFrequency', validators: [FieldBlocValidators.required]);

  PayoutsettingsFormBloc({required this.context, required this.payout})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [payoutFrequency]);
    payoutFrequency.updateInitialValue(payout.payoutFrequency);
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      debugPrint('${payoutFrequency.value}');
      final _res = await PayoutRest(context: context)
          .editPayout(payout: payoutFrequency.value!);
      if (_res.success) {
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}

class _PayoutSetting {
  String value;
  String title;
  String description;

  _PayoutSetting({
    required this.value,
    required this.title,
    required this.description,
  });
}
