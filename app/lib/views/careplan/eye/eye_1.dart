import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class EyeStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const EyeStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Eye'),
              Text('1 of 1',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => EyeFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<EyeFormBlocStep1, CarePlanViewModel, CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  CreateCarePlanStepThree(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _eyeFormBloc = context.read<EyeFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, bottom: 5, left: 10),
                                            child: Text('Are you suffering from any of these?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _eyeFormBloc.dryEyes,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Dry Eyes',
                                                onChecked: (option) {
                                                  _eyeFormBloc.dryEyes.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _eyeFormBloc.wateryEye,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Watery Eyes',
                                                onChecked: (option) {
                                                  _eyeFormBloc.wateryEye.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _eyeFormBloc.itchyEye,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Itchy Eyes',
                                                onChecked: (option) {
                                                  _eyeFormBloc.itchyEye.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _eyeFormBloc.strainEyes,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Strained Eyes',
                                                onChecked: (option) {
                                                  _eyeFormBloc.strainEyes.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _eyeFormBloc.darkCircle,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Dark Circle',
                                                onChecked: (option) {
                                                  _eyeFormBloc.darkCircle.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _eyeFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _eyeFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class EyeFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final dryEyes = InputFieldBloc<String, Object>(name: 'dryEyes', initialValue: "No");
  final wateryEye = InputFieldBloc<String, Object>(name: 'wateryEye', initialValue: "No");
  final itchyEye = InputFieldBloc<String, Object>(name: 'itchyEye', initialValue: "No");
  final strainEyes = InputFieldBloc<String, Object>(name: 'strainEyes', initialValue: "No");
  final darkCircle = InputFieldBloc<String, Object>(name: 'darkCircle', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  EyeFormBlocStep1({required this.context, required this.viewModel}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [dryEyes, wateryEye, itchyEye, strainEyes, darkCircle, comments]);
  }

  @override
  void onSubmitting() {
    if (viewModel.eye == null) {
      viewModel.eye = CarePlanEye();
    }
    emitSuccess(
        successResponse: viewModel
          ..eye?.dryEyes = dryEyes.value
          ..eye?.wateryEye = wateryEye.value
          ..eye?.itchyEye = itchyEye.value
          ..eye?.strainEyes = strainEyes.value
          ..eye?.darkCircle = darkCircle.value
          ..eye?.comments = comments.value,
        canSubmitAgain: true);
  }
}
