import 'package:app/model/address.dart';
import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/address.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/datepicker/flutter_datetime_picker.dart';
import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_2.dart';
import 'package:app/views/profile/address/list.dart';
import 'package:app/views/profile/profile.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';

class CreateCarePlanStepOne extends StatelessWidget {
  final int relationshipId;
  const CreateCarePlanStepOne({required this.relationshipId, Key? key})
      : super(key: key);

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Care Plan'),
              Text('1 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: FutureProvider<Response<User>>(
            create: (context) async {
              if (relationshipId == 1) {
                return await UserRest(context: context).me();
              } else {
                return Response(httpCode: 0);
              }
            },
            initialData: Response(httpCode: -1),
            child: Consumer<Response<User>>(
                builder: (context, response, child) => response.httpCode == -1
                    ? StatelessLoadingIndicator()
                    : Container(
                        margin: EdgeInsets.only(top: 15),
                        child: BlocProvider(
                            create: (context) => CarePlanFormBlocStep1(
                                context: context,
                                user: response.dataResults?[0],
                                relationshipId: relationshipId),
                            child: Builder(
                              builder: (context) {
                                return FormBlocListener<CarePlanFormBlocStep1,
                                        CarePlanViewModel, CarePlanViewModel>(
                                    onSuccess: (context, success) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          CreateCarePlanStepTwo(
                                              viewModel:
                                                  success.successResponse!)));
                                }, onFailure: (context, failure) {
                                  CommonUI(context: context)
                                      .snackbar(message: 'An error occured.');
                                }, child: Builder(
                                  builder: (context) {
                                    final _carePlanBloc =
                                        context.read<CarePlanFormBlocStep1>();
                                    return Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 15),
                                      child: CustomScrollView(
                                        slivers: [
                                          SliverToBoxAdapter(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Full Name',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                TextFieldBlocBuilder(
                                                  textFieldBloc:
                                                      _carePlanBloc.name,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2,
                                                  decoration:
                                                      CommonUI(context: context)
                                                          .decoration2(),
                                                  textInputAction:
                                                      TextInputAction.next,
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Gender',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                BlocBuilder<
                                                    SelectFieldBloc<Gender,
                                                        dynamic>,
                                                    SelectFieldBlocState<Gender,
                                                        dynamic>>(
                                                  bloc: _carePlanBloc.gender,
                                                  builder: (context, state) =>
                                                      GestureDetector(
                                                    onTap: () {
                                                      showCupertinoModalPopup(
                                                          context: context,
                                                          builder:
                                                              (context) =>
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        bottom:
                                                                            35,
                                                                        left: 7,
                                                                        right:
                                                                            7),
                                                                    child:
                                                                        CupertinoActionSheet(
                                                                      title: Text(
                                                                          'Choose Gender'),
                                                                      cancelButton: CupertinoActionSheetAction(
                                                                          isDestructiveAction: false,
                                                                          onPressed: () {
                                                                            Navigator.of(context).pop();
                                                                          },
                                                                          child: Text('Cancel')),
                                                                      actions: [
                                                                        Gender
                                                                            .male,
                                                                        Gender
                                                                            .female
                                                                      ]
                                                                          .map((gender) => CupertinoActionSheetAction(
                                                                              onPressed: () {
                                                                                Navigator.of(context).pop();
                                                                                _carePlanBloc.gender.updateValue(gender);
                                                                              },
                                                                              child: Text(gender.name)))
                                                                          .toList(),
                                                                    ),
                                                                  ));
                                                    },
                                                    child: TextField(
                                                      enabled: false,
                                                      controller:
                                                          TextEditingController(
                                                              text: state.value
                                                                      ?.name ??
                                                                  ''),
                                                      decoration: CommonUI(
                                                              context: context)
                                                          .decoration2()
                                                          .copyWith(
                                                              disabledBorder: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  borderSide: BorderSide(
                                                                      color: Color(
                                                                          0x73bcd5ed),
                                                                      width:
                                                                          1)),
                                                              suffixIcon: Icon(Icons
                                                                  .arrow_drop_down)),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Date of Birth',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                BlocBuilder<
                                                        InputFieldBloc<DateTime,
                                                            Object>,
                                                        InputFieldBlocState>(
                                                    bloc: _carePlanBloc.dob,
                                                    builder:
                                                        (context, state) =>
                                                            GestureDetector(
                                                              onTap: () {
                                                                showCupertinoModalPopup(
                                                                    context:
                                                                        context,
                                                                    builder:
                                                                        (context) =>
                                                                            Container(
                                                                              height: 461,
                                                                              decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)), color: CupertinoColors.white),
                                                                              child: Column(
                                                                                children: [
                                                                                  Container(
                                                                                    margin: EdgeInsets.symmetric(horizontal: 13),
                                                                                    height: 371,
                                                                                    child: CustomDatePickerWidget(
                                                                                        pickerModel: DatePickerModel(
                                                                                          currentTime: _carePlanBloc.dob.value ?? DateTime.parse('1990-06-15'),
                                                                                          minTime: DateTime.parse('1900-01-01'),
                                                                                          maxTime: DateTime.now(),
                                                                                        ),
                                                                                        theme: DatePickerTheme(containerHeight: 371, itemHeight: 65, itemStyle: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 30)),
                                                                                        onChanged: (time) {
                                                                                          _carePlanBloc.dob.updateValue(time);
                                                                                        }),
                                                                                  ),
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                                                                                    child: Row(
                                                                                      children: [
                                                                                        Expanded(
                                                                                          child: ElevatedButton(
                                                                                              child: Text('Okay'),
                                                                                              onPressed: () {
                                                                                                Navigator.of(context).pop();
                                                                                              }),
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ));
                                                              },
                                                              child: TextField(
                                                                enabled: false,
                                                                controller: TextEditingController(
                                                                    text: state.value !=
                                                                            null
                                                                        ? '${DateFormat('dd MMM yyyy').format(state.value!)}'
                                                                        : null),
                                                                decoration: CommonUI(
                                                                        context:
                                                                            context)
                                                                    .decoration2()
                                                                    .copyWith(
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(
                                                                              7),
                                                                          borderSide: BorderSide(
                                                                              color: Color(0x73bcd5ed),
                                                                              width: 1)),
                                                                      suffixIcon:
                                                                          Icon(Icons
                                                                              .arrow_drop_down),
                                                                      errorText: state
                                                                              .hasError
                                                                          ? 'Date of Birth is required.'
                                                                          : null,
                                                                    ),
                                                              ),
                                                            )),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Contact',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                TextFieldBlocBuilder(
                                                  textFieldBloc:
                                                      _carePlanBloc.contact,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  decoration:
                                                      CommonUI(context: context)
                                                          .decoration2(),
                                                  textInputAction:
                                                      TextInputAction.next,
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Email',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                TextFieldBlocBuilder(
                                                  textFieldBloc:
                                                      _carePlanBloc.email,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2,
                                                  decoration:
                                                      CommonUI(context: context)
                                                          .decoration2(),
                                                  textInputAction:
                                                      TextInputAction.next,
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 20),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 9),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        bottom:
                                                                            4),
                                                                child: Text(
                                                                    'Weight (kg)',
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .subtitle1),
                                                              ),
                                                              TextFieldBlocBuilder(
                                                                textFieldBloc:
                                                                    _carePlanBloc
                                                                        .weight,
                                                                keyboardType: TextInputType
                                                                    .numberWithOptions(
                                                                        signed:
                                                                            false,
                                                                        decimal:
                                                                            false),
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText2,
                                                                decoration: CommonUI(
                                                                        context:
                                                                            context)
                                                                    .decoration2(),
                                                                textInputAction:
                                                                    TextInputAction
                                                                        .next,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 9),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        bottom:
                                                                            4),
                                                                child: Text(
                                                                    'Height (cm)',
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .subtitle1),
                                                              ),
                                                              TextFieldBlocBuilder(
                                                                textFieldBloc:
                                                                    _carePlanBloc
                                                                        .height,
                                                                keyboardType: TextInputType
                                                                    .numberWithOptions(
                                                                        signed:
                                                                            false,
                                                                        decimal:
                                                                            false),
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText2,
                                                                decoration: CommonUI(
                                                                        context:
                                                                            context)
                                                                    .decoration2(),
                                                                textInputAction:
                                                                    TextInputAction
                                                                        .next,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 4),
                                                  child: Text('Address',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle1),
                                                ),
                                                if (relationshipId == 1)
                                                  BlocBuilder<
                                                          InputFieldBloc<
                                                              Address, Object>,
                                                          InputFieldBlocState>(
                                                      bloc:
                                                          _carePlanBloc.address,
                                                      builder: (context,
                                                              state) =>
                                                          LoadingIndicator(
                                                            child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      bottom:
                                                                          10),
                                                              child:
                                                                  _AddressDropdown(
                                                                onSelected:
                                                                    (address) {
                                                                  _carePlanBloc
                                                                      .address
                                                                      .updateValue(
                                                                          address);
                                                                },
                                                              ),
                                                            ),
                                                          ))
                                                else ...[
                                                  TextFieldBlocBuilder(
                                                    textFieldBloc: _carePlanBloc
                                                        .addressLong,
                                                    minLines: 2,
                                                    maxLines: 2,
                                                    decoration: CommonUI(
                                                            context: context)
                                                        .decoration2(),
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20, bottom: 4),
                                                    child: Text('State',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .subtitle1),
                                                  ),
                                                  DropdownFieldBlocBuilder<
                                                      String>(
                                                    selectFieldBloc:
                                                        _carePlanBloc
                                                            .addressState,
                                                    itemBuilder:
                                                        (context, value) =>
                                                            value,
                                                    decoration: CommonUI(
                                                            context: context)
                                                        .decoration2(),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20, bottom: 4),
                                                    child: Text('Postcode',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .subtitle1),
                                                  ),
                                                  TextFieldBlocBuilder(
                                                    textFieldBloc:
                                                        _carePlanBloc.postcode,
                                                    decoration: CommonUI(
                                                            context: context)
                                                        .decoration2(),
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  ),
                                                ],
                                              ],
                                            ),
                                          ),
                                          SliverFillRemaining(
                                            hasScrollBody: false,
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 40),
                                                    child: ElevatedButton(
                                                        child: Text('Next'),
                                                        onPressed: _carePlanBloc
                                                            .submit),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ));
                              },
                            )),
                      )),
          ),
        ));
  }
}

class CarePlanFormBlocStep1
    extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  User? user;
  int relationshipId;
  CarePlanViewModel? viewModel;

  final name = TextFieldBloc(
      name: 'fullname', validators: [FieldBlocValidators.required]);

  final gender = SelectFieldBloc<Gender, dynamic>(
      name: 'gender', items: [Gender.empty, Gender.male, Gender.female]);

  final dob = InputFieldBloc<DateTime, Object>(
      name: 'birthday', validators: [FieldBlocValidators.required]);

  final contact = TextFieldBloc(
      name: 'phoneNumber', validators: [FieldBlocValidators.required]);

  final email = TextFieldBloc(
      name: 'email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final weight = TextFieldBloc(
      name: 'weight', validators: [FieldBlocValidators.required, moreThanOne]);

  final height = TextFieldBloc(
      name: 'height', validators: [FieldBlocValidators.required, moreThanOne]);

  final address = InputFieldBloc<Address, Object>(
      name: 'address', validators: [FieldBlocValidators.required]);

  final addressLong = TextFieldBloc(
      name: 'addressLong', validators: [FieldBlocValidators.required]);

  final addressState = SelectFieldBloc<String, dynamic>(
      name: 'addressState',
      items: [],
      validators: [FieldBlocValidators.required]);

  final postcode = TextFieldBloc(
      name: 'postcode', validators: [FieldBlocValidators.required]);

  CarePlanFormBlocStep1(
      {required this.context,
      required this.relationshipId,
      this.user,
      this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(
        fieldBlocs: [name, dob, gender, contact, email, weight, height]);
    if (relationshipId == 1) {
      addFieldBlocs(fieldBlocs: [address]);
    } else {
      addFieldBlocs(fieldBlocs: [addressLong, addressState, postcode]);
    }
    AddressRest(context: context).getStates().then((value) {
      addressState.updateItems(
          value.dataResults!.map<String>((data) => data.stateName).toList());
    });
    name.updateInitialValue(user?.name ?? '');
    dob.updateInitialValue(user?.birthday);
    if (user?.gender != null) {
      switch (user!.gender) {
        case 0:
          gender.updateValue(Gender.empty);
          break;
        case 1:
          gender.updateValue(Gender.male);
          break;
        case 2:
          gender.updateValue(Gender.female);
          break;
      }
    }
    contact.updateInitialValue(user?.phoneNumber ?? '');
    email.updateInitialValue(user?.email ?? '');
    weight.updateInitialValue(user?.weight?.toString() ?? '');
    height.updateInitialValue(user?.height?.toString() ?? '');
  }

  static String? moreThanOne(String? input) {
    if (input == null) {
      return "This field is required.";
    } else if (double.tryParse(input) == null) {
      return "Must be a number.";
    } else if (double.tryParse(input)! < 1) {
      return "Must be more than 1.";
    }
    return null;
  }

  @override
  void onSubmitting() {
    if (viewModel == null) {
      viewModel = CarePlanViewModel()..patient = CarePlanPatient();
    }
    if (relationshipId == 1) {
      emitSuccess(
          successResponse: viewModel
            ?..patient?.name = name.value
            ..patient?.relationshipId = relationshipId
            ..patient?.birthday = dob.value
            ..patient?.gender = gender.value?.value.toString()
            ..patient?.phoneNumber = contact.value
            ..patient?.email = email.value
            ..patient?.weight = weight.valueToDouble
            ..patient?.height = height.valueToDouble
            ..patient?.address = address.value?.address
            ..patient?.state = address.value?.state
            ..patient?.postalCode = address.value?.postalCode,
          canSubmitAgain: true);
    } else {
      emitSuccess(
          successResponse: viewModel
            ?..patient?.name = name.value
            ..patient?.relationshipId = relationshipId
            ..patient?.birthday = dob.value
            ..patient?.gender = gender.value?.value.toString()
            ..patient?.phoneNumber = contact.value
            ..patient?.email = email.value
            ..patient?.weight = weight.valueToDouble
            ..patient?.height = height.valueToDouble
            ..patient?.address = addressLong.value
            ..patient?.state = addressState.value
            ..patient?.postalCode = postcode.value,
          canSubmitAgain: true);
    }
  }
}

class _AddressDropdown extends StatefulWidget {
  final Address? initialValue;
  final Function(Address? address) onSelected;

  const _AddressDropdown(
      {required this.onSelected, this.initialValue, Key? key})
      : super(key: key);

  @override
  __AddressDropdownState createState() => __AddressDropdownState();
}

class __AddressDropdownState extends State<_AddressDropdown> {
  bool _isAddressExpanded = false;
  List<Address> _addresses = [];
  Address? _selected;

  Address? get selected => _selected;

  @override
  initState() {
    super.initState();
    _load();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 13, horizontal: 15),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: InkWell(
            onTap: () {
              setState(() {
                _isAddressExpanded = !_isAddressExpanded;
              });
            },
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(_selected?.title ?? '',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(fontWeight: FontWeight.bold)),
                      Container(
                        margin: EdgeInsets.only(top: 1),
                        child: Text(_selected?.address ?? '',
                            style: Theme.of(context).textTheme.bodyText1,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      )
                    ],
                  ),
                ),
                Icon(
                    _isAddressExpanded
                        ? Icons.arrow_drop_up
                        : Icons.arrow_drop_down,
                    color: Colors.black)
              ],
            ),
          ),
        ),
        if (_isAddressExpanded)
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            margin: EdgeInsets.only(top: 3, bottom: 40),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(3),
                  topRight: Radius.circular(3),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                )),
            child: Row(
              children: [
                Expanded(
                  child: Column(children: [
                    ..._addresses
                        .map((address) => InkWell(
                              onTap: () {
                                widget.onSelected(address);
                                setState(() {
                                  _selected = address;
                                  _isAddressExpanded = false;
                                });
                              },
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffcecece)))),
                                padding: EdgeInsets.symmetric(vertical: 13),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(address.title ?? '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            ?.copyWith(
                                                fontWeight: FontWeight.bold)),
                                    Container(
                                      margin: EdgeInsets.only(top: 1),
                                      child: Text(address.address ?? '',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis),
                                    )
                                  ],
                                ),
                              ),
                            ))
                        .toList(),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                              onTap: () {
                                Navigator.of(context)
                                    .push(MaterialPageRoute(
                                        builder: (context) => AddressList()))
                                    .then((_) {
                                  setState(() {
                                    _isAddressExpanded = false;
                                  });
                                  _load();
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 15),
                                child: Center(
                                  child: Text('Add New',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(
                                              color: Color(0xff0095cf),
                                              fontWeight: FontWeight.bold)),
                                ),
                              )),
                        ),
                      ],
                    )
                  ]),
                ),
              ],
            ),
          )
      ],
    );
  }

  Future<void> _load() async {
    try {
      //context.read<LoadingState>().toggle(value: true);
      final _res = await AddressRest(context: context).list();
      setState(() {
        _addresses = _res.dataResults ?? [];
        if (_addresses.length > 0) {
          if (_selected == null || !_addresses.contains(_selected)) {
            _selected =
                _addresses.where((element) => element.isPrimary == "Yes").first;
            widget.onSelected(_selected);
          }
        }
      });
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrintStack(stackTrace: stacktrace);
    } finally {
      //context.read<LoadingState>().toggle(value: false);
    }
  }
}
