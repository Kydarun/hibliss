import 'dart:typed_data';

import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/stress/stress_2.dart';
import 'package:app/views/careplan/stroke/stroke_3.dart';
import 'package:app/views/shared/drawing.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class StrokeStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  StrokeStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Post-stroke'),
              Text('2 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) =>
                    StrokeFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<StrokeFormBlocStep2,
                            CarePlanViewModel, CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => StrokeStep3(
                                  viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _strokeFormBloc =
                                context.read<StrokeFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BlocBuilder<InputFieldBloc<Uint8List, Object>,
                                      InputFieldBlocState>(
                                    bloc: _strokeFormBloc.affectedArea,
                                    builder: (context, state) =>
                                        PainMap(onDrew: (image) {
                                      _strokeFormBloc.affectedArea
                                          .updateValue(image);
                                      debugPrint('$image');
                                    }),
                                  ),
                                  Expanded(
                                    child: CustomScrollView(
                                      slivers: [
                                        SliverToBoxAdapter(
                                          child: Container(),
                                        ),
                                        SliverFillRemaining(
                                          hasScrollBody: false,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 40),
                                                  child: ElevatedButton(
                                                      child: Text('Next'),
                                                      onPressed: _strokeFormBloc
                                                          .submit),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class StrokeFormBlocStep2
    extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final affectedArea = InputFieldBloc<Uint8List, Object>(name: 'affectedArea');

  StrokeFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [affectedArea]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel..affectedArea = affectedArea.value,
        canSubmitAgain: true);
  }
}
