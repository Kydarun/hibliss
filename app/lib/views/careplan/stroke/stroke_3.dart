import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class StrokeStep3 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const StrokeStep3({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Post-stroke'),
              Text('3 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => StrokeFormBlocStep3(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<StrokeFormBlocStep3, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  CreateCarePlanStepThree(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _painFormBloc = context.read<StrokeFormBlocStep3>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, bottom: 5, left: 10),
                                            child: Text('Are you suffering from any of these?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.weakness,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Weakness',
                                                onChecked: (option) {
                                                  _painFormBloc.weakness.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.paralysis,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Paralysis',
                                                onChecked: (option) {
                                                  _painFormBloc.paralysis.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.poorBalance,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Poor balance & coordination',
                                                onChecked: (option) {
                                                  _painFormBloc.poorBalance.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.painNumbness,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Pain & numbness',
                                                onChecked: (option) {
                                                  _painFormBloc.painNumbness.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.burning,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Burning & tingling sensation',
                                                onChecked: (option) {
                                                  _painFormBloc.burning.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _painFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _painFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class StrokeFormBlocStep3 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final weakness = InputFieldBloc<String, Object>(name: 'weakness', initialValue: "No");
  final paralysis = InputFieldBloc<String, Object>(name: 'paralysis', initialValue: "No");
  final poorBalance = InputFieldBloc<String, Object>(name: 'poorBalance', initialValue: "No");
  final painNumbness = InputFieldBloc<String, Object>(name: 'painNumbness', initialValue: "No");
  final burning = InputFieldBloc<String, Object>(name: 'burning', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  StrokeFormBlocStep3({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [weakness, paralysis, poorBalance, painNumbness, burning, comments]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..stroke?.weakness = weakness.value
          ..stroke?.paralysis = paralysis.value
          ..stroke?.poorBalance = poorBalance.value
          ..stroke?.painNumbness = painNumbness.value
          ..stroke?.burning = burning.value
          ..stroke?.comments = comments.value,
        canSubmitAgain: true);
  }
}
