import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/stroke/stroke_2.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/toggle.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class StrokeStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const StrokeStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Post-stroke'),
              Text('1 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => StrokeFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<StrokeFormBlocStep1, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  StrokeStep2(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _strokeFormBloc = context.read<StrokeFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, left: 5, right: 5),
                                            child: Text(
                                                'How long have you been suffering from stroke?')),
                                        Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(right: 8),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(left: 5),
                                                        child: Text('Year',
                                                            style: Theme.of(context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontWeight: FontWeight.bold)),
                                                      ),
                                                      TextFieldBlocBuilder(
                                                        textFieldBloc: _strokeFormBloc.sufferYears,
                                                        decoration: CommonUI(context: context)
                                                            .decoration2(),
                                                        keyboardType:
                                                            TextInputType.numberWithOptions(
                                                                decimal: false),
                                                        textInputAction: TextInputAction.next,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(left: 8),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(left: 5),
                                                        child: Text('Month',
                                                            style: Theme.of(context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontWeight: FontWeight.bold)),
                                                      ),
                                                      TextFieldBlocBuilder(
                                                        textFieldBloc: _strokeFormBloc.sufferMonths,
                                                        decoration: CommonUI(context: context)
                                                            .decoration2(),
                                                        keyboardType:
                                                            TextInputType.numberWithOptions(
                                                                signed: false, decimal: false),
                                                        textInputAction: TextInputAction.next,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.dailyActivities,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(
                                                top: 15, bottom: 15, left: 10, right: 10),
                                            child: ToggleControl(
                                                text:
                                                    'Are you able to carry out daily activities without a caretaker?',
                                                errorText: state.hasError
                                                    ? 'This field is required.'
                                                    : null,
                                                onChanged: (option) {
                                                  _strokeFormBloc.dailyActivities
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 20, bottom: 5, left: 10, right: 10),
                                            child: Text('Level of stamina')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.levelOfStamina,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: VerticalToggleControl(
                                              onChanged: (option) {
                                                _strokeFormBloc.levelOfStamina.updateValue(option);
                                              },
                                              errorText:
                                                  state.hasError ? 'This field is required.' : null,
                                            ),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 15, left: 10, right: 10),
                                            child: Text(
                                                'Do you need any devices to assist your daily activities?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.walkingStick,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Walking Stick',
                                                onChecked: (option) {
                                                  _strokeFormBloc.walkingStick.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.wheelchair,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Wheelchair',
                                                onChecked: (option) {
                                                  _strokeFormBloc.wheelchair.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.walkingFrame,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Walking Frame',
                                                onChecked: (option) {
                                                  _strokeFormBloc.walkingFrame.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.splint,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Splint',
                                                onChecked: (option) {
                                                  _strokeFormBloc.splint.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _strokeFormBloc.brace,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Brace',
                                                onChecked: (option) {
                                                  _strokeFormBloc.brace.updateValue(option);
                                                }),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _strokeFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class StrokeFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final sufferYears =
      TextFieldBloc<int>(name: 'sufferYears', validators: [FieldBlocValidators.required]);
  final sufferMonths =
      TextFieldBloc<int>(name: 'sufferMonths', validators: [FieldBlocValidators.required]);
  final dailyActivities = InputFieldBloc<String, Object>(
      name: 'dailyActivities', validators: [FieldBlocValidators.required]);
  final levelOfStamina = InputFieldBloc<String, Object>(
      name: 'levelOfStamina', validators: [FieldBlocValidators.required]);
  final walkingStick = InputFieldBloc<String, Object>(name: 'walkingStick', initialValue: "No");
  final wheelchair = InputFieldBloc<String, Object>(name: 'wheelchair', initialValue: "No");
  final walkingFrame = InputFieldBloc<String, Object>(name: 'walkingFrame', initialValue: "No");
  final splint = InputFieldBloc<String, Object>(name: 'splint', initialValue: "No");
  final brace = InputFieldBloc<String, Object>(name: 'brace', initialValue: "No");

  StrokeFormBlocStep1({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      sufferYears,
      sufferMonths,
      dailyActivities,
      levelOfStamina,
      walkingStick,
      wheelchair,
      walkingFrame,
      splint,
      brace
    ]);
  }

  @override
  void onSubmitting() {
    if (viewModel.stroke == null) {
      viewModel.stroke = CarePlanStroke();
    }
    emitSuccess(
        successResponse: viewModel
          ..stroke?.sufferYears = sufferYears.valueToInt
          ..stroke?.sufferMonths = sufferMonths.valueToInt
          ..stroke?.dailyActivities = dailyActivities.value
          ..stroke?.levelOfStamina = levelOfStamina.value
          ..stroke?.walkingStick = walkingStick.value
          ..stroke?.wheelchair = wheelchair.value
          ..stroke?.walkingFrame = walkingFrame.value
          ..stroke?.splint = splint.value
          ..stroke?.brace = brace.value,
        canSubmitAgain: true);
  }
}
