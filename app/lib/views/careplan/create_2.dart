import 'package:app/rest/careplan.dart';
import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/detox/detox_1.dart';
import 'package:app/views/careplan/eye/eye_1.dart';
import 'package:app/views/careplan/osteoarthritis/osteoarthritis_1.dart';
import 'package:app/views/careplan/pain/pain_1.dart';
import 'package:app/views/careplan/skin/skin_1.dart';
import 'package:app/views/careplan/stress/stress_1.dart';
import 'package:app/views/careplan/stroke/stroke_1.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';

class CreateCarePlanStepTwo extends StatelessWidget {
  final CarePlanViewModel viewModel;

  CreateCarePlanStepTwo({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Care Plan'),
              Text('2 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: FutureProvider<List<CarePlanDef>>(
          create: (context) => CarePlanRest(context: context).getDefs(),
          initialData: [],
          child: Consumer<List<CarePlanDef>>(
            builder: (context, carePlans, child) => Unauthenticated(
              child: Container(
                margin: EdgeInsets.only(top: 15),
                child: BlocProvider(
                    create: (context) =>
                        CarePlanFormBlocStep2(context: context, viewModel: viewModel),
                    child: Builder(
                      builder: (context) {
                        return FormBlocListener<CarePlanFormBlocStep2, CarePlanViewModel,
                                CarePlanViewModel>(
                            onSuccess: (context, success) {},
                            onFailure: (context, failure) {},
                            child: Builder(
                              builder: (context) {
                                final _carePlanBloc = context.read<CarePlanFormBlocStep2>();
                                return Container(
                                  margin: EdgeInsets.symmetric(horizontal: 15),
                                  child: CustomScrollView(
                                    slivers: [
                                      SliverToBoxAdapter(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                margin: EdgeInsets.only(top: 20, bottom: 15),
                                                child: Text('Please Select a category.')),
                                            ...carePlans.map((carePlan) => InkWell(
                                                  onTap: () {
                                                    nextStep(context, carePlan);
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(bottom: 5),
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius: BorderRadius.circular(10)),
                                                    padding: EdgeInsets.only(
                                                        left: 15, top: 15, bottom: 15, right: 10),
                                                    child: Stack(
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Image.asset(
                                                                'res/icons/img_careplan_round.png',
                                                                width: 42),
                                                            Container(
                                                              margin: EdgeInsets.only(left: 15),
                                                              child: Text(carePlan.title!,
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .headline5),
                                                            ),
                                                          ],
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            showCupertinoModalPopup(
                                                                context: context,
                                                                builder: (ctx) => Container(
                                                                      padding: EdgeInsets.symmetric(
                                                                          horizontal: 20),
                                                                      height: 382,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.only(
                                                                                  topLeft: Radius
                                                                                      .circular(20),
                                                                                  topRight: Radius
                                                                                      .circular(
                                                                                          20)),
                                                                          color: CupertinoColors
                                                                              .white),
                                                                      child: Column(
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment
                                                                                .start,
                                                                        children: [
                                                                          Container(
                                                                            margin: EdgeInsets.only(
                                                                                top: 30),
                                                                            child: Center(
                                                                              child: Text(
                                                                                  carePlan.title!,
                                                                                  style: Theme.of(
                                                                                          context)
                                                                                      .textTheme
                                                                                      .headline5),
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            margin: EdgeInsets.only(
                                                                                top: 5, bottom: 20),
                                                                            child: Center(
                                                                              child: Text(
                                                                                  carePlan
                                                                                      .subtitle!,
                                                                                  style: Theme.of(
                                                                                          context)
                                                                                      .textTheme
                                                                                      .bodyText1),
                                                                            ),
                                                                          ),
                                                                          ...carePlan.symptoms!
                                                                              .map(
                                                                                  (symptom) =>
                                                                                      Container(
                                                                                        margin: EdgeInsets.only(
                                                                                            bottom:
                                                                                                10),
                                                                                        child: Row(
                                                                                          children: [
                                                                                            Container(
                                                                                              height:
                                                                                                  7,
                                                                                              width:
                                                                                                  7,
                                                                                              decoration:
                                                                                                  new BoxDecoration(
                                                                                                color:
                                                                                                    Colors.black,
                                                                                                shape:
                                                                                                    BoxShape.circle,
                                                                                              ),
                                                                                            ),
                                                                                            Expanded(
                                                                                              child:
                                                                                                  Container(
                                                                                                margin:
                                                                                                    EdgeInsets.only(left: 10),
                                                                                                child:
                                                                                                    Text(symptom, style: Theme.of(context).textTheme.bodyText1),
                                                                                              ),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ))
                                                                              .toList(),
                                                                          Expanded(
                                                                            child: Container(
                                                                              margin:
                                                                                  EdgeInsets.only(
                                                                                      bottom: 40),
                                                                              child: Row(
                                                                                children: [
                                                                                  Expanded(
                                                                                    child:
                                                                                        ElevatedButton(
                                                                                            child: Text(
                                                                                                'Continue'),
                                                                                            onPressed:
                                                                                                () {
                                                                                              Navigator.of(context)
                                                                                                  .pop();
                                                                                              viewModel.type =
                                                                                                  carePlan.type;
                                                                                              nextStep(
                                                                                                  context,
                                                                                                  carePlan);
                                                                                            }),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ));
                                                          },
                                                          child: Align(
                                                            alignment: Alignment.topRight,
                                                            child: Container(
                                                              child: Image.asset(
                                                                  'res/icons/ic_info_green.png',
                                                                  width: 18),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ))
                                          ],
                                        ),
                                      ),
                                      SliverFillRemaining(
                                        hasScrollBody: false,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.only(bottom: 40),
                                                child: ElevatedButton(
                                                    child: Text('Next'),
                                                    onPressed: _carePlanBloc.submit),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            ));
                      },
                    )),
              ),
            ),
          ),
        ));
  }

  nextStep(BuildContext context, CarePlanDef def) {
    viewModel.type = def.type;
    switch (def.type) {
      case "pain":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => PainStep1(viewModel: viewModel)));
        break;
      case "osteoarthritis":
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => OsteoarthritisStep1(viewModel: viewModel)));
        break;
      case "detox":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DetoxStep1(viewModel: viewModel)));
        break;
      case "skin":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => SkinStep1(viewModel: viewModel)));
        break;
      case "stress":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => StressStep1(viewModel: viewModel)));
        break;
      case "eye":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => EyeStep1(viewModel: viewModel)));
        break;
      case "stroke":
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => StrokeStep1(viewModel: viewModel)));
        break;
    }
  }
}

class CarePlanFormBlocStep2 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final type =
      InputFieldBloc<String, Object>(name: 'type', validators: [FieldBlocValidators.required]);

  CarePlanFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: []);
  }

  @override
  void onSubmitting() {
    emitSuccess(successResponse: viewModel..type = type.value, canSubmitAgain: true);
  }
}
