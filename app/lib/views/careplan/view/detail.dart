import 'package:app/model/careplan.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/careplan.dart';
import 'package:app/views/careplan/view/pdf.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CarePlanDetail extends StatelessWidget {
  final CarePlan carePlan;

  const CarePlanDetail({required this.carePlan, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Care Plan'),
        ),
        body: FutureProvider<Response<CarePlan>>(
          initialData: Response(httpCode: -1),
          create: (context) =>
              CarePlanRest(context: context).get(carePlanId: carePlan.id!),
          child: Consumer<Response<CarePlan>>(
            builder: (context, carePlan, child) => carePlan.httpCode == -1
                ? StatelessLoadingIndicator()
                : Column(
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              margin: EdgeInsets.only(bottom: 5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(7),
                                  color: Colors.white),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      width: 84,
                                      height: 84,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          gradient: LinearGradient(
                                              colors: [
                                                Color(0x36018aca),
                                                Color(0x36afe5ff)
                                              ],
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight)),
                                      child: Center(
                                          child: Image.asset(
                                              'res/icons/ic_description.png',
                                              width: 36))),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(carePlan.dataResults![0].name!,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5),
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            child: Row(children: [
                                              Image.asset(
                                                  'res/icons/ic_shield.png',
                                                  width: 15),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                    'Care Plan: ${carePlan.dataResults![0].category}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyText1),
                                              )
                                            ]),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                    'res/icons/ic_clock.png',
                                                    width: 13),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Text(
                                                      carePlan.dataResults![0]
                                                          .submissionDate!,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(top: 20, left: 15, right: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Dear valued customer,',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1),
                                  Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Text(
                                          'We will get back to you within 2 business days from the date of the care plan submission.',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1)),
                                  Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Text(
                                          'Thank you,\nHi-Bliss Therapy.',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1)),
                                  Container(
                                      margin: EdgeInsets.only(top: 35),
                                      child: Text(
                                          DateFormat('dd MMM yyyy, hh:mm aa')
                                              .format(DateTime.now()),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(
                                                  color: Color(0xff727375))))
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(
                                  bottom: 40, left: 20, right: 20),
                              child: ElevatedButton(
                                  child: Text('View'),
                                  onPressed: () {
                                    if (carePlan.dataResults![0].pdfLink
                                            ?.isNotEmpty ??
                                        false) {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ViewCarePlanPdf(
                                                      carePlan: carePlan
                                                          .dataResults![0])));
                                    } else {
                                      CommonUI(context: context).snackbar(
                                          message:
                                              'PDF Report not available right now.');
                                    }
                                  }),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
          ),
        ));
  }
}
