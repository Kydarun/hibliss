import 'package:app/model/careplan.dart';
import 'package:app/model/notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ViewCarePlanPdf extends StatefulWidget {
  final CarePlan carePlan;

  const ViewCarePlanPdf({required this.carePlan, Key? key}) : super(key: key);

  @override
  _ViewCarePlanPdfState createState() => _ViewCarePlanPdfState();
}

class _ViewCarePlanPdfState extends State<ViewCarePlanPdf> {
  bool _loaded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.carePlan.name}'),
        actions: [
          IconButton(
              onPressed: () {
                showCupertinoModalPopup(
                    context: context,
                    builder: (context) => Container(
                          margin:
                              EdgeInsets.only(bottom: 35, left: 7, right: 7),
                          child: CupertinoActionSheet(
                            cancelButton: CupertinoActionSheetAction(
                                isDestructiveAction: false,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text('Cancel')),
                            actions: [
                              CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    launch(widget.carePlan.pdfLink!);
                                  },
                                  child: Text('Save')),
                              CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Share.share(widget.carePlan.pdfLink!);
                                  },
                                  child: Text('Share')),
                            ],
                          ),
                        ));
              },
              icon: Icon(Icons.share, color: Colors.white))
        ],
      ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            color: Color(0xff494949),
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              initialUrl: widget.carePlan.pdfLink,
              onPageFinished: (url) {
                setState(() {
                  _loaded = true;
                });
              },
            ),
          ),
          if (!_loaded)
            Container(child: Center(child: CircularProgressIndicator())),
        ],
      ),
    );
  }
}
