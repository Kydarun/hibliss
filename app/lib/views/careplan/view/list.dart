import 'package:app/main.dart';
import 'package:app/model/careplan.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/careplan.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/careplan/create_1.dart';
import 'package:app/views/careplan/view/detail.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CarePlanList extends StatefulWidget {
  const CarePlanList({Key? key}) : super(key: key);

  @override
  _CarePlanListState createState() => _CarePlanListState();
}

class _CarePlanListState extends State<CarePlanList> {
  final _toWhom = [
    "Myself",
    "Spouse",
    "Parents",
    "Grandparents",
    "Siblings",
    "Others"
  ];
  String _selectedToWhom = "Myself";

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CarePlanChangeNotifier>(
      create: (context) => CarePlanChangeNotifier(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('My Care Plan'),
          actions: [
            Consumer<Session>(
              builder: (context, session, child) => IconButton(
                  onPressed: () {
                    if (session.isLoggedIn) {
                      showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          builder: (ctx) => Container(
                                height: 558,
                                child: ListView(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 35),
                                      child: Center(
                                        child: Text('Create Care Plan',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5),
                                      ),
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 5, bottom: 20),
                                      child: Center(
                                        child: Text(
                                            'You are creating this plan for...',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2
                                                ?.copyWith(
                                                    color: Color(0xff5b5b79))),
                                      ),
                                    ),
                                    ..._toWhom.map((to) => InkWell(
                                          onTap: () {
                                            setState(() {
                                              _selectedToWhom = to;
                                            });
                                            Navigator.of(context).pop();
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                    builder: (context) =>
                                                        CreateCarePlanStepOne(
                                                            relationshipId:
                                                                _toWhom.indexOf(
                                                                        to) +
                                                                    1)))
                                                .then((_) {
                                              _reload(context: context);
                                            });
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                left: 20, right: 20, top: 10),
                                            padding: EdgeInsets.symmetric(
                                                vertical: 17, horizontal: 20),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Color(0xffe4ebf2)),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(to,
                                                      style: Theme.of(ctx)
                                                          .textTheme
                                                          .bodyText1
                                                          ?.copyWith(
                                                              fontFamily:
                                                                  'Soleil',
                                                              fontSize: 17,
                                                              color: Color(
                                                                  0xff161640))),
                                                ),
                                                Image.asset(
                                                    'res/icons/ic_check_round_${to == _selectedToWhom ? "blue" : "blueborder"}.png',
                                                    width: 21)
                                              ],
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              ));
                    } else {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Login()));
                    }
                  },
                  icon: Icon(Icons.add, color: Colors.white)),
            )
          ],
        ),
        body: LoadingIndicator(
          child: Container(
            margin: EdgeInsets.all(10),
            child: FutureProvider<Response<CarePlan>>(
              create: (context) => CarePlanRest(context: context).list(),
              initialData: Response(httpCode: -1),
              child: Consumer<Response<CarePlan>>(
                builder: (context, response, child) => response.httpCode == -1
                    ? StatelessLoadingIndicator()
                    : ListView(
                        children: (context
                                        .watch<CarePlanChangeNotifier>()
                                        .response ??
                                    response)
                                .dataResults
                                ?.map((carePlan) => InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    CarePlanDetail(
                                                        carePlan: carePlan)));
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        margin: EdgeInsets.only(bottom: 5),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(7),
                                            color: Colors.white),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                                width: 84,
                                                height: 84,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    gradient: LinearGradient(
                                                        colors: [
                                                          Color(0x36018aca),
                                                          Color(0x36afe5ff)
                                                        ],
                                                        begin: Alignment
                                                            .centerLeft,
                                                        end: Alignment
                                                            .centerRight)),
                                                child: Center(
                                                    child: Image.asset(
                                                        'res/icons/ic_description.png',
                                                        width: 36))),
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(carePlan.name!,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline5),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Row(children: [
                                                        Image.asset(
                                                            'res/icons/ic_shield.png',
                                                            width: 15),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                              'Care Plan: ${carePlan.category}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1),
                                                        )
                                                      ]),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Row(
                                                        children: [
                                                          Image.asset(
                                                              'res/icons/ic_clock.png',
                                                              width: 13),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 5),
                                                            child: Text(
                                                                carePlan
                                                                    .submissionDate!,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .bodyText1),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList() ??
                            [],
                      ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _reload({required BuildContext context}) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await CarePlanRest(context: context).list();
    context.read<CarePlanChangeNotifier>().update(response: _res);
    context.read<LoadingState>().toggle(value: false);
  }
}

class CarePlanChangeNotifier extends ChangeNotifier {
  Response<CarePlan>? response;

  CarePlanChangeNotifier({this.response});

  update({required Response<CarePlan> response}) {
    this.response = response;
    notifyListeners();
  }
}
