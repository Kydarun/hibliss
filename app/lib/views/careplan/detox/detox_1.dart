import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/detox/detox_2.dart';
import 'package:app/views/shared/mood.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class DetoxStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  DetoxStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Detox'),
              Text('1 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => DetoxFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<DetoxFormBlocStep1, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  DetoxStep2(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _detoxFormBloc = context.read<DetoxFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.sleepQuality,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(top: 12),
                                            child: MoodControl(
                                                text: 'How is your sleep quality?',
                                                errorText: state.hasError
                                                    ? 'This field is required.'
                                                    : null,
                                                onChanged: (option) {
                                                  _detoxFormBloc.sleepQuality.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.appetite,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(top: 20),
                                            child: MoodControl(
                                                text: 'How is your appetite?',
                                                errorText: state.hasError
                                                    ? 'This field is required.'
                                                    : null,
                                                onChanged: (option) {
                                                  _detoxFormBloc.appetite.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.toiletCondition,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.symmetric(vertical: 20),
                                            child: MoodControl(
                                                text: 'How is your toilet condition?',
                                                errorText: state.hasError
                                                    ? 'This field is required.'
                                                    : null,
                                                onChanged: (option) {
                                                  _detoxFormBloc.toiletCondition
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _detoxFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class DetoxFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final sleepQuality = InputFieldBloc<String, Object>(
      name: 'sleepQuality', validators: [FieldBlocValidators.required]);
  final appetite =
      InputFieldBloc<String, Object>(name: 'appetite', validators: [FieldBlocValidators.required]);
  final toiletCondition = InputFieldBloc<String, Object>(
      name: 'toiletCondition', validators: [FieldBlocValidators.required]);

  DetoxFormBlocStep1({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [sleepQuality, appetite, toiletCondition]);
  }

  @override
  void onSubmitting() {
    viewModel.detox = CarePlanDetox();
    emitSuccess(
        successResponse: viewModel
          ..detox?.sleepQuality = sleepQuality.value
          ..detox?.appetite = appetite.value
          ..detox?.toiletCondition = toiletCondition.value,
        canSubmitAgain: true);
  }
}
