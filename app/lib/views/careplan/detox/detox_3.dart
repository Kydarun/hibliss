import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class DetoxStep3 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const DetoxStep3({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Detox'),
              Text('3 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) =>
                    DetoxFormBlocStep3(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<DetoxFormBlocStep3,
                            CarePlanViewModel, CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => CreateCarePlanStepThree(
                                  viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _detoxFormBloc =
                                context.read<DetoxFormBlocStep3>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 20, bottom: 5, left: 10),
                                            child:
                                                Text('I want to improve my:')),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.improveHighBlood,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'High Blood Pressure',
                                                onChecked: (option) {
                                                  _detoxFormBloc
                                                      .improveHighBlood
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc:
                                              _detoxFormBloc.improveCholesterol,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'High Cholesterol',
                                                onChecked: (option) {
                                                  _detoxFormBloc
                                                      .improveCholesterol
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.improveDiabetes,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Diabetes',
                                                onChecked: (option) {
                                                  _detoxFormBloc.improveDiabetes
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.improveKidney,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Kidney Condition',
                                                onChecked: (option) {
                                                  _detoxFormBloc.improveKidney
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.improveLiver,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Liver Inflammation',
                                                onChecked: (option) {
                                                  _detoxFormBloc.improveLiver
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc
                                              .improveReproductive,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Reproductive System',
                                                onChecked: (option) {
                                                  _detoxFormBloc
                                                      .improveReproductive
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.improveStamina,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Stamina/Energy Level',
                                                onChecked: (option) {
                                                  _detoxFormBloc.improveStamina
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc:
                                              _detoxFormBloc.improveComments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed:
                                                    _detoxFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class DetoxFormBlocStep3
    extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final improveHighBlood = InputFieldBloc<String, Object>(
      name: 'improveHighBlood', initialValue: "No");
  final improveCholesterol = InputFieldBloc<String, Object>(
      name: 'improveCholestrol', initialValue: "No");
  final improveDiabetes = InputFieldBloc<String, Object>(
      name: 'improveDiabetes', initialValue: "No");
  final improveKidney =
      InputFieldBloc<String, Object>(name: 'improveKidney', initialValue: "No");
  final improveLiver =
      InputFieldBloc<String, Object>(name: 'improveLiver', initialValue: "No");
  final improveReproductive = InputFieldBloc<String, Object>(
      name: 'improveReproductive', initialValue: "No");
  final improveStamina = InputFieldBloc<String, Object>(
      name: 'improveStamina', initialValue: "No");
  final improveComments = TextFieldBloc(name: 'improveComments');

  DetoxFormBlocStep3({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      improveHighBlood,
      improveCholesterol,
      improveDiabetes,
      improveKidney,
      improveLiver,
      improveReproductive,
      improveStamina,
      improveComments
    ]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..detox?.improveHighBlood = improveHighBlood.value
          ..detox?.improveCholesterol = improveCholesterol.value
          ..detox?.improveDiabetes = improveDiabetes.value
          ..detox?.improveKidney = improveKidney.value
          ..detox?.improveLiver = improveLiver.value
          ..detox?.improveReproductive = improveReproductive.value
          ..detox?.improveStamina = improveStamina.value
          ..detox?.improveComments = improveComments.value,
        canSubmitAgain: true);
  }
}
