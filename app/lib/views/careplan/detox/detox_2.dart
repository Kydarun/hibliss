import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/careplan/detox/detox_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class DetoxStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const DetoxStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Detox'),
              Text('2 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) =>
                    DetoxFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<DetoxFormBlocStep2,
                            CarePlanViewModel, CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetoxStep3(
                                  viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _detoxFormBloc =
                                context.read<DetoxFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 20, bottom: 5, left: 10),
                                            child: Text(
                                                'Are you suffering from any of these?')),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc:
                                              _detoxFormBloc.highBloodPressure,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'High Blood Pressure',
                                                onChecked: (option) {
                                                  _detoxFormBloc
                                                      .highBloodPressure
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.highCholesterol,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'High Cholesterol',
                                                onChecked: (option) {
                                                  _detoxFormBloc.highCholesterol
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.diabetes,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Diabetes',
                                                onChecked: (option) {
                                                  _detoxFormBloc.diabetes
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.insomnia,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Insomnia',
                                                onChecked: (option) {
                                                  _detoxFormBloc.insomnia
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.lethargy,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Lethargy/Fatigue',
                                                onChecked: (option) {
                                                  _detoxFormBloc.lethargy
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _detoxFormBloc.constipation,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text:
                                                    'Constipation/Indigestion',
                                                onChecked: (option) {
                                                  _detoxFormBloc.constipation
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc:
                                              _detoxFormBloc.hormonalImbalance,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Hormonal Imbalance',
                                                onChecked: (option) {
                                                  _detoxFormBloc
                                                      .hormonalImbalance
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc:
                                              _detoxFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed:
                                                    _detoxFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class DetoxFormBlocStep2
    extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final highBloodPressure = InputFieldBloc<String, Object>(
      name: 'highBloodPressure', initialValue: "No");
  final highCholesterol = InputFieldBloc<String, Object>(
      name: 'highCholesterol', initialValue: "No");
  final diabetes =
      InputFieldBloc<String, Object>(name: 'diabetes', initialValue: "No");
  final insomnia =
      InputFieldBloc<String, Object>(name: 'insomnia', initialValue: "No");
  final lethargy =
      InputFieldBloc<String, Object>(name: 'lethargy', initialValue: "No");
  final constipation =
      InputFieldBloc<String, Object>(name: 'constipation', initialValue: "No");
  final hormonalImbalance = InputFieldBloc<String, Object>(
      name: 'hormonalImbalance', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  DetoxFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      highBloodPressure,
      highCholesterol,
      diabetes,
      insomnia,
      lethargy,
      constipation,
      hormonalImbalance,
      comments
    ]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..detox?.highBloodPressure = highBloodPressure.value
          ..detox?.highCholesterol = highCholesterol.value
          ..detox?.diabetes = diabetes.value
          ..detox?.insomnia = insomnia.value
          ..detox?.lethargy = lethargy.value
          ..detox?.constipation = constipation.value
          ..detox?.hormonalImbalance = hormonalImbalance.value
          ..detox?.comments = comments.value,
        canSubmitAgain: true);
  }
}
