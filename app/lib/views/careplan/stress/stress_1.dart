import 'dart:typed_data';

import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/stress/stress_2.dart';
import 'package:app/views/shared/drawing.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class StressStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  StressStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Stress'),
              Text('1 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => StressFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<StressFormBlocStep1, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  StressStep2(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _painFormBloc = context.read<StressFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BlocBuilder<InputFieldBloc<Uint8List, Object>,
                                      InputFieldBlocState>(
                                    bloc: _painFormBloc.affectedArea,
                                    builder: (context, state) => PainMap(onDrew: (image) {
                                      _painFormBloc.affectedArea.updateValue(image);
                                      debugPrint('$image');
                                    }),
                                  ),
                                  Expanded(
                                    child: CustomScrollView(
                                      slivers: [
                                        SliverToBoxAdapter(
                                          child: Container(),
                                        ),
                                        SliverFillRemaining(
                                          hasScrollBody: false,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(bottom: 40),
                                                  child: ElevatedButton(
                                                      child: Text('Next'),
                                                      onPressed: _painFormBloc.submit),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class StressFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final affectedArea = InputFieldBloc<Uint8List, Object>(name: 'affectedArea');

  StressFormBlocStep1({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [affectedArea]);
  }

  @override
  void onSubmitting() {
    viewModel.stress = CarePlanStress();
    emitSuccess(
        successResponse: viewModel..affectedArea = affectedArea.value, canSubmitAgain: true);
  }
}
