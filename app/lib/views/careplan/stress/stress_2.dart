import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class StressStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const StressStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Stress'),
              Text('2 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => StressFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<StressFormBlocStep2, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  CreateCarePlanStepThree(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _painFormBloc = context.read<StressFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, bottom: 5, left: 10),
                                            child: Text('Are you suffering from any of these?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.insomnia,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Insomnia, anxiety, depressed',
                                                onChecked: (option) {
                                                  _painFormBloc.insomnia.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.headache,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Headaches, migraines',
                                                onChecked: (option) {
                                                  _painFormBloc.headache.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.chestPain,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Chest pain, rapid heartbeat',
                                                onChecked: (option) {
                                                  _painFormBloc.chestPain.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.lackAppetite,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Lack of appetite',
                                                onChecked: (option) {
                                                  _painFormBloc.lackAppetite.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.hairLoss,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Hair loss',
                                                onChecked: (option) {
                                                  _painFormBloc.hairLoss.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _painFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _painFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class StressFormBlocStep2 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final insomnia = InputFieldBloc<String, Object>(name: 'insomnia', initialValue: "No");
  final headache = InputFieldBloc<String, Object>(name: 'headache', initialValue: "No");
  final chestPain = InputFieldBloc<String, Object>(name: 'chestPain', initialValue: "No");
  final lackAppetite = InputFieldBloc<String, Object>(name: 'lackAppetite', initialValue: "No");
  final hairLoss = InputFieldBloc<String, Object>(name: 'hairLoss', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  StressFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [insomnia, headache, chestPain, lackAppetite, hairLoss, comments]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..stress?.insomnia = insomnia.value
          ..stress?.headache = headache.value
          ..stress?.chestPain = chestPain.value
          ..stress?.lackAppetite = lackAppetite.value
          ..stress?.hairLoss = hairLoss.value
          ..stress?.comments = comments.value,
        canSubmitAgain: true);
  }
}
