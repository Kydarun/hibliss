import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class OsteoarthritisStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const OsteoarthritisStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Osteoarthritis'),
              Text('2 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) =>
                    OsteoarthritisFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<OsteoarthritisFormBlocStep2, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  CreateCarePlanStepThree(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _osteoarthritisFormBloc =
                                context.read<OsteoarthritisFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, bottom: 5, left: 10),
                                            child: Text('Are you suffering from any of these?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.numbness,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Numbness',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.numbness
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.jointSwelling,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Joint swelling, tenderness or stiffness',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.jointSwelling
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.jointInstability,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Joint instability/buckling (knee gives out)',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.jointInstability
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.muscleWeakness,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Muscle weakness around the joint',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.muscleWeakness
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.lossMuscle,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Loss of muscle mass',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.lossMuscle
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.diabetes,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Diabetes',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.diabetes
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _osteoarthritisFormBloc.chronicPain,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Chronic pain that worsens over time',
                                                onChecked: (option) {
                                                  _osteoarthritisFormBloc.chronicPain
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _osteoarthritisFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _osteoarthritisFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class OsteoarthritisFormBlocStep2 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final numbness = InputFieldBloc<String, Object>(name: 'numbness', initialValue: "No");
  final jointSwelling = InputFieldBloc<String, Object>(name: 'jointSwelling', initialValue: "No");
  final jointInstability =
      InputFieldBloc<String, Object>(name: 'jointInstability', initialValue: "No");
  final muscleWeakness = InputFieldBloc<String, Object>(name: 'muscleWeakness', initialValue: "No");
  final lossMuscle = InputFieldBloc<String, Object>(name: 'lossMuscle', initialValue: "No");
  final diabetes = InputFieldBloc<String, Object>(name: 'diabetes', initialValue: "No");
  final chronicPain = InputFieldBloc<String, Object>(name: 'chronicPain', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  OsteoarthritisFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      numbness,
      jointSwelling,
      jointInstability,
      muscleWeakness,
      lossMuscle,
      chronicPain,
      diabetes,
      comments
    ]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..osteoarthritis?.numbness = numbness.value
          ..osteoarthritis?.jointSwelling = jointSwelling.value
          ..osteoarthritis?.jointInstability = jointInstability.value
          ..osteoarthritis?.muscleWeakness = muscleWeakness.value
          ..osteoarthritis?.lossMuscle = lossMuscle.value
          ..osteoarthritis?.chronicPain = chronicPain.value
          ..osteoarthritis?.diabetes = diabetes.value
          ..osteoarthritis?.comments = comments.value,
        canSubmitAgain: true);
  }
}
