import 'dart:typed_data';

import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/skin/skin_2.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/drawing.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class SkinStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  SkinStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Skin'),
              Text('1 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => SkinFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<SkinFormBlocStep1, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  SkinStep2(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _skinFormBloc = context.read<SkinFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BlocBuilder<InputFieldBloc<Uint8List, Object>,
                                      InputFieldBlocState>(
                                    bloc: _skinFormBloc.affectedArea,
                                    builder: (context, state) => PainMap(onDrew: (image) {
                                      _skinFormBloc.affectedArea.updateValue(image);
                                      debugPrint('$image');
                                    }),
                                  ),
                                  Expanded(
                                    child: CustomScrollView(
                                      slivers: [
                                        SliverToBoxAdapter(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(top: 15, left: 10, right: 10),
                                                  child: Text('Triggering factor of symptom.')),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 20, left: 10, right: 10),
                                                child: CheckBoxControl(
                                                  text: 'Food',
                                                  onChecked: (checked) {
                                                    _skinFormBloc.factorFood.updateValue(checked);
                                                  },
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(left: 10, right: 10),
                                                child: CheckBoxControl(
                                                  text: 'Weather',
                                                  onChecked: (checked) {
                                                    _skinFormBloc.factorWeather
                                                        .updateValue(checked);
                                                  },
                                                ),
                                              ),
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(top: 15, left: 10, right: 10),
                                                  child: Text('Triggering factor of symptom.')),
                                              Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: TextFieldBlocBuilder(
                                                  textFieldBloc: _skinFormBloc.factorOthers,
                                                  style: Theme.of(context).textTheme.bodyText2,
                                                  decoration:
                                                      CommonUI(context: context).decoration2(),
                                                  textInputAction: TextInputAction.next,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        SliverFillRemaining(
                                          hasScrollBody: false,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(bottom: 40),
                                                  child: ElevatedButton(
                                                      child: Text('Next'),
                                                      onPressed: _skinFormBloc.submit),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class SkinFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final affectedArea = InputFieldBloc<Uint8List, Object>(name: 'affectedArea');

  final factorFood = InputFieldBloc<String, Object>(name: 'factorFood');

  final factorWeather = InputFieldBloc<String, Object>(name: 'factorWeather');

  final factorOthers = TextFieldBloc(name: 'factorOthers');

  SkinFormBlocStep1({required this.context, required this.viewModel}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [affectedArea, factorFood, factorWeather, factorOthers]);
  }

  @override
  void onSubmitting() {
    viewModel.skin = CarePlanSkin();
    emitSuccess(
        successResponse: viewModel
          ..affectedArea = affectedArea.value
          ..skin?.factorFood = factorFood.value
          ..skin?.factorWeather = factorWeather.value
          ..skin?.factorOthers = factorOthers.value,
        canSubmitAgain: true);
  }
}
