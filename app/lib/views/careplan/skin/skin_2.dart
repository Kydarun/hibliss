import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class SkinStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const SkinStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Skin'),
              Text('2 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) =>
                    SkinFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<SkinFormBlocStep2,
                            CarePlanViewModel, CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => CreateCarePlanStepThree(
                                  viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _skinFormBloc =
                                context.read<SkinFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 20, bottom: 5, left: 10),
                                            child: Text(
                                                'Are you suffering from any of these?')),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _skinFormBloc.dryness,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Dry, cracked, scaly',
                                                onChecked: (option) {
                                                  _skinFormBloc.dryness
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _skinFormBloc.itchiness,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Itchiness',
                                                onChecked: (option) {
                                                  _skinFormBloc.itchiness
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _skinFormBloc.patches,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text:
                                                    'Red/brownish-grey patches',
                                                onChecked: (option) {
                                                  _skinFormBloc.patches
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _skinFormBloc.bumps,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Small and raised bumps',
                                                onChecked: (option) {
                                                  _skinFormBloc.bumps
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<
                                            InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _skinFormBloc.reddish,
                                          builder: (context, state) =>
                                              Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text:
                                                    'Reddish raw, sensitive, swollen',
                                                onChecked: (option) {
                                                  _skinFormBloc.reddish
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 5, bottom: 5, left: 10),
                                            child:
                                                Text('Diagnosis from doctor.')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc:
                                              _skinFormBloc.diagnosisDoctor,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _skinFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed:
                                                    _skinFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class SkinFormBlocStep2 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final dryness =
      InputFieldBloc<String, Object>(name: 'dryness', initialValue: "No");
  final itchiness =
      InputFieldBloc<String, Object>(name: 'itchiness', initialValue: "No");
  final patches =
      InputFieldBloc<String, Object>(name: 'patches', initialValue: "No");
  final bumps =
      InputFieldBloc<String, Object>(name: 'bumps', initialValue: "No");
  final reddish =
      InputFieldBloc<String, Object>(name: 'reddish', initialValue: "No");
  final diagnosisDoctor = TextFieldBloc(name: 'diagnosisDoctor');
  final comments = TextFieldBloc(name: 'comments');

  SkinFormBlocStep2({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      dryness,
      itchiness,
      patches,
      bumps,
      reddish,
      diagnosisDoctor,
      comments
    ]);
  }

  @override
  void onSubmitting() {
    debugPrint(comments.value);
    emitSuccess(
        successResponse: viewModel
          ..skin?.dryness = dryness.value
          ..skin?.itchiness = itchiness.value
          ..skin?.patches = patches.value
          ..skin?.bumps = bumps.value
          ..skin?.reddish = reddish.value
          ..skin?.diagnosisDoctor = diagnosisDoctor.value
          ..skin?.comments = comments.value,
        canSubmitAgain: true);
  }
}
