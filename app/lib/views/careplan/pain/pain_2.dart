import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/create_3.dart';
import 'package:app/views/shared/checkbox.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class PainStep2 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  const PainStep2({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Pain'),
              Text('2 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => PainFormBlocStep2(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<PainFormBlocStep2, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  CreateCarePlanStepThree(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _painFormBloc = context.read<PainFormBlocStep2>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: 20, bottom: 5, left: 10),
                                            child: Text('Are you suffering from any of these?')),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.numbness,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Numbness',
                                                onChecked: (option) {
                                                  _painFormBloc.numbness.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.muscleAches,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Muscle aches and spasms',
                                                onChecked: (option) {
                                                  _painFormBloc.muscleAches.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.stabbingPain,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Stabbing pain along the spine',
                                                onChecked: (option) {
                                                  _painFormBloc.stabbingPain.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.limitedFlexibility,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Limited flexibility/range of motion',
                                                onChecked: (option) {
                                                  _painFormBloc.limitedFlexibility
                                                      .updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.backMovement,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Back movement limitation',
                                                onChecked: (option) {
                                                  _painFormBloc.backMovement.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.painRadiates,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text: 'Pain radiates down the leg (sciatica)',
                                                onChecked: (option) {
                                                  _painFormBloc.painRadiates.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        BlocBuilder<InputFieldBloc<String, Object>,
                                            InputFieldBlocState>(
                                          bloc: _painFormBloc.painBending,
                                          builder: (context, state) => Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: CheckBoxControl(
                                                text:
                                                    'Pain from bending, lifting, standing or walking',
                                                onChecked: (option) {
                                                  _painFormBloc.painBending.updateValue(option);
                                                }),
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                                            child: Text('Any other comments?')),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _painFormBloc.comments,
                                          minLines: 5,
                                          maxLines: 5,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Next'),
                                                onPressed: _painFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class PainFormBlocStep2 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final numbness = InputFieldBloc<String, Object>(name: 'numbness', initialValue: "No");
  final muscleAches = InputFieldBloc<String, Object>(name: 'muscleAches', initialValue: "No");
  final stabbingPain = InputFieldBloc<String, Object>(name: 'stabbingPain', initialValue: "No");
  final limitedFlexibility =
      InputFieldBloc<String, Object>(name: 'limitedFlexibility', initialValue: "No");
  final backMovement = InputFieldBloc<String, Object>(name: 'backMovement', initialValue: "No");
  final painRadiates = InputFieldBloc<String, Object>(name: 'painRadiates', initialValue: "No");
  final painBending = InputFieldBloc<String, Object>(name: 'painBending', initialValue: "No");
  final comments = TextFieldBloc(name: 'comments');

  PainFormBlocStep2({required this.context, required this.viewModel}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      numbness,
      muscleAches,
      stabbingPain,
      limitedFlexibility,
      backMovement,
      painRadiates,
      painBending,
      comments
    ]);
  }

  @override
  void onSubmitting() {
    emitSuccess(
        successResponse: viewModel
          ..pain?.numbness = numbness.value
          ..pain?.muscleAches = muscleAches.value
          ..pain?.limitedFlexibility = limitedFlexibility.value
          ..pain?.stabbingPain = stabbingPain.value
          ..pain?.backMovement = backMovement.value
          ..pain?.painRadiates = painRadiates.value
          ..pain?.painBending = painBending.value
          ..pain?.comments = comments.value,
        canSubmitAgain: true);
  }
}
