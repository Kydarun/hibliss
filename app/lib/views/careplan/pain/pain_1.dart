import 'dart:typed_data';

import 'package:app/view_model/careplan.dart';
import 'package:app/views/careplan/pain/pain_2.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/drawing.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/toggle.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class PainStep1 extends StatelessWidget {
  final CarePlanViewModel viewModel;

  final List<_PainKeyValue> _levelOfPains = [
    _PainKeyValue(value: 1, title: 'Mild Pain'),
    _PainKeyValue(value: 2, title: 'Moderate Pain'),
    _PainKeyValue(value: 3, title: 'Severe Pain'),
    _PainKeyValue(value: 4, title: 'Very Severe Pain'),
    _PainKeyValue(value: 5, title: 'Worst Severe Pain')
  ];

  final List<_PainKeyValue> _painStarts = [
    _PainKeyValue(value: 1, title: 'Few Days Ago'),
    _PainKeyValue(value: 2, title: 'Few Weeks Ago'),
    _PainKeyValue(value: 3, title: 'A Month Ago'),
    _PainKeyValue(value: 4, title: '2-3 Months Ago'),
    _PainKeyValue(value: 5, title: 'More Than 3 Months Ago')
  ];

  PainStep1({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Pain'),
              Text('1 of 2',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: Unauthenticated(
          child: Container(
            margin: EdgeInsets.only(top: 15),
            child: BlocProvider(
                create: (context) => PainFormBlocStep1(context: context, viewModel: viewModel),
                child: Builder(
                  builder: (context) {
                    return FormBlocListener<PainFormBlocStep1, CarePlanViewModel,
                            CarePlanViewModel>(
                        onSuccess: (context, success) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  PainStep2(viewModel: success.successResponse!)));
                        },
                        onFailure: (context, failure) {},
                        child: Builder(
                          builder: (context) {
                            final _painFormBloc = context.read<PainFormBlocStep1>();
                            return Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BlocBuilder<InputFieldBloc<Uint8List, Object>,
                                      InputFieldBlocState>(
                                    bloc: _painFormBloc.affectedArea,
                                    builder: (context, state) => PainMap(onDrew: (image) {
                                      _painFormBloc.affectedArea.updateValue(image);
                                      debugPrint('$image');
                                    }),
                                  ),
                                  Expanded(
                                    child: CustomScrollView(
                                      slivers: [
                                        SliverToBoxAdapter(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      top: 15, bottom: 15, left: 10),
                                                  child: Text('Level of pain.')),
                                              BlocBuilder<InputFieldBloc<_PainKeyValue, Object>,
                                                  InputFieldBlocState>(
                                                bloc: _painFormBloc.levelOfPain,
                                                builder: (context, state) => GestureDetector(
                                                  onTap: () {
                                                    showCupertinoModalPopup(
                                                        context: context,
                                                        builder: (context) => Container(
                                                              margin: EdgeInsets.only(
                                                                  bottom: 35, left: 7, right: 7),
                                                              child: CupertinoActionSheet(
                                                                title: Text('Choose Level of Pain'),
                                                                cancelButton:
                                                                    CupertinoActionSheetAction(
                                                                        isDestructiveAction: false,
                                                                        onPressed: () {
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                        child: Text('Cancel')),
                                                                actions: _levelOfPains
                                                                    .map((levelOfPain) =>
                                                                        CupertinoActionSheetAction(
                                                                            onPressed: () {
                                                                              Navigator.of(context)
                                                                                  .pop();
                                                                              _painFormBloc
                                                                                  .levelOfPain
                                                                                  .updateValue(
                                                                                      levelOfPain);
                                                                            },
                                                                            child: Text(
                                                                                levelOfPain.title)))
                                                                    .toList(),
                                                              ),
                                                            ));
                                                  },
                                                  child: TextField(
                                                    enabled: false,
                                                    controller: TextEditingController(
                                                        text: _painFormBloc
                                                                .levelOfPain.value?.title ??
                                                            ''),
                                                    decoration: CommonUI(context: context)
                                                        .decoration2()
                                                        .copyWith(
                                                            hintText: 'Please select',
                                                            errorText: state.hasError
                                                                ? 'This field is required.'
                                                                : null,
                                                            suffixIcon:
                                                                Icon(Icons.arrow_drop_down)),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      top: 20, bottom: 15, left: 10),
                                                  child: Text('When did the pain start?')),
                                              BlocBuilder<InputFieldBloc<_PainKeyValue, Object>,
                                                  InputFieldBlocState>(
                                                bloc: _painFormBloc.painStart,
                                                builder: (context, state) => GestureDetector(
                                                  onTap: () {
                                                    showCupertinoModalPopup(
                                                        context: context,
                                                        builder: (context) => Container(
                                                              margin: EdgeInsets.only(
                                                                  bottom: 35, left: 7, right: 7),
                                                              child: CupertinoActionSheet(
                                                                title: Text(
                                                                    'When did the pain start?'),
                                                                cancelButton:
                                                                    CupertinoActionSheetAction(
                                                                        isDestructiveAction: false,
                                                                        onPressed: () {
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                        child: Text('Cancel')),
                                                                actions: _painStarts
                                                                    .map((painStart) =>
                                                                        CupertinoActionSheetAction(
                                                                            onPressed: () {
                                                                              Navigator.of(context)
                                                                                  .pop();
                                                                              _painFormBloc
                                                                                  .painStart
                                                                                  .updateValue(
                                                                                      painStart);
                                                                            },
                                                                            child: Text(
                                                                                painStart.title)))
                                                                    .toList(),
                                                              ),
                                                            ));
                                                  },
                                                  child: TextField(
                                                    enabled: false,
                                                    controller: TextEditingController(
                                                        text:
                                                            _painFormBloc.painStart.value?.title ??
                                                                ''),
                                                    decoration: CommonUI(context: context)
                                                        .decoration2()
                                                        .copyWith(
                                                            hintText: 'Please select',
                                                            errorText: state.hasError
                                                                ? 'This field is required.'
                                                                : null,
                                                            suffixIcon:
                                                                Icon(Icons.arrow_drop_down)),
                                                  ),
                                                ),
                                              ),
                                              BlocBuilder<InputFieldBloc<String, Object>,
                                                  InputFieldBlocState>(
                                                bloc: _painFormBloc.xray,
                                                builder: (context, state) => Container(
                                                  margin: EdgeInsets.only(
                                                      top: 15, bottom: 15, left: 10),
                                                  child: ToggleControl(
                                                      text: 'Any X-rays lately?',
                                                      errorText: state.hasError
                                                          ? 'This field is required.'
                                                          : null,
                                                      onChanged: (option) {
                                                        _painFormBloc.xray.updateValue(option);
                                                      }),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SliverFillRemaining(
                                          hasScrollBody: false,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(bottom: 40),
                                                  child: ElevatedButton(
                                                      child: Text('Next'),
                                                      onPressed: _painFormBloc.submit),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ));
                  },
                )),
          ),
        ));
  }
}

class PainFormBlocStep1 extends FormBloc<CarePlanViewModel, CarePlanViewModel> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final affectedArea = InputFieldBloc<Uint8List, Object>(name: 'affectedArea');

  final levelOfPain = InputFieldBloc<_PainKeyValue, Object>(
      name: 'levelOfPain', validators: [FieldBlocValidators.required]);

  final painStart = InputFieldBloc<_PainKeyValue, Object>(
      name: 'painStart', validators: [FieldBlocValidators.required]);

  final xray =
      InputFieldBloc<String, Object>(name: 'xray', validators: [FieldBlocValidators.required]);

  PainFormBlocStep1({required this.context, required this.viewModel}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [affectedArea, levelOfPain, painStart, xray]);
  }

  @override
  void onSubmitting() {
    viewModel.pain = CarePlanPain();
    emitSuccess(
        successResponse: viewModel
          ..affectedArea = affectedArea.value
          ..pain?.levelOfPain = levelOfPain.value?.value
          ..pain?.painStart = painStart.value?.value
          ..pain?.xray = xray.value,
        canSubmitAgain: true);
  }
}

class _PainKeyValue {
  int value;
  String title;

  _PainKeyValue({required this.value, required this.title});
}
