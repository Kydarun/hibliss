import 'dart:convert';

import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/careplan.dart';
import 'package:app/view_model/careplan.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/quit_plan.dart';
import 'package:app/views/shared/toggle.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class CreateCarePlanStepThree extends StatelessWidget {
  final CarePlanViewModel viewModel;

  CreateCarePlanStepThree({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          leading: null,
          title: Column(
            children: [
              Text('Care Plan'),
              Text('3 of 3',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.white, fontSize: 12))
            ],
          ),
          actions: [QuitPlanButton()],
        ),
        body: LoadingIndicator(
          child: Unauthenticated(
            child: Container(
              margin: EdgeInsets.only(top: 15),
              child: BlocProvider(
                  create: (context) =>
                      CarePlanFormBlocStep3(context: context, viewModel: viewModel),
                  child: Builder(
                    builder: (context) {
                      final _carePlanBloc = context.read<CarePlanFormBlocStep3>();
                      return FormBlocListener<CarePlanFormBlocStep3, Response2, Response2>(
                          onSuccess: (context, success) {
                            showModalBottomSheet(
                                context: context,
                                isDismissible: true,
                                isScrollControlled: true,
                                enableDrag: true,
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(20),
                                        topLeft: Radius.circular(20))),
                                builder: (ctx) => Container(
                                      margin: EdgeInsets.symmetric(horizontal: 15),
                                      constraints: BoxConstraints(
                                          maxHeight: MediaQuery.of(ctx).size.height > 727
                                              ? 727
                                              : MediaQuery.of(ctx).size.height),
                                      height: 727,
                                      child: Column(
                                        children: [
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                Image.asset('res/icons/ic_msg_check.png',
                                                    width: 78),
                                                Container(
                                                  margin: EdgeInsets.only(top: 30),
                                                  child: Text('Form has been submitted.',
                                                      style: Theme.of(context).textTheme.headline5),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(top: 3, bottom: 30),
                                                  child: Text(
                                                      'Hi-Bliss will get back to you within 2\nbusiness days.',
                                                      style: Theme.of(context).textTheme.bodyText1,
                                                      textAlign: TextAlign.center),
                                                )
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 40, top: 10, left: 10, right: 10),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                          child: ElevatedButton(
                                                              onPressed: () {
                                                                Navigator.of(context).pop();
                                                              },
                                                              child: Text('Okay')))
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )).then((_) {
                              Navigator.of(context).popUntil(ModalRoute.withName('/home'));
                            });
                          },
                          onFailure: (context, failure) {
                            CommonUI(context: context)
                                .snackbar(message: 'Failed to submit. Please try again.');
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 15),
                            child: CustomScrollView(
                              slivers: [
                                SliverToBoxAdapter(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      BlocBuilder<InputFieldBloc<String, Object>,
                                          InputFieldBlocState>(
                                        bloc: _carePlanBloc.currentSymptoms,
                                        builder: (context, state) => Container(
                                          margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                                          child: ToggleControl(
                                              text: 'Any current symptoms?',
                                              errorText:
                                                  state.hasError ? 'This field is required.' : null,
                                              onChanged: (option) {
                                                _carePlanBloc.currentSymptoms.updateValue(option);
                                              }),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 15),
                                        child: TextFieldBlocBuilder(
                                          textFieldBloc: _carePlanBloc.currentSymptomsDesc,
                                          style: Theme.of(context).textTheme.bodyText2,
                                          decoration: CommonUI(context: context)
                                              .decoration2()
                                              .copyWith(hintText: 'If yes...'),
                                          textInputAction: TextInputAction.next,
                                        ),
                                      ),
                                      BlocBuilder<InputFieldBloc<String, Object>,
                                          InputFieldBlocState>(
                                        bloc: _carePlanBloc.anyMedication,
                                        builder: (context, state) => Container(
                                          margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                                          child: ToggleControl(
                                              text: 'Currently on any medication?',
                                              errorText:
                                                  state.hasError ? 'This field is required.' : null,
                                              onChanged: (option) {
                                                _carePlanBloc.anyMedication.updateValue(option);
                                              }),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 15),
                                        child: TextFieldBlocBuilder(
                                          textFieldBloc: _carePlanBloc.anyMedicationDesc,
                                          style: Theme.of(context).textTheme.bodyText2,
                                          decoration: CommonUI(context: context)
                                              .decoration2()
                                              .copyWith(hintText: 'If yes...'),
                                          textInputAction: TextInputAction.next,
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                    'Any other medical history that you wish our therapist to be aware of?',
                                                    style: Theme.of(context).textTheme.bodyText1,
                                                    textAlign: TextAlign.start),
                                              ),
                                            ],
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: TextFieldBlocBuilder(
                                          textFieldBloc: _carePlanBloc.medicalHistory,
                                          style: Theme.of(context).textTheme.bodyText2,
                                          decoration: CommonUI(context: context).decoration2(),
                                          textInputAction: TextInputAction.next,
                                        ),
                                      ),
                                      BlocBuilder<InputFieldBloc<String, Object>,
                                          InputFieldBlocState>(
                                        bloc: _carePlanBloc.existingCustomer,
                                        builder: (context, state) => Container(
                                          margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                                          child: ToggleControl(
                                              text:
                                                  'Are you an existing customer from Hi-Bliss outlet?',
                                              errorText:
                                                  state.hasError ? 'This field is required.' : null,
                                              onChanged: (option) {
                                                _carePlanBloc.existingCustomer.updateValue(option);
                                              }),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 20, left: 5, right: 5),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                    'Please select a timing for Hi-Bliss to call back to understand the condition better.',
                                                    style: Theme.of(context).textTheme.bodyText1,
                                                    textAlign: TextAlign.start),
                                              ),
                                            ],
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: BlocBuilder<InputFieldBloc<DateTime, Object>,
                                                InputFieldBlocState>(
                                            bloc: _carePlanBloc.callback,
                                            builder: (context, state) => GestureDetector(
                                                  onTap: () {
                                                    showModalBottomSheet(
                                                        context: context,
                                                        isScrollControlled: true,
                                                        backgroundColor: Colors.white,
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius.circular(20)),
                                                        builder: (context) => Container(
                                                            constraints: BoxConstraints(
                                                                maxHeight: MediaQuery.of(context)
                                                                            .size
                                                                            .height >
                                                                        727
                                                                    ? 727
                                                                    : MediaQuery.of(context)
                                                                        .size
                                                                        .height),
                                                            height: 727,
                                                            child: NoAsyncBookingCalendarView(
                                                                initialValue:
                                                                    _carePlanBloc.callback.value,
                                                                callback: (timeslot) {
                                                                  if (timeslot != null) {
                                                                    _carePlanBloc.callback
                                                                        .updateValue(timeslot);
                                                                  }
                                                                })));
                                                  },
                                                  child: TextField(
                                                    enabled: false,
                                                    controller: TextEditingController(
                                                        text: state.value != null
                                                            ? '${DateFormat('dd MMM yyyy, hh:mm').format(state.value!)}${DateFormat('aa').format(state.value!).toLowerCase()}'
                                                            : null),
                                                    decoration: CommonUI(context: context)
                                                        .decoration2()
                                                        .copyWith(
                                                            disabledBorder: OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius.circular(7),
                                                                borderSide: BorderSide(
                                                                    color: Color(0x73bcd5ed),
                                                                    width: 1)),
                                                            errorText: state.hasError
                                                                ? 'Time Slot is required.'
                                                                : null,
                                                            suffixIcon:
                                                                Icon(Icons.arrow_drop_down)),
                                                  ),
                                                )),
                                      ),
                                    ],
                                  ),
                                ),
                                SliverFillRemaining(
                                  hasScrollBody: false,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 40),
                                          child: ElevatedButton(
                                              child: Text('Submit'),
                                              onPressed: _carePlanBloc.submit),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ));
                    },
                  )),
            ),
          ),
        ));
  }
}

class CarePlanFormBlocStep3 extends FormBloc<Response2, Response2> {
  BuildContext context;
  CarePlanViewModel viewModel;

  final currentSymptoms = InputFieldBloc<String, Object>(
      name: 'currentSymptoms', validators: [FieldBlocValidators.required]);

  final currentSymptomsDesc = TextFieldBloc(name: 'currentSymptomDesc');

  final anyMedication = InputFieldBloc<String, Object>(
      name: 'anyMedication', validators: [FieldBlocValidators.required]);

  final anyMedicationDesc = TextFieldBloc(name: 'anyMedicationDesc');

  final medicalHistory = TextFieldBloc(name: 'medicalHistory');

  final existingCustomer = InputFieldBloc<String, Object>(
      name: 'existingCustomer', validators: [FieldBlocValidators.required]);

  final callback = InputFieldBloc<DateTime, Object>(
      name: 'callback', validators: [FieldBlocValidators.required]);

  CarePlanFormBlocStep3({required this.context, required this.viewModel})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      currentSymptoms,
      currentSymptomsDesc,
      anyMedication,
      anyMedicationDesc,
      medicalHistory,
      existingCustomer,
      callback
    ]);
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      final _plan = viewModel
        ..currentSymptoms = currentSymptoms.value
        ..currentSymptomsDesc = currentSymptomsDesc.value
        ..anyMedication = anyMedication.value
        ..anyMedicationDesc = anyMedicationDesc.value
        ..medicalHistory = medicalHistory.value
        ..existingCustomer = existingCustomer.value
        ..callback = callback.value;
      Response2<String>? _res;

      switch (_plan.type) {
        case "pain":
          _res = await CarePlanRest(context: context).createPain(plan: _plan);
          break;
        case "osteoarthritis":
          _res = await CarePlanRest(context: context).createOsteoarthritis(plan: _plan);
          break;
        case "detox":
          _res = await CarePlanRest(context: context).createDetox(plan: _plan);
          break;
        case "skin":
          _res = await CarePlanRest(context: context).createSkin(plan: _plan);
          break;
        case "stress":
          _res = await CarePlanRest(context: context).createStress(plan: _plan);
          break;
        case "eye":
          _res = await CarePlanRest(context: context).createEye(plan: _plan);
          break;
        case "stroke":
          _res = await CarePlanRest(context: context).createStroke(plan: _plan);
          break;
        default:
          debugPrint('${_plan.type}');
          break;
      }
      if (_res?.success ?? false) {
        final _id = _res!.message;
        if (_id != null) {
          if (viewModel.affectedArea != null) {
            await CarePlanRest(context: context)
                .upload(id: _id, base64Image: base64.encode(viewModel.affectedArea!));
          }
        }
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: Response2(httpCode: 1));
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrintStack(stackTrace: stacktrace);
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}

class NoAsyncBookingCalendarView extends StatefulWidget {
  final Function(DateTime?) callback;
  final DateTime? initialValue;

  const NoAsyncBookingCalendarView({required this.callback, this.initialValue, Key? key})
      : super(key: key);

  @override
  _NoAsyncBookingCalendarViewState createState() => _NoAsyncBookingCalendarViewState();
}

class _NoAsyncBookingCalendarViewState extends State<NoAsyncBookingCalendarView> {
  DateTime? _selectedDay;
  Timeslot? _selectedTimeslot;
  DateTime _focusedDay = DateTime.now();
  List<Timeslot> _timeslots = [];

  @override
  void initState() {
    super.initState();
    if (widget.initialValue != null) {
      _selectedDay = DateTime(
          widget.initialValue!.year, widget.initialValue!.month, widget.initialValue!.day, 9);
      _selectedTimeslot = Timeslot(
          value: widget.initialValue,
          display: DateFormat('hh:mmaa').format(widget.initialValue!).toLowerCase(),
          isEnabled: DateTime.now().difference(widget.initialValue!).inMinutes < 0);
      getTimeslots();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          TableCalendar(
              headerStyle: HeaderStyle(
                  titleTextStyle: Theme.of(context).textTheme.headline5!,
                  titleCentered: true,
                  formatButtonVisible: false),
              daysOfWeekHeight: 24,
              daysOfWeekStyle: DaysOfWeekStyle(
                  weekdayStyle: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 19),
                  weekendStyle: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 19),
                  dowTextFormatter: (date, locale) =>
                      DateFormat('E').format(date).characters.first),
              calendarStyle: CalendarStyle(
                  todayDecoration: BoxDecoration(shape: BoxShape.circle, color: Colors.transparent),
                  todayTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 17),
                  selectedDecoration:
                      BoxDecoration(shape: BoxShape.circle, color: Color(0xff17b491)),
                  defaultTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 17),
                  weekendTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 17),
                  outsideTextStyle: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontSize: 17, color: Color(0xffb2b2b2))),
              startingDayOfWeek: StartingDayOfWeek.monday,
              onDaySelected: (selectedDay, focusedDay) {
                setState(() {
                  _selectedDay = selectedDay;
                  _focusedDay = focusedDay;
                });
                getTimeslots();
              },
              selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
              focusedDay: _focusedDay,
              firstDay: DateTime.utc(2010, 1, 1),
              lastDay: DateTime.now().add(Duration(days: 1500))),
          Divider(height: 1),
          Container(
              margin: EdgeInsets.only(top: 18, left: 2, bottom: 11),
              child: Text('Available time slots:', style: Theme.of(context).textTheme.headline5)),
          Expanded(
            child: ListView(
                children: _timeslots
                    .map((timeslot) => InkWell(
                          onTap: (timeslot.isEnabled ?? false)
                              ? () {
                                  setState(() {
                                    _selectedTimeslot = timeslot;
                                  });
                                  widget.callback(timeslot.value);
                                }
                              : null,
                          child: Container(
                            margin: EdgeInsets.only(left: 2, right: 2, bottom: 5),
                            padding: EdgeInsets.symmetric(vertical: 17),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: (timeslot.isEnabled ?? false)
                                    ? Color(0xffe4ebf2)
                                    : Color(0xffededf0)),
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child:
                                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                Text(timeslot.display!,
                                    style: Theme.of(context).textTheme.bodyText1?.copyWith(
                                        fontSize: 17,
                                        color: (timeslot.isEnabled ?? false)
                                            ? Color(0xff161640)
                                            : Color(0xffb2b2b2))),
                                if (timeslot.isEnabled ?? false)
                                  Image.asset(
                                      'res/icons/ic_check_round_${(timeslot.value == _selectedTimeslot?.value) ? "blue" : "blueborder"}.png',
                                      width: 21)
                                else
                                  Container(
                                      width: 21,
                                      height: 21,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle, color: Color(0xffb2b2b2)))
                              ]),
                            ),
                          ),
                        ))
                    .toList()),
          ),
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 40),
                    child: ElevatedButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          widget.callback(_selectedTimeslot?.value);
                        }),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  getTimeslots() {
    setState(() {
      _timeslots = [];
      if (_selectedDay != null) {
        DateTime _dateToLoop =
            DateTime(_selectedDay!.year, _selectedDay!.month, _selectedDay!.day, 9);
        final _now = DateTime.now();
        do {
          _timeslots.add(Timeslot(
              value: _dateToLoop,
              display: DateFormat('hh:mmaa').format(_dateToLoop).toLowerCase(),
              isEnabled: _now.difference(_dateToLoop).inMinutes < 0));
          _dateToLoop = _dateToLoop.add(Duration(minutes: 30));
        } while (_dateToLoop.hour < 21);
      }
    });
  }
}
