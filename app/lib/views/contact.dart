import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatelessWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffededf0),
      appBar: AppBar(
        title: Text('Contact Us'),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
        child: ListView(
          children: [
            _Outlets(
                name: 'Bangsar Shopping Centre, KL',
                hours: '10am - 10pm',
                contact: '+603-2011 8729',
                realNumber: '0320118729'),
            _Outlets(
                name: 'Starling Mall, Damansara Uptown',
                hours: '10am - 10pm',
                contact: '+603-7610 3718',
                realNumber: '0376103718'),
            _Outlets(
                name: 'Menara TH Uptown Compassionate Care',
                contact: '+603-7710 0729',
                realNumber: '0377100729')
          ]
              .map((outlet) => Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: Colors.white),
                    padding: EdgeInsets.only(
                        top: 15, left: 15, right: 15, bottom: 10),
                    margin: EdgeInsets.only(bottom: 5),
                    child: InkWell(
                      onTap: () {
                        showCupertinoModalPopup(
                            context: context,
                            builder: (context) => CupertinoActionSheet(
                                  cancelButton: CupertinoActionSheetAction(
                                    child: Text('Cancel'),
                                    isDestructiveAction: false,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  actions: [
                                    CupertinoActionSheetAction(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          launch('tel:${outlet.realNumber}');
                                        },
                                        child: Text('Call ${outlet.contact}'))
                                  ],
                                ));
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(outlet.name,
                              style: Theme.of(context).textTheme.headline5,
                              overflow: TextOverflow.ellipsis),
                          if (outlet.hours?.isNotEmpty ?? false)
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Row(
                                children: [
                                  Image.asset('res/icons/ic_clock.png',
                                      width: 13),
                                  Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(outlet.hours!))
                                ],
                              ),
                            ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Row(
                              children: [
                                Image.asset('res/icons/ic_call.png', width: 13),
                                Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: Text(outlet.contact))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}

class _Outlets {
  String name;
  String? hours;
  String contact;
  String realNumber;

  _Outlets(
      {required this.name,
      this.hours,
      required this.contact,
      required this.realNumber});
}
