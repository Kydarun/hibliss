import 'package:app/main.dart';
import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/model/store.dart';
import 'package:app/rest/booking.dart';
import 'package:app/rest/cart.dart';
import 'package:app/rest/store.dart';
import 'package:app/views/booking/booking.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shop/info.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:timelines/timelines.dart';
import 'package:collection/collection.dart';

import '../home.dart';

class BookingDetail extends StatefulWidget {
  final int? bookingId;
  final bool isHistory;
  final BookingOrder? booking;

  const BookingDetail(
      {required this.bookingId, this.isHistory = false, this.booking, Key? key})
      : super(key: key);

  @override
  _BookingDetailState createState() => _BookingDetailState();
}

class _BookingDetailState extends State<BookingDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Booking Details'),
      ),
      body: LoadingIndicator(
        child: FutureProvider<Response2<BookingOrder>>(
          initialData: Response2(httpCode: -1),
          create: (context) => BookingRest(context: context)
              .getBooking(bookingId: widget.bookingId!),
          child: Consumer<Response2<BookingOrder>>(
              builder: (context, response, child) => response.httpCode == -1
                  ? StatelessLoadingIndicator()
                  : ChangeNotifierProvider<BookingOrderChangeNotifier>(
                      create: (context) => BookingOrderChangeNotifier(
                          order: response.dataResults!),
                      child: Consumer<BookingOrderChangeNotifier>(
                        builder: (context, orderNotifier, child) =>
                            LoadingIndicator(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                Expanded(
                                  child: CustomScrollView(
                                    slivers: [
                                      SliverToBoxAdapter(
                                        child: Column(
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    color: Colors.white),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 20,
                                                              vertical: 10),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          5),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          5)),
                                                          color: Color(
                                                              0xff17b491)),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text('Order ID',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1
                                                                  ?.copyWith(
                                                                      fontFamily:
                                                                          'Soleil',
                                                                      color: Colors
                                                                          .white)),
                                                          Text(
                                                              '${orderNotifier.order.displayOrderId}',
                                                              style: Theme.of(
                                                                      context)
                                                                  .textTheme
                                                                  .bodyText1
                                                                  ?.copyWith(
                                                                      fontFamily:
                                                                          'Soleil',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Colors
                                                                          .white))
                                                        ],
                                                      ),
                                                    ),
                                                    Row(
                                                      children: [
                                                        if ((orderNotifier
                                                                    .order
                                                                    .imageLink
                                                                    ?.length ??
                                                                0) >
                                                            0)
                                                          Container(
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10)),
                                                              child: Image.network(
                                                                  orderNotifier
                                                                      .order
                                                                      .imageLink![
                                                                          0]
                                                                      .link!,
                                                                  width: 84,
                                                                  fit: BoxFit
                                                                      .fitWidth))
                                                        else
                                                          Image.asset(
                                                            'res/images/home.png',
                                                            width: 84,
                                                          ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                  orderNotifier
                                                                          .order
                                                                          .itemName ??
                                                                      'No title',
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .headline5),
                                                              Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              5),
                                                                  child: Row(
                                                                    children: [
                                                                      Image.asset(
                                                                          'res/icons/ic_clock.png',
                                                                          width:
                                                                              13),
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                5),
                                                                        child: Text(
                                                                            '${orderNotifier.order.duration} minutes',
                                                                            style:
                                                                                Theme.of(context).textTheme.bodyText1),
                                                                      ),
                                                                    ],
                                                                  )),
                                                              Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              5),
                                                                  child: Row(
                                                                    children: [
                                                                      Image.asset(
                                                                          'res/icons/ic_money.png',
                                                                          width:
                                                                              13),
                                                                      Container(
                                                                          margin: EdgeInsets.only(
                                                                              left:
                                                                                  5),
                                                                          child: Text(
                                                                              'RM${orderNotifier.order.total}',
                                                                              style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Color(0xff0095cf), fontWeight: FontWeight.bold)))
                                                                    ],
                                                                  )),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 10,
                                                          bottom: 15,
                                                          left: 20,
                                                          right: 20),
                                                      child: Divider(
                                                        color:
                                                            Color(0xffededf0),
                                                        height: 1,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 15),
                                                      child: FixedTimeline
                                                          .tileBuilder(
                                                        theme: TimelineThemeData(
                                                            nodePosition: 0,
                                                            indicatorPosition:
                                                                0),
                                                        builder: TimelineTileBuilder
                                                            .connected(
                                                                itemCount: 2,
                                                                indicatorBuilder:
                                                                    (context,
                                                                        index) {
                                                                  switch (
                                                                      index) {
                                                                    case 0:
                                                                      return Image.asset(
                                                                          'res/icons/ic_user.png',
                                                                          width:
                                                                              17);
                                                                    case 1:
                                                                      return Image
                                                                          .asset(
                                                                        'res/icons/ic_location.png',
                                                                        width:
                                                                            17,
                                                                      );
                                                                    default:
                                                                      return Container();
                                                                  }
                                                                },
                                                                connectorBuilder:
                                                                    (context,
                                                                            index,
                                                                            connectorType) =>
                                                                        Padding(
                                                                          padding:
                                                                              EdgeInsets.symmetric(vertical: 8),
                                                                          child:
                                                                              DashedLineConnector(
                                                                            color:
                                                                                Color(0xffdfdfe3),
                                                                          ),
                                                                        ),
                                                                contentsBuilder:
                                                                    (context,
                                                                        index) {
                                                                  switch (
                                                                      index) {
                                                                    case 0:
                                                                      return Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                10,
                                                                            bottom:
                                                                                20),
                                                                        child:
                                                                            Column(
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            Text('Therapist: ${orderNotifier.order.therapistName}',
                                                                                style: Theme.of(context).textTheme.headline5),
                                                                            Text('${orderNotifier.order.therapistAddress}',
                                                                                style: Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.normal))
                                                                          ],
                                                                        ),
                                                                      );
                                                                    case 1:
                                                                      return Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                10),
                                                                        child:
                                                                            Column(
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            Text('Customer: ${orderNotifier.order.customerName}',
                                                                                style: Theme.of(context).textTheme.headline5),
                                                                            Text('${orderNotifier.order.customerAddress}',
                                                                                style: Theme.of(context).textTheme.headline5?.copyWith(fontWeight: FontWeight.normal))
                                                                          ],
                                                                        ),
                                                                      );
                                                                    default:
                                                                      return Container();
                                                                  }
                                                                }),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: double.infinity,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          5),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          5)),
                                                          color: Colors.white),
                                                      child: Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 15,
                                                                  right: 15,
                                                                  top: 16,
                                                                  bottom: 15),
                                                          padding:
                                                              EdgeInsets.all(8),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xffededf0)),
                                                          child: Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            children: [
                                                              Image.asset(
                                                                  'res/icons/ic_calendar_gray.png',
                                                                  width: 13.5),
                                                              Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10),
                                                                  child: Text(
                                                                      '${orderNotifier.order.displayTimeSlot}',
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .bodyText1
                                                                          ?.copyWith(
                                                                              color: Color(0xff0095cf),
                                                                              fontWeight: FontWeight.bold)))
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                )),
                                            Container(
                                              margin: EdgeInsets.only(top: 5),
                                              padding: EdgeInsets.only(
                                                  top: 15,
                                                  left: 20,
                                                  right: 20,
                                                  bottom: 20),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.white),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text('Subtotal:',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1
                                                                  ?.copyWith(
                                                                      fontSize:
                                                                          17)),
                                                      Text(
                                                          'RM${orderNotifier.order.subtotal!.toStringAsFixed(2)}',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyText1
                                                                  ?.copyWith(
                                                                      fontSize:
                                                                          17))
                                                    ],
                                                  ),
                                                  Container(
                                                    margin:
                                                        EdgeInsets.only(top: 4),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text('Tax:',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontSize:
                                                                        17)),
                                                        Text(
                                                            'RM${orderNotifier.order.tax!.toStringAsFixed(2)}',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontSize:
                                                                        17))
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        EdgeInsets.only(top: 4),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text('Travel Fee:',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontSize:
                                                                        17)),
                                                        Text(
                                                            'RM${orderNotifier.order.travelFee!.toStringAsFixed(2)}',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .bodyText1
                                                                ?.copyWith(
                                                                    fontSize:
                                                                        17))
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Divider(
                                                      height: 1,
                                                      color: Color(0xffededf0),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text('Total:',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .headline5),
                                                        Text(
                                                            'RM${response.dataResults!.total!.toStringAsFixed(2)}',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .headline5)
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                if (widget.isHistory) ...[
                                  if (orderNotifier.order.reorderFlag ?? false)
                                    Container(
                                        margin: EdgeInsets.only(
                                            bottom: 40, top: 10),
                                        child: Row(children: [
                                          Expanded(
                                              child: Container(
                                            margin: EdgeInsets.only(right: 7),
                                            child: ElevatedButton(
                                                onPressed: () {
                                                  reorder(orderNotifier.order);
                                                },
                                                child: Text('Reorder')),
                                          )),
                                        ]))
                                ] else
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 40, top: 10),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            child: Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: ElevatedButton(
                                              onPressed: () {
                                                DateFormat _format = DateFormat(
                                                    'dd MMM yyyy hh:mm a');
                                                DateTime _timeSlot =
                                                    _format.parse(orderNotifier
                                                        .order
                                                        .displayTimeSlot!);
                                                bool _noRefund = _timeSlot
                                                        .difference(
                                                            DateTime.now())
                                                        .inDays <
                                                    2;
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: true,
                                                    builder:
                                                        (ctx) => HiBlissDialog(
                                                              context: ctx,
                                                              title:
                                                                  'Confirm cancellation.',
                                                              message: _noRefund
                                                                  ? 'There will be no refund upon cancelation of your appointment'
                                                                  : 'Are you sure you want to cancel your booking?',
                                                              onConfirm: () {
                                                                cancel(
                                                                    context:
                                                                        context,
                                                                    order: orderNotifier
                                                                        .order);
                                                              },
                                                            ));
                                              },
                                              child: Text('Cancel Booking')),
                                        )),
                                        if (orderNotifier
                                                .order.rescheduleFlag ??
                                            false)
                                          Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 7),
                                                  child: ElevatedButton(
                                                      onPressed: () {
                                                        showModalBottomSheet(
                                                            enableDrag: true,
                                                            context: context,
                                                            isScrollControlled:
                                                                true,
                                                            backgroundColor:
                                                                Colors.white,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            20)),
                                                            builder: (ctx) =>
                                                                Container(
                                                                    constraints: BoxConstraints(
                                                                        maxHeight: MediaQuery.of(ctx).size.height >
                                                                                727
                                                                            ? 727
                                                                            : MediaQuery.of(ctx).size.height),
                                                                    height: 727,
                                                                    child: BookingCalendarView(
                                                                        order: orderNotifier.order,
                                                                        callback: (timeslot) {
                                                                          if (timeslot !=
                                                                              null) {
                                                                            reschedule(
                                                                                context: context,
                                                                                order: response.dataResults!,
                                                                                timeslot: timeslot);
                                                                          }
                                                                        })));
                                                      },
                                                      child:
                                                          Text('Reschedule'))))
                                      ],
                                    ),
                                  )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
        ),
      ),
    );
  }

  Future<void> reorder(BookingOrder book) async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await BookingRest(context: context)
        .getBooking(bookingId: book.bookingId!);
    try {
      if (_res.dataResults!.productType == 'Service') {
        await CartRest(context: context).addServiceToCart(
            productId: book.productId ?? widget.booking?.productId ?? 0,
            customerAddressId: book.customerAddressId ??
                widget.booking?.customerAddressId ??
                0,
            therapistId: book.therapistId ?? widget.booking?.therapistId ?? 0,
            therapistAddressId: book.therapistAddressId ??
                widget.booking?.therapistAddressId ??
                0);
      } else {
        await CartRest(context: context).addGoodsToCart(
            productId: book.productId ?? widget.booking?.productId ?? 0,
            customerAddressId: book.customerAddressId ??
                widget.booking?.customerAddressId ??
                0);
      }
      await context.read<Cart>().load();
      Navigator.of(context).pop(4);
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }

    /* try {
      final _res = await BookingRest(context: context)
          .getBooking(bookingId: book.bookingId!);
      if (_res.dataResults!.productType == "Service") {
        final _category = await StoreRest(context: context)
            .service(id: _res.dataResults!.productId!);
        final _list = await StoreRest(context: context)
            .list(categoryId: _category.dataResults!.categoryId!);
        Navigator.of(context).pop(4);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ItemInfo(
                alacarte: _list.dataResults!.alacarte?.firstWhereOrNull(
                    (element) => element.id! == _res.dataResults!.productId!),
                package: _list.dataResults!.package?.firstWhereOrNull(
                    (element) =>
                        element.id! == _res.dataResults!.productId!))));
      } else if (_res.dataResults!.productType == "Package") {
        final _category = await StoreRest(context: context)
            .package(id: _res.dataResults!.productId!);
        final _list = await StoreRest(context: context)
            .list(categoryId: _category.dataResults!.categoryId!);
        Navigator.of(context).pop(4);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ItemInfo(
                package: _list.dataResults!.package!.firstWhere(
                    (element) => element.id! == _res.dataResults!.productId!,
                    orElse: () => StorePackage()))));
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    } */
  }

  Future cancel(
      {required BuildContext context, required BookingOrder order}) async {
    context.read<LoadingState>().toggle(value: true);
    DateFormat _format = DateFormat('dd MMM yyyy hh:mm a');
    DateTime _timeSlot = _format.parse(order.displayTimeSlot!);
    bool _noRefund = _timeSlot.difference(DateTime.now()).inDays < 2;
    var _res;
    if (_noRefund) {
      _res = await BookingRest(context: context)
          .cancel(bookingId: order.bookingId!);
    } else {
      _res = await BookingRest(context: context)
          .delete(bookingId: order.bookingId!);
    }
    context.read<LoadingState>().toggle(value: false);
    if (_res.success) {
      showModalBottomSheet(
          context: context,
          isDismissible: true,
          isScrollControlled: true,
          enableDrag: true,
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20), topLeft: Radius.circular(20))),
          builder: (ctx) => Container(
                margin: EdgeInsets.symmetric(horizontal: 15),
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(ctx).size.height > 727
                        ? 727
                        : MediaQuery.of(ctx).size.height),
                height: 727,
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Image.asset('res/icons/ic_msg_check.png', width: 78),
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Text('Booking has been cancelled.',
                                style: Theme.of(context).textTheme.headline5),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 2, bottom: 30),
                            child: Text(
                                DateFormat('dd MMM yyyy h:mmaa')
                                    .format(DateTime.now()),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(fontSize: 17)),
                          )
                        ],
                      ),
                    ),
                    Divider(height: 1, color: Color(0xffcecece)),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin:
                                EdgeInsets.only(top: 8, left: 15, right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Order ID',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(fontSize: 17)),
                                Text('${order.displayOrderId}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(fontSize: 17))
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )).then((value) {
        Navigator.of(context).pop();
      });
    } else {
      CommonUI(context: context)
          .snackbar(message: 'Failed to cancel, ${_res.message!}');
    }
  }

  Future reschedule(
      {required BuildContext context,
      required BookingOrder order,
      required Timeslot timeslot}) async {
    context.read<LoadingState>().toggle(value: true);
    debugPrint(DateFormat('yyyy-MM-dd HH:mm:ss').format(timeslot.value!));
    final _res = await BookingRest(context: context)
        .reschedule(bookingId: order.bookingId!, date: timeslot.value!);
    if (_res.success) {
      CommonUI(context: context).snackbar(message: 'Rescheduled successfully.');
      final _updatedRes = await BookingRest(context: context)
          .getBooking(bookingId: widget.bookingId!);
      context
          .read<BookingOrderChangeNotifier>()
          .update(order: _updatedRes.dataResults!);
    } else {
      CommonUI(context: context)
          .snackbar(message: 'Failed to reschedule, ${_res.message!}');
    }
    context.read<LoadingState>().toggle(value: false);
  }
}

class BookingOrderChangeNotifier with ChangeNotifier {
  BookingOrder order;

  BookingOrderChangeNotifier({required this.order});

  update({required BookingOrder order}) {
    this.order = order;
    notifyListeners();
  }
}
