import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/booking.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/datepicker/flutter_datetime_picker.dart';
import 'package:app/view_model/booking_form.dart';
import 'package:app/views/booking/detail.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class BookingPage extends StatelessWidget {
  final int relationshipId;
  final BookingOrder order;

  BookingPage({required this.order, this.relationshipId = 1});

  @override
  Widget build(BuildContext context) {
    return FutureProvider<Response<User>>(
      initialData: Response<User>(httpCode: -1),
      create: (context) async {
        if (this.relationshipId == 1) {
          return UserRest(context: context).me();
        }
        return Response(httpCode: 0);
      },
      child: Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          title: Text('Booking Details'),
          centerTitle: true,
        ),
        body: LoadingIndicator(
          child: Consumer<Response<User>>(
            builder: (context, response, child) => response.httpCode == -1
                ? StatelessLoadingIndicator()
                : Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    child: BlocProvider(
                      create: (context) => BookingDetailFormBloc(
                          context: context,
                          order: order,
                          relationshipId: this.relationshipId,
                          user: response.dataResults?[0]),
                      child: Builder(
                        builder: (context) {
                          final _bookingFormBloc =
                              context.read<BookingDetailFormBloc>();
                          return FormBlocListener<BookingDetailFormBloc,
                              Response, Response>(
                            onSuccess: (context, success) {
                              CommonUI(context: context)
                                  .snackbar(message: 'Booking successful.');
                              Navigator.of(context).pop(int.tryParse(
                                  success.successResponse!.message!));
                            },
                            onFailure: (context, failure) {
                              CommonUI(context: context).fail(failure: failure);
                            },
                            child: CustomScrollView(
                              slivers: [
                                SliverToBoxAdapter(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Full Name',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      TextFieldBlocBuilder(
                                        textFieldBloc: _bookingFormBloc.name,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                        decoration: CommonUI(context: context)
                                            .decoration2(),
                                        textInputAction: TextInputAction.next,
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Date of Birth',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      BlocBuilder<
                                              InputFieldBloc<DateTime, Object>,
                                              InputFieldBlocState>(
                                          bloc: _bookingFormBloc.dob,
                                          builder:
                                              (context, state) =>
                                                  GestureDetector(
                                                    onTap: () {
                                                      showCupertinoModalPopup(
                                                          context: context,
                                                          builder:
                                                              (context) =>
                                                                  Container(
                                                                    height: 461,
                                                                    decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.only(
                                                                            topLeft: Radius.circular(
                                                                                20),
                                                                            topRight: Radius.circular(
                                                                                20)),
                                                                        color: CupertinoColors
                                                                            .white),
                                                                    child:
                                                                        Column(
                                                                      children: [
                                                                        Container(
                                                                          margin:
                                                                              EdgeInsets.symmetric(horizontal: 13),
                                                                          height:
                                                                              371,
                                                                          child: CustomDatePickerWidget(
                                                                              pickerModel: DatePickerModel(
                                                                                currentTime: _bookingFormBloc.dob.value ?? DateTime.parse('1990-06-15'),
                                                                                minTime: DateTime.parse('1900-01-01'),
                                                                                maxTime: DateTime.now(),
                                                                              ),
                                                                              theme: DatePickerTheme(containerHeight: 371, itemHeight: 65, itemStyle: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 30)),
                                                                              onChanged: (time) {
                                                                                _bookingFormBloc.dob.updateValue(time);
                                                                              }),
                                                                        ),
                                                                        Container(
                                                                          margin: EdgeInsets.only(
                                                                              bottom: 40,
                                                                              left: 20,
                                                                              right: 20),
                                                                          child:
                                                                              Row(
                                                                            children: [
                                                                              Expanded(
                                                                                child: ElevatedButton(
                                                                                    child: Text('Okay'),
                                                                                    onPressed: () {
                                                                                      Navigator.of(context).pop();
                                                                                    }),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ));
                                                    },
                                                    child: TextField(
                                                      enabled: false,
                                                      controller:
                                                          TextEditingController(
                                                              text: state.value !=
                                                                      null
                                                                  ? '${DateFormat('dd MMM yyyy').format(state.value!)}'
                                                                  : null),
                                                      decoration: CommonUI(
                                                              context: context)
                                                          .decoration2()
                                                          .copyWith(
                                                              disabledBorder: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  borderSide: BorderSide(
                                                                      color: Color(
                                                                          0x73bcd5ed),
                                                                      width:
                                                                          1)),
                                                              suffixIcon: Icon(Icons
                                                                  .arrow_drop_down)),
                                                    ),
                                                  )),
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Gender',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      BlocBuilder<
                                          SelectFieldBloc<_Gender, dynamic>,
                                          SelectFieldBlocState<_Gender,
                                              dynamic>>(
                                        bloc: _bookingFormBloc.gender,
                                        builder: (context, state) =>
                                            GestureDetector(
                                          onTap: () {
                                            showCupertinoModalPopup(
                                                context: context,
                                                builder: (context) => Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 35,
                                                          left: 7,
                                                          right: 7),
                                                      child:
                                                          CupertinoActionSheet(
                                                        title: Text(
                                                            'Choose Gender'),
                                                        cancelButton:
                                                            CupertinoActionSheetAction(
                                                                isDestructiveAction:
                                                                    false,
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                    'Cancel')),
                                                        actions: [
                                                          _Gender.male,
                                                          _Gender.female
                                                        ]
                                                            .map((gender) =>
                                                                CupertinoActionSheetAction(
                                                                    onPressed:
                                                                        () {
                                                                      Navigator.of(
                                                                              context)
                                                                          .pop();
                                                                      _bookingFormBloc
                                                                          .gender
                                                                          .updateValue(
                                                                              gender);
                                                                    },
                                                                    child: Text(
                                                                        gender
                                                                            .name)))
                                                            .toList(),
                                                      ),
                                                    ));
                                          },
                                          child: TextField(
                                            enabled: false,
                                            controller: TextEditingController(
                                                text: state.value?.name ?? ''),
                                            decoration: CommonUI(
                                                    context: context)
                                                .decoration2()
                                                .copyWith(
                                                    disabledBorder:
                                                        OutlineInputBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        7),
                                                            borderSide: BorderSide(
                                                                color: Color(
                                                                    0x73bcd5ed),
                                                                width: 1)),
                                                    suffixIcon: Icon(
                                                        Icons.arrow_drop_down)),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(right: 9),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 4),
                                                      child: Text('Weight (kg)',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .subtitle1),
                                                    ),
                                                    TextFieldBlocBuilder(
                                                      textFieldBloc:
                                                          _bookingFormBloc
                                                              .weight,
                                                      keyboardType: TextInputType
                                                          .numberWithOptions(
                                                              signed: true,
                                                              decimal: false),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText2,
                                                      decoration: CommonUI(
                                                              context: context)
                                                          .decoration2(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 9),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 4),
                                                      child: Text('Height (cm)',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .subtitle1),
                                                    ),
                                                    TextFieldBlocBuilder(
                                                      textFieldBloc:
                                                          _bookingFormBloc
                                                              .height,
                                                      keyboardType: TextInputType
                                                          .numberWithOptions(
                                                              signed: true,
                                                              decimal: false),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText2,
                                                      decoration: CommonUI(
                                                              context: context)
                                                          .decoration2(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Email',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      TextFieldBlocBuilder(
                                        textFieldBloc: _bookingFormBloc.email,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                        decoration: CommonUI(context: context)
                                            .decoration2(),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Contact',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      TextFieldBlocBuilder(
                                        textFieldBloc: _bookingFormBloc.contact,
                                        keyboardType: TextInputType.number,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2,
                                        decoration: CommonUI(context: context)
                                            .decoration2(),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(top: 20, bottom: 4),
                                        child: Text('Time Slot',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 10),
                                        child:
                                            BlocBuilder<
                                                    InputFieldBloc<DateTime,
                                                        Object>,
                                                    InputFieldBlocState>(
                                                bloc: _bookingFormBloc.timeslot,
                                                builder:
                                                    (context, state) =>
                                                        GestureDetector(
                                                          onTap: () {
                                                            showModalBottomSheet(
                                                                context:
                                                                    context,
                                                                isScrollControlled:
                                                                    true,
                                                                backgroundColor:
                                                                    Colors
                                                                        .white,
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            20)),
                                                                builder: (context) => Container(
                                                                    constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height > 727 ? 727 : MediaQuery.of(context).size.height),
                                                                    height: 727,
                                                                    child: BookingCalendarView(
                                                                        order: order,
                                                                        callback: (timeslot) {
                                                                          if (timeslot !=
                                                                              null) {
                                                                            _bookingFormBloc.timeslot.updateValue(timeslot.value);
                                                                          }
                                                                        })));
                                                          },
                                                          child: TextField(
                                                            enabled: false,
                                                            controller: TextEditingController(
                                                                text: state.value !=
                                                                        null
                                                                    ? '${DateFormat('dd MMM yyyy, hh:mm').format(state.value!)}${DateFormat('aa').format(state.value!).toLowerCase()}'
                                                                    : null),
                                                            decoration: CommonUI(context: context).decoration2().copyWith(
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                7),
                                                                    borderSide: BorderSide(
                                                                        color: Color(
                                                                            0x73bcd5ed),
                                                                        width:
                                                                            1)),
                                                                errorText: state
                                                                        .hasError
                                                                    ? 'Time Slot is required.'
                                                                    : null,
                                                                suffixIcon:
                                                                    Icon(Icons
                                                                        .arrow_drop_down)),
                                                          ),
                                                        )),
                                      ),
                                    ],
                                  ),
                                ),
                                SliverFillRemaining(
                                  hasScrollBody: false,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 40),
                                          child: ElevatedButton(
                                              child: Text('Book'),
                                              onPressed:
                                                  _bookingFormBloc.submit),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

class BookingDetailFormBloc extends FormBloc<Response, Response> {
  final BuildContext context;
  final BookingOrder order;
  final int relationshipId;
  User? user;

  final name =
      TextFieldBloc(name: 'name', validators: [FieldBlocValidators.required]);

  final gender = SelectFieldBloc<_Gender, dynamic>(
      name: 'gender',
      items: [_Gender.empty, _Gender.male, _Gender.female],
      validators: [FieldBlocValidators.required]);

  final dob = InputFieldBloc<DateTime, Object>(name: 'birthday');

  final contact = TextFieldBloc(name: 'phoneNumber', validators: [
    FieldBlocValidators.required,
    (input) {
      if (input != null) {
        if (input.length > 30) {
          return "Phone number must not exceed 30 characters.";
        }
        String pattern = r'^[0-9]+$';
        RegExp regExp = new RegExp(pattern);
        if (!regExp.hasMatch(input)) {
          return 'Phone number must consists of numbers only. (No dash or space)';
        }
      }
      return null;
    }
  ]);

  final email = TextFieldBloc(
      name: 'email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final weight =
      TextFieldBloc(name: 'weight', validators: [FieldBlocValidators.required]);

  final height =
      TextFieldBloc(name: 'height', validators: [FieldBlocValidators.required]);

  final timeslot = InputFieldBloc<DateTime, Object>(
      name: 'timeslot', validators: [FieldBlocValidators.required]);

  BookingDetailFormBloc(
      {required this.context,
      required this.order,
      required this.relationshipId,
      this.user})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [
      name,
      gender,
      dob,
      contact,
      email,
      weight,
      height,
      timeslot
    ]);
    name.updateInitialValue(user?.name ?? '');
    if (user?.gender != null) {
      switch (user!.gender) {
        case 0:
          gender.updateInitialValue(_Gender.empty);
          break;
        case 1:
          gender.updateInitialValue(_Gender.male);
          break;
        case 2:
          gender.updateInitialValue(_Gender.female);
          break;
      }
    }
    dob.updateInitialValue(user?.birthday);
    contact.updateInitialValue(user?.phoneNumber ?? '');
    email.updateInitialValue(user?.email ?? '');
    weight.updateInitialValue(user?.weight?.toString() ?? '');
    height.updateInitialValue(user?.height?.toString() ?? '');
  }

  @override
  void onSubmitting() async {
    try {
      context.read<LoadingState>().toggle(value: true);
      final _res = await BookingRest(context: context).book(
          bookingForm: BookingFormViewModel(
              orderId: order.orderId,
              relationshipId: this.relationshipId,
              therapistId: order.therapistId,
              fullName: name.value,
              gender: gender.value?.value,
              dateOfBirth: dob.value.toString(),
              contact: contact.value,
              email: email.value,
              weight: weight.valueToDouble,
              height: height.valueToDouble,
              timeslot:
                  '${DateFormat('yyyy-MM-dd HH:mm:ss').format(timeslot.value!)}'));
      if (_res.success) {
        emitSuccess(successResponse: _res);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}

class _Gender {
  static final empty = _Gender(name: '', value: 0);
  static final male = _Gender(name: 'Male', value: 1);
  static final female = _Gender(name: 'Female', value: 2);

  String name;
  int value;

  _Gender({required this.name, required this.value});
}

class BookingCalendarView extends StatefulWidget {
  final BookingOrder order;
  final Function(Timeslot?) callback;

  const BookingCalendarView(
      {required this.order, required this.callback, Key? key})
      : super(key: key);

  @override
  _BookingCalendarViewState createState() => _BookingCalendarViewState();
}

class _BookingCalendarViewState extends State<BookingCalendarView> {
  DateTime? _selectedDay;
  DateTime _focusedDay = DateTime.now().add(Duration(days: 2));
  Timeslot? _selectedTimeSlot;
  bool _isTimeslotLoading = false;

  List<Timeslot> _timeslots = [];

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TableCalendar(
                    headerStyle: HeaderStyle(
                        titleTextStyle: Theme.of(context).textTheme.headline5!,
                        titleCentered: true,
                        formatButtonVisible: false),
                    daysOfWeekHeight: 24,
                    daysOfWeekStyle: DaysOfWeekStyle(
                        weekdayStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 19),
                        weekendStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 19),
                        dowTextFormatter: (date, locale) =>
                            DateFormat('E').format(date).characters.first),
                    calendarStyle: CalendarStyle(
                        todayDecoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.transparent),
                        todayTextStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 17),
                        selectedDecoration: BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xff17b491)),
                        defaultTextStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 17),
                        weekendTextStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 17),
                        outsideTextStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 17, color: Color(0xffb2b2b2))),
                    startingDayOfWeek: StartingDayOfWeek.monday,
                    onDaySelected: (selectedDay, focusedDay) {
                      setState(() {
                        _selectedDay = selectedDay;
                        _focusedDay = focusedDay;
                      });
                      getTimeslots();
                    },
                    selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
                    focusedDay: _focusedDay,
                    firstDay: DateTime.now().add(Duration(days: 2)),
                    lastDay: DateTime.now().add(Duration(days: 1500))),
                Divider(height: 1),
                Container(
                    margin: EdgeInsets.only(top: 18, left: 2, bottom: 11),
                    child: Text('Available time slots:',
                        style: Theme.of(context).textTheme.headline5)),
                if (_isTimeslotLoading)
                  Center(child: CircularProgressIndicator()),
                ..._timeslots
                    .map((timeslot) => InkWell(
                          onTap: (timeslot.isEnabled ?? false)
                              ? () {
                                  setState(() {
                                    _selectedTimeSlot = timeslot;
                                  });
                                }
                              : null,
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 2, right: 2, bottom: 5),
                            padding: EdgeInsets.symmetric(vertical: 17),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: (timeslot.isEnabled ?? false)
                                    ? Color(0xffe4ebf2)
                                    : Color(0xffededf0)),
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(timeslot.display!,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            ?.copyWith(
                                                fontSize: 17,
                                                color: (timeslot.isEnabled ??
                                                        false)
                                                    ? Color(0xff161640)
                                                    : Color(0xffb2b2b2))),
                                    if (timeslot.isEnabled ?? false)
                                      Image.asset(
                                          'res/icons/ic_check_round_${timeslot == _selectedTimeSlot ? "blue" : "blueborder"}.png',
                                          width: 21)
                                    else
                                      Container(
                                          width: 21,
                                          height: 21,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Color(0xffb2b2b2)))
                                  ]),
                            ),
                          ),
                        ))
                    .toList()
              ],
            ),
          ),
        ),
        SliverFillRemaining(
          hasScrollBody: false,
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 40),
                    child: ElevatedButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          widget.callback(_selectedTimeSlot);
                        }),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Future<void> getTimeslots() async {
    setState(() {
      _isTimeslotLoading = true;
      _timeslots = [];
    });
    Response<Timeslot> _res = await BookingRest(context: context)
        .timeslots(date: _selectedDay!, therapistId: widget.order.therapistId!);
    setState(() {
      _isTimeslotLoading = false;
      _timeslots = _res.dataResults ?? [];
    });
  }
}
