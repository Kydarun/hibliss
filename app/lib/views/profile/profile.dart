import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/user.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:app/util/datepicker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Unauthenticated(
      child: FutureProvider<Response<User>>(
        initialData: Response<User>(httpCode: -1),
        create: (context) => UserRest(context: context).me(),
        child: Scaffold(
          backgroundColor: Color(0xfff4f7fa),
          appBar: AppBar(
            title: Text('Profile'),
            centerTitle: true,
          ),
          body: LoadingIndicator(
            child: Consumer<Response<User>>(
              builder: (context, response, child) => response.httpCode == -1
                  ? StatelessLoadingIndicator()
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 15),
                      child: BlocProvider(
                        create: (context) => ProfileFormBloc(
                            context: context, user: response.dataResults?[0]),
                        child: Builder(
                          builder: (context) {
                            final _profileFormBloc =
                                context.read<ProfileFormBloc>();
                            return FormBlocListener<ProfileFormBloc, Response,
                                Response>(
                              onSuccess: (context, success) {
                                CommonUI(context: context)
                                    .snackbar(message: 'Profile updated.');
                              },
                              onFailure: (context, failure) {
                                CommonUI(context: context)
                                    .fail(failure: failure);
                              },
                              child: CustomScrollView(
                                slivers: [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 4),
                                          child: Text('Full Name',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                        ),
                                        TextFieldBlocBuilder(
                                            textFieldBloc:
                                                _profileFormBloc.name,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                            decoration:
                                                CommonUI(context: context)
                                                    .decoration2(),
                                            textInputAction:
                                                TextInputAction.next,
                                            maxLength: 200),
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 4),
                                          child: Text('Gender',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                        ),
                                        BlocBuilder<
                                            SelectFieldBloc<Gender, dynamic>,
                                            SelectFieldBlocState<Gender,
                                                dynamic>>(
                                          bloc: _profileFormBloc.gender,
                                          builder: (context, state) =>
                                              GestureDetector(
                                            onTap: () {
                                              showCupertinoModalPopup(
                                                  context: context,
                                                  builder:
                                                      (context) => Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom: 35,
                                                                    left: 7,
                                                                    right: 7),
                                                            child:
                                                                CupertinoActionSheet(
                                                              title: Text(
                                                                  'Choose Gender'),
                                                              cancelButton:
                                                                  CupertinoActionSheetAction(
                                                                      isDestructiveAction:
                                                                          false,
                                                                      onPressed:
                                                                          () {
                                                                        Navigator.of(context)
                                                                            .pop();
                                                                      },
                                                                      child: Text(
                                                                          'Cancel')),
                                                              actions: [
                                                                Gender.male,
                                                                Gender.female
                                                              ]
                                                                  .map((gender) =>
                                                                      CupertinoActionSheetAction(
                                                                          onPressed:
                                                                              () {
                                                                            Navigator.of(context).pop();
                                                                            _profileFormBloc.gender.updateValue(gender);
                                                                          },
                                                                          child:
                                                                              Text(gender.name)))
                                                                  .toList(),
                                                            ),
                                                          ));
                                            },
                                            child: TextField(
                                              enabled: false,
                                              controller: TextEditingController(
                                                  text:
                                                      state.value?.name ?? ''),
                                              decoration: CommonUI(
                                                      context: context)
                                                  .decoration2()
                                                  .copyWith(
                                                      disabledBorder: OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(7),
                                                          borderSide: BorderSide(
                                                              color: Color(
                                                                  0x73bcd5ed),
                                                              width: 1)),
                                                      suffixIcon: Icon(Icons
                                                          .arrow_drop_down)),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 4),
                                          child: Text('Date of Birth',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                        ),
                                        BlocBuilder<
                                                InputFieldBloc<DateTime,
                                                    Object>,
                                                InputFieldBlocState>(
                                            bloc: _profileFormBloc.dob,
                                            builder:
                                                (context, state) =>
                                                    GestureDetector(
                                                      onTap: () {
                                                        showCupertinoModalPopup(
                                                            context: context,
                                                            builder:
                                                                (context) =>
                                                                    Container(
                                                                      height:
                                                                          461,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.only(
                                                                              topLeft: Radius.circular(20),
                                                                              topRight: Radius.circular(20)),
                                                                          color: CupertinoColors.white),
                                                                      child:
                                                                          Column(
                                                                        children: [
                                                                          Container(
                                                                            margin:
                                                                                EdgeInsets.symmetric(horizontal: 13),
                                                                            height:
                                                                                371,
                                                                            child: CustomDatePickerWidget(
                                                                                pickerModel: DatePickerModel(
                                                                                  currentTime: _profileFormBloc.dob.value ?? DateTime.parse('1990-06-15'),
                                                                                  minTime: DateTime.parse('1900-01-01'),
                                                                                  maxTime: DateTime.now(),
                                                                                ),
                                                                                theme: DatePickerTheme(containerHeight: 371, itemHeight: 65, itemStyle: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 30)),
                                                                                onChanged: (time) {
                                                                                  _profileFormBloc.dob.updateValue(time);
                                                                                }),
                                                                          ),
                                                                          Container(
                                                                            margin: EdgeInsets.only(
                                                                                bottom: 40,
                                                                                left: 20,
                                                                                right: 20),
                                                                            child:
                                                                                Row(
                                                                              children: [
                                                                                Expanded(
                                                                                  child: ElevatedButton(
                                                                                      child: Text('Okay'),
                                                                                      onPressed: () {
                                                                                        Navigator.of(context).pop();
                                                                                      }),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ));
                                                      },
                                                      child: TextField(
                                                        enabled: false,
                                                        controller: TextEditingController(
                                                            text: state.value !=
                                                                    null
                                                                ? '${DateFormat('dd MMM yyyy').format(state.value!)}'
                                                                : null),
                                                        decoration:
                                                            CommonUI(
                                                                    context:
                                                                        context)
                                                                .decoration2()
                                                                .copyWith(
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              7),
                                                                      borderSide: BorderSide(
                                                                          color: Color(
                                                                              0x73bcd5ed),
                                                                          width:
                                                                              1)),
                                                                  suffixIcon:
                                                                      Icon(Icons
                                                                          .arrow_drop_down),
                                                                  errorText: state
                                                                          .hasError
                                                                      ? 'Date of Birth is required.'
                                                                      : null,
                                                                ),
                                                      ),
                                                    )),
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 4),
                                          child: Text('Contact',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                        ),
                                        TextFieldBlocBuilder(
                                          textFieldBloc:
                                              _profileFormBloc.contact,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2,
                                          keyboardType: TextInputType.number,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 4),
                                          child: Text('Email',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1),
                                        ),
                                        TextFieldBlocBuilder(
                                          textFieldBloc: _profileFormBloc.email,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2,
                                          decoration: CommonUI(context: context)
                                              .decoration2(),
                                          textInputAction: TextInputAction.next,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 20),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(right: 9),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 4),
                                                        child: Text(
                                                            'Weight (kg)',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1),
                                                      ),
                                                      TextFieldBlocBuilder(
                                                        textFieldBloc:
                                                            _profileFormBloc
                                                                .weight,
                                                        keyboardType: TextInputType
                                                            .numberWithOptions(
                                                                signed: false,
                                                                decimal: true),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText2,
                                                        decoration: CommonUI(
                                                                context:
                                                                    context)
                                                            .decoration2(),
                                                        textInputAction:
                                                            TextInputAction
                                                                .next,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 9),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 4),
                                                        child: Text(
                                                            'Height (cm)',
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle1),
                                                      ),
                                                      TextFieldBlocBuilder(
                                                        textFieldBloc:
                                                            _profileFormBloc
                                                                .height,
                                                        keyboardType: TextInputType
                                                            .numberWithOptions(
                                                                signed: false,
                                                                decimal: true),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText2,
                                                        decoration: CommonUI(
                                                                context:
                                                                    context)
                                                            .decoration2(),
                                                        textInputAction:
                                                            TextInputAction
                                                                .next,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SliverFillRemaining(
                                    hasScrollBody: false,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 40),
                                            child: ElevatedButton(
                                                child: Text('Save'),
                                                onPressed:
                                                    _profileFormBloc.submit),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileFormBloc extends FormBloc<Response, Response> {
  BuildContext context;
  User? user;

  final name =
      TextFieldBloc(name: 'name', validators: [FieldBlocValidators.required]);

  final gender = SelectFieldBloc<Gender, dynamic>(
      name: 'gender', items: [Gender.empty, Gender.male, Gender.female]);

  final dob = InputFieldBloc<DateTime, Object>(
      name: 'birthday', validators: [FieldBlocValidators.required]);

  final contact = TextFieldBloc(
      name: 'phoneNumber',
      validators: <String? Function(String?)>[
        FieldBlocValidators.required,
        (input) {
          if (input != null) {
            if (input.length > 30) {
              return "Phone number must not exceed 30 characters.";
            }
            String pattern = r'^(?=.*?[0-9])(?!.*?[.!@#\$&*~-]).{0,}$';
            RegExp regExp = new RegExp(pattern);
            if (!regExp.hasMatch(input)) {
              return 'Invalid phone number format.';
            }
          }
          return null;
        }
      ]);

  final email = TextFieldBloc(
      name: 'email',
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final weight = TextFieldBloc(
      name: 'weight', validators: [FieldBlocValidators.required, moreThanOne]);

  final height = TextFieldBloc(
      name: 'height', validators: [FieldBlocValidators.required, moreThanOne]);

  ProfileFormBloc({required this.context, this.user})
      : super(autoValidate: false) {
    addFieldBlocs(
        fieldBlocs: [name, gender, dob, contact, email, weight, height]);
    name.updateValue(user?.name ?? '');
    if (user?.gender != null) {
      switch (user!.gender) {
        case 0:
          gender.updateValue(Gender.empty);
          break;
        case 1:
          gender.updateValue(Gender.male);
          break;
        case 2:
          gender.updateValue(Gender.female);
          break;
      }
    }
    dob.updateValue(user?.birthday);
    contact.updateValue(user?.phoneNumber ?? '');
    email.updateValue(user?.email ?? '');
    weight.updateValue(user?.weight?.toString() ?? '');
    height.updateValue(user?.height?.toString() ?? '');
  }

  static String? moreThanOne(String? input) {
    if (input == null) {
      return "This field is required.";
    } else if (double.tryParse(input) == null) {
      return "Must be a number.";
    } else if (double.tryParse(input)! < 1) {
      return "Must be more than 1.";
    } else {
      String pattern = r'^(?!\d{7,})\d*(\.\d{1})?$';
      RegExp regExp = new RegExp(pattern);
      if (!regExp.hasMatch(input)) {
        return 'Maximum of 6 digits and 1 decimal point.';
      }
    }
    return null;
  }

  @override
  void onSubmitting() async {
    try {
      FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
      }
      context.read<LoadingState>().toggle(value: true);
      final _res = await UserRest(context: context).edit(
          user: User(
              name: name.value,
              gender: gender.value?.value,
              birthday: dob.value,
              phoneNumber: contact.value,
              email: email.value,
              weight: weight.valueToDouble,
              height: height.valueToDouble));
      if (_res.success) {
        emitSuccess(successResponse: _res, canSubmitAgain: true);
      } else {
        emitFailure(failureResponse: _res);
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}

class Gender {
  static final empty = Gender(name: '', value: 0);
  static final male = Gender(name: 'Male', value: 1);
  static final female = Gender(name: 'Female', value: 2);

  String name;
  int value;

  Gender({required this.name, required this.value});
}
