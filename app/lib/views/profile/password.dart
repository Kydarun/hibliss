import 'package:app/model/response.dart';
import 'package:app/rest/auth.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class Password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Unauthenticated(
      child: Scaffold(
        backgroundColor: Color(0xfff4f7fa),
        appBar: AppBar(
          title: Text('Password'),
          centerTitle: true,
        ),
        body: LoadingIndicator(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: BlocProvider(
              create: (context) => PasswordFormBloc(context: context),
              child: Builder(builder: (context) {
                final _passwordFormBloc = context.read<PasswordFormBloc>();
                return FormBlocListener<PasswordFormBloc, Response, Response>(
                  onSuccess: (context, success) {
                    CommonUI(context: context).success(success: success);
                    Navigator.of(context).pop();
                  },
                  onFailure: (context, failure) {
                    CommonUI(context: context).fail(failure: failure);
                  },
                  child: CustomScrollView(
                    slivers: [
                      SliverToBoxAdapter(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              child: TextFieldBlocBuilder(
                                  textFieldBloc:
                                      _passwordFormBloc.currentPassword,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  suffixButton: SuffixButton.obscureText,
                                  autofillHints: [AutofillHints.password],
                                  decoration: CommonUI(context: context)
                                      .decoration(
                                          hint: 'Enter current password')),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Text('Please enter new password',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        ?.copyWith(
                                            fontWeight: FontWeight.bold))),
                            Text(
                                'Minimum 8 characters incl. numbers & symbols.'),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: TextFieldBlocBuilder(
                                  textFieldBloc: _passwordFormBloc.newPassword,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  suffixButton: SuffixButton.obscureText,
                                  autofillHints: [AutofillHints.password],
                                  decoration: CommonUI(context: context)
                                      .decoration(hint: 'Enter new password')),
                            ),
                            TextFieldBlocBuilder(
                              textFieldBloc: _passwordFormBloc.confirm,
                              style: Theme.of(context).textTheme.bodyText2,
                              suffixButton: SuffixButton.obscureText,
                              autofillHints: [AutofillHints.password],
                              decoration: CommonUI(context: context)
                                  .decoration(hint: 'Re-enter new password'),
                            ),
                          ],
                        ),
                      ),
                      SliverFillRemaining(
                        hasScrollBody: false,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 40),
                                child: ElevatedButton(
                                  child: Text('Save'),
                                  onPressed: _passwordFormBloc.submit,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}

class PasswordFormBloc extends FormBloc<Response, Response> {
  BuildContext context;

  final currentPassword = TextFieldBloc(
      name: 'Current Password',
      validators: <String? Function(String?)>[FieldBlocValidators.required]);

  final newPassword = TextFieldBloc(
      name: 'New Password',
      validators: <String? Function(String?)>[
        FieldBlocValidators.required,
        (input) {
          if (input != null) {
            String pattern = r'^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~_?/\"' +
                '\'' r'])(?!.*?[ ]).{8,}$';
            RegExp regExp = new RegExp(pattern);
            if (!regExp.hasMatch(input)) {
              return 'Must contain at least 8 characters incl. numbers & symbols and no whitespace.';
            }
          }
          return null;
        }
      ]);

  final confirm = TextFieldBloc(name: 'Confirm Password');

  PasswordFormBloc({required this.context}) : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [currentPassword, newPassword, confirm]);
    confirm
      ..addValidators([_confirmPassword(newPassword)])
      ..subscribeToFieldBlocs([newPassword]);
  }

  Validator<String> _confirmPassword(TextFieldBloc password) {
    return (String? confirmPassword) {
      if (confirmPassword == password.value) {
        return null;
      }
      return 'Password does not match.';
    };
  }

  @override
  void onSubmitting() async {
    context.read<LoadingState>().toggle(value: true);
    final _res = await AuthRest(context: context).changePassword(
        currentPassword: currentPassword.value!,
        newPassword: newPassword.value!);
    if (_res.success) {
      emitSuccess(successResponse: _res, canSubmitAgain: true);
    } else {
      emitFailure(failureResponse: _res);
    }
    context.read<LoadingState>().toggle(value: false);
  }
}
