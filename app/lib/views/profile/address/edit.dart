import 'package:app/main.dart';
import 'package:app/model/address.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/address.dart';
import 'package:app/views/shared/common.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:provider/provider.dart';

class EditAddress extends StatelessWidget {
  final Address? address;

  EditAddress({this.address});

  @override
  Widget build(BuildContext context) {
    return Unauthenticated(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(address == null ? 'Add Address' : 'Edit Address'),
        ),
        body: LoadingIndicator(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: BlocProvider(
              create: (context) =>
                  AddressFormBloc(context: context, address: address),
              child: Builder(
                builder: (context) {
                  final _addressFormBloc = context.read<AddressFormBloc>();
                  return FormBlocListener<AddressFormBloc, Response, Response>(
                    onSuccess: (context, success) {
                      CommonUI(context: context).success(
                          success: success,
                          duration: Duration(seconds: 2),
                          customMessage: 'Saved successful!');
                      context.read<Cart>().load(
                          orderIds: context.read<Cart>().selectedOrderIds(),
                          shouldFetchPrimaryEmail: true);
                      Navigator.of(context).pop();
                    },
                    onFailure: (context, failure) {
                      CommonUI(context: context).fail(failure: failure);
                    },
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 20, bottom: 4),
                                child: Text('Title',
                                    style:
                                        Theme.of(context).textTheme.subtitle1),
                              ),
                              TextFieldBlocBuilder(
                                textFieldBloc: _addressFormBloc.title,
                                maxLength: 200,
                                style: Theme.of(context).textTheme.bodyText2,
                                decoration:
                                    CommonUI(context: context).decoration2(),
                                textInputAction: TextInputAction.next,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20, bottom: 4),
                                child: Text('Address',
                                    style:
                                        Theme.of(context).textTheme.subtitle1),
                              ),
                              TextFieldBlocBuilder(
                                minLines: 2,
                                maxLines: 2,
                                maxLength: 200,
                                textFieldBloc: _addressFormBloc.addr,
                                style: Theme.of(context).textTheme.bodyText2,
                                decoration:
                                    CommonUI(context: context).decoration2(),
                                textInputAction: TextInputAction.next,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20, bottom: 4),
                                child: Text('State',
                                    style:
                                        Theme.of(context).textTheme.subtitle1),
                              ),
                              DropdownFieldBlocBuilder<String>(
                                selectFieldBloc: _addressFormBloc.addrState,
                                itemBuilder: (context, value) => value,
                                decoration:
                                    CommonUI(context: context).decoration2(),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20, bottom: 4),
                                child: Text('Postcode',
                                    style:
                                        Theme.of(context).textTheme.subtitle1),
                              ),
                              TextFieldBlocBuilder(
                                textFieldBloc: _addressFormBloc.postcode,
                                maxLength: 5,
                                keyboardType: TextInputType.numberWithOptions(
                                    signed: false, decimal: false),
                                style: Theme.of(context).textTheme.bodyText2,
                                decoration:
                                    CommonUI(context: context).decoration2(),
                                textInputAction: TextInputAction.next,
                              ),
                              BlocBuilder<BooleanFieldBloc,
                                      BooleanFieldBlocState>(
                                  bloc: _addressFormBloc.isPrimary,
                                  builder: (context, state) => Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('Set as primary address',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2
                                                    ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.w500)),
                                            CupertinoSwitch(
                                              value: state.value ?? false,
                                              onChanged: _addressFormBloc
                                                  .isPrimary.updateValue,
                                            )
                                          ],
                                        ),
                                      )),
                            ],
                          ),
                        ),
                        SliverFillRemaining(
                          hasScrollBody: false,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  child: ElevatedButton(
                                      child: Text('Save'),
                                      onPressed: _addressFormBloc.submit),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AddressFormBloc extends FormBloc<Response, Response> {
  BuildContext context;
  Address? address;

  final title =
      TextFieldBloc(name: 'title', validators: [FieldBlocValidators.required]);

  final addr = TextFieldBloc(
      name: 'address', validators: [FieldBlocValidators.required]);

  final addrState = SelectFieldBloc<String, dynamic>(
      name: 'state', items: [], validators: [FieldBlocValidators.required]);

  final postcode = TextFieldBloc(
      name: 'postcode', validators: [FieldBlocValidators.required]);

  final isPrimary = BooleanFieldBloc(name: 'isPrimary', initialValue: false);

  AddressFormBloc({required this.context, this.address})
      : super(autoValidate: false) {
    addFieldBlocs(fieldBlocs: [title, addr, addrState, postcode, isPrimary]);
    title.updateValue(address?.title ?? '');
    addr.updateValue(address?.address ?? '');
    AddressRest(context: context).getStates().then((value) {
      addrState.updateItems(
          value.dataResults!.map<String>((data) => data.stateName).toList());
      addrState.updateValue(address?.state);
    });
    postcode.updateValue(address?.postalCode ?? '');
    isPrimary.updateValue((address?.isPrimary ?? '') == "Yes");
  }

  @override
  void onSubmitting() async {
    try {
      FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
      }
      context.read<LoadingState>().toggle(value: true);
      if (address != null) {
        final _res = await AddressRest(context: context).edit(
            address: Address(
                addressId: address!.addressId,
                title: title.value,
                address: addr.value,
                state: addrState.value,
                postalCode: postcode.value,
                isPrimary: isPrimary.value! ? "Yes" : "No"));
        if (_res.success) {
          emitSuccess(successResponse: _res, canSubmitAgain: true);
        } else {
          emitFailure(failureResponse: _res);
        }
      } else {
        final _res = await AddressRest(context: context).add(
            address: Address(
                title: title.value,
                address: addr.value,
                state: addrState.value,
                postalCode: postcode.value,
                isPrimary: (isPrimary.value ?? false) ? "Yes" : "No"));
        if (_res.success) {
          emitSuccess(successResponse: _res, canSubmitAgain: true);
        } else {
          emitFailure(failureResponse: _res);
        }
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
    } finally {
      context.read<LoadingState>().toggle(value: false);
    }
  }
}
