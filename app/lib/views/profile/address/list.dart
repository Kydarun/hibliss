import 'package:app/model/address.dart';
import 'package:app/rest/address.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/profile/address/edit.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:app/views/shared/listview.dart';
import 'package:app/views/shared/loading.dart';
import 'package:app/views/shared/unauthenticated.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddressList extends StatefulWidget {
  final Function(Address address)? callback;

  AddressList({this.callback});

  @override
  _AddressListState createState() => _AddressListState();
}

class _AddressListState extends State<AddressList> {
  final GlobalKey<AsyncListViewState<Address>> _key = GlobalKey<AsyncListViewState<Address>>();

  final _MultiSelectMode _mode = _MultiSelectMode();

  @override
  Widget build(BuildContext context) {
    return Unauthenticated(
      child: ChangeNotifierProvider<_MultiSelectMode>(
        create: (context) => _mode,
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text('Address'),
              actions: [
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      _mode.toggle();
                      _key.currentState?.toggleMultiSelect();
                    },
                    icon: Consumer<_MultiSelectMode>(
                      builder: (context, mode, child) => Image.asset(mode.multiSelectMode
                          ? 'res/icons/ic_top_cancel.png'
                          : 'res/icons/ic_top_trash.png'),
                    )),
              ],
            ),
            body: LoadingIndicator(
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: AsyncListView<Address>(
                              key: _key,
                              create: (context) async {
                                final _res = await AddressRest(context: context).list();
                                context.read<LoadingState>().toggle(value: false);
                                return _res;
                              },
                              onTap: (context, item) {
                                if (widget.callback != null) {
                                  Navigator.of(context).pop();
                                  widget.callback!(item!);
                                } else {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(
                                          builder: (context) => EditAddress(
                                                address: item,
                                              )))
                                      .then((value) => _key.currentState?.runAsync());
                                }
                              },
                              onDelete: (context, item) =>
                                  AddressRest(context: context).delete(addressId: item.addressId!),
                              builder: (context, item) => Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: Colors.white),
                                    margin: EdgeInsets.only(bottom: 5),
                                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text('${item?.title ?? ''}',
                                                  style: Theme.of(context).textTheme.headline5),
                                            ),
                                            if ((item?.isPrimary ?? "") == "Yes")
                                              Container(
                                                  decoration: BoxDecoration(
                                                      color: Color(0xff0189ca),
                                                      borderRadius: BorderRadius.circular(2)),
                                                  margin: EdgeInsets.only(left: 10),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 8, vertical: 1),
                                                  child: Text('Primary',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1
                                                          ?.copyWith(color: Colors.white)))
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 8),
                                          child: Text(
                                              '${item?.address}, ${item?.postalCode}, ${item?.state}',
                                              style: Theme.of(context).textTheme.subtitle2),
                                        )
                                      ],
                                    ),
                                  )),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(bottom: 40),
                              child: Consumer<_MultiSelectMode>(
                                builder: (context, mode, child) => ElevatedButton(
                                    child: Text(mode.multiSelectMode ? 'Delete' : 'Add'),
                                    onPressed: () {
                                      if (mode.multiSelectMode) {
                                        int _totalItems = _key.currentState?.length ?? 0;
                                        debugPrint('${_key.currentState?.selectedLength}');
                                        if (_totalItems == 1 ||
                                            _totalItems - (_key.currentState?.selectedLength ?? 0) <
                                                1) {
                                          showDialog(
                                              context: context,
                                              builder: (context) => HiBlissDialog(
                                                    context: context,
                                                    title: 'Unable to Delete Address',
                                                    message:
                                                        'There must be at least one address.\nIf you wish to delete this address,\nadd a new one first.',
                                                  ));
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (context) => HiBlissDialog(
                                                  context: context,
                                                  title: 'Delete Address',
                                                  message:
                                                      'Are you sure you want to delete address?',
                                                  onConfirm: () {
                                                    context
                                                        .read<LoadingState>()
                                                        .toggle(value: true);
                                                    _key.currentState?.runDelete().then((value) {
                                                      _mode.toggle();
                                                    });
                                                  }));
                                        }
                                      } else {
                                        if ((_key.currentState?.length ?? 0) >= 3) {
                                          showDialog(
                                              context: context,
                                              builder: (context) => HiBlissDialog(
                                                  context: context,
                                                  title: 'Maximum Reached',
                                                  message:
                                                      'You have met the maximum number of addresses. Kindly edit/delete an address to add a new address.'));
                                        } else {
                                          Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (context) => EditAddress()))
                                              .then((value) => _key.currentState?.runAsync());
                                        }
                                      }
                                    }),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  )),
            )),
      ),
    );
  }
}

class _MultiSelectMode with ChangeNotifier {
  bool multiSelectMode;

  _MultiSelectMode({this.multiSelectMode = false});

  toggle() {
    multiSelectMode = !multiSelectMode;
    notifyListeners();
  }
}
