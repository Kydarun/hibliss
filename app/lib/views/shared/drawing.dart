import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:screenshot/screenshot.dart';

class PainMap extends StatefulWidget {
  final Function(Uint8List? imageByte) onDrew;

  const PainMap({required this.onDrew, Key? key}) : super(key: key);

  @override
  _PainMapState createState() => _PainMapState();
}

class _PainMapState extends State<PainMap> {
  List<List<Offset>> _points = [];
  ScreenshotController _screenshot = ScreenshotController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Text('Please circle the affected area.'))),
            Container(
              margin: EdgeInsets.only(left: 5),
              child: GestureDetector(
                onTap: () {
                  if (_points.length > 0) {
                    setState(() {
                      _points.removeLast();
                    });
                  }
                },
                child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                    padding: EdgeInsets.all(8),
                    child: Icon(Icons.undo, color: Colors.grey)),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, top: 5),
              child: GestureDetector(
                onTap: () {
                  if (_points.length > 0) {
                    setState(() {
                      _points.clear();
                    });
                  }
                },
                child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                    padding: EdgeInsets.all(8),
                    child: Icon(Icons.close, color: Colors.grey)),
              ),
            )
          ],
        ),
        GestureDetector(
          onPanStart: (details) {
            setState(() {
              _points.add([details.localPosition]);
            });
          },
          onPanUpdate: (details) {
            setState(() {
              _points.last.add(details.localPosition);
            });
          },
          onPanEnd: (details) {
            _getImage();
          },
          child: Screenshot(
            controller: _screenshot,
            child: Stack(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset('res/images/pain_map.png')),
                ClipRect(
                  child: CustomPaint(
                    size: Size(MediaQuery.of(context).size.width - 30,
                        (MediaQuery.of(context).size.width - 30) / 1.519),
                    painter: PainPainter(points: _points),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  _getImage() async {
    final _image = await _screenshot.capture(
        delay: Duration(milliseconds: 10), pixelRatio: MediaQuery.of(context).devicePixelRatio);
    widget.onDrew(_image);
  }
}

class PainPainter extends CustomPainter {
  List<List<Offset>> points;

  PainPainter({required this.points});

  @override
  void paint(Canvas canvas, Size size) {
    Paint _paint = Paint();
    _paint.color = Colors.red;
    _paint.strokeWidth = 4;

    points.forEach((childPoints) {
      for (int i = 0; i < childPoints.length - 1; i++) {
        canvas.drawLine(childPoints[i], childPoints[i + 1], _paint);
      }
    });
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
