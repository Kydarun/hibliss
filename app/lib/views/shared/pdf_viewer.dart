import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomPdfViewer extends StatelessWidget {
  final String url;

  const CustomPdfViewer({required this.url, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty) {
      return Container(
        child: Center(
            child:
                Text('Invalid link. Please contact Hi-Bliss administrator.')),
      );
    }
    var _ext = p.extension(url).toLowerCase();
    return _ext == '.pdf'
        ? SfPdfViewer.network(url)
        : (_ext == '.png' || _ext == '.jpg' || _ext == '.jpeg'
            ? Column(
                children: [
                  Expanded(
                    child: InteractiveViewer(child: Image.network(url)),
                  ),
                ],
              )
            : WebView(
                javascriptMode: JavascriptMode.unrestricted,
                initialUrl: url,
              ));
  }
}
