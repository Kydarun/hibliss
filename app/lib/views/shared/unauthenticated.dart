import 'package:app/main.dart';
import 'package:app/util/environment.dart';
import 'package:app/views/auth/login.dart';
import 'package:app/views/home.dart';
import 'package:app/views/home_therapist.dart';
import 'package:app/views/shared/dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Unauthenticated extends StatefulWidget {
  final Widget child;

  Unauthenticated({required this.child});

  @override
  _UnauthenticatedState createState() => _UnauthenticatedState();
}

class _UnauthenticatedState extends State<Unauthenticated> {
  @override
  Widget build(BuildContext context) {
    return Consumer<Session>(
      builder: (context, session, child) => Stack(
        children: [
          widget.child,
          if (!session.isLoggedIn)
            Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.black54,
                child: Center(
                  child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                    ),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Text('Session Expired',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    ?.copyWith(color: Colors.black)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15, left: 7, right: 7),
                            child: Text('Your session has expired, please login again.',
                                style: Theme.of(context).textTheme.bodyText2),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 20, bottom: 14),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  ElevatedButton(
                                    child: Text('Okay'),
                                    style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                                          textStyle: MaterialStateProperty.all<TextStyle?>(
                                              TextStyle(
                                                  color: Color(0xff6d6f72),
                                                  fontFamily: 'Niveau Grotesk',
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14)),
                                          shadowColor:
                                              MaterialStateProperty.all<Color>(Colors.transparent),
                                        ),
                                    onPressed: () {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              settings: RouteSettings(name: '/home'),
                                              builder: (context) => EnvironmentConfig.isTherapist
                                                  ? TherapistHome()
                                                  : Home()),
                                          (route) => false);
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(builder: (context) => Login()));
                                    },
                                  )
                                ],
                              ))
                        ]),
                  ),
                ))
        ],
      ),
    );
  }
}

class UnauthenticatedWithoutOverlay extends StatelessWidget {
  final Widget child;
  final String unauthenticatedText;

  const UnauthenticatedWithoutOverlay(
      {required this.child,
      this.unauthenticatedText = 'You must login to see this page.',
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<Session>(
        builder: (context, session, c) => session.isLoggedIn
            ? child
            : Center(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 15, left: 7, right: 7),
                          child: Text(unauthenticatedText,
                              style: Theme.of(context).textTheme.headline5,
                              textAlign: TextAlign.center),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 20, bottom: 14),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                ElevatedButton(
                                  child: Text('Login'),
                                  style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                                        textStyle: MaterialStateProperty.all<TextStyle?>(TextStyle(
                                            color: Color(0xff6d6f72),
                                            fontFamily: 'Niveau Grotesk',
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14)),
                                        shadowColor:
                                            MaterialStateProperty.all<Color>(Colors.transparent),
                                      ),
                                  onPressed: () {
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (context) => EnvironmentConfig.isTherapist
                                                ? TherapistHome()
                                                : Home()),
                                        (route) => false);
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(builder: (context) => Login()));
                                  },
                                )
                              ],
                            ))
                      ]),
                ),
              ));
  }
}
