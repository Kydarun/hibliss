import 'package:flutter/material.dart';

class HiBlissDialog extends StatefulWidget {
  final BuildContext context;
  final Key? key;

  // Dialog Title
  final String? title;

  // Dialog icon
  final String? icon;

  // Dialog message
  final String? message;

  final VoidCallback? onConfirm;

  // Close button is enabled
  final bool canClose;

  HiBlissDialog(
      {required this.context,
      this.key,
      this.title,
      this.icon,
      this.message,
      this.onConfirm,
      this.canClose = true})
      : super(key: key);

  createState() => HiBlissDialogState();
}

class HiBlissDialogState extends State<HiBlissDialog> {
  @override
  Widget build(BuildContext context) {
    final dialogWidget = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (widget.title?.isNotEmpty ?? false)
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Text(widget.title!,
                        style:
                            Theme.of(context).textTheme.headline5?.copyWith(color: Colors.black)),
                  ),
                if (widget.icon?.isNotEmpty ?? false)
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Image.asset(widget.icon!, width: 45),
                  ),
                if (widget.message?.isNotEmpty ?? false)
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 7, right: 7),
                    child: Text(widget.message!,
                        style: Theme.of(context).textTheme.bodyText2, textAlign: TextAlign.center),
                  ),
                Container(
                    margin: EdgeInsets.only(top: 20, bottom: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        if (widget.onConfirm == null) _defaultButton,
                        if (widget.onConfirm != null)
                          ..._confirmButton(onConfirm: widget.onConfirm),
                      ],
                    ))
              ]),
        ));
    return dialogWidget;
  }

  Widget get _defaultButton => Expanded(
        child: ElevatedButton(
          child: Text('Okay'),
          style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                textStyle: MaterialStateProperty.all<TextStyle?>(TextStyle(
                    color: Color(0xff6d6f72),
                    fontFamily: 'Niveau Grotesk',
                    fontWeight: FontWeight.w500,
                    fontSize: 14)),
                shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
              ),
          onPressed: () => Navigator.pop(context),
        ),
      );

  List<Widget> _confirmButton({VoidCallback? onConfirm}) => [
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 5),
            child: ElevatedButton(
              child: Text('Cancel'),
              style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                    foregroundColor: MaterialStateProperty.all<Color>(Color(0xff6d6f72)),
                    backgroundColor: MaterialStateProperty.all<Color>(Color(0xffEdf1f7)),
                    textStyle: MaterialStateProperty.all<TextStyle?>(TextStyle(
                        color: Color(0xff6d6f72),
                        fontFamily: 'Niveau Grotesk',
                        fontWeight: FontWeight.w500,
                        fontSize: 14)),
                    shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
                  ),
              onPressed: () => Navigator.pop(context),
            ),
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(left: 5),
            child: ElevatedButton(
              child: Text('Confirm'),
              style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                    textStyle: MaterialStateProperty.all<TextStyle?>(TextStyle(
                        color: Color(0xff6d6f72),
                        fontFamily: 'Niveau Grotesk',
                        fontWeight: FontWeight.w500,
                        fontSize: 14)),
                    shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
                  ),
              onPressed: () {
                Navigator.pop(context);
                onConfirm!();
              },
            ),
          ),
        )
      ];
}
