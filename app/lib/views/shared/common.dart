import 'package:app/model/response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class CommonUI {
  static const _defaultDuration = Duration(seconds: 1, milliseconds: 500);

  final BuildContext context;

  CommonUI({required this.context});

  void fail({required FormBlocFailure<Response, Response> failure}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content:
            Text(failure.failureResponse?.message ?? 'An error occured. Please try again later.'),
        duration: Duration(seconds: 1, milliseconds: 500)));
  }

  void fail2({required FormBlocFailure<Response2, Response2> failure}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content:
            Text(failure.failureResponse?.message ?? 'An error occured. Please try again later.'),
        duration: Duration(seconds: 1, milliseconds: 500)));
  }

  void success(
      {required FormBlocSuccess<Response, Response> success,
      String? customMessage,
      SnackBarAction? action,
      Duration duration = _defaultDuration}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text((success.successResponse?.message?.isEmpty ?? true)
            ? 'Request successful!'
            : customMessage ?? success.successResponse?.message ?? 'Request successful!'),
        duration: duration,
        action: action));
  }

  void success2(
      {required FormBlocSuccess<Response2, Response2> success,
      String? customMessage,
      SnackBarAction? action,
      Duration duration = _defaultDuration}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(customMessage ?? success.successResponse?.message ?? 'Request successful!'),
        duration: duration,
        action: action));
  }

  void snackbar(
      {required String message, Duration duration = _defaultDuration, SnackBarAction? action}) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message), duration: duration, action: action));
  }

  InputDecoration decoration2({String? hint}) => InputDecoration(
      filled: true,
      fillColor: Colors.white,
      contentPadding: EdgeInsets.symmetric(vertical: 11, horizontal: 15),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(7),
          borderSide: BorderSide(color: Color(0x73bcd5ed), width: 1)),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(7),
          borderSide: BorderSide(color: Color(0x73bcd5ed), width: 1)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(7),
          borderSide: BorderSide(color: Color(0x73bcd5ed), width: 1)),
      hintStyle: Theme.of(context).textTheme.bodyText2?.copyWith(color: Color(0xff727375)),
      hintText: hint,
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(7), borderSide: BorderSide(color: Colors.red)),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(7),
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
      errorStyle: Theme.of(context).textTheme.caption?.copyWith(color: Colors.red));

  InputDecoration decoration({String? hint, IconButton? suffixIcon}) => InputDecoration(
      filled: true,
      fillColor: Color(0xfff2f2f4),
      suffixIcon: suffixIcon,
      contentPadding: EdgeInsets.symmetric(vertical: 11, horizontal: 15),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Color(0xff0d78a5))),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: Color(0xff0d78a5), width: 2)),
      hintStyle: Theme.of(context).textTheme.bodyText2?.copyWith(color: Color(0xff9193a6)),
      hintText: hint,
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5), borderSide: BorderSide(color: Colors.red)),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(color: Colors.red, width: 2),
      ),
      errorStyle: Theme.of(context).textTheme.caption?.copyWith(color: Colors.red));

  Container infoBox({bool isSuccess = true, required String message}) => Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: isSuccess ? Color(0x3317b491) : Color(0x33ff0000)),
        padding: EdgeInsets.symmetric(vertical: 9, horizontal: 15),
        child: Row(
          children: [
            if (isSuccess) Image.asset('res/icons/ic_msg_check.png', width: 27),
            if (!isSuccess) Image.asset('res/icons/ic_msg_alert.png', width: 27),
            Expanded(
              child: Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text(message, style: Theme.of(context).textTheme.caption)),
            )
          ],
        ),
      );
}
