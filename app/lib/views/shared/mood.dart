import 'package:flutter/material.dart';

class MoodControl extends StatefulWidget {
  final String text;
  final String? errorText;
  final Function(String option) onChanged;

  const MoodControl({required this.text, required this.onChanged, this.errorText, Key? key})
      : super(key: key);

  @override
  _MoodControlState createState() => _MoodControlState();
}

class _MoodControlState extends State<MoodControl> {
  String? _option;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(child: Text(widget.text, style: Theme.of(context).textTheme.bodyText1)),
            GestureDetector(
              onTap: () {
                setState(() {
                  _option = "Good";
                });
                widget.onChanged("Good");
              },
              child: Container(
                width: 40,
                height: 40,
                margin: EdgeInsets.symmetric(horizontal: 10),
                child:
                    Image.asset('res/icons/ic_smile_${_option == "Good" ? 'blue' : 'white'}.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _option = "Bad";
                });
                widget.onChanged("Bad");
              },
              child: Container(
                width: 40,
                height: 40,
                child: Image.asset('res/icons/ic_sad_${_option == "Bad" ? 'blue' : 'white'}.png'),
              ),
            ),
          ],
        ),
        if (widget.errorText != null)
          Container(
            margin: EdgeInsets.only(top: 5),
            child: Text(widget.errorText!,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(color: Colors.red, fontSize: 12)),
          )
      ],
    );
  }

  String? get option => _option;
}
