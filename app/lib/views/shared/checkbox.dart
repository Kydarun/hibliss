import 'package:flutter/material.dart';

class CheckBoxControl extends StatefulWidget {
  final String text;
  final String? errorText;
  final Function(String checked) onChecked;

  const CheckBoxControl({required this.text, required this.onChecked, this.errorText, Key? key})
      : super(key: key);

  @override
  _CheckBoxControlState createState() => _CheckBoxControlState();
}

class _CheckBoxControlState extends State<CheckBoxControl> {
  String _checked = "No";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _checked = _checked == "No" ? "Yes" : "No";
        });
        widget.onChecked(_checked);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.only(left: 5),
                child: Image.asset(
                    'res/icons/ic_check_round_blue${_checked == "Yes" ? '' : 'border'}.png',
                    width: 21)),
            Expanded(
              child: Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: Text(widget.text, style: Theme.of(context).textTheme.bodyText1)),
            )
          ],
        ),
      ),
    );
  }

  String get checked => _checked;
}
