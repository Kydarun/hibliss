import 'package:app/views/shared/dialog.dart';
import 'package:flutter/material.dart';

class QuitPlanButton extends StatelessWidget {
  const QuitPlanButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (ctx) => HiBlissDialog(
                    context: context,
                    title: 'Confirm Cancellation',
                    message: 'Are you sure you want to cancel the care plan?',
                    onConfirm: () {
                      Navigator.popUntil(context, ModalRoute.withName('/home'));
                    },
                  ));
        },
        icon: Icon(Icons.close, color: Colors.white));
  }
}

mixin QuitableCarePlanMixin {}
