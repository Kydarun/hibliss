import 'package:flutter/material.dart';

class ToggleControl extends StatefulWidget {
  final String text;
  final String? errorText;
  final Function(String option) onChanged;

  const ToggleControl({required this.text, required this.onChanged, this.errorText, Key? key})
      : super(key: key);

  @override
  _ToggleControlState createState() => _ToggleControlState();
}

class _ToggleControlState extends State<ToggleControl> {
  String? _option;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(child: Text(widget.text, style: Theme.of(context).textTheme.bodyText1)),
            GestureDetector(
              onTap: () {
                setState(() {
                  _option = "Yes";
                });
                widget.onChanged("Yes");
              },
              child: Container(
                width: 40,
                height: 40,
                margin: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _option == "Yes" ? Theme.of(context).primaryColor : Colors.white),
                child: Center(
                  child: Text('Y',
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: _option == "Yes" ? Colors.white : Color(0xff161640))),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _option = "No";
                });
                widget.onChanged("No");
              },
              child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _option == "No" ? Theme.of(context).primaryColor : Colors.white),
                child: Center(
                  child: Text('N',
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: _option == "No" ? Colors.white : Color(0xff161640))),
                ),
              ),
            ),
          ],
        ),
        if (widget.errorText != null)
          Container(
            margin: EdgeInsets.only(top: 5),
            child: Text(widget.errorText!,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(color: Colors.red, fontSize: 12)),
          )
      ],
    );
  }

  String? get option => _option;
}

class VerticalToggleControl extends StatefulWidget {
  final String? errorText;
  final Function(String option) onChanged;

  const VerticalToggleControl({required this.onChanged, this.errorText, Key? key})
      : super(key: key);

  @override
  _VerticalToggleControlState createState() => _VerticalToggleControlState();
}

class _VerticalToggleControlState extends State<VerticalToggleControl> {
  String? _option;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              _option = "Weak";
            });
            widget.onChanged("Weak");
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset(
                        'res/icons/ic_check_round_blue${_option == "Weak" ? '' : 'border'}.png',
                        width: 21)),
                Expanded(
                  child: Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: Text("Weak", style: Theme.of(context).textTheme.bodyText1)),
                )
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              _option = "Normal";
            });
            widget.onChanged("Normal");
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset(
                        'res/icons/ic_check_round_blue${_option == "Normal" ? '' : 'border'}.png',
                        width: 21)),
                Expanded(
                  child: Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: Text("Normal", style: Theme.of(context).textTheme.bodyText1)),
                )
              ],
            ),
          ),
        ),
        if (widget.errorText != null)
          Container(
            margin: EdgeInsets.only(top: 5),
            child: Text(widget.errorText!,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(color: Colors.red, fontSize: 12)),
          )
      ],
    );
  }

  String? get option => _option;
}
