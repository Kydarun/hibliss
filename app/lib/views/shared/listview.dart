import 'package:app/model/response.dart';
import 'package:app/views/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AsyncListView<T> extends StatefulWidget {
  final Key? key;
  final Widget Function(BuildContext context, T? item) builder;
  final Future<Response<T>> Function(BuildContext context) create;
  final Function(BuildContext context, T? item)? onTap;
  final Future<Response<T>> Function(BuildContext context, T item)? onDelete;

  AsyncListView(
      {this.key,
      required this.create,
      required this.builder,
      this.onTap,
      this.onDelete});

  @override
  AsyncListViewState createState() => AsyncListViewState<T>();
}

class AsyncListViewState<T> extends State<AsyncListView<T>> {
  Response<T> _data = Response(httpCode: -1, dataResults: []);

  int get length => _data.dataResults?.length ?? 0;
  int get selectedLength => _itemStatus.where((element) => element).length;
  bool get multiSelectMode => _isMultiSelect;

  bool _isMultiSelect = false;
  List<bool> _itemStatus = [];

  @override
  void didChangeDependencies() {
    runAsync();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Provider<Response<T>>.value(
      value: _data,
      child: Consumer<Response<T>>(
          builder: (context, data, child) => RefreshIndicator(
                onRefresh: () async {
                  return runAsync();
                },
                child: Container(
                  child: ListView.builder(
                      itemCount: data.dataResults?.length ?? 0,
                      itemBuilder: (context, i) => GestureDetector(
                            onTap: () {
                              if (_isMultiSelect) {
                                setState(() {
                                  _itemStatus[i] = !_itemStatus[i];
                                });
                              } else if (widget.onTap != null) {
                                widget.onTap!(
                                    context, data.dataResults?[i] ?? null);
                              }
                            },
                            child: Row(
                              children: [
                                if (_isMultiSelect)
                                  Container(
                                    margin: EdgeInsets.only(right: 5),
                                    child: Image.asset(
                                        'res/icons/${_itemStatus[i] ? "ic_check_round_blue.png" : "ic_check_round_blueborder.png"}',
                                        width: 25),
                                  ),
                                Expanded(
                                  child: widget.builder(
                                      context, data.dataResults?[i] ?? null),
                                ),
                              ],
                            ),
                          )),
                ),
              )),
    );
  }

  Future<void> runAsync() async {
    setState(() {
      _data.dataResults?.clear();
    });
    final _res = await widget.create(context);
    setState(() {
      _data = _res;
      _itemStatus = _res.dataResults?.map((value) => false).toList() ?? [];
    });
  }

  toggleMultiSelect() {
    setState(() {
      _isMultiSelect = !_isMultiSelect;
      if (!_isMultiSelect) {
        for (var i = 0; i < _itemStatus.length; i++) {
          _itemStatus[i] = false;
        }
      }
    });
  }

  Future runDelete() async {
    if (widget.onDelete != null) {
      for (var i = 0; i < _itemStatus.length; i++) {
        if (_itemStatus[i]) {
          await widget.onDelete!(context, _data.dataResults![i]);
        }
      }
    }
    setState(() {
      _isMultiSelect = false;
    });
    await runAsync();
    context.read<LoadingState>().toggle(value: false);
  }
}

class MultiSelectableListView<T> extends StatefulWidget {
  final Key? key;
  final Widget Function(BuildContext context, T? item) builder;
  final List<T> items;
  final Function(List<T> items)? onSelected;
  final Future Function(BuildContext context, T item)? onDelete;

  const MultiSelectableListView(
      {this.key,
      this.items = const [],
      required this.builder,
      this.onDelete,
      this.onSelected})
      : super(key: key);

  @override
  MultiSelectableListViewState createState() =>
      MultiSelectableListViewState<T>();
}

class MultiSelectableListViewState<T>
    extends State<MultiSelectableListView<T>> {
  List<bool> _itemStatus = [];

  @override
  void initState() {
    super.initState();
    _itemStatus = widget.items.map((item) => false).toList();
  }

  List<T> selected() => widget.items
      .where((element) => _itemStatus[widget.items.indexOf(element)])
      .toList();

  bool isAllSelected() =>
      widget.items
          .where((element) => _itemStatus[widget.items.indexOf(element)])
          .toList()
          .length ==
      widget.items.length;

  selectAll() {
    setState(() {
      for (var i = 0; i < _itemStatus.length; i++) {
        _itemStatus[i] = true;
      }
    });
    if (widget.onSelected != null) {
      widget.onSelected!(selected());
    }
  }

  deSelectAll() {
    setState(() {
      for (var i = 0; i < _itemStatus.length; i++) {
        _itemStatus[i] = false;
      }
    });
    if (widget.onSelected != null) {
      widget.onSelected!(selected());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
            children: widget.items
                .map((item) => Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _itemStatus[widget.items.indexOf(item)] =
                                  !_itemStatus[widget.items.indexOf(item)];
                            });
                            if (widget.onSelected != null) {
                              widget.onSelected!(selected());
                            }
                          },
                          child: Container(
                            margin: EdgeInsets.only(right: 5, left: 10),
                            child: Image.asset(
                                'res/icons/${_itemStatus[widget.items.indexOf(item)] ? "ic_check_round_blue.png" : "ic_check_round_blueborder.png"}',
                                width: 25),
                          ),
                        ),
                        Expanded(
                          child: widget.builder(context, item),
                        ),
                      ],
                    ))
                .toList()));
  }
}

class LoadMoreListView<T> extends StatefulWidget {
  final Future<Response<T>> Function(BuildContext context, int page) create;
  final Widget Function(BuildContext context, T item) builder;
  final Function(BuildContext context, T item)? onTap;
  final int itemsPerPage;

  const LoadMoreListView(
      {required this.create,
      required this.builder,
      this.onTap,
      this.itemsPerPage = 10,
      Key? key})
      : super(key: key);

  @override
  _LoadMoreListViewState<T> createState() => _LoadMoreListViewState<T>();
}

class _LoadMoreListViewState<T> extends State<LoadMoreListView<T>> {
  List<T> _items = [];
  int _page = 1;

  bool _isLoading = false;
  bool _isEof = false;
  ScrollController? _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._controller = ScrollController()
      ..addListener(() {
        if (this._controller!.position.extentAfter < 100) {
          runAsync();
        }
      });
    runAsync(page: 1);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: RefreshIndicator(
            onRefresh: () async {
              return runAsync(page: 1);
            },
            child: Container(
              child: ListView.builder(
                  controller: this._controller,
                  itemCount: this._items.length,
                  itemBuilder: (context, i) => GestureDetector(
                      onTap: () {
                        if (widget.onTap != null) {
                          widget.onTap!(context, _items[i]);
                        }
                      },
                      child: widget.builder(context, _items[i]))),
            ),
          ),
        ),
        if (_isLoading)
          Center(
              child: Container(
                  margin: EdgeInsets.all(10),
                  child: CircularProgressIndicator()))
      ],
    );
  }

  Future<void> runAsync({int? page}) async {
    if (page == 1) {
      setState(() {
        _isEof = false;
      });
    }
    if (!_isLoading && !_isEof) {
      setState(() {
        _isLoading = true;
        if (page != null) {
          this._page = page;
        } else {
          this._page++;
        }
      });
      List<T> _res =
          (await widget.create(context, this._page)).dataResults ?? [];
      debugPrint('${_res.length}');
      setState(() {
        if (_res.length < widget.itemsPerPage) {
          _isEof = true;
        }
        if (this._page != 1) {
          this._items.addAll(_res);
        } else {
          this._items = _res;
        }
        _isLoading = false;
      });
    }
  }
}
