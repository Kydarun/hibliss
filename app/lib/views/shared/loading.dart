import 'package:app/model/response.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AsyncIndicator<T> extends StatelessWidget {
  final Future<Response<T>> create;
  final Widget child;

  AsyncIndicator({required this.create, required this.child});

  @override
  Widget build(BuildContext context) {
    return Consumer<LoadingState>(
      builder: (context, loadingState, _) => Stack(
        children: [
          child,
          if (loadingState.isLoading)
            Container(
              child: Center(
                child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(10),
                    color: Colors.grey,
                    child: CircularProgressIndicator()),
              ),
            )
        ],
      ),
    );
  }
}

class Async2Indicator<T> extends StatelessWidget {
  final Future<Response2<T>> create;
  final Widget child;

  Async2Indicator({required this.create, required this.child});

  @override
  Widget build(BuildContext context) {
    return Consumer<LoadingState>(
      builder: (context, loadingState, _) => Stack(
        children: [
          child,
          if (loadingState.isLoading)
            Container(
              child: Center(
                child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(10),
                    color: Colors.grey,
                    child: CircularProgressIndicator()),
              ),
            )
        ],
      ),
    );
  }
}

class LoadingIndicator extends StatefulWidget {
  final Widget child;

  LoadingIndicator({required this.child});

  @override
  _LoadingIndicatorState createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<LoadingIndicator> {
  @override
  Widget build(BuildContext context) {
    return Consumer<LoadingState>(
      builder: (context, loadingState, child) => Stack(
        children: [
          widget.child,
          if (loadingState.isLoading)
            Container(
                color: Colors.black38,
                child: Center(
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.all(Radius.circular(10))),
                        padding: EdgeInsets.all(15),
                        child: CircularProgressIndicator())))
        ],
      ),
    );
  }
}

class LoadingState with ChangeNotifier {
  bool _isLoading = false;

  void toggle({bool? value}) {
    if (value != null)
      _isLoading = value;
    else
      _isLoading = !_isLoading;
    notifyListeners();
  }

  void fetch({required Future<dynamic> future}) {
    _isLoading = true;
    notifyListeners();
    future.whenComplete(() {
      _isLoading = false;
      notifyListeners();
    });
  }

  bool get isLoading => _isLoading;
}

class StatelessLoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.black38,
        child: Center(
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[300], borderRadius: BorderRadius.all(Radius.circular(10))),
                padding: EdgeInsets.all(15),
                child: CircularProgressIndicator())));
  }
}
