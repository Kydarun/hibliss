// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingOrder _$BookingOrderFromJson(Map<String, dynamic> json) => BookingOrder(
      bookingId: json['bookingId'] as int?,
      orderId: json['orderId'] as int?,
      displayOrderId: json['displayOrderId'] as String?,
      itemName: json['itemName'] as String?,
      duration: json['duration'] as int?,
      subtotal: (json['subtotal'] as num?)?.toDouble(),
      tax: (json['tax'] as num?)?.toDouble(),
      travelFee: (json['travelFee'] as num?)?.toDouble(),
      total: (json['total'] as num?)?.toDouble(),
      therapistName: json['therapistName'] as String?,
      therapistAddress: json['therapistAddress'] as String?,
      customerName: json['customerName'] as String?,
      customerAddress: json['customerAddress'] as String?,
      timeSlot: json['timeSlot'] as String?,
      displayTimeSlot: json['displayTimeSlot'] as String?,
      appointmentDate: json['appointmentDate'] as String?,
      cancelDate: json['cancelDate'] as String?,
      totalSession: json['totalSession'] as int?,
      completedSession: json['completedSession'] as int?,
      therapistId: json['therapistId'] as int?,
      therapistAddressId: json['therapistAddressId'] as int?,
      productId: json['productId'] as int?,
      customerAddressId: json['customerAddressId'] as int?,
      productType: json['productType'] as String?,
      rescheduleFlag: json['rescheduleFlag'] as bool?,
      reorderFlag: json['reorderFlag'] as bool?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
      authCode: json['authCode'] as String?,
      authCodeRequestTime: json['authCodeRequestTime'] as String?,
      isCompleteEnabled: json['isCompleteEnabled'] as bool?,
    )
      ..displayReportId = json['displayReportId'] as String?
      ..reportLink = json['reportLink'] as String?
      ..reportGeneratedBy = json['reportGeneratedBy'] as String?
      ..reportGeneratedDate = json['reportGeneratedDate'] as String?;

Map<String, dynamic> _$BookingOrderToJson(BookingOrder instance) =>
    <String, dynamic>{
      'bookingId': instance.bookingId,
      'orderId': instance.orderId,
      'displayOrderId': instance.displayOrderId,
      'itemName': instance.itemName,
      'duration': instance.duration,
      'subtotal': instance.subtotal,
      'tax': instance.tax,
      'travelFee': instance.travelFee,
      'total': instance.total,
      'therapistName': instance.therapistName,
      'therapistAddress': instance.therapistAddress,
      'customerName': instance.customerName,
      'customerAddress': instance.customerAddress,
      'timeSlot': instance.timeSlot,
      'displayTimeSlot': instance.displayTimeSlot,
      'appointmentDate': instance.appointmentDate,
      'cancelDate': instance.cancelDate,
      'totalSession': instance.totalSession,
      'completedSession': instance.completedSession,
      'therapistId': instance.therapistId,
      'therapistAddressId': instance.therapistAddressId,
      'productId': instance.productId,
      'customerAddressId': instance.customerAddressId,
      'productType': instance.productType,
      'rescheduleFlag': instance.rescheduleFlag,
      'reorderFlag': instance.reorderFlag,
      'imageLink': instance.imageLink,
      'authCode': instance.authCode,
      'authCodeRequestTime': instance.authCodeRequestTime,
      'displayReportId': instance.displayReportId,
      'reportLink': instance.reportLink,
      'reportGeneratedBy': instance.reportGeneratedBy,
      'reportGeneratedDate': instance.reportGeneratedDate,
      'isCompleteEnabled': instance.isCompleteEnabled,
    };

Timeslot _$TimeslotFromJson(Map<String, dynamic> json) => Timeslot(
      value: json['value'] == null
          ? null
          : DateTime.parse(json['value'] as String),
      display: json['display'] as String?,
      isEnabled: json['isEnabled'] as bool?,
    );

Map<String, dynamic> _$TimeslotToJson(Timeslot instance) => <String, dynamic>{
      'value': instance.value?.toIso8601String(),
      'display': instance.display,
      'isEnabled': instance.isEnabled,
    };
