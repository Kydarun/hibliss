import 'package:json_annotation/json_annotation.dart';

part 'service.g.dart';

@JsonSerializable()
class Service {
  @JsonKey(name: 'auto_No')
  int autoNo;
  String serviceName;
  String? serviceDesc;
  int? category;
  double? price;
  int? duration;
  DateTime? deleteDate;
  int? deleteBy;
  DateTime? createDate;
  int? createBy;
  DateTime? lastUpdate;
  int? lastUpdateBy;

  Service(
      {required this.autoNo,
      required this.serviceName,
      this.serviceDesc,
      this.category,
      this.price,
      this.duration,
      this.deleteDate,
      this.deleteBy,
      this.createDate,
      this.createBy,
      this.lastUpdate,
      this.lastUpdateBy});

  factory Service.fromJson(Map<String, dynamic> json) => _$ServiceFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceToJson(this);
}
