// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Auth _$AuthFromJson(Map<String, dynamic> json) => Auth(
      userId: json['userid'] as String,
      name: json['name'] as String,
      accountType: json['accountType'] as String,
      role: json['role'] as String?,
      resetPassword: json['resetPassword'] as bool?,
      token: json['token'] as String,
    );

Map<String, dynamic> _$AuthToJson(Auth instance) => <String, dynamic>{
      'userid': instance.userId,
      'name': instance.name,
      'accountType': instance.accountType,
      'role': instance.role,
      'resetPassword': instance.resetPassword,
      'token': instance.token,
    };
