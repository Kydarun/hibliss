// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoppingCart _$ShoppingCartFromJson(Map<String, dynamic> json) => ShoppingCart(
      orderId: json['orderId'] as int?,
      itemName: json['itemName'] as String?,
      productType: json['productType'] as String?,
      subTotal: json['subTotal'] as String?,
      duration: json['duration'] as int?,
      productRemark: json['productRemark'] as String?,
      totalItem: json['totalItem'] as int?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ShoppingCartToJson(ShoppingCart instance) =>
    <String, dynamic>{
      'orderId': instance.orderId,
      'itemName': instance.itemName,
      'productType': instance.productType,
      'subTotal': instance.subTotal,
      'duration': instance.duration,
      'productRemark': instance.productRemark,
      'totalItem': instance.totalItem,
      'imageLink': instance.imageLink,
    };
