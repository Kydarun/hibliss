// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Package _$PackageFromJson(Map<String, dynamic> json) => Package(
      autoNo: json['auto_No'] as int,
      packageName: json['packageName'] as String,
      packageDesc: json['packageDesc'] as String?,
      category: json['category'] as int?,
      therapyServiceId: json['therapyServiceId'] as int?,
      numberOfSessions: json['numberOfSessions'] as int?,
      durationSession: json['durationSession'] as int?,
      price: (json['price'] as num?)?.toDouble(),
      deleteDate: json['deleteDate'] == null
          ? null
          : DateTime.parse(json['deleteDate'] as String),
      deleteBy: json['deleteBy'] as int?,
      createDate: json['createDate'] == null
          ? null
          : DateTime.parse(json['createDate'] as String),
      createBy: json['createBy'] as int?,
      lastUpdate: json['lastUpdate'] == null
          ? null
          : DateTime.parse(json['lastUpdate'] as String),
      lastUpdateBy: json['lastUpdateBy'] as int?,
    );

Map<String, dynamic> _$PackageToJson(Package instance) => <String, dynamic>{
      'auto_No': instance.autoNo,
      'packageName': instance.packageName,
      'packageDesc': instance.packageDesc,
      'category': instance.category,
      'therapyServiceId': instance.therapyServiceId,
      'numberOfSessions': instance.numberOfSessions,
      'durationSession': instance.durationSession,
      'price': instance.price,
      'deleteDate': instance.deleteDate?.toIso8601String(),
      'deleteBy': instance.deleteBy,
      'createDate': instance.createDate?.toIso8601String(),
      'createBy': instance.createBy,
      'lastUpdate': instance.lastUpdate?.toIso8601String(),
      'lastUpdateBy': instance.lastUpdateBy,
    };
