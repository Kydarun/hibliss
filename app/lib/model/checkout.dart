import 'package:app/model/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'checkout.g.dart';

@JsonSerializable()
class Checkout {
  double? subtotal;
  double? tax;
  double? travelFee;
  double? total;
  List<CheckoutItem>? orderItem;

  Checkout({this.subtotal, this.tax, this.travelFee, this.total, this.orderItem});

  factory Checkout.fromJson(Map<String, dynamic> json) => _$CheckoutFromJson(json);
  Map<String, dynamic> toJson() => _$CheckoutToJson(this);
}

@JsonSerializable()
class CheckoutItem {
  int? orderId;
  String? itemName;
  String? itemType;
  int? itemCount;
  double? itemPrice;
  String? therapistName;
  String? therapistAddress;
  String? customerAddress;
  List<ImageLink>? imageLink;

  CheckoutItem(
      {this.orderId,
      this.itemName,
      this.itemType,
      this.itemCount,
      this.itemPrice,
      this.therapistName,
      this.therapistAddress,
      this.customerAddress,
      this.imageLink});

  factory CheckoutItem.fromJson(Map<String, dynamic> json) => _$CheckoutItemFromJson(json);
  Map<String, dynamic> toJson() => _$CheckoutItemToJson(this);
}
