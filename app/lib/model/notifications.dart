import 'package:json_annotation/json_annotation.dart';

part 'notifications.g.dart';

@JsonSerializable()
class Notifications {
  bool? messageNotification;
  bool? appointmentNotification;
  bool? promotionNotification;

  Notifications(
      {this.messageNotification = true, this.appointmentNotification, this.promotionNotification});

  factory Notifications.fromJson(Map<String, dynamic> json) => _$NotificationsFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationsToJson(this);
}
