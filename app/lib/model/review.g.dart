// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) => Review(
      id: json['id'] as int?,
      name: json['name'] as String?,
      profilePicture: json['profilePicture'] as String?,
      rating: (json['rating'] as num?)?.toDouble(),
      review: json['review'] as String?,
      dateReviewed: json['dateReviewed'] == null
          ? null
          : DateTime.parse(json['dateReviewed'] as String),
    );

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'profilePicture': instance.profilePicture,
      'rating': instance.rating,
      'review': instance.review,
      'dateReviewed': instance.dateReviewed?.toIso8601String(),
    };
