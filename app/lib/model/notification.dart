import 'package:json_annotation/json_annotation.dart';

part 'notification.g.dart';

@JsonSerializable()
class NotificationMessage {
  final int? notifId;
  final String? title;
  final String? message;
  final int? type;

  NotificationMessage({
    this.notifId,
    this.title,
    this.message,
    this.type,
  });

  factory NotificationMessage.fromJson(Map<String, dynamic> json) =>
      _$NotificationMessageFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationMessageToJson(this);
}

@JsonSerializable()
class AppointmentReminder {
  final int? notifId;
  final int? type;
  final String? displayId;
  final String? itemName;
  final int? duration;
  final String? appointmentTime;
  final String? customerName;
  final String? customerAddress;
  final String? therapistName;
  final String? therapistAddress;
  final String sentDate;
  final int? jobId;
  final int? jobEnable;

  AppointmentReminder(
      {this.notifId,
      this.type,
      this.displayId,
      this.itemName,
      this.duration,
      this.appointmentTime,
      this.customerName,
      this.customerAddress,
      this.therapistName,
      this.therapistAddress,
      required this.sentDate,
      this.jobId,
      this.jobEnable});

  factory AppointmentReminder.fromJson(Map<String, dynamic> json) =>
      _$AppointmentReminderFromJson(json);
  Map<String, dynamic> toJson() => _$AppointmentReminderToJson(this);
}

@JsonSerializable()
class CarePlanNotification {
  final int? notifId;
  final int? type;
  final int? reportId;
  final String? displayId;
  final String? patientFullName;
  final String? category;
  final String? carePlanCreateDate;
  final String? therapistName;
  final String? sentDate;
  final int? recommendedServices;
  final String? reportLink;

  CarePlanNotification({
    this.notifId,
    this.type,
    this.reportId,
    this.displayId,
    this.patientFullName,
    this.category,
    this.carePlanCreateDate,
    this.therapistName,
    this.sentDate,
    this.recommendedServices,
    this.reportLink,
  });

  factory CarePlanNotification.fromJson(Map<String, dynamic> json) =>
      _$CarePlanNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanNotificationToJson(this);
}

@JsonSerializable()
class BookingReminder {
  final int? notifId;
  final int? type;
  final String? displayId;
  final String? sentDate;
  final String? appointmentTime;

  BookingReminder(
      {this.notifId,
      this.type,
      this.displayId,
      this.sentDate,
      this.appointmentTime});

  factory BookingReminder.fromJson(Map<String, dynamic> json) =>
      _$BookingReminderFromJson(json);
  Map<String, dynamic> toJson() => _$BookingReminderToJson(this);
}

@JsonSerializable()
class ReviewCarePlanNotification {
  final int? notifId;
  final int? type;
  final String? displayId;
  final String? createBy;
  final String? therapistName;
  final String? sentDate;

  ReviewCarePlanNotification({
    this.notifId,
    this.type,
    this.displayId,
    this.createBy,
    this.therapistName,
    this.sentDate,
  });

  factory ReviewCarePlanNotification.fromJson(Map<String, dynamic> json) =>
      _$ReviewCarePlanNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewCarePlanNotificationToJson(this);
}

@JsonSerializable()
class PayoutNotification {
  final int? notifId;
  final int? type;
  final int? payoutId;
  final String? displayId;
  final String? createBy;
  final String? therapistName;
  final String? sentDate;
  final String? receiptLink;

  PayoutNotification({
    this.notifId,
    this.type,
    this.payoutId,
    this.displayId,
    this.createBy,
    this.therapistName,
    this.sentDate,
    this.receiptLink,
  });

  factory PayoutNotification.fromJson(Map<String, dynamic> json) =>
      _$PayoutNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$PayoutNotificationToJson(this);
}
