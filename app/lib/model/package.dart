import 'package:json_annotation/json_annotation.dart';

part "package.g.dart";

@JsonSerializable()
class Package {
  @JsonKey(name: 'auto_No')
  int autoNo;
  String packageName;
  String? packageDesc;
  int? category;
  int? therapyServiceId;
  int? numberOfSessions;
  int? durationSession;
  double? price;
  DateTime? deleteDate;
  int? deleteBy;
  DateTime? createDate;
  int? createBy;
  DateTime? lastUpdate;
  int? lastUpdateBy;

  Package(
      {required this.autoNo,
      required this.packageName,
      this.packageDesc,
      this.category,
      this.therapyServiceId,
      this.numberOfSessions,
      this.durationSession,
      this.price,
      this.deleteDate,
      this.deleteBy,
      this.createDate,
      this.createBy,
      this.lastUpdate,
      this.lastUpdateBy});

  factory Package.fromJson(Map<String, dynamic> json) => _$PackageFromJson(json);
  Map<String, dynamic> toJson() => _$PackageToJson(this);
}
