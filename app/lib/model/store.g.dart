// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Store _$StoreFromJson(Map<String, dynamic> json) => Store(
      alacarte: (json['alacarte'] as List<dynamic>?)
          ?.map((e) => StoreAlacarte.fromJson(e as Map<String, dynamic>))
          .toList(),
      package: (json['package'] as List<dynamic>?)
          ?.map((e) => StorePackage.fromJson(e as Map<String, dynamic>))
          .toList(),
      store: (json['store'] as List<dynamic>?)
          ?.map((e) => StoreProduct.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StoreToJson(Store instance) => <String, dynamic>{
      'alacarte': instance.alacarte,
      'package': instance.package,
      'store': instance.store,
    };

StoreAlacarte _$StoreAlacarteFromJson(Map<String, dynamic> json) =>
    StoreAlacarte(
      id: json['id'] as int?,
      categoryId: json['categoryId'] as int?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      duration: json['duration'] as int?,
      price: json['price'] as String?,
      activeFlag: json['activeFlag'] as bool?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
      rating: (json['rating'] as num?)?.toDouble(),
      totalReviews: json['totalReviews'] as int?,
      reviews: (json['reviews'] as List<dynamic>?)
          ?.map((e) => Review.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StoreAlacarteToJson(StoreAlacarte instance) =>
    <String, dynamic>{
      'id': instance.id,
      'categoryId': instance.categoryId,
      'title': instance.title,
      'description': instance.description,
      'duration': instance.duration,
      'price': instance.price,
      'activeFlag': instance.activeFlag,
      'imageLink': instance.imageLink,
      'rating': instance.rating,
      'totalReviews': instance.totalReviews,
      'reviews': instance.reviews,
    };

StorePackage _$StorePackageFromJson(Map<String, dynamic> json) => StorePackage(
      id: json['id'] as int?,
      categoryId: json['categoryId'] as int?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      duration: json['duration'] as int?,
      price: json['price'] as String?,
      activeFlag: json['activeFlag'] as bool?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
      rating: (json['rating'] as num?)?.toDouble(),
      totalReviews: json['totalReviews'] as int?,
      reviews: (json['reviews'] as List<dynamic>?)
          ?.map((e) => Review.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StorePackageToJson(StorePackage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'categoryId': instance.categoryId,
      'title': instance.title,
      'description': instance.description,
      'duration': instance.duration,
      'price': instance.price,
      'activeFlag': instance.activeFlag,
      'imageLink': instance.imageLink,
      'rating': instance.rating,
      'totalReviews': instance.totalReviews,
      'reviews': instance.reviews,
    };

StoreProduct _$StoreProductFromJson(Map<String, dynamic> json) => StoreProduct(
      id: json['id'] as int?,
      categoryId: json['categoryId'] as int?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      remarks: json['remarks'] as String?,
      price: json['price'] as String?,
      activeFlag: json['activeFlag'] as bool?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
      rating: (json['rating'] as num?)?.toDouble(),
      totalReviews: json['totalReviews'] as int?,
      reviews: (json['reviews'] as List<dynamic>?)
          ?.map((e) => Review.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StoreProductToJson(StoreProduct instance) =>
    <String, dynamic>{
      'id': instance.id,
      'categoryId': instance.categoryId,
      'title': instance.title,
      'description': instance.description,
      'remarks': instance.remarks,
      'price': instance.price,
      'activeFlag': instance.activeFlag,
      'imageLink': instance.imageLink,
      'rating': instance.rating,
      'totalReviews': instance.totalReviews,
      'reviews': instance.reviews,
    };

ImageLink _$ImageLinkFromJson(Map<String, dynamic> json) => ImageLink(
      name: json['name'] as String?,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$ImageLinkToJson(ImageLink instance) => <String, dynamic>{
      'name': instance.name,
      'link': instance.link,
    };

Review _$ReviewFromJson(Map<String, dynamic> json) => Review(
      customerName: json['customerName'] as String?,
      rating: (json['rating'] as num?)?.toDouble(),
      review: json['review'] as String?,
    );

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'customerName': instance.customerName,
      'rating': instance.rating,
      'review': instance.review,
    };
