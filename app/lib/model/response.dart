import 'package:json_annotation/json_annotation.dart';

part 'response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
@JsonSerializable()
class Response<T> {
  final int httpCode;
  final int? errorCode;
  String? message;
  final List<T>? dataResults;
  @JsonKey(ignore: true)
  HttpResult? result;

  Response({required this.httpCode, this.errorCode, this.message, this.dataResults, this.result});

  factory Response.fromJson(Map<String, dynamic> json, T Function(Object?) fromJson) {
    final _res = _$ResponseFromJson(json, fromJson);
    switch (_res.httpCode) {
      case 0:
        _res.result = HttpResult.success;
        break;
      case 1:
        _res.result = HttpResult.failed;
        break;
      case 2:
        _res.result = HttpResult.exception;
        break;
    }
    return _res;
  }
  Map<String, dynamic> toJson(Object? Function(T) toJson) => _$ResponseToJson(this, toJson);

  bool get success => this.result == HttpResult.success;
}

@JsonSerializable(genericArgumentFactories: true)
class Response2<T> {
  final int httpCode;
  final int? errorCode;
  String? message;
  final T? dataResults;
  HttpResult? result;

  Response2({required this.httpCode, this.errorCode, this.message, this.dataResults, this.result});

  factory Response2.fromJson(Map<String, dynamic> json, T Function(Object?) fromJson) {
    final _res = _$Response2FromJson(json, fromJson);
    switch (_res.httpCode) {
      case 0:
        _res.result = HttpResult.success;
        break;
      case 1:
        _res.result = HttpResult.failed;
        break;
      case 2:
        _res.result = HttpResult.exception;
        break;
    }
    return _res;
  }
  Map<String, dynamic> toJson(Object? Function(T) toJson) => _$Response2ToJson(this, toJson);

  bool get success => this.result == HttpResult.success;
}

enum HttpResult { success, failed, exception }
