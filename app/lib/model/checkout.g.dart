// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checkout.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Checkout _$CheckoutFromJson(Map<String, dynamic> json) => Checkout(
      subtotal: (json['subtotal'] as num?)?.toDouble(),
      tax: (json['tax'] as num?)?.toDouble(),
      travelFee: (json['travelFee'] as num?)?.toDouble(),
      total: (json['total'] as num?)?.toDouble(),
      orderItem: (json['orderItem'] as List<dynamic>?)
          ?.map((e) => CheckoutItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CheckoutToJson(Checkout instance) => <String, dynamic>{
      'subtotal': instance.subtotal,
      'tax': instance.tax,
      'travelFee': instance.travelFee,
      'total': instance.total,
      'orderItem': instance.orderItem,
    };

CheckoutItem _$CheckoutItemFromJson(Map<String, dynamic> json) => CheckoutItem(
      orderId: json['orderId'] as int?,
      itemName: json['itemName'] as String?,
      itemType: json['itemType'] as String?,
      itemCount: json['itemCount'] as int?,
      itemPrice: (json['itemPrice'] as num?)?.toDouble(),
      therapistName: json['therapistName'] as String?,
      therapistAddress: json['therapistAddress'] as String?,
      customerAddress: json['customerAddress'] as String?,
      imageLink: (json['imageLink'] as List<dynamic>?)
          ?.map((e) => ImageLink.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CheckoutItemToJson(CheckoutItem instance) =>
    <String, dynamic>{
      'orderId': instance.orderId,
      'itemName': instance.itemName,
      'itemType': instance.itemType,
      'itemCount': instance.itemCount,
      'itemPrice': instance.itemPrice,
      'therapistName': instance.therapistName,
      'therapistAddress': instance.therapistAddress,
      'customerAddress': instance.customerAddress,
      'imageLink': instance.imageLink,
    };
