import 'package:app/model/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart.g.dart';

@JsonSerializable()
class ShoppingCart {
  int? orderId;
  String? itemName;
  String? productType;
  String? subTotal;
  int? duration;
  String? productRemark;
  int? totalItem;
  List<ImageLink>? imageLink;

  ShoppingCart(
      {this.orderId,
      this.itemName,
      this.productType,
      this.subTotal,
      this.duration,
      this.productRemark,
      this.totalItem,
      this.imageLink});

  factory ShoppingCart.fromJson(Map<String, dynamic> json) => _$ShoppingCartFromJson(json);
  Map<String, dynamic> toJson() => _$ShoppingCartToJson(this);
}
