import 'package:app/model/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'booking.g.dart';

@JsonSerializable()
class BookingOrder {
  int? bookingId;
  int? orderId;
  String? displayOrderId;
  String? itemName;
  int? duration;
  double? subtotal;
  double? tax;
  double? travelFee;
  double? total;
  String? therapistName;
  String? therapistAddress;
  String? customerName;
  String? customerAddress;
  String? timeSlot;
  String? displayTimeSlot;
  String? appointmentDate;
  String? cancelDate;
  int? totalSession;
  int? completedSession;
  int? therapistId;
  int? therapistAddressId;
  int? productId;
  int? customerAddressId;
  String? productType;
  bool? rescheduleFlag;
  bool? reorderFlag;
  List<ImageLink>? imageLink;
  String? authCode;
  String? authCodeRequestTime;
  String? displayReportId;
  String? reportLink;
  String? reportGeneratedBy;
  String? reportGeneratedDate;
  bool? isCompleteEnabled;

  BookingOrder(
      {this.bookingId,
      this.orderId,
      this.displayOrderId,
      this.itemName,
      this.duration,
      this.subtotal,
      this.tax,
      this.travelFee,
      this.total,
      this.therapistName,
      this.therapistAddress,
      this.customerName,
      this.customerAddress,
      this.timeSlot,
      this.displayTimeSlot,
      this.appointmentDate,
      this.cancelDate,
      this.totalSession,
      this.completedSession,
      this.therapistId,
      this.therapistAddressId,
      this.productId,
      this.customerAddressId,
      this.productType,
      this.rescheduleFlag,
      this.reorderFlag,
      this.imageLink,
      this.authCode,
      this.authCodeRequestTime,
      this.isCompleteEnabled});

  factory BookingOrder.fromJson(Map<String, dynamic> json) =>
      _$BookingOrderFromJson(json);
  Map<String, dynamic> toJson() => _$BookingOrderToJson(this);
}

@JsonSerializable()
class Timeslot {
  DateTime? value;
  String? display;
  bool? isEnabled;

  Timeslot({this.value, this.display, this.isEnabled});

  factory Timeslot.fromJson(Map<String, dynamic> json) =>
      _$TimeslotFromJson(json);
  Map<String, dynamic> toJson() => _$TimeslotToJson(this);
}
