// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationMessage _$NotificationMessageFromJson(Map<String, dynamic> json) =>
    NotificationMessage(
      notifId: json['notifId'] as int?,
      title: json['title'] as String?,
      message: json['message'] as String?,
      type: json['type'] as int?,
    );

Map<String, dynamic> _$NotificationMessageToJson(
        NotificationMessage instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'title': instance.title,
      'message': instance.message,
      'type': instance.type,
    };

AppointmentReminder _$AppointmentReminderFromJson(Map<String, dynamic> json) =>
    AppointmentReminder(
      notifId: json['notifId'] as int?,
      type: json['type'] as int?,
      displayId: json['displayId'] as String?,
      itemName: json['itemName'] as String?,
      duration: json['duration'] as int?,
      appointmentTime: json['appointmentTime'] as String?,
      customerName: json['customerName'] as String?,
      customerAddress: json['customerAddress'] as String?,
      therapistName: json['therapistName'] as String?,
      therapistAddress: json['therapistAddress'] as String?,
      sentDate: json['sentDate'] as String,
      jobId: json['jobId'] as int?,
      jobEnable: json['jobEnable'] as int?,
    );

Map<String, dynamic> _$AppointmentReminderToJson(
        AppointmentReminder instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'type': instance.type,
      'displayId': instance.displayId,
      'itemName': instance.itemName,
      'duration': instance.duration,
      'appointmentTime': instance.appointmentTime,
      'customerName': instance.customerName,
      'customerAddress': instance.customerAddress,
      'therapistName': instance.therapistName,
      'therapistAddress': instance.therapistAddress,
      'sentDate': instance.sentDate,
      'jobId': instance.jobId,
      'jobEnable': instance.jobEnable,
    };

CarePlanNotification _$CarePlanNotificationFromJson(
        Map<String, dynamic> json) =>
    CarePlanNotification(
      notifId: json['notifId'] as int?,
      type: json['type'] as int?,
      reportId: json['reportId'] as int?,
      displayId: json['displayId'] as String?,
      patientFullName: json['patientFullName'] as String?,
      category: json['category'] as String?,
      carePlanCreateDate: json['carePlanCreateDate'] as String?,
      therapistName: json['therapistName'] as String?,
      sentDate: json['sentDate'] as String?,
      recommendedServices: json['recommendedServices'] as int?,
      reportLink: json['reportLink'] as String?,
    );

Map<String, dynamic> _$CarePlanNotificationToJson(
        CarePlanNotification instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'type': instance.type,
      'reportId': instance.reportId,
      'displayId': instance.displayId,
      'patientFullName': instance.patientFullName,
      'category': instance.category,
      'carePlanCreateDate': instance.carePlanCreateDate,
      'therapistName': instance.therapistName,
      'sentDate': instance.sentDate,
      'recommendedServices': instance.recommendedServices,
      'reportLink': instance.reportLink,
    };

BookingReminder _$BookingReminderFromJson(Map<String, dynamic> json) =>
    BookingReminder(
      notifId: json['notifId'] as int?,
      type: json['type'] as int?,
      displayId: json['displayId'] as String?,
      sentDate: json['sentDate'] as String?,
      appointmentTime: json['appointmentTime'] as String?,
    );

Map<String, dynamic> _$BookingReminderToJson(BookingReminder instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'type': instance.type,
      'displayId': instance.displayId,
      'sentDate': instance.sentDate,
      'appointmentTime': instance.appointmentTime,
    };

ReviewCarePlanNotification _$ReviewCarePlanNotificationFromJson(
        Map<String, dynamic> json) =>
    ReviewCarePlanNotification(
      notifId: json['notifId'] as int?,
      type: json['type'] as int?,
      displayId: json['displayId'] as String?,
      createBy: json['createBy'] as String?,
      therapistName: json['therapistName'] as String?,
      sentDate: json['sentDate'] as String?,
    );

Map<String, dynamic> _$ReviewCarePlanNotificationToJson(
        ReviewCarePlanNotification instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'type': instance.type,
      'displayId': instance.displayId,
      'createBy': instance.createBy,
      'therapistName': instance.therapistName,
      'sentDate': instance.sentDate,
    };

PayoutNotification _$PayoutNotificationFromJson(Map<String, dynamic> json) =>
    PayoutNotification(
      notifId: json['notifId'] as int?,
      type: json['type'] as int?,
      payoutId: json['payoutId'] as int?,
      displayId: json['displayId'] as String?,
      createBy: json['createBy'] as String?,
      therapistName: json['therapistName'] as String?,
      sentDate: json['sentDate'] as String?,
      receiptLink: json['receiptLink'] as String?,
    );

Map<String, dynamic> _$PayoutNotificationToJson(PayoutNotification instance) =>
    <String, dynamic>{
      'notifId': instance.notifId,
      'type': instance.type,
      'payoutId': instance.payoutId,
      'displayId': instance.displayId,
      'createBy': instance.createBy,
      'therapistName': instance.therapistName,
      'sentDate': instance.sentDate,
      'receiptLink': instance.receiptLink,
    };
