// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Response<T> _$ResponseFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    Response<T>(
      httpCode: json['httpCode'] as int,
      errorCode: json['errorCode'] as int?,
      message: json['message'] as String?,
      dataResults:
          (json['dataResults'] as List<dynamic>?)?.map(fromJsonT).toList(),
    );

Map<String, dynamic> _$ResponseToJson<T>(
  Response<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'httpCode': instance.httpCode,
      'errorCode': instance.errorCode,
      'message': instance.message,
      'dataResults': instance.dataResults?.map(toJsonT).toList(),
    };

Response2<T> _$Response2FromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    Response2<T>(
      httpCode: json['httpCode'] as int,
      errorCode: json['errorCode'] as int?,
      message: json['message'] as String?,
      dataResults: _$nullableGenericFromJson(json['dataResults'], fromJsonT),
      result: $enumDecodeNullable(_$HttpResultEnumMap, json['result']),
    );

Map<String, dynamic> _$Response2ToJson<T>(
  Response2<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'httpCode': instance.httpCode,
      'errorCode': instance.errorCode,
      'message': instance.message,
      'dataResults': _$nullableGenericToJson(instance.dataResults, toJsonT),
      'result': _$HttpResultEnumMap[instance.result],
    };

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

const _$HttpResultEnumMap = {
  HttpResult.success: 'success',
  HttpResult.failed: 'failed',
  HttpResult.exception: 'exception',
};

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
