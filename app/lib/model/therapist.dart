import 'package:json_annotation/json_annotation.dart';

part 'therapist.g.dart';

@JsonSerializable()
class Therapist {
  final int? id;
  final String? fullName;
  final String? coordinate;
  final String? travellingDistance;
  final String? travellingFee;
  final String? contactNo;
  final int? addressId;
  final String? fullAddress;

  Therapist(
      {this.id,
      this.fullName,
      this.coordinate,
      this.travellingDistance,
      this.travellingFee,
      this.contactNo,
      this.addressId,
      this.fullAddress});

  factory Therapist.fromJson(Map<String, dynamic> json) => _$TherapistFromJson(json);
  Map<String, dynamic> toJson() => _$TherapistToJson(this);
}
