import 'package:json_annotation/json_annotation.dart';

part "payout.g.dart";

@JsonSerializable()
class PendingPayout {
  int? accountId;
  String? pendingAmount;
  String? nextPayoutDate;
  String? payoutFrequency;
  String? payoutBankName;
  String? payoutBankAccount;
  List<Payouts>? pendingPayout;

  PendingPayout({
    this.accountId,
    this.pendingAmount,
    this.nextPayoutDate,
    this.payoutFrequency,
    this.payoutBankName,
    this.payoutBankAccount,
    this.pendingPayout,
  });

  factory PendingPayout.fromJson(Map<String, dynamic> json) => _$PendingPayoutFromJson(json);
  Map<String, dynamic> toJson() => _$PendingPayoutToJson(this);
}

@JsonSerializable()
class Payouts {
  String? appointmentDate;
  List<PayoutOrder>? orderDetails;

  Payouts({
    this.appointmentDate,
    this.orderDetails,
  });

  factory Payouts.fromJson(Map<String, dynamic> json) => _$PayoutsFromJson(json);
  Map<String, dynamic> toJson() => _$PayoutsToJson(this);
}

@JsonSerializable()
class PayoutOrder {
  String? orderId;
  String? timeSlot;
  String? amount;

  PayoutOrder({
    this.orderId,
    this.timeSlot,
    this.amount,
  });

  factory PayoutOrder.fromJson(Map<String, dynamic> json) => _$PayoutOrderFromJson(json);
  Map<String, dynamic> toJson() => _$PayoutOrderToJson(this);
}

@JsonSerializable()
class ReceivedPayout {
  int? payoutId;
  String? displayPayoutId;
  String? transactionDate;
  String? totalCommission;

  ReceivedPayout({
    this.payoutId,
    this.displayPayoutId,
    this.transactionDate,
    this.totalCommission,
  });

  factory ReceivedPayout.fromJson(Map<String, dynamic> json) => _$ReceivedPayoutFromJson(json);
  Map<String, dynamic> toJson() => _$ReceivedPayoutToJson(this);
}

@JsonSerializable()
class ReceivedPayoutDetail {
  int? payoutId;
  String? displayPayoutId;
  String? transactionDate;
  String? totalCommission;
  String? pdfReceiptLink;
  List<Payouts>? receivedPayout;

  ReceivedPayoutDetail({
    this.payoutId,
    this.displayPayoutId,
    this.transactionDate,
    this.totalCommission,
    this.pdfReceiptLink,
    this.receivedPayout,
  });

  factory ReceivedPayoutDetail.fromJson(Map<String, dynamic> json) =>
      _$ReceivedPayoutDetailFromJson(json);
  Map<String, dynamic> toJson() => _$ReceivedPayoutDetailToJson(this);
}
