import 'package:json_annotation/json_annotation.dart';

part "user.g.dart";

@JsonSerializable()
class User {
  final String? name;
  final DateTime? birthday;
  final String? phoneNumber;
  final String? email;
  final int? gender;
  final double? weight;
  final double? height;

  User(
      {this.name,
      this.birthday,
      this.phoneNumber,
      this.email,
      this.gender,
      this.weight,
      this.height});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
