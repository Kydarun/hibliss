import 'package:json_annotation/json_annotation.dart';

part 'review.g.dart';

@JsonSerializable()
class Review {
  int? id;
  String? name;
  String? profilePicture;
  double? rating;
  String? review;
  DateTime? dateReviewed;

  Review({this.id, this.name, this.profilePicture, this.rating, this.review, this.dateReviewed});

  factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewToJson(this);
}
