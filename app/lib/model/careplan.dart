import 'package:json_annotation/json_annotation.dart';

part 'careplan.g.dart';

@JsonSerializable()
class CarePlan {
  final int? id;
  final String? name;
  final int? categoryId;
  final String? category;
  final String? submissionDate;
  final String? createdBy;
  final String? pdfLink;

  CarePlan(
      {this.id,
      this.name,
      this.categoryId,
      this.category,
      this.submissionDate,
      this.createdBy,
      this.pdfLink});

  factory CarePlan.fromJson(Map<String, dynamic> json) =>
      _$CarePlanFromJson(json);
  Map<String, dynamic> toJson() => _$CarePlanToJson(this);
}
