// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      autoNo: json['auto_No'] as int,
      productName: json['productName'] as String,
      productDesc: json['productDesc'] as String?,
      productPrice: (json['productPrice'] as num?)?.toDouble(),
      productRemark: json['productRemark'] as String?,
      category: json['category'] as int?,
      deleteDate: json['deleteDate'] == null
          ? null
          : DateTime.parse(json['deleteDate'] as String),
      deleteBy: json['deleteBy'] as int?,
      createDate: json['createDate'] == null
          ? null
          : DateTime.parse(json['createDate'] as String),
      createBy: json['createBy'] as int?,
      lastUpdate: json['lastUpdate'] == null
          ? null
          : DateTime.parse(json['lastUpdate'] as String),
      lastUpdateBy: json['lastUpdateBy'] as int?,
    );

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'auto_No': instance.autoNo,
      'productName': instance.productName,
      'productDesc': instance.productDesc,
      'productPrice': instance.productPrice,
      'productRemark': instance.productRemark,
      'category': instance.category,
      'deleteDate': instance.deleteDate?.toIso8601String(),
      'deleteBy': instance.deleteBy,
      'createDate': instance.createDate?.toIso8601String(),
      'createBy': instance.createBy,
      'lastUpdate': instance.lastUpdate?.toIso8601String(),
      'lastUpdateBy': instance.lastUpdateBy,
    };
