// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payout.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PendingPayout _$PendingPayoutFromJson(Map<String, dynamic> json) =>
    PendingPayout(
      accountId: json['accountId'] as int?,
      pendingAmount: json['pendingAmount'] as String?,
      nextPayoutDate: json['nextPayoutDate'] as String?,
      payoutFrequency: json['payoutFrequency'] as String?,
      payoutBankName: json['payoutBankName'] as String?,
      payoutBankAccount: json['payoutBankAccount'] as String?,
      pendingPayout: (json['pendingPayout'] as List<dynamic>?)
          ?.map((e) => Payouts.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PendingPayoutToJson(PendingPayout instance) =>
    <String, dynamic>{
      'accountId': instance.accountId,
      'pendingAmount': instance.pendingAmount,
      'nextPayoutDate': instance.nextPayoutDate,
      'payoutFrequency': instance.payoutFrequency,
      'payoutBankName': instance.payoutBankName,
      'payoutBankAccount': instance.payoutBankAccount,
      'pendingPayout': instance.pendingPayout,
    };

Payouts _$PayoutsFromJson(Map<String, dynamic> json) => Payouts(
      appointmentDate: json['appointmentDate'] as String?,
      orderDetails: (json['orderDetails'] as List<dynamic>?)
          ?.map((e) => PayoutOrder.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PayoutsToJson(Payouts instance) => <String, dynamic>{
      'appointmentDate': instance.appointmentDate,
      'orderDetails': instance.orderDetails,
    };

PayoutOrder _$PayoutOrderFromJson(Map<String, dynamic> json) => PayoutOrder(
      orderId: json['orderId'] as String?,
      timeSlot: json['timeSlot'] as String?,
      amount: json['amount'] as String?,
    );

Map<String, dynamic> _$PayoutOrderToJson(PayoutOrder instance) =>
    <String, dynamic>{
      'orderId': instance.orderId,
      'timeSlot': instance.timeSlot,
      'amount': instance.amount,
    };

ReceivedPayout _$ReceivedPayoutFromJson(Map<String, dynamic> json) =>
    ReceivedPayout(
      payoutId: json['payoutId'] as int?,
      displayPayoutId: json['displayPayoutId'] as String?,
      transactionDate: json['transactionDate'] as String?,
      totalCommission: json['totalCommission'] as String?,
    );

Map<String, dynamic> _$ReceivedPayoutToJson(ReceivedPayout instance) =>
    <String, dynamic>{
      'payoutId': instance.payoutId,
      'displayPayoutId': instance.displayPayoutId,
      'transactionDate': instance.transactionDate,
      'totalCommission': instance.totalCommission,
    };

ReceivedPayoutDetail _$ReceivedPayoutDetailFromJson(
        Map<String, dynamic> json) =>
    ReceivedPayoutDetail(
      payoutId: json['payoutId'] as int?,
      displayPayoutId: json['displayPayoutId'] as String?,
      transactionDate: json['transactionDate'] as String?,
      totalCommission: json['totalCommission'] as String?,
      pdfReceiptLink: json['pdfReceiptLink'] as String?,
      receivedPayout: (json['receivedPayout'] as List<dynamic>?)
          ?.map((e) => Payouts.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ReceivedPayoutDetailToJson(
        ReceivedPayoutDetail instance) =>
    <String, dynamic>{
      'payoutId': instance.payoutId,
      'displayPayoutId': instance.displayPayoutId,
      'transactionDate': instance.transactionDate,
      'totalCommission': instance.totalCommission,
      'pdfReceiptLink': instance.pdfReceiptLink,
      'receivedPayout': instance.receivedPayout,
    };
