// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'therapist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Therapist _$TherapistFromJson(Map<String, dynamic> json) => Therapist(
      id: json['id'] as int?,
      fullName: json['fullName'] as String?,
      coordinate: json['coordinate'] as String?,
      travellingDistance: json['travellingDistance'] as String?,
      travellingFee: json['travellingFee'] as String?,
      contactNo: json['contactNo'] as String?,
      addressId: json['addressId'] as int?,
      fullAddress: json['fullAddress'] as String?,
    );

Map<String, dynamic> _$TherapistToJson(Therapist instance) => <String, dynamic>{
      'id': instance.id,
      'fullName': instance.fullName,
      'coordinate': instance.coordinate,
      'travellingDistance': instance.travellingDistance,
      'travellingFee': instance.travellingFee,
      'contactNo': instance.contactNo,
      'addressId': instance.addressId,
      'fullAddress': instance.fullAddress,
    };
