import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable()
class Address {
  int? addressId;
  String? title;
  String? address;
  String? state;
  String? postalCode;
  String? coordinate;
  String? isPrimary;

  Address(
      {this.addressId,
      this.title,
      this.address,
      this.state,
      this.postalCode,
      this.coordinate,
      this.isPrimary});

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);
  Map<String, dynamic> toJson() => _$AddressToJson(this);
}
