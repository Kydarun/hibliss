import 'package:json_annotation/json_annotation.dart';

part 'addr_state.g.dart';

@JsonSerializable()
class AddrState {
  String stateName;

  AddrState({required this.stateName});

  factory AddrState.fromJson(Map<String, dynamic> json) => _$AddrStateFromJson(json);
  Map<String, dynamic> toJson() => _$AddrStateToJson(this);
}
