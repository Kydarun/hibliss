// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'careplan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarePlan _$CarePlanFromJson(Map<String, dynamic> json) => CarePlan(
      id: json['id'] as int?,
      name: json['name'] as String?,
      categoryId: json['categoryId'] as int?,
      category: json['category'] as String?,
      submissionDate: json['submissionDate'] as String?,
      createdBy: json['createdBy'] as String?,
      pdfLink: json['pdfLink'] as String?,
    );

Map<String, dynamic> _$CarePlanToJson(CarePlan instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'categoryId': instance.categoryId,
      'category': instance.category,
      'submissionDate': instance.submissionDate,
      'createdBy': instance.createdBy,
      'pdfLink': instance.pdfLink,
    };
