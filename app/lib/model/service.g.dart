// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Service _$ServiceFromJson(Map<String, dynamic> json) => Service(
      autoNo: json['auto_No'] as int,
      serviceName: json['serviceName'] as String,
      serviceDesc: json['serviceDesc'] as String?,
      category: json['category'] as int?,
      price: (json['price'] as num?)?.toDouble(),
      duration: json['duration'] as int?,
      deleteDate: json['deleteDate'] == null
          ? null
          : DateTime.parse(json['deleteDate'] as String),
      deleteBy: json['deleteBy'] as int?,
      createDate: json['createDate'] == null
          ? null
          : DateTime.parse(json['createDate'] as String),
      createBy: json['createBy'] as int?,
      lastUpdate: json['lastUpdate'] == null
          ? null
          : DateTime.parse(json['lastUpdate'] as String),
      lastUpdateBy: json['lastUpdateBy'] as int?,
    );

Map<String, dynamic> _$ServiceToJson(Service instance) => <String, dynamic>{
      'auto_No': instance.autoNo,
      'serviceName': instance.serviceName,
      'serviceDesc': instance.serviceDesc,
      'category': instance.category,
      'price': instance.price,
      'duration': instance.duration,
      'deleteDate': instance.deleteDate?.toIso8601String(),
      'deleteBy': instance.deleteBy,
      'createDate': instance.createDate?.toIso8601String(),
      'createBy': instance.createBy,
      'lastUpdate': instance.lastUpdate?.toIso8601String(),
      'lastUpdateBy': instance.lastUpdateBy,
    };
