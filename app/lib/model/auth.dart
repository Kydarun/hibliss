import 'package:json_annotation/json_annotation.dart';

part 'auth.g.dart';

@JsonSerializable()
class Auth {
  @JsonKey(name: 'userid')
  final String userId;
  final String name;
  final String accountType;
  final String? role;
  final bool? resetPassword;
  final String token;

  Auth(
      {required this.userId,
      required this.name,
      required this.accountType,
      this.role,
      this.resetPassword,
      required this.token});

  factory Auth.fromJson(Map<String, dynamic> json) => _$AuthFromJson(json);
  Map<String, dynamic> toJson() => _$AuthToJson(this);
}
