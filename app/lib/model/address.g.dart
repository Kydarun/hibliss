// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) => Address(
      addressId: json['addressId'] as int?,
      title: json['title'] as String?,
      address: json['address'] as String?,
      state: json['state'] as String?,
      postalCode: json['postalCode'] as String?,
      coordinate: json['coordinate'] as String?,
      isPrimary: json['isPrimary'] as String?,
    );

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'addressId': instance.addressId,
      'title': instance.title,
      'address': instance.address,
      'state': instance.state,
      'postalCode': instance.postalCode,
      'coordinate': instance.coordinate,
      'isPrimary': instance.isPrimary,
    };
