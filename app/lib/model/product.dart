import 'package:json_annotation/json_annotation.dart';

part "product.g.dart";

@JsonSerializable()
class Product {
  @JsonKey(name: 'auto_No')
  int autoNo;
  String productName;
  String? productDesc;
  double? productPrice;
  String? productRemark;
  int? category;
  DateTime? deleteDate;
  int? deleteBy;
  DateTime? createDate;
  int? createBy;
  DateTime? lastUpdate;
  int? lastUpdateBy;

  Product(
      {required this.autoNo,
      required this.productName,
      this.productDesc,
      this.productPrice,
      this.productRemark,
      this.category,
      this.deleteDate,
      this.deleteBy,
      this.createDate,
      this.createBy,
      this.lastUpdate,
      this.lastUpdateBy});

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
