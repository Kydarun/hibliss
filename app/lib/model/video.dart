import 'package:json_annotation/json_annotation.dart';

part "video.g.dart";

@JsonSerializable()
class Video {
  final int id;
  final String link;

  Video({required this.id, required this.link});

  factory Video.fromJson(Map<String, dynamic> json) => _$VideoFromJson(json);
  Map<String, dynamic> toJson() => _$VideoToJson(this);
}
