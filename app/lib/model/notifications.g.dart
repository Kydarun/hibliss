// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notifications _$NotificationsFromJson(Map<String, dynamic> json) =>
    Notifications(
      messageNotification: json['messageNotification'] as bool? ?? true,
      appointmentNotification: json['appointmentNotification'] as bool?,
      promotionNotification: json['promotionNotification'] as bool?,
    );

Map<String, dynamic> _$NotificationsToJson(Notifications instance) =>
    <String, dynamic>{
      'messageNotification': instance.messageNotification,
      'appointmentNotification': instance.appointmentNotification,
      'promotionNotification': instance.promotionNotification,
    };
