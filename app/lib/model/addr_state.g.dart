// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'addr_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddrState _$AddrStateFromJson(Map<String, dynamic> json) => AddrState(
      stateName: json['stateName'] as String,
    );

Map<String, dynamic> _$AddrStateToJson(AddrState instance) => <String, dynamic>{
      'stateName': instance.stateName,
    };
