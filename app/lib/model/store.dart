import 'package:json_annotation/json_annotation.dart';

part 'store.g.dart';

@JsonSerializable()
class Store {
  List<StoreAlacarte>? alacarte;
  List<StorePackage>? package;
  List<StoreProduct>? store;

  Store({this.alacarte, this.package, this.store});

  factory Store.fromJson(Map<String, dynamic> json) => _$StoreFromJson(json);
  Map<String, dynamic> toJson() => _$StoreToJson(this);
}

@JsonSerializable()
class StoreAlacarte {
  final int? id;
  final int? categoryId;
  final String? title;
  final String? description;
  final int? duration;
  final String? price;
  final bool? activeFlag;
  final List<ImageLink>? imageLink;
  final double? rating;
  final int? totalReviews;
  final List<Review>? reviews;

  StoreAlacarte({
    this.id,
    this.categoryId,
    this.title,
    this.description,
    this.duration,
    this.price,
    this.activeFlag,
    this.imageLink,
    this.rating,
    this.totalReviews,
    this.reviews,
  });

  factory StoreAlacarte.fromJson(Map<String, dynamic> json) => _$StoreAlacarteFromJson(json);
  Map<String, dynamic> toJson() => _$StoreAlacarteToJson(this);
}

@JsonSerializable()
class StorePackage {
  final int? id;
  final int? categoryId;
  final String? title;
  final String? description;
  final int? duration;
  final String? price;
  final bool? activeFlag;
  final List<ImageLink>? imageLink;
  final double? rating;
  final int? totalReviews;
  final List<Review>? reviews;

  StorePackage({
    this.id,
    this.categoryId,
    this.title,
    this.description,
    this.duration,
    this.price,
    this.activeFlag,
    this.imageLink,
    this.rating,
    this.totalReviews,
    this.reviews,
  });

  factory StorePackage.fromJson(Map<String, dynamic> json) => _$StorePackageFromJson(json);
  Map<String, dynamic> toJson() => _$StorePackageToJson(this);
}

@JsonSerializable()
class StoreProduct {
  final int? id;
  final int? categoryId;
  final String? title;
  final String? description;
  final String? remarks;
  final String? price;
  final bool? activeFlag;
  final List<ImageLink>? imageLink;
  final double? rating;
  final int? totalReviews;
  final List<Review>? reviews;

  StoreProduct({
    this.id,
    this.categoryId,
    this.title,
    this.description,
    this.remarks,
    this.price,
    this.activeFlag,
    this.imageLink,
    this.rating,
    this.totalReviews,
    this.reviews,
  });

  factory StoreProduct.fromJson(Map<String, dynamic> json) => _$StoreProductFromJson(json);
  Map<String, dynamic> toJson() => _$StoreProductToJson(this);
}

@JsonSerializable()
class ImageLink {
  final String? name;
  final String? link;

  ImageLink({this.name, this.link});

  factory ImageLink.fromJson(Map<String, dynamic> json) => _$ImageLinkFromJson(json);
  Map<String, dynamic> toJson() => _$ImageLinkToJson(this);
}

@JsonSerializable()
class Review {
  final String? customerName;
  final double? rating;
  final String? review;

  Review({
    this.customerName,
    this.rating,
    this.review,
  });

  factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewToJson(this);
}
