import 'dart:convert';

import 'package:app/model/address.dart';
import 'package:app/model/cart.dart';
import 'package:app/model/checkout.dart';
import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/address.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class CartRest extends RestClient {
  CartRest({required BuildContext context}) : super(context: context);

  Future<Response2<String>> addServiceToCart(
          {required int productId,
          required int customerAddressId,
          required int therapistId,
          required int therapistAddressId}) =>
      invokeApi2(
          request: (client) async =>
              client.post(Uri.parse("${await super.apiUrl()}Order/serviceOrder"),
                  body: json.encode({
                    'productId': productId,
                    'customerAddressId': customerAddressId,
                    'therapistId': therapistId,
                    'therapistAddressId': therapistAddressId
                  })),
          fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));

  Future<Response2<String>> addGoodsToCart(
          {required int productId, required int customerAddressId}) =>
      invokeApi2(
          request: (client) async => client.post(
              Uri.parse("${await super.apiUrl()}Order/goodOrder"),
              body: json.encode({'productId': productId, 'customerAddressId': customerAddressId})),
          fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));

  Future<Response<ShoppingCart>> getShoppingCart({int? addressId}) async {
    if (addressId == null) {
      addressId = (await AddressRest(context: context).list())
          .dataResults
          ?.firstWhere((element) => (element.isPrimary ?? "No") == "Yes", orElse: () => Address())
          .addressId;
      if (addressId == null) {
        return Response(httpCode: -2, message: 'Please enter primary address to continue.');
      }
    }
    return invokeApi(
        request: (client) async =>
            client.get(Uri.parse("${await super.apiUrl()}Order/cartOrder/$addressId")),
        fromJson: (res) => Response<ShoppingCart>.fromJson(
            res, (data) => ShoppingCart.fromJson(data as Map<String, dynamic>)));
  }

  Future<Response2<Checkout>> getCheckoutSummary({int? addressId, String? orderIds}) async {
    if (orderIds == null || orderIds == '') {
      return Response2(httpCode: 0, dataResults: Checkout());
    }

    return invokeApi2(
        request: (client) async =>
            client.get(Uri.parse("${await super.apiUrl()}Order/checkoutOrder/$orderIds")),
        fromJson: (res) => Response2<Checkout>.fromJson(
            res, (data) => Checkout.fromJson(data as Map<String, dynamic>)));
  }

  Future<Response2<String>> updateOrder({required int orderId, required int itemCount}) =>
      invokeApi2(
          request: (client) async =>
              client.put(Uri.parse("${await super.apiUrl()}Order/itemCount/$orderId/$itemCount")),
          fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));

  Future<void> deleteOrder({required int orderId}) => invokeApi2(
      request: (client) async =>
          client.delete(Uri.parse("${await super.apiUrl()}Order?orderId=$orderId")),
      fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));
}
