import 'dart:convert';

import 'package:app/model/careplan.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:app/view_model/careplan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class CarePlanRest extends RestClient {
  CarePlanRest({required BuildContext context}) : super(context: context);

  Future<List<CarePlanDef>> getDefs() async {
    final _res =
        json.decode(await rootBundle.loadString("res/strings/careplans.json"));
    return (_res as List).map((res) => CarePlanDef.fromJson(res)).toList();
  }

  Future<Response2<String>> createPain({required CarePlanViewModel plan}) {
    if (plan.pain == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.pain!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());

    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/pain"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createOsteoarthritis(
      {required CarePlanViewModel plan}) {
    if (plan.osteoarthritis == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.osteoarthritis!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/osteoarthritis"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createDetox({required CarePlanViewModel plan}) {
    if (plan.detox == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.detox!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/detox"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createSkin({required CarePlanViewModel plan}) {
    if (plan.skin == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.skin!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/skin"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createStress({required CarePlanViewModel plan}) {
    if (plan.stress == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.stress!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/stress"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createEye({required CarePlanViewModel plan}) {
    if (plan.eye == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.eye!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/eye"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response2<String>> createStroke({required CarePlanViewModel plan}) {
    if (plan.stroke == null) {
      return Future.value(Response2(httpCode: 1, message: 'Care Plan invalid'));
    }
    final _param = plan.stroke!.toJson();
    _param.putIfAbsent('patient', () => plan.patient!.toJson());
    _param.putIfAbsent('currentSymptoms', () => plan.currentSymptoms);
    _param.putIfAbsent('currentSymptomsDesc', () => plan.currentSymptomsDesc);
    _param.putIfAbsent('anyMedication', () => plan.anyMedication);
    _param.putIfAbsent('anyMedicationDesc', () => plan.anyMedicationDesc);
    _param.putIfAbsent('medicalHistory', () => plan.medicalHistory);
    _param.putIfAbsent('existingCustomer', () => plan.existingCustomer);
    _param.putIfAbsent('callback', () => plan.callback?.toIso8601String());
    debugPrint(json.encode(_param));
    return invokeApi2(
        request: (client) async => client.post(
            Uri.parse("${await super.apiUrl()}CarePlan/stroke"),
            body: json.encode(_param)),
        fromJson: (res) => Response2.fromJson(res, (data) => data as String));
  }

  Future<Response<CarePlan>> list() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}CarePlan")),
      fromJson: (res) => Response<CarePlan>.fromJson(
          res, (data) => CarePlan.fromJson(data as Map<String, dynamic>)));

  Future<Response<CarePlan>> get({required int carePlanId}) => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}CarePlan/$carePlanId")),
      fromJson: (res) => Response<CarePlan>.fromJson(
          res, (data) => CarePlan.fromJson(data as Map<String, dynamic>)));

  Future<Response> upload({required String id, required String base64Image}) =>
      invokeApi(
          request: (client) async => client.post(
              Uri.parse("${await super.apiUrl()}General/uploadFiles/$id"),
              body: json.encode({
                'fileName': '$id.png',
                'fileContent': base64Image,
                'module': 'customer',
                'submodule': 'careplan'
              })),
          fromJson: (res) => Response<CarePlan>.fromJson(
              res, (data) => CarePlan.fromJson(data as Map<String, dynamic>)));
}
