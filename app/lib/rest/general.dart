import 'package:app/model/response.dart';
import 'package:app/model/video.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class GeneralRest extends RestClient {
  GeneralRest({required BuildContext context}) : super(context: context);

  Future<Response<Video>> videos() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}SystemRoles/hiblisstv")),
      fromJson: (res) =>
          Response<Video>.fromJson(res, (data) => Video.fromJson(data as Map<String, dynamic>)));
}
