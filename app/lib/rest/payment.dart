import 'dart:convert';

import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PaymentRest extends RestClient {
  PaymentRest({required BuildContext context}) : super(context: context);

  Future<Response2<String>> getRefNo(
          {required String orderIds, required double amount}) =>
      invokeApi2(
          request: (client) async => client.post(
              Uri.parse("${await super.apiUrl()}Payment/requestPayment"),
              body: json.encode({'amount': '$amount', 'orderIds': orderIds})),
          fromJson: (res) =>
              Response2<String>.fromJson(res, (data) => data as String));

  Future<Response2<bool>> verifyPayment(
          {required String refNo, String? transactionId}) =>
      invokeApi2(
          request: (client) async => client
                  .get(Uri.parse(
                      "${await super.apiUrl()}Payment/verifyPayment/$refNo/$transactionId"))
                  .timeout(Duration(seconds: 10), onTimeout: () {
                return http.Response(
                    'Unable to retrieve transaction status. Please contact Hi-Bliss Customer Support.',
                    400);
              }),
          fromJson: (res) =>
              Response2<bool>.fromJson(res, (data) => data as bool));
}
