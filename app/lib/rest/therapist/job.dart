import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class TherapistJobRest extends RestClient {
  TherapistJobRest({required BuildContext context}) : super(context: context);

  Future<Response<BookingOrder>> getUpcomingJobs() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/upcoming")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response<BookingOrder>> getReportingJobs() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/reporting")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response<BookingOrder>> getHistoryJobs() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/history")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response2<BookingOrder>> getJob({required int bookingId, required String jobCategory}) =>
      invokeApi2(
          request: (client) async => client
              .get(Uri.parse("${await super.apiUrl()}TherapistJob/app/$jobCategory/$bookingId")),
          fromJson: (res) => Response2<BookingOrder>.fromJson(
              res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response2<BookingOrder>> getUpcomingJob({required int bookingId}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/upcoming/$bookingId")),
      fromJson: (res) => Response2<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response2<BookingOrder>> getReportingJob({required int bookingId}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/reporting/$bookingId")),
      fromJson: (res) => Response2<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response2<BookingOrder>> getHistoryJob({required int bookingId}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}TherapistJob/app/history/$bookingId")),
      fromJson: (res) => Response2<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<
      Response2<
          String>> startJob({required int bookingId, required String qrCode}) => invokeApi2(
      request: (client) async => client.post(Uri.parse(
          "${await super.apiUrl()}TherapistJob/app/qrStartSession/${Uri.encodeComponent('$qrCode,$bookingId')}")),
      fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));

  Future<
      Response2<
          String>> completeJob({required int bookingId, required String qrCode}) => invokeApi2(
      request: (client) async => client.post(Uri.parse(
          "${await super.apiUrl()}TherapistJob/app/qrEndSession/${Uri.encodeComponent('$qrCode,$bookingId')}")),
      fromJson: (res) => Response2<String>.fromJson(res, (data) => data as String));
}
