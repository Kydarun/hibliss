import 'dart:convert';

import 'package:app/model/payout.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class PayoutRest extends RestClient {
  PayoutRest({required BuildContext context}) : super(context: context);

  Future<Response<ReceivedPayout>> listReceived() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Payout/receivedPayout")),
      fromJson: (res) => Response<ReceivedPayout>.fromJson(
          res, (data) => ReceivedPayout.fromJson(data as Map<String, dynamic>)));

  Future<Response2<ReceivedPayoutDetail>> getReceived({required int payoutId}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Payout/receivedPayout/$payoutId")),
      fromJson: (res) => Response2<ReceivedPayoutDetail>.fromJson(
          res, (data) => ReceivedPayoutDetail.fromJson(data as Map<String, dynamic>)));

  Future<Response2<PendingPayout>> listPending() => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Payout/pendingPayout")),
      fromJson: (res) => Response2<PendingPayout>.fromJson(
          res, (data) => PendingPayout.fromJson(data as Map<String, dynamic>)));

  Future<Response> editPayout({required String payout}) => invokeApi(
      request: (client) async =>
          client.put(Uri.parse("${await super.apiUrl()}Payout/payoutSettings?payout=$payout")),
      fromJson: (res) => Response.fromJson(res, (data) => data as String));

  Future<Response> editBank({required String bankName, required String bankAccount}) => invokeApi(
      request: (client) async => client.put(
            Uri.parse(
                "${await super.apiUrl()}Payout/bankSettings?bankName=$bankName&bankAccount=$bankAccount"),
          ),
      fromJson: (res) => Response.fromJson(res, (data) => data as String));
}
