import 'package:app/model/notification.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class NotificationRest extends RestClient {
  NotificationRest({required BuildContext context}) : super(context: context);

  Future<Response<NotificationMessage>> list() => invokeApi(
      request: (client) async => client.get(Uri.parse("${await super.apiUrl()}Notification")),
      fromJson: (res) => Response<NotificationMessage>.fromJson(
          res, (data) => NotificationMessage.fromJson(data as Map<String, dynamic>)));

  Future<Response<AppointmentReminder>> getAppointmentReminder({required int notificationId}) =>
      invokeApi(
          request: (client) async =>
              client.get(Uri.parse("${await super.apiUrl()}Notification/$notificationId")),
          fromJson: (res) => Response<AppointmentReminder>.fromJson(
              res, (data) => AppointmentReminder.fromJson(data as Map<String, dynamic>)));

  Future<Response<CarePlanNotification>> getCarePlanNotification({required int notificationId}) =>
      invokeApi(
          request: (client) async =>
              client.get(Uri.parse("${await super.apiUrl()}Notification/$notificationId")),
          fromJson: (res) => Response<CarePlanNotification>.fromJson(
              res, (data) => CarePlanNotification.fromJson(data as Map<String, dynamic>)));

  Future<Response<BookingReminder>> getBookingReminder({required int notificationId}) => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Notification/$notificationId")),
      fromJson: (res) => Response<BookingReminder>.fromJson(
          res, (data) => BookingReminder.fromJson(data as Map<String, dynamic>)));

  Future<Response<ReviewCarePlanNotification>> getReviewCarePlanNotificationr(
          {required int notificationId}) =>
      invokeApi(
          request: (client) async =>
              client.get(Uri.parse("${await super.apiUrl()}Notification/$notificationId")),
          fromJson: (res) => Response<ReviewCarePlanNotification>.fromJson(
              res, (data) => ReviewCarePlanNotification.fromJson(data as Map<String, dynamic>)));

  Future<Response<PayoutNotification>> getPayoutNotification({required int notificationId}) =>
      invokeApi(
          request: (client) async =>
              client.get(Uri.parse("${await super.apiUrl()}Notification/$notificationId")),
          fromJson: (res) => Response<PayoutNotification>.fromJson(
              res, (data) => PayoutNotification.fromJson(data as Map<String, dynamic>)));
}
