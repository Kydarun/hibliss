import 'dart:convert';
import 'dart:io';

import 'package:app/model/auth.dart';
import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/client.dart';
import 'package:app/util/cache.dart';
import 'package:app/util/environment.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart';

class AuthRest extends RestClient {
  AuthRest({required BuildContext context}) : super(context: context);

  Future<Response2<Auth>> login(
      {required String username, required String password}) async {
    DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
    String _deviceId = "";
    String _os = "";
    String? _debug = await FirebaseMessaging.instance.getToken();
    debugPrint('$_debug');
    if (Platform.isAndroid) {
      _deviceId = (await FirebaseMessaging.instance.getToken()) ??
          (await _deviceInfo.androidInfo).androidId;
      _os = "android";
    } else if (Platform.isIOS) {
      _deviceId = (await FirebaseMessaging.instance.getToken()) ??
          (await _deviceInfo.iosInfo).identifierForVendor;
      _os = "ios";
    }
    final _auth = await invokeApi2(
        request: (client) async => client.post(
            Uri.parse(
                "${await super.apiUrl()}Authentication${EnvironmentConfig.isTherapist ? "/trpApp" : ""}"),
            body: json.encode({
              'username': username,
              'password': md5.convert(utf8.encode(password)).toString(),
              'token': _deviceId,
              'osType': _os
            })),
        fromJson: (res) => Response2<Auth>.fromJson(
            res, (data) => Auth.fromJson(data as Map<String, dynamic>)));
    if (_auth.success && _auth.dataResults != null) {
      await Cache(context: context).login(auth: _auth.dataResults!);
    }
    return _auth;
  }

  Future<Response> forgotPassword({required String email}) async {
    DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
    String _deviceId = "";
    String _os = "";
    if (Platform.isAndroid) {
      _deviceId = (await _deviceInfo.androidInfo).androidId;
      _os = "android";
    } else if (Platform.isIOS) {
      _deviceId = (await _deviceInfo.iosInfo).identifierForVendor;
      _os = "ios";
    }
    return invokeApi(
        request: (client) async => client.post(
            Uri.parse(
                "${await super.apiUrl()}Authentication/mobile/forgotpassword"),
            body: json
                .encode({'email': email, 'osType': _os, 'token': _deviceId})),
        fromJson: (res) => Response.fromJson(res, (data) => {}));
  }

  Future<Response2<String>> signup(
      {required String email,
      required String password,
      required String name}) async {
    DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
    String _deviceId = "";
    String _os = "";
    if (Platform.isAndroid) {
      _deviceId = (await _deviceInfo.androidInfo).androidId;
      _os = "android";
    } else if (Platform.isIOS) {
      _deviceId = (await _deviceInfo.iosInfo).identifierForVendor;
      _os = "ios";
    }
    return invokeApi2(
        request: (client) async =>
            client.post(Uri.parse("${await super.apiUrl()}User/member"),
                body: json.encode({
                  'email': email,
                  'password': md5.convert(utf8.encode(password)).toString(),
                  'name': name,
                  'osType': _os,
                  'token': _deviceId
                })),
        fromJson: (res) =>
            Response2<String>.fromJson(res, (data) => data as String));
  }

  Future<Response> changePassword(
          {required String currentPassword,
          required String newPassword}) async =>
      invokeApi(
          request: (client) async => client.put(Uri.parse(
              "${await super.apiUrl()}Authentication/mobile/updatePassword/${md5.convert(utf8.encode(currentPassword)).toString()}/${md5.convert(utf8.encode(newPassword)).toString()}")),
          fromJson: (res) => Response<User>.fromJson(
              res, (data) => User.fromJson(data as Map<String, dynamic>)));

  Future<Response> firstTime({required String newPassword}) async => invokeApi(
      request: (client) async => client.put(Uri.parse(
          "${await super.apiUrl()}Authentication/mobile/firstPassword/${md5.convert(utf8.encode(newPassword)).toString()}")),
      fromJson: (res) => Response<User>.fromJson(
          res, (data) => User.fromJson(data as Map<String, dynamic>)));

  Future<Response> logout() async => invokeApi(
      request: (client) async => client
          .post(Uri.parse("${await super.apiUrl()}Authentication/logout")),
      fromJson: (res) => Response.fromJson(res, (data) => data as String));
}
