import 'package:app/model/product.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class ProductRest extends RestClient {
  ProductRest({required BuildContext context}) : super(context: context);

  Future<Response<Product>> list() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/goods")),
      fromJson: (res) => Response<Product>.fromJson(
          res, (data) => Product.fromJson(data as Map<String, dynamic>)));

  Future<Response<Product>> get(int id) => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/goods/$id")),
      fromJson: (res) => Response<Product>.fromJson(
          res, (data) => Product.fromJson(data as Map<String, dynamic>)));
}
