import 'dart:async';
import 'dart:convert';

import 'package:app/model/response.dart';
import 'package:app/util/cache.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

abstract class RestClient {
  BuildContext context;

  RestClient({required this.context});

  apiUrl() async => json.decode(
      await rootBundle.loadString("res/strings/settings.json"))['api_url'];

  Future<Response<T>> invokeApi<T>(
      {required Future<http.Response> Function(AuthClient) request,
      required Response<T> Function(Map<String, dynamic>) fromJson}) async {
    try {
      final _res =
          await request(AuthClient(client: http.Client(), context: context));
      debugPrint('${_res.statusCode}');
      debugPrint('${_res.body}');
      if (_res.statusCode >= 200 && _res.statusCode < 300) {
        final _resBody = json.decode(_res.body);
        final _response = fromJson(_resBody);
        switch (_response.errorCode) {
          case 10:
            _response.message = "Please fill in all required fields.";
            break;
          case 11:
            _response.message =
                "An exception occurred. Please contact administrator.";
            break;
          case 100:
            _response.message = "Invalid username or password.";
            break;
          case 101:
            _response.message =
                "The user account is inactive. Please contact administrator.";
            break;
          case 102:
            _response.message = "The email address does not exists.";
            break;
          case 105:
            _response.message = "Wrong original password.";
            break;
          case 200:
            _response.message =
                "The serial number already exists in our system. Pleaes use another one.";
            break;
          case 300:
            _response.message =
                "The email already exists in our system. Please use another one.";
            break;
          default:
            break;
        }
        return _response;
      } else if (_res.statusCode == 401) {
        await Cache(context: context).logout();
        return Response<T>(
            httpCode: _res.statusCode,
            errorCode: 201,
            message: "You are logged out. Please login again.");
      } else {
        return Response<T>(
            httpCode: _res.statusCode,
            errorCode: 11,
            message: "Request error ${_res.statusCode}");
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      return Response<T>(httpCode: 1, errorCode: 111);
    }
  }

  Future<Response2<T>> invokeApi2<T>(
      {required Future<http.Response> Function(AuthClient) request,
      required Response2<T> Function(Map<String, dynamic>) fromJson}) async {
    try {
      final _res =
          await request(AuthClient(client: http.Client(), context: context));
      debugPrint('${_res.statusCode}');
      debugPrint('${_res.body}');
      if (_res.statusCode >= 200 && _res.statusCode < 300) {
        final _resBody = json.decode(_res.body);
        final _response = fromJson(_resBody);
        switch (_response.errorCode) {
          case 10:
            _response.message = "Please fill in all required fields.";
            break;
          case 11:
            _response.message =
                "An exception occurred. Please contact administrator.";
            break;
          case 100:
            _response.message = "Invalid username or password.";
            break;
          case 101:
            _response.message =
                "The user account is inactive. Please contact administrator.";
            break;
          case 102:
            _response.message = "The email address does not exists.";
            break;
          case 105:
            _response.message = "Wrong original password.";
            break;
          case 200:
            _response.message =
                "The serial number already exists in our system. Pleaes use another one.";
            break;
          case 300:
            _response.message =
                "The email already exists in our system. Please use another one.";
            break;
          default:
            break;
        }
        return _response;
      } else if (_res.statusCode == 401) {
        await Cache(context: context).logout();
        return Response2<T>(
            httpCode: _res.statusCode,
            errorCode: 201,
            message: "You are logged out. Please login again.");
      } else {
        return Response2<T>(
            httpCode: _res.statusCode,
            errorCode: 11,
            message: "Request error ${_res.statusCode}");
      }
    } on Exception catch (e, stacktrace) {
      debugPrint('$e');
      debugPrint('$stacktrace');
      return Response2<T>(httpCode: 1, errorCode: 111);
    }
  }
}

class AuthClient extends http.BaseClient {
  final http.Client client;
  final BuildContext context;

  AuthClient({required this.client, required this.context});

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    Completer<http.StreamedResponse> completer =
        Completer<http.StreamedResponse>();
    request.headers['Accept'] = 'application/json';
    request.headers['Content-type'] = 'application/json';
    final _auth = await Cache(context: context).me();
    if (_auth != null) {
      request.headers['Authorization'] = _auth.token;
    }
    debugPrint(request.url.toString());
    //debugPrint(request.headers.toString());
    completer.complete(client.send(request));
    return completer.future;
  }
}
