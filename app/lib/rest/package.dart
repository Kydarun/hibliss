import 'package:app/model/package.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class PackageRest extends RestClient {
  PackageRest({required BuildContext context}) : super(context: context);

  Future<Response<Package>> list() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/package")),
      fromJson: (res) => Response<Package>.fromJson(
          res, (data) => Package.fromJson(data as Map<String, dynamic>)));

  Future<Response<Package>> get(int id) => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/package/$id")),
      fromJson: (res) => Response<Package>.fromJson(
          res, (data) => Package.fromJson(data as Map<String, dynamic>)));
}
