import 'dart:convert';

import 'package:app/model/booking.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:app/view_model/booking_form.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BookingRest extends RestClient {
  BookingRest({required BuildContext context}) : super(context: context);

  Future<Response<BookingOrder>> getUnbookedOrder() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Booking/app")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response<BookingOrder>> getUpcomingBookingOrder() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Booking/app/upcoming")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response<BookingOrder>> getBookingHistory() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Booking/app/history")),
      fromJson: (res) => Response<BookingOrder>.fromJson(
          res, (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response2<BookingOrder>> getBooking({required int bookingId}) =>
      invokeApi2(
          request: (client) async => client
              .get(Uri.parse("${await super.apiUrl()}Booking/app/$bookingId")),
          fromJson: (res) => Response2<BookingOrder>.fromJson(res,
              (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response> book({required BookingFormViewModel bookingForm}) =>
      invokeApi(
          request: (client) async => client.post(
              Uri.parse("${await super.apiUrl()}Booking/app/"),
              body: json.encode(bookingForm.toJson())),
          fromJson: (res) => Response<BookingOrder>.fromJson(res,
              (data) => BookingOrder.fromJson(data as Map<String, dynamic>)));

  Future<Response<Timeslot>> timeslots(
          {required DateTime date, required int therapistId}) =>
      invokeApi(
          request: (client) async => client.get(Uri.parse(
              "${await super.apiUrl()}Booking/app/${DateFormat('yyyy-MM-dd').format(date)}/$therapistId")),
          fromJson: (res) => Response<Timeslot>.fromJson(
              res, (data) => Timeslot.fromJson(data as Map<String, dynamic>)));

  Future<Response> reschedule(
          {required int bookingId, required DateTime date}) =>
      invokeApi(
          request: (client) async => client.put(Uri.parse(
              "${await super.apiUrl()}Booking/app/$bookingId/${DateFormat('yyyy-MM-dd HH:mm:ss').format(date)}")),
          fromJson: (res) => Response<Timeslot>.fromJson(
              res, (data) => Timeslot.fromJson(data as Map<String, dynamic>)));

  Future<Response2<String>> cancel({required int bookingId}) => invokeApi2(
      request: (client) async => client
          .delete(Uri.parse("${await super.apiUrl()}Booking/app/$bookingId")),
      fromJson: (res) =>
          Response2<String>.fromJson(res, (data) => data as String));

  Future<Response2<String>> delete({required int bookingId}) => invokeApi2(
      request: (client) async => client.delete(
          Uri.parse("${await super.apiUrl()}Booking/app/delete/$bookingId")),
      fromJson: (res) =>
          Response2<String>.fromJson(res, (data) => data as String));
}
