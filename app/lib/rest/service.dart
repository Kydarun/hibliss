import 'package:app/model/response.dart';
import 'package:app/model/service.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class ServiceRest extends RestClient {
  ServiceRest({required BuildContext context}) : super(context: context);

  Future<Response<Service>> list() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/service")),
      fromJson: (res) => Response<Service>.fromJson(
          res, (data) => Service.fromJson(data as Map<String, dynamic>)));

  Future<Response<Service>> get(int id) => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}ProductService/service/$id")),
      fromJson: (res) => Response<Service>.fromJson(
          res, (data) => Service.fromJson(data as Map<String, dynamic>)));
}
