import 'dart:convert';

import 'package:app/model/addr_state.dart';
import 'package:app/model/address.dart';
import 'package:app/model/response.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class AddressRest extends RestClient {
  AddressRest({required BuildContext context}) : super(context: context);

  Future<Response<Address>> list() => invokeApi(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}User/member/address")),
      fromJson: (res) => Response<Address>.fromJson(
          res, (data) => Address.fromJson(data as Map<String, dynamic>)));

  Future<Response<Address>> add({required Address address}) async => invokeApi(
      request: (client) async => client.post(
          Uri.parse("${await super.apiUrl()}User/member/address"),
          body: json.encode(address.toJson())),
      fromJson: (res) => Response<Address>.fromJson(
          res, (data) => Address.fromJson(data as Map<String, dynamic>)));

  Future<Response<Address>> edit({required Address address}) async => invokeApi(
      request: (client) async => client.put(
          Uri.parse("${await super.apiUrl()}User/member/address/${address.addressId}"),
          body: json.encode(address.toJson())),
      fromJson: (res) => Response<Address>.fromJson(
          res, (data) => Address.fromJson(data as Map<String, dynamic>)));

  Future<Response<Address>> delete({required int addressId}) async => invokeApi(
      request: (client) async =>
          client.delete(Uri.parse("${await super.apiUrl()}User/member/address/$addressId")),
      fromJson: (res) => Response<Address>.fromJson(
          res, (data) => Address.fromJson(data as Map<String, dynamic>)));

  Future<Response<Address>> primary({required int addressId}) async => invokeApi(
      request: (client) async =>
          client.put(Uri.parse("${await super.apiUrl()}User/member/address/primary/$addressId")),
      fromJson: (res) => Response<Address>.fromJson(
          res, (data) => Address.fromJson(data as Map<String, dynamic>)));

  Future<Response<AddrState>> getStates() async => invokeApi(
      request: (client) async => client.get(Uri.parse("${await super.apiUrl()}General/states")),
      fromJson: (res) => Response<AddrState>.fromJson(
          res, (data) => AddrState.fromJson(data as Map<String, dynamic>)));
}
