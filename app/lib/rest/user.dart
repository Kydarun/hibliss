import 'dart:convert';

import 'package:app/model/notifications.dart';
import 'package:app/model/response.dart';
import 'package:app/model/user.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class UserRest extends RestClient {
  UserRest({required BuildContext context}) : super(context: context);

  Future<Response<User>> me() async => invokeApi(
      request: (client) async => client.get(Uri.parse("${await super.apiUrl()}User/member")),
      fromJson: (res) =>
          Response<User>.fromJson(res, (data) => User.fromJson(data as Map<String, dynamic>)));

  Future<Response> edit({required User user}) async => invokeApi(
      request: (client) async => client.put(Uri.parse("${await super.apiUrl()}User/member"),
          body: json.encode(user.toJson())),
      fromJson: (res) =>
          Response<User>.fromJson(res, (data) => User.fromJson(data as Map<String, dynamic>)));

  Future<Response> editNotification({required Notifications notification}) => invokeApi(
      request: (client) async =>
          client.put(Uri.parse("${await super.apiUrl()}User/member/notification"),
              body: json.encode({
                'messageNotification': notification.messageNotification ?? false,
                'appointmentNotification': notification.appointmentNotification ?? false,
                'promotionNotification': notification.promotionNotification ?? false
              })),
      fromJson: (res) => Response<String?>.fromJson(res, (data) => data as String?));

  Future<Response2<Notifications>> getNotification() => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}User/member/notification")),
      fromJson: (res) => Response2<Notifications>.fromJson(
          res, (data) => Notifications.fromJson(data as Map<String, dynamic>)));
}
