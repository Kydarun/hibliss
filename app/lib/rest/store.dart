import 'package:app/model/address.dart';
import 'package:app/model/category.dart';
import 'package:app/model/response.dart';
import 'package:app/model/store.dart';
import 'package:app/model/therapist.dart';
import 'package:app/rest/address.dart';
import 'package:app/rest/client.dart';
import 'package:flutter/material.dart';

class StoreRest extends RestClient {
  StoreRest({required BuildContext context}) : super(context: context);

  Future<Response<Category>> categories() => invokeApi(
      request: (client) async => client
          .get(Uri.parse("${await super.apiUrl()}ProductService/category")),
      fromJson: (res) => Response<Category>.fromJson(
          res, (data) => Category.fromJson(data as Map<String, dynamic>)));

  Future<Response2<Store>> list({required int categoryId}) => invokeApi2(
      request: (client) async => client
          .get(Uri.parse("${await super.apiUrl()}Shop/store/$categoryId")),
      fromJson: (res) => Response2<Store>.fromJson(
          res, (data) => Store.fromJson(data as Map<String, dynamic>)));

  Future<Response2<StoreAlacarte>> service({required int id}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Shop/services/$id")),
      fromJson: (res) => Response2<StoreAlacarte>.fromJson(
          res, (data) => StoreAlacarte.fromJson(data as Map<String, dynamic>)));

  Future<Response2<StorePackage>> package({required int id}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Shop/services/$id")),
      fromJson: (res) => Response2<StorePackage>.fromJson(
          res, (data) => StorePackage.fromJson(data as Map<String, dynamic>)));

  Future<Response2<StoreProduct>> product({required int id}) => invokeApi2(
      request: (client) async =>
          client.get(Uri.parse("${await super.apiUrl()}Shop/goods/$id")),
      fromJson: (res) => Response2<StoreProduct>.fromJson(
          res, (data) => StoreProduct.fromJson(data as Map<String, dynamic>)));

  Future<Response<Therapist>> therapists(
      {DateTime? appointmentDate, int? addressId, int? page}) async {
    if (addressId == null) {
      addressId = (await AddressRest(context: context).list())
          .dataResults
          ?.firstWhere((element) => (element.isPrimary ?? "No") == "Yes",
              orElse: () => Address())
          .addressId;
      if (addressId == null) {
        return Response(
            httpCode: -2, message: 'Please enter primary address to continue.');
      }
    }
    return invokeApi(
        request: (client) async => client.get(Uri.parse(
            "${await super.apiUrl()}Order/availableTherapist/$addressId/$page")),
        fromJson: (res) => Response<Therapist>.fromJson(
            res, (data) => Therapist.fromJson(data as Map<String, dynamic>)));
  }
}
