package com.example.app

import android.content.Intent
import androidx.annotation.NonNull
import com.ipay.IPayIH
import com.ipay.IPayIHPayment
import com.ipay.IPayIHResultDelegate
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.IOException
import java.io.Serializable

class MainActivity: FlutterActivity(), MethodChannel.MethodCallHandler {
    private val channel = "com.hibliss/payment"
    private lateinit var result: MethodChannel.Result

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                result.error("ERROR", "Payment is cancelled", null)
            }
        }
        /*if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                result.success("Success")
            }
            else {
                result.error("ERROR", "Transaction failed.", null)
            }
        }*/
    }

    /**
     * Handles the specified method call received from Flutter.
     *
     *
     * Handler implementations must submit a result for all incoming calls, by making a single
     * call on the given [Result] callback. Failure to do so will result in lingering Flutter
     * result handlers. The result may be submitted asynchronously. Calls to unknown or
     * unimplemented methods should be handled using [Result.notImplemented].
     *
     *
     * Any uncaught exception thrown by this method will be caught by the channel implementation
     * and logged, and an error result will be sent back to Flutter.
     *
     *
     * The handler is called on the platform thread (Android main thread). For more details see
     * [Threading in
 * the Flutter Engine](https://github.com/flutter/engine/wiki/Threading-in-the-Flutter-Engine).
     *
     * @param call A [MethodCall].
     * @param result A [Result] used for submitting the result of the call.
     */
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        this.result = result
        when (call.method) {
            "pay" -> {
                val payment = IPayIHPayment()
                payment.merchantCode = call.argument("ipay_merchant_code")
                payment.merchantKey = call.argument("ipay_merchant_key")
                payment.refNo = call.argument("ref_no")
                payment.prodDesc = call.argument("product_description")
                payment.userName = call.argument("user_name")
                payment.userEmail = call.argument("user_email")
                payment.amount = "1.00" //call.argument<String>("amount")
                payment.currency = "MYR"
                payment.paymentId = "2"
                payment.responseURL = "https://webtest.hi-bliss.com/App_Payment/Response.aspx"
                //payment.responseURL = "http://46.137.192.155:90/App_Payment/Response.aspx"
                payment.backendPostURL = "https://webtest.hi-bliss.com/App_Payment/BackendResponse.aspx"
                //payment.backendPostURL = "http://46.137.192.155:90/App_Payment/BackendResponse.aspx"

                val checkoutIntent = IPayIH.getInstance().checkout(payment, this.context, PaymentDelegate(), IPayIH.PAY_METHOD_CREDIT_CARD)
                startActivityForResult(checkoutIntent, 1)
            }
            "verify" -> {

            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public fun onMessageEvent(event: MessageEvent) {
        EventBus.getDefault().removeAllStickyEvents()
        if (event.success) {
            result.success(event.message)
        }
        else {
            result.error("ERROR", event.message, null)
        }
    }
}

class PaymentDelegate: IPayIHResultDelegate, Serializable {
    override fun onConnectionError(p0: String?, p1: String?, p2: String?, p3: String?, p4: String?, p5: String?, p6: String?) {
        //result.error("CONNECTION", "Unable to connect to payment gateway. Please try again.", null)
        println("onConnectionError")
        println("p0: $p0")
        println("p1: $p1")
        println("p2: $p2")
        println("p3: $p3")
        println("p4: $p4")
        println("p5: $p5")
        println("p6: $p6")
        EventBus.getDefault().postSticky(MessageEvent(message = "There was a connection failure. Please try again.", success = false))
        //result.error("ERROR", "There was a connection failure. Please try again.", null)
        //errorDescription = "There was a connection failure. Please try again."
    }

    override fun onPaymentSucceeded(transId: String?, refNo: String?, amount: String?, remark: String?, authCode: String?) {
        //result.success(transId);
        println("onPaymentSucceeded")
        println("transId: $transId")
        println("refNo: $refNo")
        println("amount: $amount")
        println("remark: $remark")
        println("authCode: $authCode")
        EventBus.getDefault().postSticky(MessageEvent(message = transId ?: "", success = true))
        //result.success(transId ?: "")
        //transactionId = transId
    }

    override fun onPaymentFailed(transId: String?, refNo: String?, amount: String?, remark: String?, errDesc: String?) {
        //result.error("FAILED", errDesc, null)
        println("onPaymentFailed")
        println("transId: $transId")
        println("refNo: $refNo")
        println("amount: $amount")
        println("remark: $remark")
        println("errDesc: $errDesc")
        EventBus.getDefault().postSticky(MessageEvent(message = errDesc ?: "", success = false))
        //result.error("ERROR",errDesc ?: "", null)
        //errorDescription = errDesc
    }

    override fun onPaymentCanceled(transId: String?, refNo: String?, amount: String?, remark: String?, errDesc: String?) {
        //result.error("CANCELLED", errDesc, null)
        println("onPaymentCanceled")
        println("transId: $transId")
        println("refNo: $refNo")
        println("amount: $amount")
        println("remark: $remark")
        println("errDesc: $errDesc")
        EventBus.getDefault().postSticky(MessageEvent(message = "Payment is cancelled", success = false))
        //result.error("ERROR","Payment is cancelled", null)
        //errorDescription = "Payment is cancelled"
    }

    override fun onRequeryResult(merchantCode: String?, refNo: String?, amount: String?, requeryResult: String?) {
        // No need to implement
        println("onRequeryResult")
        println("merchantCode: $merchantCode")
        println("refNo: $refNo")
        print("amount: $amount")
        print("result: $requeryResult")
        EventBus.getDefault().postSticky(MessageEvent(message = requeryResult ?: "", success = false))
        //result.error("ERROR",requeryResult ?: "", null)
        //errorDescription = result
    }
}

data class MessageEvent(val message: String, val success: Boolean)