import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, PaymentResultDelegate {
    var result: FlutterResult?
    var paymentView: UIView?
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    let controller = window?.rootViewController as! FlutterViewController
    let paymentChannel = FlutterMethodChannel(name: "com.hibliss/payment",
                                              binaryMessenger: controller.binaryMessenger)
    let paymentSdk = Ipay()
    paymentSdk.delegate = self
    
    paymentChannel.setMethodCallHandler({
        (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
        self.result = result
        if (call.method == "pay") {
            let payment = IpayPayment()
            if let args = call.arguments as? Dictionary<String, Any> {
                payment.merchantCode = args["ipay_merchant_code"] as? String
                payment.merchantKey = args["ipay_merchant_key"] as? String
                payment.refNo = args["ref_no"] as? String
                payment.prodDesc = args["product_description"] as? String
                payment.userName = args["user_name"] as? String
                payment.userEmail = args["user_email"] as? String
                payment.userContact = args["user_contact"] as? String
                payment.amount = "1.00" //args["amount"] as? String
                payment.currency = "MYR"
                payment.paymentId = "2"
                payment.country = "MY"
                payment.backendPostURL = "https://webtest.hi-bliss.com/App_Payment/BackendResponse.aspx"
                
                self.paymentView = paymentSdk.checkout(payment)
                controller.view.addSubview(self.paymentView!)
            }
            else {
              result(FlutterError.init(code: "ERROR", message: "An error occurred, please try again.", details: nil))
            }
        }
        else if (call.method == "verify") {
            
        }
        else {
            result(FlutterMethodNotImplemented)
        }
    })
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func paymentSuccess(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withAuthCode authCode: String!) {
        self.paymentView?.removeFromSuperview()
        print("paymentSuccess")
        print("remark: \(String(describing: remark))")
        print("authCode: \(String(describing: authCode))")
        self.result?(transId)
    }
    
    func paymentFailed(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withErrDesc errDesc: String!) {
        self.paymentView?.removeFromSuperview()
        print("paymentFailed")
        print("remark: \(String(describing: remark))")
        print("errDesc: \(String(describing: errDesc))")
        self.result?(FlutterError(code: "ERROR", message: errDesc, details: nil))
        
    }
    
    func paymentCancelled(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withErrDesc errDesc: String!) {
        self.paymentView?.removeFromSuperview()
        print("paymentCancelled")
        print("remark: \(String(describing: remark))")
        print("errDesc: \(String(describing: errDesc))")
        self.result?(FlutterError(code: "ERROR", message: errDesc, details: nil))
    }
    
    func requerySuccess(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withResult result: String!) {
        self.paymentView?.removeFromSuperview()
        print("requerySuccess")
        print("result: \(String(describing: result))")
        self.result?(result)
    }
    
    func requeryFailed(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withErrDesc errDesc: String!) {
        self.paymentView?.removeFromSuperview()
        print("requeryFailed")
        print("errDesc: \(String(describing: errDesc))")
        self.result?(FlutterError(code: "ERROR", message: errDesc, details: nil))
    }
}
